####################################### 3529 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   20252 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3529) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 



####################################### 3500 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   20351 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3500) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 




####################################### 3492 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   20351 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3492) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 



####################################### 3478 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   20351 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3478) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 




####################################### 3464 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   20351 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3464) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 

 

####################################### 3398 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   22166 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3398) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 


####################################### 3329 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   20351 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3329) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 




####################################### 3303 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   22689 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3303) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 






####################################### 3289 #######################################
INSERT into predict360.templateinstances
(surveyId, UserId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   22204 as UserId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3289) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 



####################################### 3271 #######################################
INSERT into predict360.templateinstances
(surveyId, groupId, assignedOn, assignedBy, isRecurring, attemptSequence, activationDate, jiraTicketId, isJiraTicketClosed, createdOn, modifiedOn)
select 
		a.TemplateId as surveyId
,   3777 as groupId
, 	CURRENT_TIMESTAMP() as assignedOn
, 	20252 as assignedBy
, 	1 as isRecurring
, 	@curRank := @curRank + 1 as attemptSequence
, 	ActDate as activationDate
, 	concat('CPBANK-', ji.issuenum) as jiraTicketId
, 	0 as isJiraTicketClosed
, 	CURRENT_TIMESTAMP() 	as createdOn
, 	CURRENT_TIMESTAMP() 	as modifiedOn
 

from 

(
	SELECT ji.id, ji.issuenum, max(cv.STRINGVALUE) as TemplateId, max(cv.DATEVALUE) as ActDate
	from jiraissue ji 
	inner join customfieldvalue cv on cv.issue = ji.id and cv.CUSTOMFIELD in (16704,10700) 
	where issuenum in (3271) 
	and project=26204
	group by ji.id, ji.issuenum
) a
inner join issuelink il on il.source = a.id 
inner join jiraissue ji on ji.id = il.DESTINATION
inner join (SELECT @curRank := 0) r
order by  ji.issuenum ; 


