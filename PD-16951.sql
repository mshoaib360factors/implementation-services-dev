insert into feed_document (document_key, ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path, customer_key, dismiss, take_action, reason, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on)

SELECT 
replace(document_key,'NIBCSB', 'RRDEMO') as document_key,  ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path
, 'RRDEMO' as customer_key, 0 as dismiss, 0 as take_action, NULL as reason
, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on
FROM predict360.feed_document where customer_key = 'NIBCSB'  and ref_id not in (113189) and imported_on > '2021-08-16 09:20:26' order by imported_on ASC;

Drop temporary table if exists tempdb.t1 ;
create temporary table tempdb.t1 
SELECT 
replace(document_key,'NIBCSB', 'RRDEMO') as document_key,  ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path
, 'RRDEMO' as customer_key, 0 as dismiss, 0 as take_action, NULL as reason
, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on
FROM predict360.feed_document where customer_key = 'NIBCSB'  and ref_id in (113189) and imported_on > '2021-08-16 09:20:26' order by imported_on ASC;

update feed_document fd
inner join tempdb.t1 t on t.document_key = fd.document_key
set 
  fd.document_key = t.document_key
, fd.ref_id = t.ref_id
, fd.category = t.category
, fd.created_at = t.created_at
, fd.full_path = t.full_path
, fd.jurisdiction = t.jurisdiction
, fd.official_id = t.official_id
, fd.provenance = t.provenance
, fd.publication_date = t.publication_date
, fd.pdf_url = t.pdf_url
, fd.summary_text = t.summary_text
, fd.title = t.title
, fd.updated_at = t.updated_at
, fd.web_url = t.web_url
, fd.xml_path = t.xml_path
, fd.customer_key = t.customer_key
, fd.dismiss = t.dismiss
, fd.take_action = t.take_action
, fd.reason = t.reason
, fd.clean_citations = t.clean_citations
, fd.named_regulation_ids = t.named_regulation_ids
, fd.regulator_code = t.regulator_code
, fd.subj_area = t.subj_area
, fd.topic = t.topic
, fd.source_id = t.source_id
, fd.imported_on = t.imported_on;

----- feed_doc_report

drop TEMPORARY table if exists tempdb.t2;
Create TEMPORARY table  tempdb.t2

select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, report_id from feed_doc_report 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by report_id  ;


drop TEMPORARY table if exists tempdb.t3;
Create TEMPORARY table  tempdb.t3
select * from tempdb.t2 where doc_key not in (select doc_key from feed_doc_report);


insert into feed_doc_report (doc_key, report_id)
select * from tempdb.t3;
 
----- feed_doc_region
select * from feed_doc_region limit 10;

drop TEMPORARY table if exists tempdb.t4;
Create TEMPORARY table  tempdb.t4

select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, region_id from feed_doc_region 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by id  ;

select max(region_id) from feed_doc_region where doc_key like 'RRDEMO%';

drop TEMPORARY table if exists tempdb.t5;
Create TEMPORARY table  tempdb.t5
select * from tempdb.t4 where doc_key not in (select doc_key from feed_doc_region);

insert into feed_doc_region (doc_key, region_id)
select * from tempdb.t5;

 
 ----- feed_doc_product
select * from feed_doc_product limit 10;

drop TEMPORARY table if exists tempdb.t6;
Create TEMPORARY table  tempdb.t6
select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, product_id from feed_doc_product 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by id  ;

select max(product_id) from feed_doc_product where doc_key like 'RRDEMO%';



drop TEMPORARY table if exists tempdb.t7;
Create TEMPORARY table  tempdb.t7
select t.* from tempdb.t6 t
left join feed_doc_product p on p.doc_key = t.doc_key and p.product_id = t.product_id
where p.doc_key is null;


insert into feed_doc_product (doc_key, product_id)
select * from tempdb.t7;
 
 ----- feed_doc_country
select * from feed_doc_country limit 10;

drop TEMPORARY table if exists tempdb.t8;
Create TEMPORARY table  tempdb.t8
select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, country_id from feed_doc_country 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by id  ;

select max(country_id) from feed_doc_country where doc_key like 'RRDEMO%';

drop TEMPORARY table if exists tempdb.t9;
Create TEMPORARY table  tempdb.t9
select t.* from tempdb.t8 t
left join feed_doc_country p on p.doc_key = t.doc_key and p.country_id = t.country_id
where p.doc_key is null;


insert into feed_doc_country (doc_key, country_id)
select * from tempdb.t9;
 
 
 ----- feed_doc_function
select * from feed_doc_function limit 10;

drop TEMPORARY table if exists tempdb.t10;
Create TEMPORARY table  tempdb.t10
select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, function_id from feed_doc_function 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by function_id  ;

select max(function_id) from feed_doc_function where doc_key like 'RRDEMO%';

drop TEMPORARY table if exists tempdb.t11;
Create TEMPORARY table  tempdb.t11
select t.* from tempdb.t10 t
left join feed_doc_function f on f.doc_key = t.doc_key and f.function_id = t.function_id
where f.doc_key is null;



insert into feed_doc_function (doc_key, function_id)
select * from tempdb.t11;
 
 
----- feed_doc_regulator
select * from feed_doc_regulator limit 10;

drop TEMPORARY table if exists tempdb.t12;
Create TEMPORARY table  tempdb.t12
select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, regulator_id from feed_doc_regulator 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by id  ;

select max(regulator_id) from feed_doc_regulator where doc_key like 'RRDEMO%';

drop TEMPORARY table if exists tempdb.t13;
Create TEMPORARY table  tempdb.t13
select t.* from tempdb.t12 t
left join feed_doc_regulator r on r.doc_key = t.doc_key and r.regulator_id = t.regulator_id
where r.doc_key is null;


insert into feed_doc_regulator (doc_key, regulator_id)
select * from tempdb.t13;
 
 ----- feed_doc_entity_type
select * from feed_doc_entity_type limit 10;

drop TEMPORARY table if exists tempdb.t14;
Create TEMPORARY table  tempdb.t14
select replace(doc_key, 'NIBCSB', 'RRDEMO') as doc_key, entity_type_id from feed_doc_entity_type 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = 'NIBCSB'   and imported_on > '2021-08-16 09:20:26'
)  order by id  ;

select max(entity_type_id) from feed_doc_entity_type where doc_key like 'RRDEMO%';

drop TEMPORARY table if exists tempdb.t15;
Create TEMPORARY table  tempdb.t15 
select t.* from tempdb.t14 t
left join feed_doc_entity_type e on e.doc_key = t.doc_key and e.entity_type_id = t.entity_type_id
where e.doc_key is null;


insert into feed_doc_entity_type (doc_key, entity_type_id)
select * from tempdb.t15;
 
 

