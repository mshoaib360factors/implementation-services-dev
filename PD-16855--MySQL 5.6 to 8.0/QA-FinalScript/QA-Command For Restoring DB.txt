### Create MySQL Dump


mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events predict360 > predict360-april-26-2022.sql
mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events etl > etl-april-26-2022.sql
mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events messaging > messaging-april-26-2022.sql
mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events workflowengine > workflowengine-april-26-2022.sql
mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events goalmanagement > goalmanagement-april-26-2022.sql
mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events notificationengine > notificationengine-april-26-2022.sql
mysqldump -h 172.16.249.47 -u shoaib.qa -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events hotlinereporting > hotlinereporting-april-26-2022.sql


### Restoring MySQL Dump


mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -f -D predict360 < "/home/shoaib.qa/mysql.bkp/predict360-april-26-2022.sql"
mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -f -D etl < "/home/shoaib.qa/mysql.bkp/etl-april-26-2022.sql"
mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -D messaging < "/home/shoaib.qa/mysql.bkp/messaging-april-26-2022.sql"
mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -D workflowengine < "/home/shoaib.qa/mysql.bkp/workflowengine-april-26-2022.sql"
mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -D goalmanagement < "/home/shoaib.qa/mysql.bkp/goalmanagement-april-26-2022.sql"
mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -D notificationengine < "/home/shoaib.qa/mysql.bkp/notificationengine-april-26-2022.sql"
mysql -p -h 172.16.249.63 -u qamysql8@qa-mysql8 -D hotlinereporting < "/home/shoaib.qa/mysql.bkp/hotlinereporting-april-26-2022.sql"


