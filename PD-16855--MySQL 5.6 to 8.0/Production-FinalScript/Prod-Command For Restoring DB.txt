### Create MySQL Dump


mysqldump -h 172.16.250.77 -u azmysql@prod-mysql8 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events predict360 > predict360-april-23-2022_New.sql
mysqldump -h 172.16.250.49 -u azmysql@azdb-st-01 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events etl > etl-april-23-2022.sql
mysqldump -h 172.16.250.49 -u azmysql@azdb-st-01 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events messaging > messaging-april-23-2022.sql
mysqldump -h 172.16.250.49 -u azmysql@azdb-st-01 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events workflowengine > workflowengine-april-23-2022.sql
mysqldump -h 172.16.250.49 -u azmysql@azdb-st-01 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events goalmanagement > goalmanagement-april-23-2022.sql
mysqldump -h 172.16.250.49 -u azmysql@azdb-st-01 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events notificationengine > notificationengine-april-23-2022.sql
mysqldump -h 172.16.250.49 -u azmysql@azdb-st-01 -p -f --single-transaction --quick --skip-lock-tables --triggers --routines --events hotlinereporting > hotlinereporting-april-23-2022.sql


### Restoring MySQL Dump


mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -f -D predict360 < "/opt/casemakerdata/prod-db-bkp/predict360-april-23-2022_New.sql"
mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -f -D etl < "/opt/casemakerdata/prod-db-bkp/etl-april-23-2022.sql"
mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -D messaging < "/opt/casemakerdata/prod-db-bkp/messaging-april-23-2022.sql"
mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -D workflowengine < "/opt/casemakerdata/prod-db-bkp/workflowengine-april-23-2022.sql"
mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -D goalmanagement < "/opt/casemakerdata/prod-db-bkp/goalmanagement-april-23-2022.sql"
mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -D notificationengine < "/opt/casemakerdata/prod-db-bkp/notificationengine-april-23-2022.sql"
mysql -p -h 172.16.250.75 -u azmysql@prodmysql-8 -D hotlinereporting < "/opt/casemakerdata/prod-db-bkp/hotlinereporting-april-23-2022.sql"

