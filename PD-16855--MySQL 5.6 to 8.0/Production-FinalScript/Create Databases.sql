CREATE DATABASE `predict360` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `etl` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `messaging` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `workflowengine` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `goalmanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `notificationengine` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `hotlinereporting` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE DATABASE `tempdb` /*!40100 DEFAULT CHARACTER SET latin1 */;


SELECT @@max_allowed_packet;

