SELECT 
table_schema AS `SCHEMA_NAME`, 
round(sum(((data_length + index_length) / 1024 / 1024)),2) as `Size in MB` ,
round(sum(((data_length + index_length) / 1024 / 1024 / 1024)),2) `Size in GB` ,
round(sum(((data_length + index_length) / 1024 / 1024 / 1024 / 1024)),2) `Size in TB` 
FROM information_schema.TABLES
where table_schema in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')
group by  table_schema
order by 3 desc;


Select DB, Object, count(1) from 
(
SELECT table_schema as DB, 'Tables' as Object FROM information_schema.TABLES
where  TABLE_TYPE <> 'VIEW' and table_schema in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')

UNION ALL

SELECT  table_schema as DB, 'Views' as Object FROM information_schema.VIEWS
where table_schema in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')

UNION ALL

SELECT ROUTINE_SCHEMA as DB, 'Routines' as Object FROM information_schema.ROUTINES
where ROUTINE_SCHEMA in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')

UNION ALL
SELECT  TRIGGER_SCHEMA as DB, 'TRIGGERS' as Object  FROM information_schema.TRIGGERS
where TRIGGER_SCHEMA in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')

UNION ALL
SELECT TABLE_SCHEMA as DB, 'KEY_COLUMN_USAGE' as Object FROM information_schema.KEY_COLUMN_USAGE 
where  TABLE_SCHEMA in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')

UNION ALL
 
SELECT CONSTRAINT_SCHEMA as DB, 'REFERENTIAL_CONSTRAINTS' as Object FROM information_schema.REFERENTIAL_CONSTRAINTS  
where CONSTRAINT_SCHEMA in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')
 
)s
group by DB, Object
order by 1,2 ;

SELECT TABLE_SCHEMA as DB, TABLE_NAME, TABLE_ROWS as TABLE_ROWS FROM information_schema.TABLES 
where  TABLE_TYPE <> 'VIEW' And TABLE_SCHEMA in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')
order by 1,2 ;
 
 
SELECT distinct TABLE_SCHEMA, TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE  TABLE_SCHEMA in ('predict360', 'etl', 'messaging', 'workflowengine', 'goalmanagement', 'notificationengine', 'hotlinereporting')


 