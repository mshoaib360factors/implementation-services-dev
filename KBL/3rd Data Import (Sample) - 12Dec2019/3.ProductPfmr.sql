#Transformation
set SQL_SAFE_UPDATES =0;
update etl.`kbl_dec19.import_riskandproduct2`
 set 
   ProductTypeL1 = trim(ProductTypeL1)
 , ProducTypeL2 = trim(`ProducTypeL2`)
 , `Description` = trim(`Description`);
set SQL_SAFE_UPDATES =1;


/*
 Following codes needs to be reviewed again while importing the actual data as on sample data this screen have been created manually.
 
 */

set @CustomerId = (select Id from predict360.Customer where customerCode  = 'Kbl');
set @CbAndMb   = (select  id from predict360.PredictUser where CustomerId = 521 and username = 'implementation.factors@kbl1');










SELECT * FROM predict360.ProductPfmr where customerid = 1813; 
 
select distinct ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null;


Select distinct ProductTypeL1, ProducTypeL2, Description  FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y;

# Inserting Level 1 
insert into ProductPfmr
 Select distinct  null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId,
 ProductTypeL1 as name,  #, ProducTypeL2, Description  
 1 status, null parentId, 0 isDefinition
 FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y;


# Inserting Level 2
insert into ProductPfmr
 Select distinct  null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId,
 ProducTypeL2 as name,  
 1 status, z.id parentId, 0 isDefinition
 FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y
inner join ProductPfmr z on y.ProductTypeL1 = z.name and z.customerId = 541;
 
 
 
# Inserting Level 3
insert into ProductPfmr
 Select distinct  null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId,
 Description as Name  ,
 1 status, z.id parentId, 1 isDefinition
 FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y
 inner join 
(select l2.Id, l2.name as Level2, l1.name as Level1  from ProductPfmr l2 inner join ProductPfmr l1 on l2.ParentId = l1.Id  and l2.CustomerId = l1.CustomerId where l2.CustomerId=541) z 
on y.ProducTypeL2 = z.Level2 and y.ProductTypeL1= z.Level1;