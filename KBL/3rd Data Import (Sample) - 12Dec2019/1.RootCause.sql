### Analysis ###
SELECT  *  FROM etl.`kbl_dec19.import_rootcause7` limit 10;
SELECT count(1)  FROM etl.`kbl_dec19.import_rootcause7`; #215
select RootCauseL2, Description, count(1)  FROM etl.`kbl_dec19.import_rootcause7` group by RootCauseL2, Description having count(1) > 1; 0 

###Transformation###
update etl.`kbl_dec19.import_rootcause7`
set
  RootCauseL1 = trim(RootCauseL1)
, RootCauseL2 = trim(RootCauseL2)
, Description = trim(Description)
;



### Insert Statements ###
set @CustomerId = (select Id from predict360.Customer where customerCode  = 'Kbl');
set @CbAndMb = (select  id from predict360.PredictUser where CustomerId = 521 and username = 'implementation.factors@kbl1');

select @CustomerId, @CbAndMb;


# Inserting Level 1 
insert into predict360.RootCausePfmr
SELECT distinct null as id, @CbAndMb as createdBy, current_timestamp() as createdOn, @CbAndMb as modifiedBy, current_timestamp() as modifiedOn, @CustomerId, trim(RootCauseL1) as label, 1 as status, null parentId, 0 isDefinition
FROM etl.`kbl_dec19.import_rootcause7`;



# Inserting Level 2
insert into predict360.RootCausePfmr
SELECT  distinct null as id, @CbAndMb as createdBy, current_timestamp() as createdOn, @CbAndMb as modifiedBy, current_timestamp() as modifiedOn, @CustomerId, trim(RootCauseL2) label, 1 as status, ft.id as parentId, 0 isDefinition
FROM etl.`kbl_dec19.import_rootcause7` bt
inner join 
(
select Label, max(id) as id from  predict360.RootCausePfmr 
where parentId is null and customerId = @customerId
group by Label 
)ft on RootCauseL1 = ft.label ; 


# Inserting Level 3
insert into predict360.RootCausePfmr
SELECT  distinct null as id, @CbAndMb as createdBy, current_timestamp() as createdOn, @CbAndMb as modifiedBy, current_timestamp() as modifiedOn, @CustomerId, Description as label, 1 as status, ft.RootCauseL2Id  parentId, 1 isDefinition
FROM etl.`kbl_dec19.import_rootcause7` bt
inner join 
(
  select l2.RootCauseL2, l2.id as RootCauseL2Id, l1.label as RootCauseL1 from 
  (
  select Label as RootCauseL2, max(id) as id from  predict360.RootCausePfmr 
  where customerId = @customerId and parentId is not null and isDefinition = 0
  group by Label 
  )l2 # level 2 with max(id)
  inner join predict360.RootCausePfmr l2P on l2.id = l2P.id #finding the parentId of level2
  inner join predict360.RootCausePfmr l1 on l1.id = l2P.parentId and l1.parentId is null #finding the parentId from level2
)ft
on ft.RootCauseL2 = bt.RootCauseL2 and  ft.RootCauseL1 = bt.RootCauseL1;





