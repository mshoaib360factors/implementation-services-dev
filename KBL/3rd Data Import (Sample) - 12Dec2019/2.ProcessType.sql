# Transformation
set SQL_SAFE_UPDATES =0;
update etl.`kbl_dec19.import_processtypeid9`
set 
  ProcessTypeL1 = trim(ProcessTypeL1)
, ProcessTypeL2 = trim(ProcessTypeL2)
, Description = trim(Description)
;

set SQL_SAFE_UPDATES =1;


set @CustomerId = (select Id from predict360.Customer where customerCode  = 'Kbl');
set @CbAndMb   = (select  id from predict360.PredictUser where CustomerId = 521 and username = 'implementation.factors@kbl1');


# Inserting Level 1 
insert into ProcessTypePfmr
SELECT distinct null as id, @CbAndMb as  createdBy,current_timestamp() createdOn, @CbAndMb as  modifiedBy, current_timestamp() modifiedOn, 
@CustomerId, ProcessTypeL1 label, 1 status, null parentId, 0 isDefinition
 FROM etl.`kbl_dec19.import_processtypeid9`;
 
 
# Inserting Level 2
insert into ProcessTypePfmr
SELECT distinct null as id, @CbAndMb as  createdBy,current_timestamp() createdOn, @CbAndMb as  modifiedBy, current_timestamp() modifiedOn,  
@CustomerId, ProcessTypeL2 label, 1 status, p.id parentId, 0 isDefinition  
FROM etl.`kbl_dec19.import_processtypeid9` f 
inner join  
( 
 select label as Level1 , max(id) as id
 from ProcessTypePfmr 
 where ParentId is null  and customerId = @CustomerId 
 group by Label
) p on p.Level1  = f.ProcessTypeL1;



# Inserting Level 3
insert into ProcessTypePfmr
SELECT  null as id, @CbAndMb as  createdBy, current_timestamp() createdOn, @CbAndMb as  modifiedBy, current_timestamp() modifiedOn,   
@CustomerId, Description as label, 1 status, l2l1.id parentId, 1 isDefinition   FROM etl.`kbl_dec19.import_processtypeid9` f   
inner join 
( 
SELECT 
    l2.Id, l2.Label AS Level2, l1.Label AS Level1
FROM
    ProcessTypePfmr l2
        INNER JOIN
    ProcessTypePfmr l1 ON l2.ParentId = l1.Id
        AND l2.CustomerId = l1.CustomerId
WHERE
    l2.CustomerId = @CustomerId and cast(l2.modifiedOn as date) = cast(current_timestamp()  as date)
)l2l1
on f.ProcessTypeL2 =  l2l1.Level2 and ProcessTypeL1= l2l1.Level1 ;

 
 