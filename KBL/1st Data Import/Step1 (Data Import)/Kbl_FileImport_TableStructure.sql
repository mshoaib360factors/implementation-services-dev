
-- Table structure for table `kbl.fileimport_controltab`

DROP TABLE IF EXISTS `kbl.fileimport_controltab`;
SET character_set_client = utf8mb4 ;
CREATE TABLE `kbl.fileimport_controltab` (
  `ID` varchar(4) DEFAULT NULL,
  `KeyControlName` varchar(255) DEFAULT NULL,
  `ControlProcedure` varchar(200) DEFAULT NULL,
  `Description` varchar(1500) DEFAULT NULL,
  `Objectives` varchar(1000) DEFAULT NULL,
  `Type` varchar(10) DEFAULT NULL,
  `ControlOwner` varchar(10) DEFAULT NULL,
  `Dependency` varchar(10) DEFAULT NULL,
  `ControlEffectiveness` varchar(10) DEFAULT NULL,
  `JustificationControlEffectiveness` varchar(255) DEFAULT NULL,
  `OverallControlAdequacy` varchar(2) DEFAULT NULL,
  `JustificationOverallControlAdequacy` varchar(255) DEFAULT NULL,
  `LERIds` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
 

--
-- Table structure for table `kbl.fileimport_processtab`
--

DROP TABLE IF EXISTS `kbl.fileimport_processtab`; 
CREATE TABLE `kbl.fileimport_processtab` (
  `ID` varchar(10) DEFAULT NULL,
  `ProcessName` varchar(206) DEFAULT NULL,
  `ProcessDescription` varchar(3000) DEFAULT NULL,
  `ProcessOwnerID` int(10) DEFAULT NULL,
  `ProcessTypeID` varchar(10) DEFAULT NULL,
  `Capacity` varchar(10) DEFAULT NULL,
  `AnnualNCOutputQty` varchar(10) DEFAULT NULL,
  `Criticality` varchar(1) DEFAULT NULL,
  `FTE` varchar(10) DEFAULT NULL,
  `Predecessor` varchar(10) DEFAULT NULL,
  `PredecessorOther` varchar(255) DEFAULT NULL,
  `Terminator` varchar(10) DEFAULT NULL,
  `Seasonality` varchar(10) DEFAULT NULL,
  `DependanceToHumanKnowHow` varchar(10) DEFAULT NULL,
  `DiscontinuedProcess` varchar(10) DEFAULT NULL,
  `ThirdPartyDependent` varchar(10) DEFAULT NULL,
  `Guest` varchar(15) DEFAULT NULL,
  `WorkflowStatus` varchar(30) DEFAULT NULL,
  `ProcessType` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Table structure for table `kbl.fileimport_riskandproducttab`
--

DROP TABLE IF EXISTS `kbl.fileimport_riskandproducttab`;
CREATE TABLE `kbl.fileimport_riskandproducttab` (
  `ID_RISK` varchar(4) DEFAULT NULL,
  `ID_PRODUCT` varchar(4) DEFAULT NULL,
  `ProductTypeL1` varchar(40) DEFAULT NULL,
  `ProducTypeL2` varchar(70) DEFAULT NULL,
  `Description` varchar(800) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ; 
--
-- Table structure for table `kbl.fileimport_risktab`
--

DROP TABLE IF EXISTS `kbl.fileimport_risktab`; 
CREATE TABLE `kbl.fileimport_risktab` (
  `ID` varchar(10) DEFAULT NULL,
  `ReferentialID` varchar(10) DEFAULT NULL,
  `ProcessedOperation` varchar(1500) DEFAULT NULL,
  `OperationMemorandum` varchar(200) DEFAULT NULL,
  `Failure` varchar(255) DEFAULT NULL,
  `RootCause` varchar(10) DEFAULT NULL,
  `InitialAssessment` varchar(10) DEFAULT NULL,
  `Risk` varchar(10) DEFAULT NULL,
  `RiskOwner` varchar(10) DEFAULT NULL,
  `AllocationToRiskTaxonomyL1` varchar(100) DEFAULT NULL,
  `AllocationToRiskTaxonomyL2` varchar(100) DEFAULT NULL,
  `AllocationToRiskTaxonomyL3` varchar(100) DEFAULT NULL,
  `AllocationToRiskTaxonomyL4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ; 

--
-- Table structure for table `kbl.fileimport_rootcausetab`
--

DROP TABLE IF EXISTS `kbl.fileimport_rootcausetab`; 
CREATE TABLE `kbl.fileimport_rootcausetab` (
  `ID` varchar(4) DEFAULT NULL,
  `RootCauseL1` varchar(50) DEFAULT NULL,
  `RootCauseL2` varchar(50) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ; 

--
-- Table structure for table `kbl.fileimport_taxonomytab`
--

DROP TABLE IF EXISTS `kbl.fileimport_taxonomytab`;  
CREATE TABLE `kbl.fileimport_taxonomytab` (
  `ID` int(4) DEFAULT NULL,
  `Level1` varchar(50) DEFAULT NULL,
  `Level2` varchar(50) DEFAULT NULL,
  `Level3` varchar(100) DEFAULT NULL,
  `Logi` int(2) DEFAULT NULL,
  `Level4` varchar(255) DEFAULT NULL,
  `isNew` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ; 

--
-- Table structure for table `kbl.fileimport_processtypeidtab`
--

DROP TABLE IF EXISTS `kbl.fileimport_processtypeidtab`;
CREATE TABLE `kbl.fileimport_processtypeidtab` (
  `ID` varchar(3) DEFAULT NULL,
  `Process_Type_Level_1` varchar(255) DEFAULT NULL,
  `Process_Type_Level_2` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `kbl.fileimport_allprocessestab`
--
DROP TABLE IF EXISTS `kbl.fileimport_allprocessestab`;
CREATE TABLE `kbl.fileimport_allprocessestab` (
  `Entity` varchar(255) DEFAULT NULL,
  `ProcessOwner` varchar(255) DEFAULT NULL,
  `Process_Owner_Name` varchar(255) DEFAULT NULL,
  `Department` varchar(255) DEFAULT NULL,
  `ID_Process` varchar(255) DEFAULT NULL,
  `ProcessName` varchar(255) DEFAULT NULL,
  `Criticality` varchar(255) DEFAULT NULL,
  `WorkflowStatus` varchar(255) DEFAULT NULL,
  `Inherent_Risk_Impact` varchar(255) DEFAULT NULL,
  `Inherent_Risk_Proba` varchar(255) DEFAULT NULL,
  `Current_Risk_Proba` varchar(255) DEFAULT NULL,
  `Current_Risk_Impact` varchar(255) DEFAULT NULL,
  `ProcessTypeL1` varchar(255) DEFAULT NULL,
  `ProcessTypeL2` varchar(255) DEFAULT NULL,
  `ID_RISK` varchar(255) DEFAULT NULL,
  `MY_OPERATIONS` varchar(2000) DEFAULT NULL,
  `Failure_Mode` varchar(255) DEFAULT NULL,
  `RootCauseL1` varchar(255) DEFAULT NULL,
  `RootCauseL2` varchar(255) DEFAULT NULL,
  `RISK_Description` varchar(255) DEFAULT NULL,
  `InitialAssessment` varchar(255) DEFAULT NULL,
  `AllocationToRiskTaxonomyL1` varchar(255) DEFAULT NULL,
  `AllocationToRiskTaxonomyL2` varchar(255) DEFAULT NULL,
  `AllocationToRiskTaxonomyL3` varchar(255) DEFAULT NULL,
  `TaxonomyL4` varchar(255) DEFAULT NULL,
  `ID_Control` varchar(255) DEFAULT NULL,
  `KeyControlName` varchar(255) DEFAULT NULL,
  `TypeControle` varchar(255) DEFAULT NULL,
  `ControlValue_ControlEffectiveness` varchar(255) DEFAULT NULL,
  `JustificationControlEffectiveness` varchar(255) DEFAULT NULL,
  `ControlValue_ControlAdequacy` varchar(255) DEFAULT NULL,
  `JustificationOverallControlAdequacy` varchar(255) DEFAULT NULL,
  `ID_Action_Plan` varchar(255) DEFAULT NULL,
  `ControlValue` varchar(255) DEFAULT NULL,
  `RelevantControl` varchar(255) DEFAULT NULL,
  `DescriptionOfTheAction` varchar(2000) DEFAULT NULL,
  `ControlValue1` varchar(255) DEFAULT NULL,
  `DealineOfTheAction` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `kbl.fileimport_operationandproducttab`
--
DROP TABLE IF EXISTS `kbl.fileimport_operationandproducttab`;
CREATE TABLE `kbl.fileimport_operationandproducttab` (
  `IdCustomerCol` int(11) NOT NULL AUTO_INCREMENT,
  `ProcessedOperation` varchar(2000) DEFAULT NULL,
  `ID_PRODUCT` varchar(10) DEFAULT NULL,
  `ProductTypeL1` varchar(255) DEFAULT NULL,
  `ProducTypeL2` varchar(255) DEFAULT NULL,
  `Description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`IdCustomerCol`)
) ENGINE=InnoDB AUTO_INCREMENT=1816 DEFAULT CHARSET=utf8mb4;
