
###############
################## Risk And Product Tab ###############
###############


ALTER TABLE `etl`.`kbl.fileimport_riskandproducttab` 
DROP COLUMN `Column37`,
DROP COLUMN `Column36`,
DROP COLUMN `Column35`,
DROP COLUMN `Column34`,
DROP COLUMN `Column33`,
DROP COLUMN `Column32`,
DROP COLUMN `Column31`,
DROP COLUMN `Column30`,
DROP COLUMN `Column29`,
DROP COLUMN `Column28`,
DROP COLUMN `Column27`,
DROP COLUMN `Column26`,
DROP COLUMN `Column25`,
DROP COLUMN `Column24`,
DROP COLUMN `Column23`,
DROP COLUMN `Column22`,
DROP COLUMN `Column21`,
DROP COLUMN `Column20`,
DROP COLUMN `Column19`,
DROP COLUMN `Column18`,
DROP COLUMN `Column17`,
DROP COLUMN `Column16`,
DROP COLUMN `Column15`,
DROP COLUMN `Column14`,
DROP COLUMN `Column13`,
DROP COLUMN `Column12`,
DROP COLUMN `Column11`,
DROP COLUMN `Column10`,
DROP COLUMN `Column9`,
DROP COLUMN `Column8`,
DROP COLUMN `Column7`,
DROP COLUMN `Column6`,
DROP COLUMN `Column5`;



SELECT count(1) FROM etl.`kbl.fileimport_riskandproducttab`; #2715

set sql_safe_updates = 0;
delete FROM etl.`kbl.fileimport_riskandproducttab`
where ID_RISK is null and ID_PRODUCT is null and ProductTypeL1 is null and ProducTypeL2 is null and Description is null ;
set sql_safe_updates = 1;

SELECT count(1) FROM etl.`kbl.fileimport_riskandproducttab`; # 2091

SELECT count(1)  FROM etl.`kbl.fileimport_riskandproducttab`
where ID_RISK is not null and ID_PRODUCT is not null and ProductTypeL1 is not null and ProducTypeL2 is not null and Description is not null; #1956

SELECT count(1) FROM etl.`kbl.fileimport_riskandproducttab`
where ID_PRODUCT is null and ProductTypeL1 is null and ProducTypeL2 is null and Description is null ; #135


##############################
###############Process Tab
##############################
ALTER TABLE `etl`.`kbl.fileimport_processtab` 
DROP COLUMN `Column37`,
DROP COLUMN `Column36`,
DROP COLUMN `Column35`,
DROP COLUMN `Column34`,
DROP COLUMN `Column33`,
DROP COLUMN `Column32`,
DROP COLUMN `Column31`,
DROP COLUMN `Column30`,
DROP COLUMN `Column29`,
DROP COLUMN `Column28`,
DROP COLUMN `Column27`,
DROP COLUMN `Column26`,
DROP COLUMN `Column25`,
DROP COLUMN `Column24`,
DROP COLUMN `Column23`,
DROP COLUMN `Column22`,
DROP COLUMN `Column21`,
DROP COLUMN `Column20`,
DROP COLUMN `Column19`;


SELECT count(1) FROM etl.`kbl.fileimport_processtab`; #3295

set sql_safe_updates = 0;
Delete FROM etl.`kbl.fileimport_processtab`
where ID is null and  ProcessName is null and  ProcessDescription is null and  ProcessOwnerID is null and  ProcessTypeID is null and  Capacity is null and  AnnualNCOutputQty is null and  Criticality is null and  FTE is null and  Predecessor is null and  PredecessorOther is null and  Terminator is null and  Seasonality is null and  DependanceToHumanKnowHow is null and  DiscontinuedProcess is null and  ThirdPartyDependent is null and  Guest is null and  WorkflowStatus is null and  ProcessType is null;
set sql_safe_updates = 1;
#2414

SELECT count(1) FROM etl.`kbl.fileimport_processtab`; #881
###############
################## Risk ###############
###############
ALTER TABLE `etl`.`kbl.fileimport_risktab` 
DROP COLUMN `Column37`,
DROP COLUMN `Column36`,
DROP COLUMN `Column35`,
DROP COLUMN `Column34`,
DROP COLUMN `Column33`,
DROP COLUMN `Column32`,
DROP COLUMN `Column31`,
DROP COLUMN `Column30`,
DROP COLUMN `Column29`,
DROP COLUMN `Column28`,
DROP COLUMN `Column27`,
DROP COLUMN `Column26`,
DROP COLUMN `Column25`,
DROP COLUMN `Column24`,
DROP COLUMN `Column23`,
DROP COLUMN `Column22`,
DROP COLUMN `Column21`,
DROP COLUMN `Column20`,
DROP COLUMN `Column19`,
DROP COLUMN `Column18`,
DROP COLUMN `Column17`,
DROP COLUMN `Column16`,
DROP COLUMN `Column15`,
DROP COLUMN `Column14`,
DROP COLUMN `Column13`;


 
select count(1)  from etl.`kbl.fileimport_risktab`; #2423
 
set sql_safe_updates = 0;
Delete from etl.`kbl.fileimport_risktab`
where ID is null and ReferentialID is null and ProcessedOperation is null and OperationMemorandum is null and Failure is null and RootCause is null and InitialAssessment is null and Risk is null and RiskOwner is null and AllocationToRiskTaxonomyL1 is null and AllocationToRiskTaxonomyL2 is null and AllocationToRiskTaxonomyL3 is null and AllocationToRiskTaxonomyL4 is null;
set sql_safe_updates = 1;
#1009

select count(1)  from etl.`kbl.fileimport_risktab`; #1414



###############
################## ControlTab ###############
###############
ALTER TABLE `etl`.`kbl.fileimport_controltab` 
DROP COLUMN `Column37`,
DROP COLUMN `Column36`,
DROP COLUMN `Column35`,
DROP COLUMN `Column34`,
DROP COLUMN `Column33`,
DROP COLUMN `Column32`,
DROP COLUMN `Column31`,
DROP COLUMN `Column30`,
DROP COLUMN `Column29`,
DROP COLUMN `Column28`,
DROP COLUMN `Column27`,
DROP COLUMN `Column26`,
DROP COLUMN `Column25`,
DROP COLUMN `Column24`,
DROP COLUMN `Column23`,
DROP COLUMN `Column22`,
DROP COLUMN `Column21`,
DROP COLUMN `Column20`,
DROP COLUMN `Column19`,
DROP COLUMN `Column18`,
DROP COLUMN `Column17`,
DROP COLUMN `Column16`,
DROP COLUMN `Column15`,
DROP COLUMN `Column14`,
DROP COLUMN `Column13`;



 
select count(1)  from etl.`kbl.fileimport_controltab`; #3084

set sql_safe_updates = 0;
Delete from etl.`kbl.fileimport_controltab`
where ID is null and KeyControlName is null and ControlProcedure is null and Description is null and Objectives is null and Type is null and ControlOwner is null and Dependency is null and ControlEffectiveness is null and JustificationControlEffectiveness is null and OverallControlAdequacy is null and JustificationOverallControlAdequacy is null and LERIds is null;
set sql_safe_updates = 1;
#1009

select count(1)  from etl.`kbl.fileimport_controltab`; #2070



###############
################## Root Cause Tab ###############
###############



ALTER TABLE `etl`.`kbl.fileimport_rootcausetab` 
DROP COLUMN `Column37`,
DROP COLUMN `Column36`,
DROP COLUMN `Column35`,
DROP COLUMN `Column34`,
DROP COLUMN `Column33`,
DROP COLUMN `Column32`,
DROP COLUMN `Column31`,
DROP COLUMN `Column30`,
DROP COLUMN `Column29`,
DROP COLUMN `Column28`,
DROP COLUMN `Column27`,
DROP COLUMN `Column26`,
DROP COLUMN `Column25`,
DROP COLUMN `Column24`,
DROP COLUMN `Column23`,
DROP COLUMN `Column22`,
DROP COLUMN `Column21`,
DROP COLUMN `Column20`,
DROP COLUMN `Column19`,
DROP COLUMN `Column18`,
DROP COLUMN `Column17`,
DROP COLUMN `Column16`,
DROP COLUMN `Column15`,
DROP COLUMN `Column14`,
DROP COLUMN `Column13`,
DROP COLUMN `Column12`,
DROP COLUMN `Column11`,
DROP COLUMN `Column10`,
DROP COLUMN `Column9`,
DROP COLUMN `Column8`,
DROP COLUMN `Column7`,
DROP COLUMN `Column6`,
DROP COLUMN `Column5`,
DROP COLUMN `Column4`;


SELECT count(1) FROM etl.`kbl.fileimport_rootcausetab`; #175

set sql_safe_updates = 0;
Delete FROM etl.`kbl.fileimport_rootcausetab`	
where ID is null and RootCauseL1 is null and RootCauseL2 is null and Description is null;
set sql_safe_updates = 1;

SELECT count(1) FROM etl.`kbl.fileimport_rootcausetab`; #174




###############
################## Taxonomy Tab ###############
###############


ALTER TABLE `etl`.`kbl.fileimport_taxonomytab` 
DROP COLUMN `Column37`,
DROP COLUMN `Column36`,
DROP COLUMN `Column35`,
DROP COLUMN `Column34`,
DROP COLUMN `Column33`,
DROP COLUMN `Column32`,
DROP COLUMN `Column31`,
DROP COLUMN `Column30`,
DROP COLUMN `Column29`,
DROP COLUMN `Column28`,
DROP COLUMN `Column27`,
DROP COLUMN `Column26`,
DROP COLUMN `Column25`,
DROP COLUMN `Column24`,
DROP COLUMN `Column23`,
DROP COLUMN `Column22`,
DROP COLUMN `Column21`,
DROP COLUMN `Column20`,
DROP COLUMN `Column19`,
DROP COLUMN `Column18`,
DROP COLUMN `Column17`,
DROP COLUMN `Column16`,
DROP COLUMN `Column15`,
DROP COLUMN `Column14`,
DROP COLUMN `Column13`,
DROP COLUMN `Column12`,
DROP COLUMN `Column11`,
DROP COLUMN `Column10`,
DROP COLUMN `Column9`,
DROP COLUMN `Column8`,
DROP COLUMN `Column7`,
CHANGE COLUMN `ID` `ID` INT(4) NULL DEFAULT NULL FIRST;


SELECT count(1)  FROM etl.`kbl.fileimport_taxonomytab`; #1024




###############
################## PROCESS TYPE ID Tab ###############
###############

set sql_safe_updates=0;
update etl.`kbl.fileimport_processtypeidtab`
set Process_Type_Level_1 = trim(Process_Type_Level_1),
Process_Type_Level_2 = trim(Process_Type_Level_2),
Description  = trim(Description);
set sql_safe_updates=1;

# Deleting the duplicate record randomly
set SQL_safe_updates =0;
Delete from etl.`kbl.fileimport_processtypeidtab` where id = '58';
set SQL_safe_updates =1; 
 
 
###############
################## ALL PREOCESSES_1 Tab ###############
###############

set SQL_SAFE_UPDATES =0;
update etl.`kbl.fileimport_allprocessestab` set
Entity = trim(Entity),
ProcessOwner = trim(ProcessOwner),
Process_Owner_Name = trim(Process_Owner_Name),
Department = trim(Department),
ID_Process = trim(ID_Process),
ProcessName = trim(ProcessName),
Criticality = trim(Criticality),
WorkflowStatus = trim(WorkflowStatus),
Inherent_Risk_Impact = trim(Inherent_Risk_Impact),
Inherent_Risk_Proba = trim(Inherent_Risk_Proba),
Current_Risk_Proba = trim(Current_Risk_Proba),
Current_Risk_Impact = trim(Current_Risk_Impact),
ProcessTypeL1 = trim(ProcessTypeL1),
ProcessTypeL2 = trim(ProcessTypeL2),
ID_RISK = trim(ID_RISK),
MY_OPERATIONS = trim(MY_OPERATIONS),
Failure_Mode = trim(Failure_Mode),
RootCauseL1 = trim(RootCauseL1),
RootCauseL2 = trim(RootCauseL2),
RISK_Description = trim(RISK_Description),
InitialAssessment = trim(InitialAssessment),
AllocationToRiskTaxonomyL1 = trim(AllocationToRiskTaxonomyL1),
AllocationToRiskTaxonomyL2 = trim(AllocationToRiskTaxonomyL2),
AllocationToRiskTaxonomyL3 = trim(AllocationToRiskTaxonomyL3),
TaxonomyL4 = trim(TaxonomyL4),
ID_Control = trim(ID_Control),
KeyControlName = trim(KeyControlName),
TypeControle = trim(TypeControle),
ControlValue_ControlEffectiveness = trim(ControlValue_ControlEffectiveness),
JustificationControlEffectiveness = trim(JustificationControlEffectiveness),
ControlValue_ControlAdequacy = trim(ControlValue_ControlAdequacy),
JustificationOverallControlAdequacy = trim(JustificationOverallControlAdequacy),
ID_Action_Plan = trim(ID_Action_Plan),
ControlValue = trim(ControlValue),
RelevantControl = trim(RelevantControl),
DescriptionOfTheAction = trim(DescriptionOfTheAction),
ControlValue1 = trim(ControlValue1),
DealineOfTheAction = trim(DealineOfTheAction);
set SQL_SAFE_UPDATES =1;



###############
################## Operation & Product_2 Tab (Reference File) ###############
###############


SELECT * FROM etl.`kbl.fileimport_operationandproducttab`;
/*
set SQL_SAFE_UPDATEs = 0;
update etl.`kbl.fileimport_operationandproducttab`
set ProcessedOperation = trim(ProcessedOperation);
set SQL_SAFE_UPDATEs = 1;
*/
ALTER TABLE `etl`.`kbl.fileimport_operationandproducttab` 
DROP COLUMN `Column5`;

select  count(1) from etl.`kbl.fileimport_operationandproducttab` ; #2292

create table etl.`kbl.fileimport_operationandproducttabNew`
select distinct  ProcessedOperation, ID_PRODUCT, ProductTypeL1, ProducTypeL2, `Description` from etl.`kbl.fileimport_operationandproducttab` ; #1815

Drop table `etl`.`kbl.fileimport_operationandproducttab`;

ALTER TABLE `etl`.`kbl.fileimport_operationandproducttabNew` rename `etl`.`kbl.fileimport_operationandproducttab` ;

ALTER TABLE `etl`.`kbl.fileimport_operationandproducttab` 
 ADD COLUMN `IdCustomerCol` INT(11) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`IdCustomerCol`);
;

UPDATE `etl`.`kbl.fileimport_operationandproducttab` SET `ProcessedOperation` = 'clôture d\'un ou plusieurs comptes ouverts dans les livres de KBL \nsur insturction du client / WM / Legal / Compliance' WHERE (`IdCustomerCol` = '1158');

