ALTER TABLE `etl`.`kbl.fileimport_allprocessestab`  CHANGE COLUMN `ID_RISK` `ID_RISK` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `etl`.`kbl.fileimport_risktab` CHANGE COLUMN `ID` `ID` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `etl`.`kbl.fileimport_risktab` ADD INDEX `Ix_RiskTabId` (`ID` ASC) ;
ALTER TABLE `etl`.`kbl.fileimport_allprocessestab`ADD INDEX `Ix_IDRisk` (`ID_RISK` ASC) ;
ALTER TABLE `etl`.`kbl.fileimport_rootcausetab` 
CHANGE COLUMN `ID` `ID` INT(11) NULL DEFAULT NULL , ADD INDEX `Ix_RC_Id` (`ID` ASC) ;
ALTER TABLE `etl`.`kbl.fileimport_riskandproducttab` 
CHANGE COLUMN `ID_RISK` `ID_RISK` INT(11) NULL DEFAULT NULL ,ADD INDEX `Ix_IdRisk` (`ID_RISK` ASC) ;

ALTER TABLE `etl`.`kbl.fileimport_processtab` 
CHANGE COLUMN `ID` `ID` INT(11) NULL DEFAULT NULL ;


ALTER TABLE `etl`.`kbl.fileimport_allprocessestab` 
CHANGE COLUMN `ID_Process` `ID_Process` INT(11) NULL DEFAULT NULL ;


ALTER TABLE `etl`.`kbl.fileimport_allprocessestab` 
ADD COLUMN `IdCustom` INT(11) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`IdCustom`);
;


ALTER TABLE `etl`.`kbl.fileimport_allprocessestab` 
DROP COLUMN `IdCustom`,
DROP PRIMARY KEY;
;


ALTER TABLE `etl`.`kbl.fileimport_allprocessestab` 
ADD COLUMN `RowNoCustom` INT(11) NULL AFTER `DealineOfTheAction`;
 
