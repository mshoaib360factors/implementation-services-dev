Select count(1) FROM etl.`kbl.fileimport_risktab`; #1414
SELECT count(1) FROM etl.`kbl.fileimport_allprocessestab`; #2438
SELECT distinct ID_Process, ID_RISK FROM etl.`kbl.fileimport_allprocessestab`; #1439
SELECT distinct ID_Process, ID_RISK FROM etl.`kbl.fileimport_allprocessestab` where ID_RISK is not null; #1402
Select ID as RiskID, ReferentialID as ProcessID FROM etl.`kbl.fileimport_risktab`; #1414


#### Joining Import Tables 
Drop temporary table if exists tempdb.BTRisk;
Create temporary table tempdb.BTRisk 
(Index `IX_ProcessName` (`RiskID`) )
SELECT distinct 
trim(pt.ProcessName) as ProcessName , 
trim(rt.ProcessedOperation) as OpDesc,
trim(rt.Failure) as Failure,
trim(rc.RootCauseL1) as RootCauseL1, 
trim(rc.RootCauseL2) as RootCauseL2, 
trim(rc.`Description`) as RootCauseDesc,
case 
when rt.InitialAssessment =  2 then 'Low'
when rt.InitialAssessment =  3 then 'Medium'
when rt.InitialAssessment =  4 then 'High'
when rt.InitialAssessment =  5 then 'Very High'
end as BCMCriticality, 
trim(tt.Level1) as RiskTaxonomyL1, 
trim(tt.Level2) as RiskTaxonomyL2, 
trim(tt.Level3) as RiskTaxonomyL3, 
trim(tt.Level4) as RiskTaxonomyL4, 
rt.id as RiskID

FROM etl.`kbl.fileimport_allprocessestab` apt  
inner join etl.`kbl.fileimport_processtab` pt on pt.id  = apt.ID_Process 
inner join etl.`kbl.fileimport_risktab`  rt on rt.id = apt.ID_RISK
inner join etl.`kbl.fileimport_rootcausetab` rc on rc.id = rt.RootCause
inner join etl.`kbl.fileimport_taxonomytab` tt on tt.id = rt.AllocationToRiskTaxonomyL4;
#1402



#### Joining Import Remaining Tables 
Drop temporary table if exists tempdb.BTRisk2;
Create temporary table tempdb.BTRisk2
(Index `IX_Failure` (`Failure`), Index `IX_RootCause` (`RootCauseL1`,`RootCauseL2`,`RootCauseDesc`) )
SELECT distinct 
ProcessName,  
OpDesc,
Failure,
RootCauseL1, 
RootCauseL2, 
RootCauseDesc,
BCMCriticality, 
RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 
ProductTypeL1, 
ProductTypeL2,
ProdDesc
FROM tempdb.BTRisk bt  
inner join 
(select distinct s2.ID_RISK, s2.ProductTypeL1, s2.ProducTypeL2 as ProductTypeL2, s2.`Description` as ProdDesc from 
(
SELECT ID_RISK, min(ID_PRODUCT) ID_PRODUCT FROM etl.`kbl.fileimport_riskandproducttab` where ID_PRODUCT is not null and ID_PRODUCT <> -1 group by ID_RISK # 1279
) s1
inner join etl.`kbl.fileimport_riskandproducttab` s2 on s1.ID_RISK = s2.ID_RISK and s1.ID_PRODUCT = s2.ID_PRODUCT
) rp on rp.ID_RISK = bt.RiskID ; #1267




#Joining RootCause #Joining with Failure Mode #Joining with BCMCriticality
Drop temporary table if exists tempdb.RcFmBcm;
Create temporary table tempdb.RcFmBcm
(Index `IX_RootCause` (`ProductTypeL1`,`ProductTypeL2`,`ProdDesc`))
select 

distinct 

ProcessName,
OpDesc,

fm.id as FailureModeId,
rc.RCId as RootCauseId,
bcp.id as BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

ProductTypeL1, 
ProductTypeL2,
ProdDesc

from tempdb.BTRisk2 bt 
inner join 
(
select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId 
from RootCausePfmr l3 
inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  
inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null 
where  l3.CustomerId = 541
)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc #Joining with RootCause
inner join BcmCriticalityPfmr bcp 
on bcp.description = bt.BCMCriticality and bcp.customerId=541 #Joining with BCMCriticality
inner join FailureModePfmr fm on fm.`name` = bt.Failure and fm.CustomerId=541; #Joining with Failure Mode



#joining with Product
Drop temporary table if exists tempdb.AddingProd;
Create temporary table tempdb.AddingProd
(Index `IX_ProcessName` (`ProcessName`),Index `IX_Risk` (`RiskTaxonomyL1`,`RiskTaxonomyL2`,`RiskTaxonomyL3`,`RiskTaxonomyL4`))
select 
distinct 

ProcessName,
OpDesc,

FailureModeId,
RootCauseId,
BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

p.PrdId as ProductId


from tempdb.RcFmBcm rfb 
inner join 
(select l1.name as l1, l2.name as l2, l3.name as def, l3.id as PrdId from ProductPfmr l3 
inner join ProductPfmr l2 on l3.parentId = l2.id and cast(l3.definition as unsigned) = 1 
inner join ProductPfmr l1 on l2.parentId = l1.id and l1.parentid is null where l3.customerid=541 
)p on p.l1 = rfb.ProductTypeL1 and p.l2 = rfb.ProductTypeL2 and p.def = rfb.`ProdDesc` #joining with Product 
;

#joining with MyProcesses and Risk Taxonomy
Drop temporary table if exists tempdb.MPRT;
Create temporary table tempdb.MPRT
select 
distinct 

myproc.MyProcId,

OpDesc,

FailureModeId,
RootCauseId,
BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

ProductId


from tempdb.AddingProd ap 
inner join 
(
select p.`name` as ProcName, mp.id as MyProcId From MyProcessesPfmr mp  
inner join ProcessPfmr p on mp.processId = p.id and  mp.CustomerId= p.CustomerId 
where mp.customerId=541
) myproc
on myproc.ProcName = ap.ProcessName
inner join 
(
Select 
l4.`name` as Level4Name, l4.id as Level4Id, l3.`name` as Level3Name, l3.id as Level3Id, 
l2.`name` as Level2Name, l2.id as Level2Id, l1.`name` as Level1Name, l1.id as Leve11Id  
from RiskRegisterItem l4 
inner join RiskRegisterItem l3 on l3.id = l4.parentId  
inner join RiskRegisterItem l2 on l2.id = l3.parentId 
inner join RiskRegisterItem l1 on l1.id = l2.parentid 
where cast(l4.isLeaf as unsigned) = 1 and l4.CustomerId = 541
)r on r.Level4Name = bt.RiskTaxonomyL4 and r.Level3Name = bt.RiskTaxonomyL3 and r.Level2Name = bt.`RiskTaxonomyL2` and r.RiskTaxonomyL1
;



#RootCause Links Done
select count(1) from tempdb.BTRisk bt inner join (select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId from RootCausePfmr l3 inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null where  l3.CustomerId = 541)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc;
#1267

#bcmCriticalPfmrId Links Done
select count(1) from tempdb.BTRisk bt inner join (select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId from RootCausePfmr l3 inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null where  l3.CustomerId = 541)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc inner join BcmCriticalityPfmr bcp on bcp.description = bt.BCMCriticality and bcp.customerId=541;
#1267

# FailureModePfmr Links Done
select count(1) from tempdb.BTRisk bt inner join (select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId from RootCausePfmr l3 inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null where  l3.CustomerId = 541)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc inner join BcmCriticalityPfmr bcp on bcp.description = bt.BCMCriticality and bcp.customerId=541 inner join FailureModePfmr fm on fm.`name` = bt.Failure and fm.CustomerId=541;
#1268  may get remove after have distinct values











select * from tempdb.BTRisk bt
inner join FailureModePfmr fm on fm.`name` = bt.Failure
where fm.CustomerId=541 limit 1;
 
# inner join BcmCriticalityPfmr bcp on pcp.`description` =  ; #1401

 

Select * FROM etl.`kbl.fileimport_risktab` rt where rt.id = 711;


## Validate the Risk and Process Mapping

  

select * from  etl.`kbl.fileimport_risktab` ;

/*
sSelect count(Distinct ReferentialID), count(1) FROM etl.`kbl.fileimport_risktab`;

SELECT * FROM etl.`kbl.fileimport_processtab` where id in (Select  Distinct ReferentialID  FROM etl.`kbl.fileimport_risktab`);*/