
select count(1) from etl.`kbl.fileimport_controltab` where `Type` <> 24 ; #1,864

#Transformation of Data dump based on Business Rules discuss over the email
Drop temporary table if exists tempdb.CBT1;
Create temporary table tempdb.CBT1
SELECT 
ID as ControlIdInFile,
null as id, 
'??' as controlInstanceId, 
'??' processOwnerPfmrId,
trim(KeyControlName) as `name`,
trim(controlProcedure) as controlProcedure, #Can be ignored but we will be trying to do the best
trim(`Description`) as `description`,
trim(objectives) as Objectives,
case when `Type` = '25'  then 'Preventive'  when `Type` = '26'  then 'Detective' else 'Not Defined'  end as controlType,
'Manual' as functionMode, # Approved from KBL
'Sample' as examinationMethod, # Approved from KBL
17212 controlOwnerId, #Approved from KBL


case 
when OverallControlAdequacy = '27' then 4 
when OverallControlAdequacy = '28' then 3
when OverallControlAdequacy = '29' then 2
when OverallControlAdequacy = '30' then 1
end as OverallControlAdequacy,
 '??' as controlProbabilityAdequacySlabPfmrId,
trim(JustificationOverallControlAdequacy) as justificationForAdequacy, 

case 
when ControlEffectiveness = '10' then 4 
when ControlEffectiveness = '11' then 3
when ControlEffectiveness = '12' then 2
when ControlEffectiveness = '13' then 1
end as ControlEffectiveness,
 '??' as controlImpactEfficacySlabPfmrId,
trim(JustificationControlEffectiveness) as justificationForEfficacy, 


'??' as controlProcedureUrl

FROM etl.`kbl.fileimport_controltab` 
where `Type` <> 24 ;
#1,864


#Transformation of Data dump based on Business Rules discuss over the email
Drop temporary table if exists tempdb.CBT2;
Create temporary table tempdb.CBT2
select  
ControlIdInFile,
id,
controlInstanceId,
#processOwnerPfmrId,
`name`,
substring_index(controlProcedure,'/',-1) as controlProcedure,
`description`,
Objectives,
controlType,
functionMode,
examinationMethod,
controlOwnerId,

case 
when OverallControlAdequacy = 4 then 60 
when OverallControlAdequacy = 3 then 40
when OverallControlAdequacy = 2 then 25
when OverallControlAdequacy = 1 then 10
end as OverallControlAdequacy,
controlProbabilityAdequacySlabPfmrId,
justificationForAdequacy,

case 
when ControlEffectiveness = 4 then 60
when ControlEffectiveness = 3 then 40
when ControlEffectiveness = 2 then 25
when ControlEffectiveness = 1 then 10
end as ControlEffectiveness,
controlImpactEfficacySlabPfmrId,
justificationForEfficacy,

substring(controlProcedure,1, length(controlProcedure) - length(substring_index(controlProcedure,'/',-1)) - 1) as controlProcedureUrl
from tempdb.CBT1 where controlType <>  'Not Defined' ;
#1864

select `name`, count(1) from  tempdb.CBT2 group by `name` having count(1) > 1;




SELECT * FROM etl.`kbl.fileimport_allprocessestab` apt where apt.ID_Control  is not null; # 2,399




# Finding Control association with Risk and Processes
Drop temporary table if exists tempdb.CRP1;
Create temporary table tempdb.CRP1
SELECT distinct
trim(pt.ProcessName) as ProcessName , 
trim(pt.ProcessDescription) as ProcessDescription , 
trim(rt.ProcessedOperation) as OpDesc,
trim(rt.Failure) as Failure,
trim(rc.RootCauseL1) as RootCauseL1, 
trim(rc.RootCauseL2) as RootCauseL2, 
trim(rc.`Description`) as RootCauseDesc,
case 
when rt.InitialAssessment =  2 then 'Low'
when rt.InitialAssessment =  3 then 'Medium'
when rt.InitialAssessment =  4 then 'High'
when rt.InitialAssessment =  5 then 'Very High'
end as BCMCriticality, 
trim(tt.Level1) as RiskTaxonomyL1, 
trim(tt.Level2) as RiskTaxonomyL2, 
trim(tt.Level3) as RiskTaxonomyL3, 
trim(tt.Level4) as RiskTaxonomyL4, 
#rt.id as RiskID
rp.ProductTypeL1,
rp.ProducTypeL2 as ProductTypeL2,
rp.`Description` as ProdDesc,
apt.ID_Control

FROM 
(
SELECT 
distinct  ID_Process, ID_RISK, ID_Control
FROM etl.`kbl.fileimport_allprocessestab` 
where ID_Control  is not null #2,389
) apt 
inner join etl.`kbl.fileimport_processtab` pt on pt.id  = apt.ID_Process 
inner join etl.`kbl.fileimport_risktab`  rt on rt.id = apt.ID_RISK
inner join etl.`kbl.fileimport_rootcausetab` rc on rc.id = rt.RootCause
inner join etl.`kbl.fileimport_taxonomytab` tt on tt.id = rt.AllocationToRiskTaxonomyL4
inner join etl.`kbl.fileimport_riskandproducttab` rp on  rp.ID_RISK = rt.id 
where rp.ID_PRODUCT is not null and rp.ID_PRODUCT <> -1;
#3,312

#Joining with Row no. to get the value for Col n
Drop temporary table if exists tempdb.CRP2;
Create temporary table tempdb.CRP2
(Index `IX_Failure` (`Failure`), Index `IX_RootCause` (`RootCauseL1`,`RootCauseL2`,`RootCauseDesc`) )
SELECT distinct c.*, r.n

FROM tempdb.CRP1 c
inner join etl.allprocessestab_rowno r on 
r.ProcessName = c.ProcessName and r.Failure =  c.Failure  and r.RootCauseL1 =  c.RootCauseL1  and  r.RootCauseL2 =  c.RootCauseL2  
and r.RootCauseDesc =  c.RootCauseDesc  and  r.BCMCriticality =  c.BCMCriticality  and r.RiskTaxonomyL1 =  c.RiskTaxonomyL1  and 
r.RiskTaxonomyL2 =  c.RiskTaxonomyL2  and r.RiskTaxonomyL3 =  c.RiskTaxonomyL3  and r.RiskTaxonomyL4 =  c.RiskTaxonomyL4;
#3,312



#Joining RootCause #Joining with Failure Mode #Joining with BCMCriticality #Joining with Root Cause
Drop temporary table if exists tempdb.RcFmBcm;
Create temporary table tempdb.RcFmBcm
(Index `IX_RootCause` (`ProductTypeL1`,`ProductTypeL2`,`ProdDesc`))
select 

distinct  
bt.n,
ProcessName,
ProcessDescription,
OpDesc,

fm.id as FailureModeId,
rc.RCId as RootCauseId,
bcp.id as BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

ProductTypeL1, 
ProductTypeL2,
ProdDesc,

bt.ID_Control

from tempdb.CRP2 bt 
inner join 
(
select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId 
from RootCausePfmr l3 
inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  
inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null 
where  l3.CustomerId = 541
)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc #Joining with RootCause
inner join BcmCriticalityPfmr bcp 
on bcp.description = bt.BCMCriticality and bcp.customerId=541 #Joining with BCMCriticality
inner join FailureModePfmr fm on fm.`name` = bt.Failure and fm.CustomerId=541; #Joining with Failure Mode
#3312

#joining with Product
Drop temporary table if exists tempdb.AddingProd;
Create temporary table tempdb.AddingProd
(Index `IX_ProcessName` (`ProcessName`),Index `IX_Risk` (`RiskTaxonomyL1`,`RiskTaxonomyL2`,`RiskTaxonomyL3`,`RiskTaxonomyL4`))
select 
distinct 
rfb.n, 
ProcessName,
ProcessDescription,
OpDesc,

FailureModeId,
RootCauseId,
BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

ProductTypeL1, 
ProductTypeL2,
ProdDesc,

p.PrdId as ProductId,

rfb.ID_Control


from tempdb.RcFmBcm rfb 
inner join 
(select l1.name as l1, l2.name as l2, l3.name as def, l3.id as PrdId from ProductPfmr l3 
inner join ProductPfmr l2 on l3.parentId = l2.id and cast(l3.definition as unsigned) = 1 
inner join ProductPfmr l1 on l2.parentId = l1.id and l1.parentid is null where l3.customerid=541 
)p on p.l1 = rfb.ProductTypeL1 and p.l2 = rfb.ProductTypeL2 and p.def = rfb.`ProdDesc`; #joining with Product 
#3312 

#joining with MyProcesses and Risk Taxonomy
Drop temporary table if exists tempdb.MPRT;
Create temporary table tempdb.MPRT
select 
distinct 
ap.n,
OpDesc, 

ProcName,
myproc.MyProcId,
FailureModeId,
RootCauseId,
BCMCriticality,
RiskLevel4Name,
RiskLevel4Id,

ProductTypeL1, 
ProductTypeL2,
ProdDesc,
ProductId,

ap.ID_Control

from tempdb.AddingProd ap 
inner join 
(
select p.`name` as ProcName, p.`description` as  ProcessDescription, mp.id as MyProcId From MyProcessesPfmr mp  
inner join ProcessPfmr p on mp.processId = p.id and  mp.CustomerId= p.CustomerId 
where mp.customerId=541
) myproc
on myproc.ProcName = ap.ProcessName  
#and myproc.ProcessDescription = ap.ProcessDescription
and replace(replace(myproc.ProcessDescription,'\r',''),'\n','')= replace(replace(ap.ProcessDescription,'\r',''),'\n','')
inner join 
(
Select 
l4.`name` as RiskLevel4Name, l4.id as RiskLevel4Id, l3.`name` as RiskLevel3Name, l3.id as RiskLevel3Id, 
l2.`name` as RiskLevel2Name, l2.id as RiskLevel2Id, l1.`name` as RiskLevel1Name, l1.id as RiskLeve11Id  
from RiskRegisterItem l4 
inner join RiskRegisterItem l3 on l3.id = l4.parentId  
inner join RiskRegisterItem l2 on l2.id = l3.parentId 
inner join RiskRegisterItem l1 on l1.id = l2.parentid 
where cast(l4.isLeaf as unsigned) = 1 and l4.CustomerId = 541
)r 
on  r.RiskLevel4Name = ap.RiskTaxonomyL4 and r.RiskLevel3Name = ap.RiskTaxonomyL3 
and r.RiskLevel2Name = ap.RiskTaxonomyL2 and r.RiskLevel1Name = ap.RiskTaxonomyL1; 
#3312


#joining with MyOperationPfmr
Drop temporary table if exists tempdb.FinalStepMyOps;
Create temporary table tempdb.FinalStepMyOps
select 
distinct 
n,
mo.id as MyOperationId, 

ProcName,
MyProcId,
FailureModeId,
RootCauseId,
BCMCriticality,
RiskLevel4Name,
RiskLevel4Id,

ProductTypeL1, 
ProductTypeL2,
ProdDesc,
ProductId,
ID_Control
from tempdb.MPRT o
inner join MyOperationPfmr mo on o.OpDesc = mo.`description`
where mo.CustomerId=541;
#3311
 

ALTER TABLE tempdb.FinalStepMyOps CHANGE COLUMN `n` `n` varchar(10) NOT NULL ;

Drop temporary table if exists tempdb.Final1;
Create temporary table tempdb.Final1
select 
n,MyOperationId,MyProcId,FailureModeId,RootCauseId,BCMCriticality,RiskLevel4Name,RiskLevel4Id, ID_Control,min(ProductId) as ProductId
from tempdb.FinalStepMyOps group by 
n,MyOperationId,MyProcId,FailureModeId,RootCauseId,BCMCriticality,RiskLevel4Name,RiskLevel4Id,ID_Control;
#2211

select count(1) from  tempdb.Final1 f inner join tempdb.CBT2 c on f.ID_Control = c.ControlIdInFile; #2104
#2104


Drop temporary table if exists tempdb.FinalControlInstance1;
Create temporary table tempdb.FinalControlInstance1 
select distinct
n,p.id as processOwnerPfmrId, c.* 
from  tempdb.Final1 f 
inner join tempdb.CBT2 c on f.ID_Control = c.ControlIdInFile  
inner join (Select p.*, riskRegisterItemId,frequencyOccurence  from ProcessOwnerRiskPfmr p inner join RiskRegister r on r.id = p.riskId  where p.CustomerId=541) p 
on p.failureModeId= f.failureModeId and p.myProcessId = f.myProcId and p.bcmCriticalPfmrId = f.BCMCriticality and p.rootCausePfmrId = f.RootCauseId 
and p.riskRegisterItemId = f.RiskLevel4Id and p.frequencyOccurence = f.n and p.productPfmrId = f.ProductId ;
#2,103


/*
INSERT INTO `ControlInstance` (`name`, `controlOwner`, `controlTester`)
Select  distinct `name`,ControlIdInFile, processOwnerPfmrId  from tempdb.FinalControlInstance1;
*/

select count(1) from tempdb.FinalControlInstance1 f 
inner join ControlInstance c on f.ControlIdInFile = c.controlOwner and f.processOwnerPfmrId = c.controlTester
inner join   ControlProbabilityAdequacySlabPfmr a
on a.percentage = f.OverallControlAdequacy  and a.customerId = 541
inner join   ControlImpactEfficacySlabPfmr e 
on e.percentage  = f.ControlEffectiveness and e.customerId = 541;


/*
Insert into ControlInstancePfmr
select 
null as id, c.id as controlInstanceId,  f.processOwnerPfmrId, f.`name`, f.controlProcedure, f.description, f.Objectives, f.controlType, f.functionMode, f.examinationMethod, 
f.controlOwnerId, a.id as controlProbabilityAdequacySlabPfmrId, f.justificationForAdequacy, e.id as controlImpactEfficacySlabPfmrId, 
f.justificationForEfficacy, 17212 as `createdBy`, current_timestamp() as createdOn, 17212 as `modifiedBy`, current_timestamp() as `modifiedOn`, 541 as `customerId`, f.controlProcedureUrl

from tempdb.FinalControlInstance1 f 
inner join ControlInstance c on f.ControlIdInFile = c.controlOwner and f.processOwnerPfmrId = c.controlTester
inner join   ControlProbabilityAdequacySlabPfmr a
on a.percentage = f.OverallControlAdequacy  and a.customerId = 541
inner join   ControlImpactEfficacySlabPfmr e 
on e.percentage  = f.ControlEffectiveness and e.customerId = 541;
*/

/*
Update ControlInstance set isDeleted=false where id in ( select distinct controlInstanceId from ControlInstancePfmr where CustomerId=541);
*/