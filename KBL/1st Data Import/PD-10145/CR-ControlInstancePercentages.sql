DROP TEMPORARY table if exists tempdb.DerivedNewPercentagesSlab;
Create TEMPORARY table tempdb.DerivedNewPercentagesSlab
select c.id,  
 #controlProbabilityAdequacySlabPfmrId
 #New Adequacy
 case 
 when  a.percentage = 60 then 100
 when  a.percentage = 40 then 80
 when  a.percentage = 25 then 60
 when  a.percentage = 10 then 25
 end as NewAdequacyPercentage,
 #controlImpactEfficacySlabPfmrId, 
 #New Efficacy
 case 
 when  e.percentage = 60 then 100
 when  e.percentage = 40 then 80
 when  e.percentage = 25 then 60
 when  e.percentage = 10 then 25
 end as NewEfficacyPercentage
from ControlInstancePfmr c
inner join ControlProbabilityAdequacySlabPfmr a on a.id = c.controlProbabilityAdequacySlabPfmrId
inner join ControlImpactEfficacySlabPfmr e on e.id = c.controlImpactEfficacySlabPfmrId
where c.CustomerId = 541 ;

DROP TEMPORARY table if exists tempdb.CRControlInstancePfmr;
Create TEMPORARY table tempdb.CRControlInstancePfmr
select d.id, a.id as NewcontrolProbabilityAdequacySlabPfmrId, e.id as NewcontrolImpactEfficacySlabPfmrId from  tempdb.DerivedNewPercentagesSlab d
inner join ControlProbabilityAdequacySlabPfmr a on a.Percentage = d.NewAdequacyPercentage and a.customerId = 541
inner join ControlImpactEfficacySlabPfmr e on e.Percentage = d.NewEfficacyPercentage and e.customerId = 541;

select count(1) from  tempdb.CRControlInstancePfmr; #2103

/*
update ControlInstancePfmr o
inner join tempdb.CRControlInstancePfmr n on o.id = n.id
set o.controlProbabilityAdequacySlabPfmrId=  n.NewcontrolProbabilityAdequacySlabPfmrId,
    o.controlImpactEfficacySlabPfmrId= n.NewcontrolImpactEfficacySlabPfmrId;
*/