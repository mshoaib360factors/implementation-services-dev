CREATE TABLE `processownerriskpfmr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `riskId` bigint(20) NOT NULL,
  `failureModeId` bigint(20) NOT NULL,
  `myProcessId` bigint(20) NOT NULL,
  `bcmCriticalPfmrId` bigint(20) DEFAULT NULL,
  `rootCausePfmrId` bigint(20) NOT NULL,
  `riskTaker` varchar(255) NOT NULL,
  `productPfmrId` bigint(20) NOT NULL,
  `createdBy` bigint(20) NOT NULL,
  `createdOn` datetime NOT NULL,
  `modifiedBy` bigint(20) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `customerId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_failureModePfmrId_FailureModePfmr` (`failureModeId`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_processPfmrId_PorcessPfmr` (`myProcessId`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_rootCausePfmrId_RootCausePfmr` (`rootCausePfmrId`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_productPfmrId_ProductPfmr` (`productPfmrId`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_createdBy` (`createdBy`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_modifiedBy` (`modifiedBy`),
  KEY `fk_ProcessOwnerRiskDetailPfmr_customerId` (`customerId`),
  
  KEY `fk_ProcessOwnerRiskDetailPfmr_riskId` (`riskId`),
  
  
  KEY `fk_ProcessOwnerRiskDetailPfmr_BcmCriticalId` (`bcmCriticalPfmrId`),
  
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_BcmCriticalId` FOREIGN KEY (`bcmCriticalPfmrId`) REFERENCES `bcmcriticalitypfmr` (`id`),
  
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `predictuser` (`id`),
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_customerId` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`),
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_failureMode` FOREIGN KEY (`failureModeId`) REFERENCES `failuremodepfmr` (`id`),
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_modifiedBy` FOREIGN KEY (`modifiedBy`) REFERENCES `predictuser` (`id`),
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_processPfmr` FOREIGN KEY (`myProcessId`) REFERENCES `myprocessespfmr` (`id`),
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_productPfmrId_ProductPfmr` FOREIGN KEY (`productPfmrId`) REFERENCES `productpfmr` (`id`),
  
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_riskId` FOREIGN KEY (`riskId`) REFERENCES `riskregister` (`id`),
  
  CONSTRAINT `fk_ProcessOwnerRiskDetailPfmr_rootCausePfmrId_RootCausePfmr` FOREIGN KEY (`rootCausePfmrId`) REFERENCES `rootcausepfmr` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
