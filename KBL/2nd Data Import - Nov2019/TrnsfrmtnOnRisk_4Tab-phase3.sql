Select count(1) FROM etl.`kbl.fileimport_risktab`; #1414
SELECT count(1) FROM etl.`kbl.fileimport_allprocessestab`; #2438
SELECT distinct ID_Process, ID_RISK FROM etl.`kbl.fileimport_allprocessestab`; #1439
SELECT distinct ID_Process, ID_RISK FROM etl.`kbl.fileimport_allprocessestab` where ID_RISK is not null; #1402
Select ID as RiskID, ReferentialID as ProcessID FROM etl.`kbl.fileimport_risktab`; #1414


#### Joining Import Tables 
Drop temporary table if exists tempdb.BTRisk;
Create temporary table tempdb.BTRisk
SELECT distinct
trim(pt.ProcessName) as ProcessName , 
trim(pt.ProcessDescription) as ProcessDescription , 
trim(rt.ProcessedOperation) as OpDesc,
trim(rt.Failure) as Failure,
trim(rc.RootCauseL1) as RootCauseL1, 
trim(rc.RootCauseL2) as RootCauseL2, 
trim(rc.`Description`) as RootCauseDesc,
case 
when rt.InitialAssessment =  2 then 'Low'
when rt.InitialAssessment =  3 then 'Medium'
when rt.InitialAssessment =  4 then 'High'
when rt.InitialAssessment =  5 then 'Very High'
end as BCMCriticality, 
trim(tt.Level1) as RiskTaxonomyL1, 
trim(tt.Level2) as RiskTaxonomyL2, 
trim(tt.Level3) as RiskTaxonomyL3, 
trim(tt.Level4) as RiskTaxonomyL4, 
#rt.id as RiskID
rp.ProductTypeL1,
rp.ProducTypeL2 as ProductTypeL2,
rp.`Description` as ProdDesc

FROM etl.`kbl.fileimport_allprocessestab` apt  
inner join etl.`kbl.fileimport_processtab` pt on pt.id  = apt.ID_Process 
inner join etl.`kbl.fileimport_risktab`  rt on rt.id = apt.ID_RISK
inner join etl.`kbl.fileimport_rootcausetab` rc on rc.id = rt.RootCause
inner join etl.`kbl.fileimport_taxonomytab` tt on tt.id = rt.AllocationToRiskTaxonomyL4
inner join etl.`kbl.fileimport_riskandproducttab` rp on  rp.ID_RISK = rt.id 
where rp.ID_PRODUCT is not null and rp.ID_PRODUCT <> -1;
#1,941
/*
Create table etl.allprocessestab_rowno
select x.*, 
row_number() OVER (ORDER BY ProcessName, Failure, RootCauseL1, RootCauseL2, RootCauseDesc, BCMCriticality, RiskTaxonomyL1, RiskTaxonomyL2, RiskTaxonomyL3, RiskTaxonomyL4) n 
from 
(select distinct
ProcessName, Failure, RootCauseL1, RootCauseL2, RootCauseDesc, BCMCriticality, RiskTaxonomyL1, RiskTaxonomyL2, RiskTaxonomyL3, RiskTaxonomyL4
 from tempdb.BTRisk)X;
#1,264
*/

### Importing Custom Created Rank for Uniquely Identifying the unique Row in Risk based on Risk, Failure, RootCause
Drop temporary table if exists tempdb.BTRisk2;
Create temporary table tempdb.BTRisk2
(Index `IX_Failure` (`Failure`), Index `IX_RootCause` (`RootCauseL1`,`RootCauseL2`,`RootCauseDesc`) )
SELECT distinct bt.*, r.n

FROM tempdb.BTRisk bt
inner join etl.allprocessestab_rowno r on 
r.ProcessName = bt.ProcessName and r.Failure =   bt.Failure  and r.RootCauseL1 =   bt.RootCauseL1  and  r.RootCauseL2 =   bt.RootCauseL2  
and r.RootCauseDesc =   bt.RootCauseDesc  and  r.BCMCriticality =   bt.BCMCriticality  and r.RiskTaxonomyL1 =   bt.RiskTaxonomyL1  and 
r.RiskTaxonomyL2 =   bt.RiskTaxonomyL2  and r.RiskTaxonomyL3 =   bt.RiskTaxonomyL3  and r.RiskTaxonomyL4 =   bt.RiskTaxonomyL4;
#1941

#Joining RootCause #Joining with Failure Mode #Joining with BCMCriticality #Joining with Root Cause
Drop temporary table if exists tempdb.RcFmBcm;
Create temporary table tempdb.RcFmBcm
(Index `IX_RootCause` (`ProductTypeL1`,`ProductTypeL2`,`ProdDesc`))
select 

distinct  
bt.n,
ProcessName,
ProcessDescription,
OpDesc,

fm.id as FailureModeId,
rc.RCId as RootCauseId,
bcp.id as BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

ProductTypeL1, 
ProductTypeL2,
ProdDesc

from tempdb.BTRisk2 bt 
inner join 
(
select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId 
from RootCausePfmr l3 
inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  
inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null 
where  l3.CustomerId = 541
)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc #Joining with RootCause
inner join BcmCriticalityPfmr bcp 
on bcp.description = bt.BCMCriticality and bcp.customerId=541 #Joining with BCMCriticality
inner join FailureModePfmr fm on fm.`name` = bt.Failure and fm.CustomerId=541; #Joining with Failure Mode
#1941

#joining with Product
Drop temporary table if exists tempdb.AddingProd;
Create temporary table tempdb.AddingProd
(Index `IX_ProcessName` (`ProcessName`),Index `IX_Risk` (`RiskTaxonomyL1`,`RiskTaxonomyL2`,`RiskTaxonomyL3`,`RiskTaxonomyL4`))
select 
distinct 
rfb.n, 
ProcessName,
ProcessDescription,
OpDesc,

FailureModeId,
RootCauseId,
BCMCriticality,

RiskTaxonomyL1, 
RiskTaxonomyL2, 
RiskTaxonomyL3, 
RiskTaxonomyL4, 

ProductTypeL1, 
ProductTypeL2,
ProdDesc,

p.PrdId as ProductId


from tempdb.RcFmBcm rfb 
inner join 
(select l1.name as l1, l2.name as l2, l3.name as def, l3.id as PrdId from ProductPfmr l3 
inner join ProductPfmr l2 on l3.parentId = l2.id and cast(l3.definition as unsigned) = 1 
inner join ProductPfmr l1 on l2.parentId = l1.id and l1.parentid is null where l3.customerid=541 
)p on p.l1 = rfb.ProductTypeL1 and p.l2 = rfb.ProductTypeL2 and p.def = rfb.`ProdDesc` #joining with Product 
;
#1941 

#joining with MyProcesses and Risk Taxonomy
Drop temporary table if exists tempdb.MPRT;
Create temporary table tempdb.MPRT
select 
distinct 
ap.n,

OpDesc, 

ProcName,
myproc.MyProcId,
FailureModeId,
RootCauseId,
BCMCriticality,
RiskLevel4Name,
RiskLevel4Id,

ProductTypeL1, 
ProductTypeL2,
ProdDesc,
ProductId


from tempdb.AddingProd ap 
inner join 
(
select p.`name` as ProcName, p.`description` as  ProcessDescription, mp.id as MyProcId From MyProcessesPfmr mp  
inner join ProcessPfmr p on mp.processId = p.id and  mp.CustomerId= p.CustomerId 
where mp.customerId=541
) myproc
on myproc.ProcName = ap.ProcessName 
#and myproc.ProcessDescription = ap.ProcessDescription
and replace(replace(myproc.ProcessDescription,'\r',''),'\n','')= replace(replace(ap.ProcessDescription,'\r',''),'\n','')
inner join 
(
Select 
l4.`name` as RiskLevel4Name, l4.id as RiskLevel4Id, l3.`name` as RiskLevel3Name, l3.id as RiskLevel3Id, 
l2.`name` as RiskLevel2Name, l2.id as RiskLevel2Id, l1.`name` as RiskLevel1Name, l1.id as RiskLeve11Id  
from RiskRegisterItem l4 
inner join RiskRegisterItem l3 on l3.id = l4.parentId  
inner join RiskRegisterItem l2 on l2.id = l3.parentId 
inner join RiskRegisterItem l1 on l1.id = l2.parentid 
where cast(l4.isLeaf as unsigned) = 1 and l4.CustomerId = 541
)r 
on  r.RiskLevel4Name = ap.RiskTaxonomyL4 and r.RiskLevel3Name = ap.RiskTaxonomyL3 
and r.RiskLevel2Name = ap.RiskTaxonomyL2 and r.RiskLevel1Name = ap.RiskTaxonomyL1;
#1941

#joining with MyOperationPfmr
Drop temporary table if exists tempdb.FinalStepMyOps;
Create temporary table tempdb.FinalStepMyOps
select 
distinct 
n,
mo.id as MyOperationId, 

ProcName,
MyProcId,
FailureModeId,
RootCauseId,
BCMCriticality,
RiskLevel4Name,
RiskLevel4Id,

ProductTypeL1, 
ProductTypeL2,
ProdDesc,
ProductId
from tempdb.MPRT o
inner join MyOperationPfmr mo on o.OpDesc = mo.`description`
where mo.CustomerId=541;
#1940  

ALTER TABLE tempdb.FinalStepMyOps
CHANGE COLUMN `n` `n` varchar(10) NOT NULL ;

/*
delete from ProcessOwnerRiskProductPfmr where processOwnerRiskPfmrId in (select id from ProcessOwnerRiskPfmr where customerId=541);
delete from ProcessOwnerRiskMyOperationsPfmr where processOwnerRiskPfmrId in (select id from ProcessOwnerRiskPfmr where customerId=541 );
delete from ProcessOwnerRiskPfmr where customerId=541;
delete from RiskRegisterOverallAnalysis where CustomerId=541;
delete from RiskRegister where customerid=541 ;
*/

# Adding Risk Names into RiskRegister for Refering in ProcessOwnerRiskPfmr
/*
INSERT INTO `predict360`.`RiskRegister` (`riskName`, `customerId`, `riskRegisterItemId`, frequencyOccurence, `status`, `createdBy`, `createdDate`)
select distinct RiskLevel4Name as riskName, 541 as customerId, RiskLevel4Id as riskRegisterItemId, n as frequencyOccurence,'Active' as  `status`, 17212 createdBy, current_timestamp() createdDate 
from  tempdb.FinalStepMyOps;
 */
#1263

#SELECT count(1) FROM tempdb.FinalStepMyOps fo INNER JOIN RiskRegister rr ON fo.RiskLevel4Name = rr.riskName and fo.RiskLevel4Id = riskRegisterItemId WHERE rr.customerId = 541; #1966

# Importing into ProcessOwnerRiskPfmr
/*
insert into ProcessOwnerRiskPfmr 
select null as id,  riskId,FailureModeId, MyProcId, BCMCriticality,RootCauseId,17212 as riskTaker, productPfmrId, null as extremeEventPfmrId,
17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId 
from  
(
select rr.id as riskId, fo.FailureModeId, fo.MyProcId, fo.BCMCriticality, fo.RootCauseId, min(ProductId) as productPfmrId  
from  tempdb.FinalStepMyOps fo  
inner join RiskRegister rr on rr.frequencyOccurence  = fo.n 
where rr.customerId=541 group by rr.id, fo.FailureModeId, fo.MyProcId, fo.BCMCriticality, fo.RootCauseId
)x;
*/
#1263

/*
Insert into ProcessOwnerRiskProductPfmr
SELECT DISTINCT null as id, p.id  as processOwnerRiskPfmrId, t.productPfmrId as productPfmrId
FROM ProcessOwnerRiskPfmr p 
INNER JOIN
(
SELECT distinct rr.id AS riskId, fo.MyProcId, ProductId as productPfmrId
FROM tempdb.FinalStepMyOps fo
INNER JOIN RiskRegister rr ON rr.frequencyOccurence = fo.n
WHERE rr.customerId = 541
) t 
ON t.riskId = p.riskId AND t.MyProcId = p.myProcessId WHERE p.customerId = 541;
*/
#1936

# Importing into ProcessOwnerRiskMyOperationsPfmr
/*
insert into ProcessOwnerRiskMyOperationsPfmr
select distinct null as id, t.MyOperationId as myOperationPfmrId ,pr.id as processOwnerRiskPfmrId  
From ProcessOwnerRiskPfmr pr  
inner join RiskRegister r on  r.id= pr.riskid 
inner join  tempdb.FinalStepMyOps t on  
t.MyProcId = pr.myProcessId and t.n = r.frequencyOccurence;
*/


/*
#Updating the Risk Taker Name 

update ProcessOwnerRiskPfmr set riskTaker= 'Implementation KBL' where customerid=541;

*/

#RootCause Links Done
select count(1) from tempdb.BTRisk bt inner join (select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId from RootCausePfmr l3 inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null where  l3.CustomerId = 541)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc;
#1267

#bcmCriticalPfmrId Links Done
select count(1) from tempdb.BTRisk bt inner join (select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId from RootCausePfmr l3 inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null where  l3.CustomerId = 541)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc inner join BcmCriticalityPfmr bcp on bcp.description = bt.BCMCriticality and bcp.customerId=541;
#1267

# FailureModePfmr Links Done
select count(1) from tempdb.BTRisk bt inner join (select l1.label as RCL1, l2.label as RCL2, l3.label as RCDef, l3.id as RCId from RootCausePfmr l3 inner join RootCausePfmr l2 on l3.parentId = l2.id and cast(l3.Isdefinition as unsigned) = 1  inner join RootCausePfmr l1 on l2.parentId = l1.id and l1.parentid is null where  l3.CustomerId = 541)rc on rc.RCL1 = bt.RootCauseL1 and rc.RCL2 = bt.RootCauseL2 and rc.RCDef = bt.RootCauseDesc inner join BcmCriticalityPfmr bcp on bcp.description = bt.BCMCriticality and bcp.customerId=541 inner join FailureModePfmr fm on fm.`name` = bt.Failure and fm.CustomerId=541;
#1268  may get remove after have distinct values











select * from tempdb.BTRisk bt
inner join FailureModePfmr fm on fm.`name` = bt.Failure
where fm.CustomerId=541 limit 1;
 
# inner join BcmCriticalityPfmr bcp on pcp.`description` =  ; #1401

 

Select * FROM etl.`kbl.fileimport_risktab` rt where rt.id = 711;


## Validate the Risk and Process Mapping

  

select * from  etl.`kbl.fileimport_risktab` ;

/*
sSelect count(Distinct ReferentialID), count(1) FROM etl.`kbl.fileimport_risktab`;

SELECT * FROM etl.`kbl.fileimport_processtab` where id in (Select  Distinct ReferentialID  FROM etl.`kbl.fileimport_risktab`);*/