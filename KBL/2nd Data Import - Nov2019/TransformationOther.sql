/*

######################
ROOT CAUSES_7 --> Tab
######################
### Analysis ###
SELECT count(1)  FROM etl.`kbl.fileimport_rootcausetab`; #174
SELECT  count(distinct RootCauseL1), count(distinct RootCauseL2)  FROM etl.`kbl.fileimport_rootcausetab`;
#4	24

select Description, count(1)  FROM etl.`kbl.fileimport_rootcausetab` group by Description having count(1) > 1;
select RootCauseL2, Description, count(1)  FROM etl.`kbl.fileimport_rootcausetab` group by RootCauseL2, Description having count(1) > 1;
#SELECT * FROM predict360.RootCausePfmr where customerId =1813; 


SELECT  distinct trim(RootCauseL2), trim(Description) FROM etl.`kbl.fileimport_rootcausetab` bt;




# Inserting Level 1 
insert into predict360.RootCausePfmr
SELECT distinct null as id, 17212 as createdBy, current_timestamp() as createdOn, 17212 as modifiedBy, current_timestamp() as modifiedOn, 541 as customerId, trim(RootCauseL1) label, 1 as status, null parentId, 0 isDefinition
FROM etl.`kbl.fileimport_rootcausetab`;

# Inserting Level 2
insert into predict360.RootCausePfmr
SELECT  distinct null as id, 17212 as createdBy, current_timestamp() as createdOn, 17212 as modifiedBy, current_timestamp() as modifiedOn, 541 as customerId, trim(RootCauseL2) label, 1 as status, ft.id  parentId, 0 isDefinition
FROM etl.`kbl.fileimport_rootcausetab` bt
inner join predict360.RootCausePfmr  ft on trim(RootCauseL1) = ft.label 
where ft.CustomerId = 541; 

# Inserting Level 3
insert into predict360.RootCausePfmr
SELECT  distinct null as id, 17212 as createdBy, current_timestamp() as createdOn, 17212 as modifiedBy, current_timestamp() as modifiedOn, 541 as customerId, trim(Description) label, 1 as status, ft.id  parentId, 1 isDefinition
FROM etl.`kbl.fileimport_rootcausetab` bt
inner join predict360.RootCausePfmr  ft on trim(RootCauseL2) = ft.label 
where ft.CustomerId = 541 and ft.parentId is not null; 


select count(1) from predict360.RootCausePfmr where customerId = 541;
*/


/*

######################
PROCESS TYPE ID--> Tab
######################


SELECT * FROM predict360.ProcessTypePfmr where customerid = 1813; 
SELECT * FROM predict360.ProcessTypePfmr where customerid = 541; 


# Inserting Level 1 
insert into ProcessTypePfmr
SELECT distinct null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 
541 customerId, Process_Type_Level_1 label, 1 status, null parentId, 0 isDefinition
 FROM etl.`kbl.fileimport_processtypeidtab`;
 
 
# Inserting Level 2
insert into ProcessTypePfmr
SELECT distinct null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn,  
541 customerId, Process_Type_Level_2 label, 1 status, p.id parentId, 0 isDefinition  FROM etl.`kbl.fileimport_processtypeidtab` f 
inner join  ProcessTypePfmr p on f.Process_Type_Level_1 =  p.label and p.customerId = 541;

# Inserting Level 3
insert into ProcessTypePfmr
SELECT  null as id, 17212 createdBy, current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn,   
541 customerId, Description as label, 1 status, p.id parentId, 1 isDefinition   FROM etl.`kbl.fileimport_processtypeidtab` f   
inner join  (select l2.Id, l2.Label as Level2, l1.Label as Level1 from ProcessTypePfmr l2 inner join ProcessTypePfmr l1 on l2.ParentId = l1.Id  and l2.CustomerId = l1.CustomerId where l2.CustomerId=541) p  
on f.Process_Type_Level_2 =  p.Level2 and Process_Type_Level_1= p.Level1 ;

*/


/*

######################
Risk & product_2--> Tab
######################

SELECT * FROM predict360.ProductPfmr where customerid = 1813; 
 
select distinct ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null;


Select distinct ProductTypeL1, ProducTypeL2, Description  FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y;

# Inserting Level 1 
insert into ProductPfmr
 Select distinct  null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId,
 ProductTypeL1 as name,  #, ProducTypeL2, Description  
 1 status, null parentId, 0 isDefinition
 FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y;


# Inserting Level 2
insert into ProductPfmr
 Select distinct  null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId,
 ProducTypeL2 as name,  
 1 status, z.id parentId, 0 isDefinition
 FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y
inner join ProductPfmr z on y.ProductTypeL1 = z.name and z.customerId = 541;
 
 
 
# Inserting Level 3
insert into ProductPfmr
 Select distinct  null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId,
 Description as Name  ,
 1 status, z.id parentId, 1 isDefinition
 FROM  
(Select distinct ID_Product, ProductTypeL1, ProducTypeL2, Description  FROM 
(SELECT  distinct ID_RISK, ID_PRODUCT, ProductTypeL1, ProducTypeL2, Description  FROM etl.`kbl.fileimport_riskandproducttab` )x where ID_Product is not null )y
 inner join 
(select l2.Id, l2.name as Level2, l1.name as Level1  from ProductPfmr l2 inner join ProductPfmr l1 on l2.ParentId = l1.Id  and l2.CustomerId = l1.CustomerId where l2.CustomerId=541) z 
on y.ProducTypeL2 = z.Level2 and y.ProductTypeL1= z.Level1;
 */
 
 
##SELECT * FROM predict360.ProductPfmr; ## (Risk & product_2 --> Tab)
#SELECT * FROM predict360.ControlInstancePfmr; ## ( --> Tab)
#SELECT * FROM predict360.ControlInstancePfmr order by id desc;
 
 
 
 