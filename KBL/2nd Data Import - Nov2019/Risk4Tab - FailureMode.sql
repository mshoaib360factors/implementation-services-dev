#### Adding Failure Mode ##########


SELECT count(1) FROM etl.`kbl.fileimport_risktab`;  #1,414
SELECT distinct trim(Failure) FROM etl.`kbl.fileimport_risktab`; #1,177

SELECT distinct trim(substring(Failure,1,50)) FROM etl.`kbl.fileimport_risktab`; #1,167

select trim(substring(Failure,1,50)), count(1)  from (
SELECT distinct trim(Failure) as Failure FROM etl.`kbl.fileimport_risktab`)x
group by  trim(substring(Failure,1,50)) having count(1) > 1 ; #10

SELECT distinct trim(Failure) as Failure FROM etl.`kbl.fileimport_risktab` where Failure like 'Failure of IT Infrastructure, failure of OBS, fail%'; 

SELECT distinct trim(Failure_Mode) FROM etl.`kbl.fileimport_allprocessestab`; # 1,167

SELECT distinct Failure_Mode FROM etl.`kbl.fileimport_allprocessestab` 
where trim(Failure_Mode) not in (SELECT distinct trim(Failure) FROM etl.`kbl.fileimport_risktab`);#0



SELECT * FROM predict360.FailureModePfmr where customerid = 1813;


/*
insert into  predict360.FailureModePfmr
Select distinct  null, 17212 createdBy, current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId, 
trim(Failure) as `name`, 1 as numberOfDefectsPerYear, '' as description, 1 as `status`,  1 as `defaultMode`
 FROM etl.`kbl.fileimport_risktab` ;
*/

