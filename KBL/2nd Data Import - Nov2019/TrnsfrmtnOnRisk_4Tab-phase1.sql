#### Adding Failure Mode ##########


SELECT count(1) FROM etl.`kbl.fileimport_risktab`;  #1,414
SELECT distinct trim(Failure) FROM etl.`kbl.fileimport_risktab`; #1,177

SELECT distinct trim(substring(Failure,1,50)) FROM etl.`kbl.fileimport_risktab`; #1,167

select trim(substring(Failure,1,50)), count(1)  from (
SELECT distinct trim(Failure) as Failure FROM etl.`kbl.fileimport_risktab`)x
group by  trim(substring(Failure,1,50)) having count(1) > 1 ; #10

SELECT distinct trim(Failure) as Failure FROM etl.`kbl.fileimport_risktab` where Failure like 'Failure of IT Infrastructure, failure of OBS, fail%'; 

SELECT distinct trim(Failure_Mode) FROM etl.`kbl.fileimport_allprocessestab`; # 1,167

SELECT distinct Failure_Mode FROM etl.`kbl.fileimport_allprocessestab` 
where trim(Failure_Mode) not in (SELECT distinct trim(Failure) FROM etl.`kbl.fileimport_risktab`);#0



SELECT * FROM predict360.FailureModePfmr where customerid = 1813;


/*
insert into  predict360.FailureModePfmr
Select distinct  null, 17212 createdBy, current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId, 
trim(Failure) as `name`, 1 as numberOfDefectsPerYear, '' as description, 1 as `status`,  1 as `defaultMode`
 FROM etl.`kbl.fileimport_risktab` ;
*/


/*
set SQL_SAFE_UPDATES = 0;
update  etl.`kbl.fileimport_risktab`
set ProcessedOperation = trim(ProcessedOperation);
set SQL_SAFE_UPDATES = 1;
*/


SELECT count(1) FROM etl.`kbl.fileimport_risktab`; #1414 1179

########################################################
################# My Operation Screen ##################
########################################################
SELECT count(distinct ProcessedOperation) FROM etl.`kbl.fileimport_risktab`; #1,179
SELECT count(distinct MY_OPERATIONS)  FROM etl.`kbl.fileimport_allprocessestab`; #1,168
SELECT count(distinct ProcessedOperation) FROM etl.`kbl.fileimport_operationandproducttab`; #1,334


SELECT distinct ProcessedOperation FROM etl.`kbl.fileimport_risktab` rt 
left join (SELECT distinct MY_OPERATIONS  FROM etl.`kbl.fileimport_allprocessestab`) apt on rt.ProcessedOperation = apt.MY_OPERATIONS where apt.MY_OPERATIONS is null; #11

SELECT distinct rt.ProcessedOperation FROM etl.`kbl.fileimport_risktab` rt 
left join (SELECT distinct ProcessedOperation  FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation where opt.ProcessedOperation is null; #3


SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_risktab` rt 
left join (SELECT distinct MY_OPERATIONS, ID_RISK FROM etl.`kbl.fileimport_allprocessestab`) apt on rt.ProcessedOperation = apt.MY_OPERATIONS 
inner join etl.`kbl.fileimport_riskandproducttab` rpt on rpt.ID_RISK = apt.ID_RISK
where apt.MY_OPERATIONS is not null and ID_Product is not null; #110


## Since KBL emphasized us to use the file "Copy of Processed OperationProcesses V3.xlsx" for mapping the operations with Product therefore we will be using following queries



### Counts Verifiications
SELECT count(distinct rt.ProcessedOperation)  FROM etl.`kbl.fileimport_risktab` rt;  #1179

select count(1) from
(
SELECT  distinct rt.ProcessedOperation, opt.ID_PRODUCT  FROM etl.`kbl.fileimport_risktab` rt
left join (SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation 
)x;   # 1,649


select count(1) from
(
SELECT  distinct rt.ProcessedOperation, opt.ID_PRODUCT, ProductTypeL1, ProducTypeL2, `Description` as PrdDesc  FROM etl.`kbl.fileimport_risktab` rt
left join (SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation 
inner join  etl.`kbl.fileimport_riskandproducttab` rpt
on rpt.ID_PRODUCT = ifnull(opt.ID_PRODUCT ,'-1') 
where opt.ProcessedOperation is not null
)x;  # 1,647


select count(1) from
(
SELECT  distinct opt.ID_PRODUCT, ProductTypeL1, ProducTypeL2, `Description` as PrdDesc  FROM etl.`kbl.fileimport_risktab` rt
left join (SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation 
inner join  etl.`kbl.fileimport_riskandproducttab` rpt
on rpt.ID_PRODUCT = ifnull(opt.ID_PRODUCT ,'-1') 
where opt.ProcessedOperation is not null
)x;  # 135

select count(1) from
(
SELECT  distinct rt.ProcessedOperation  FROM etl.`kbl.fileimport_risktab` rt
left join (SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation 
inner join  etl.`kbl.fileimport_riskandproducttab` rpt
on rpt.ID_PRODUCT = ifnull(opt.ID_PRODUCT ,'-1') 
where opt.ProcessedOperation is not null
)x;  # 135
#####

SELECT distinct rt.ProcessedOperation, ifnull(opt.ID_PRODUCT,-1) as ID_PRODUCT, ProductTypeL1, ProducTypeL2, `Description` as PrdDesc
FROM etl.`kbl.fileimport_risktab` rt 
left join (SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation
inner join  etl.`kbl.fileimport_riskandproducttab` rpt
on rpt.ID_PRODUCT = ifnull(opt.ID_PRODUCT ,'-1') 
where opt.ProcessedOperation is not null;  # 1,647

Drop temporary table if exists tempdb.MyOperation;
create temporary table tempdb.MyOperation
(index IX_Desc (ProcessedOperation))
SELECT distinct rt.ProcessedOperation, PrdId
FROM etl.`kbl.fileimport_risktab` rt 
left join (SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab`) opt on rt.ProcessedOperation = opt.ProcessedOperation
inner join  etl.`kbl.fileimport_riskandproducttab` rpt
on rpt.ID_PRODUCT = ifnull(opt.ID_PRODUCT ,'-1') 
inner join 
(select l1.name as l1, l2.name as l2, l3.name as def, l3.id as PrdId from ProductPfmr l3 
inner join ProductPfmr l2 on l3.parentId = l2.id and cast(l3.definition as unsigned) = 1 
inner join ProductPfmr l1 on l2.parentId = l1.id and l1.parentid is null where l3.customerid=541 )ps
on ps.l1 = rpt.ProductTypeL1 and ps.l2 = rpt.ProducTypeL2 and ps.def = rpt.`Description` 
where opt.ProcessedOperation is not null;  # 1,647



/*
#This Query is wrong because its inserted duplicate entry in the table 
insert into MyOperationPfmr
select 
null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId, 
substring(ProcessedOperation,1,100) as `name`, ProcessedOperation as `description`, true as `status`,  null as operationMemorandumName, false as isRecoveryPlan, 
17212 as processOwnerId, null as operationMemorandumUuid 
from tempdb.MyOperation;
#Correction of the mistakes of above query is below 

DELETE n1 FROM MyOperationPfmr n1, MyOperationPfmr n2 WHERE n1.id > n2.id AND n1.description = n2.description;

#Above delete statment is deleting some other values as well therefore inserting those from below query

insert into MyOperationPfmr
select distinct null as id, 17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId, 
substring(ProcessedOperation,1,100) as `name`, ProcessedOperation as `description`, true as `status`,  null as operationMemorandumName, false as isRecoveryPlan, 
17212 as processOwnerId, null as operationMemorandumUuid 
from tempdb.MyOperation where ProcessedOperation not in  (select distinct description  from MyOperationPfmr where `name` not in ('TEST OPERATION CASH TRANSFERT','OPERATION TEST FOR AUM REPORTING') and customerid= 541);

*/

/*
insert into MyOperationProductPfmr select null as id, mop.id as myOperationId, PrdId as productId  from MyOperationPfmr mop  
inner join  tempdb.MyOperation t on mop.description = t.ProcessedOperation 
where customerId= 541 and `name` not in ('TEST OPERATION CASH TRANSFERT','OPERATION TEST FOR AUM REPORTING') ;
*/



#SELECT distinct ProcessedOperation, ID_PRODUCT FROM etl.`kbl.fileimport_operationandproducttab` where ProcessedOperation ='Granting of credits, Bank Guarantees, ETD Limits & Credit Cards - Credit Analysis';
#SELECT distinct rt.ProcessedOperation FROM etl.`kbl.fileimport_risktab` rt  where ProcessedOperation like '%ENVOI DU TOKEN%';
#SELECT * FROM etl.`kbl.fileimport_operationandproducttab` rt  where ProcessedOperation like '%ENVOI DU TOKEN%';


#SELECT distinct rt.ProcessedOperation FROM etl.`kbl.fileimport_risktab` rt  where ProcessedOperation like '%Edition, supervision%';
#SELECT * FROM etl.`kbl.fileimport_operationandproducttab` rt  where ProcessedOperation like '%Edition%';
 