/*
# Inserting Level 1

-- Insert Statement
insert into RiskRegisterItem
Select distinct  
null as id,  Level1 as name, null parentId, 0 as isLeaf, 
17212 createdBy,current_timestamp() createdDate, 17212 modifiedBy, current_timestamp() updatedDate, 541 customerId, 
0 as isDeleted, '' as description
FROM etl.`kbl.fileimport_taxonomytab`; #7

# Inserting Level 2

-- Insert Statement
insert into RiskRegisterItem
Select distinct  
null as id,  Level2 as name, rri.id parentId, 0 as isLeaf, 
17212 createdBy,current_timestamp() createdDate, 17212 modifiedBy, current_timestamp() updatedDate, 541 customerId, 
0 as isDeleted, '' as description
FROM etl.`kbl.fileimport_taxonomytab` tt
inner join predict360.RiskRegisterItem  rri on trim(name) = tt.Level1 
where rri.CustomerId = 541;  #21

# Inserting Level 3

-- Insert Statement
select Level3,Level2, count(1) FROM 
(
SELECT distinct  Level1, Level2, Level3 FROM etl.`kbl.fileimport_taxonomytab` 
)x group by Level3,Level2 having count(1) > 1;


select Level3, count(1) FROM 
(
SELECT distinct  Level1, Level2, Level3 FROM etl.`kbl.fileimport_taxonomytab` 
)x group by Level3 having count(1) > 1;

select * FROM 
(
SELECT distinct  Level1, Level2, Level3 FROM etl.`kbl.fileimport_taxonomytab` 
)x where level3='Disclosure of confidential information';


insert into RiskRegisterItem
Select distinct  
null as id,  Level3 as name, rri.id parentId, 0 as isLeaf, 
17212 createdBy,current_timestamp() createdDate, 17212 modifiedBy, current_timestamp() updatedDate, 541 customerId, 
0 as isDeleted, '' as description
FROM etl.`kbl.fileimport_taxonomytab` tt
inner join predict360.RiskRegisterItem  rri on trim(name) = tt.Level2 
where rri.CustomerId = 541;  #52



# Inserting Level 4 

SELECT distinct  Level1, Level2, Level3, level4 FROM etl.`kbl.fileimport_taxonomytab`  where level4 is not null; # 972
SELECT distinct level4 FROM etl.`kbl.fileimport_taxonomytab`  where level4 is not null; # 915

select Level4, count(1) FROM 
(SELECT distinct   Level3, level4 FROM etl.`kbl.fileimport_taxonomytab`  where level4 is not null # 972
)x group by Level4  having count(1) > 1;  #34

select Level3,level4 , count(1) FROM 
(SELECT distinct   level2, Level3, level4 FROM etl.`kbl.fileimport_taxonomytab`  where level4 is not null # 972
)x group by Level3, level4 having count(1) > 1;  #0

select  Level3,level4, count(1) FROM 
(
SELECT distinct   Level3, level4 FROM etl.`kbl.fileimport_taxonomytab`  where level4 is not null # 972
)x group by  Level3,level4  having count(1) > 1;  #0


select  Level3,level4  FROM 
(
SELECT distinct   Level3, level4 FROM etl.`kbl.fileimport_taxonomytab`  where level4 is not null # 972
)x where 1;  #0


-- Insert Statement
insert into RiskRegisterItem
Select distinct  
null as id,  trim(Level4) as name, rri.id parentId, 1 as isLeaf, 
17212 createdBy,current_timestamp() createdDate, 17212 modifiedBy, current_timestamp() updatedDate, 541 customerId, 
0 as isDeleted, '' as description
FROM etl.`kbl.fileimport_taxonomytab` tt
inner join (select l3.Id, trim(l3.name) as Level3, trim(l2.name) as Level2 from RiskRegisterItem l3 inner join RiskRegisterItem l2 on l3.ParentId = l2.Id  and l3.CustomerId = l2.CustomerId
where l3.CustomerId=541 and l2.parentid is not null) rri 
on rri.Level3 = trim(tt.Level3) and rri.Level2 =  trim(tt.Level2) 
where tt.Level4 is not null ;  #972

*/

#Data Analysis from Taxonomy Tab 
SELECT count(1), count(distinct ID) FROM etl.`kbl.fileimport_taxonomytab` ; #	1024	1024

SELECT distinct Level1, Level2, Level3, Level4 FROM etl.`kbl.fileimport_taxonomytab`; #1023

SELECT distinct Level1, Level2, Level3, Level4 FROM etl.`kbl.fileimport_taxonomytab` where level4 is not null; #972

SELECT max(length(Level4)) FROM etl.`kbl.fileimport_taxonomytab`;


SELECT Level1, Level2, Level3, Level4, count(1) FROM etl.`kbl.fileimport_taxonomytab`
group by Level1, Level2, Level3, Level4 having count(1) > 1;

SELECT  *  FROM etl.`kbl.fileimport_taxonomytab`  
where Level1 = 'Execution, Delivery & Process Management' and Level2 = 'Monitoring and Reporting'  and  Level3 = 'Inaccurate or incomplete reporting (Internal)' and Level4= 'TBD';
select * from 
(SELECT distinct AllocationToRiskTaxonomyL1,AllocationToRiskTaxonomyL2, AllocationToRiskTaxonomyL3,AllocationToRiskTaxonomyL4 FROM etl.`kbl.fileimport_risktab`)x
where x.AllocationToRiskTaxonomyL4 in (155,159);

#Data Analysis of Taxonomy from Risk Tab
SELECT distinct AllocationToRiskTaxonomyL1,AllocationToRiskTaxonomyL2, AllocationToRiskTaxonomyL3,AllocationToRiskTaxonomyL4 FROM etl.`kbl.fileimport_risktab`; #775


#Data Analysis of Product and Risk associations
SELECT count(1), count(distinct id)  FROM etl.`kbl.fileimport_risktab`; #1414
SELECT  * FROM etl.`kbl.fileimport_risktab`; #



##Challenges
#Doubt 1
select AllocationToRiskTaxonomyL4, count(1)  From
(SELECT distinct AllocationToRiskTaxonomyL1,AllocationToRiskTaxonomyL2, AllocationToRiskTaxonomyL3,AllocationToRiskTaxonomyL4 FROM etl.`kbl.fileimport_risktab`)x
group by AllocationToRiskTaxonomyL4 having  count(1) >1;

select * from 
(SELECT distinct AllocationToRiskTaxonomyL1,AllocationToRiskTaxonomyL2, AllocationToRiskTaxonomyL3,AllocationToRiskTaxonomyL4 FROM etl.`kbl.fileimport_risktab`)x
where x.AllocationToRiskTaxonomyL4 in (200,1725);



#Doubt 2
select * from 
(SELECT distinct AllocationToRiskTaxonomyL1,AllocationToRiskTaxonomyL2, AllocationToRiskTaxonomyL3,AllocationToRiskTaxonomyL4 FROM etl.`kbl.fileimport_risktab`)x
left join etl.`kbl.fileimport_taxonomytab`tt 
on x.AllocationToRiskTaxonomyL4 = tt.id 
and x.AllocationToRiskTaxonomyL1 = tt.Level1
and x.AllocationToRiskTaxonomyL2 = tt.Level2
and x.AllocationToRiskTaxonomyL3 = tt.Level3 
where Level1 is null or Level2 is null or Level3 is null ;


