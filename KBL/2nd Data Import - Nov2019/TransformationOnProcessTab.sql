#Initial Analysis

SELECT * FROM predict360.ProcessPfmr where customerId =1813;

SELECT max(length(ProcessDescription))  FROM etl.`kbl.fileimport_processtab`; #2,772

SELECT count(1), count(distinct id)  FROM etl.`kbl.fileimport_processtab`; #881 #881
SELECT count(1), count(distinct id)  FROM etl.`kbl.fileimport_processtab` where WorkflowStatus = 'Out of Scope'; #1 #1
SELECT count(1), count(distinct id)  FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is null ; #188 #188
SELECT count(1), count(distinct id)  FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is not null and  WorkflowStatus <> 'Out of Scope'; #692 #692 --->> 881 - 188 - 1

SELECT distinct ProcessType FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is not null and  WorkflowStatus <> 'Out of Scope'; #82

SELECT ProcessName,ProcessDescription, count(1) FROM etl.`kbl.fileimport_processtab` 
where WorkflowStatus is not null and  WorkflowStatus <> 'Out of Scope' 
group by ProcessName,ProcessDescription having count(1) > 1;

SELECT ProcessName,ProcessDescription, count(1) FROM etl.`kbl.fileimport_processtab` 
where WorkflowStatus is not null and  WorkflowStatus <> 'Out of Scope' 
group by ProcessName,ProcessDescription having count(1) > 1;


SELECT * FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is not null and  WorkflowStatus <> 'Out of Scope' and ProcessName = 'Incident management';


SELECT distinct ProcessName, length(ProcessName) FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is not null and  WorkflowStatus <> 'Out of Scope' and 
length(ProcessName) > 100;


SELECT distinct ProcessType FROM etl.`kbl.fileimport_processtab` where ProcessType not in 
(SELECT distinct ID FROM etl.`kbl.fileimport_processtypeidtab`); #0

SELECT distinct WorkflowStatus FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is not null ;

SELECT count(1) , count(distinct ID) FROM etl.`kbl.fileimport_processtypeidtab`; #121

SELECT count(1) FROM etl.`kbl.fileimport_processtab` where WorkflowStatus is null ;

#################################
###	Loading Process Screen
#################################
/*
Insert into predict360.ProcessPfmr
select 
null as id,  17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId, 
trim(ProcessName) as `name`, trim(ProcessDescription) as `description`, 1 as status, 
case when WorkflowStatus = 'Reviewed' then 3 when WorkflowStatus = 'Submitted for review' then 2 
when WorkflowStatus = 'Initiate' then 0 else  -1 end  as processStatus,
0 as isHubSupport,
Level1Id as pTLOneId, Level2Id as pTLTwoId, Level3Id as pTLDescId, 17212 as ProcessOwnerID
 
from etl.`kbl.fileimport_processtab` pt 
inner join etl.`kbl.fileimport_processtypeidtab` ptit 
on  pt.ProcessType = ptit.id  
inner join 
(
select l3.Id as Level3Id, l3.Label as Level3, l2.id as Level2Id, l2.Label as Level2, l1.id as Level1Id, l1.Label as Level1
from ProcessTypePfmr l3 
inner join ProcessTypePfmr l2 
on l3.ParentId = l2.Id  and l3.CustomerId = l2.CustomerId and l3.isDefinition=1 
inner join ProcessTypePfmr l1 
on l2.ParentId = l1.Id  and l2.CustomerId = l1.CustomerId and l1.ParentId is null
where l2.CustomerId=541 
) pd
on pd.Level1 =ptit.Process_Type_Level_1 and  pd.Level2 = ptit.Process_Type_Level_2 and pd.Level3 = ptit.`Description`
where 
WorkflowStatus is not null # Advised by KBL people over email either to ignore these rows or include them as Initiate/Initial
and WorkflowStatus <> 'Out of Scope' # Advised by KBL people over email
; #692
*/


#################################
###	Loading My Process Screen
#################################
Describe myprocessespfmr;
Describe myprocessesbcmcriticalitypfmr;
Describe myprocessesriskworseningpfmr;


## Adding further attributes of [ MyProcesses > Edit Process ]

 select max(length(PredecessorOther)) from etl.`kbl.fileimport_processtab`; #112

select count(1) from 
(
SELECT s1.ProcessName, s1.ProcessDescription, 
ifnull(s1.Capacity,1) as Capacity,  #Based on the Response from KBL
ifnull(s1.AnnualNCOutputQty,1) as AnnualNCOutputQty,  #Based on the Response from KBL
s2.ProcessName as PredecessorName, 
s2.ProcessDescription as PredecessorDescription,
s1.PredecessorOther, 
case when s1.Terminator= 1 then true else false end as Terminator
FROM etl.`kbl.fileimport_processtab` s1
left join etl.`kbl.fileimport_processtab` s2
on s1.Predecessor = s2.id
where  s1.WorkflowStatus is not null  and s1.WorkflowStatus <> 'Out of Scope' )fi
inner join  ProcessPfmr pp1 on pp1.`name` = trim(fi.ProcessName) and pp1.`description` = trim(fi.ProcessDescription) and pp1.customerid = 541
left join ProcessPfmr pp2  on pp2.`name` = fi.PredecessorName and pp2.`description` = fi.PredecessorDescription and pp2.customerid = 541;
#692

select * from MyProcessesPfmr;
/*
insert into MyProcessesPfmr
select null as id, pp1.id as processId, fi.Capacity, fi.AnnualNCOutputQty as defectsQtyPerYear, pp2.id as predecessorId,
fi.PredecessorOther, fi.Terminator,
17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId
from 
(
SELECT s1.ProcessName, s1.ProcessDescription, 
ifnull(s1.Capacity,1) as Capacity,  #Based on the Response from KBL
ifnull(s1.AnnualNCOutputQty,1) as AnnualNCOutputQty,  #Based on the Response from KBL
s2.ProcessName as PredecessorName, 
s2.ProcessDescription as PredecessorDescription,
s1.PredecessorOther, 
case when s1.Terminator= 1 then true else false end as Terminator
FROM etl.`kbl.fileimport_processtab` s1
left join etl.`kbl.fileimport_processtab` s2
on s1.Predecessor = s2.id
where  s1.WorkflowStatus is not null  and s1.WorkflowStatus <> 'Out of Scope' 
)fi
inner join  ProcessPfmr pp1 on pp1.`name` = fi.ProcessName and pp1.`description` = fi.ProcessDescription and pp1.customerid = 541
left join ProcessPfmr pp2  on pp2.`name` = fi.PredecessorName and pp2.`description` = fi.PredecessorDescription  and pp2.customerid = 541; #692

*/

select * from MyProcessesPfmr;

 
## Adding further features of [ MyProcessesFailurePfmr]
/* Feature: Once a user save the processDetail in MyProcesses [ MyProcesses > Edit Process ] then every active FailureModePfmr 
are associated to that Myprocess within that customer Space. Therefore, in order to cater this feature following insert statements are being executed
*/

/*
#This query has been executed mistakenly which is referring to ProcessPfmr i.e. wrong and it should be pointing to MyProcessesPfmr instead
insert into MyProcessesFailurePfmr
select null as id,  fmp.id failureModePfmrId, p.id as myProcessId  
From ProcessPfmr p 
inner join FailureModePfmr fmp 
on p.customerId= fmp.customerId  where p.customerId= 541 ; # 692 (MyProcesses) * 1177 (FailureModePfmr)

#Following update is to cover the mistakes made over above query.

UPDATE MyProcessesFailurePfmr fp INNER JOIN 
(SELECT f.id AS MyProcessesFailurePfmrId, f.myProcessId AS MyProcessesFailurePfmrMyProcessId, mp.id AS MyProcessId, mp.processId FROM MyProcessesFailurePfmr f INNER JOIN MyProcessesPfmr mp ON mp.processId = f.myProcessId WHERE mp.CustomerId = 541)x 
ON fp.id = x.MyProcessesFailurePfmrId
SET fp.myProcessId =  x.myProcessId;

*/


## Adding further attributes of [ MyProcesses > Risk Worsening Factors]
SELECT distinct Seasonality,DependanceToHumanKnowHow,DiscontinuedProcess,ThirdPartyDependent   FROM etl.`kbl.fileimport_processtab` where  
WorkflowStatus is not null  and WorkflowStatus <> 'Out of Scope';

SELECT distinct ProcessName,ProcessDescription, Seasonality,DependanceToHumanKnowHow,DiscontinuedProcess,ThirdPartyDependent  FROM etl.`kbl.fileimport_processtab` where  
WorkflowStatus is not null  and WorkflowStatus <> 'Out of Scope';

/*
insert into MyProcessesRiskWorseningPfmr
select null as id, mp.id as MyProcessId, null as thirdPartyId, 
case when fi.Seasonality= 1 then true else false end as seasonility,
case when fi.DependanceToHumanKnowHow= 1 then true else false end as dependencyOnHumanKnowledge,
case when fi.DiscontinuedProcess= 1 then true else false end as discontinuedProcess,
17212 createdBy,current_timestamp() createdOn, 17212 modifiedBy, current_timestamp() modifiedOn, 541 customerId
 
from MyProcessesPfmr mp inner join ProcessPfmr p   on p.id = mp.processId and p.CustomerId = mp.CustomerId 
inner join etl.`kbl.fileimport_processtab` fi on fi.ProcessName =p.Name and fi.ProcessDescription = p.description 
where mp.customerid= 541;
*/


