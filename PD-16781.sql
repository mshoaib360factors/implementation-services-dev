Select
  rroa.modifiedDate,	rroa.inherentLikelihood, rroa.inherentImpact, rroa.inherentRiskRating, rroa.residualLikelihood, rroa.residualImpact, rroa.residualRiskRating
, rroa.treatedRating,	rroa.currentRiskRating, rroa.inherentMonetaryImpact, rroa.residualMonetaryImpact, rroa.currentMonetaryImpact, rroa.currentLikelihood
, rroa.currentImpact, rroa.predictedRisk

from riskregisteroverallanalysis rroa
inner join Customer c on c.id = rroa.customerId and c.customerCode = 'FFN' 
inner join riskregister rr on rr.id = rroa.riskId
inner join riskregisteritem rri on rri.id = rr.riskregisterItemId
where (rri.IsOrphan = 1 and rri.IsDeleted = 1) or rr.`status` = 'Inactive';




update riskregisteroverallanalysis rroa
inner join Customer c on c.id = rroa.customerId and c.customerCode = 'FFN' 
inner join riskregister rr on rr.id = rroa.riskId
inner join riskregisteritem rri on rri.id = rr.riskregisterItemId

set
  
	rroa.modifiedDate	= CURRENT_TIMESTAMP()
	
,	rroa.inherentLikelihood = NULL
, rroa.inherentImpact = NULL
, rroa.inherentRiskRating = NULL

, rroa.residualLikelihood = NULL
, rroa.residualImpact = NULL
, rroa.residualRiskRating= NULL

, rroa.treatedRating= NULL

,	rroa.currentRiskRating= -1

, rroa.inherentMonetaryImpact= NULL
, rroa.residualMonetaryImpact = NULL
, rroa.currentMonetaryImpact = NULL

, rroa.currentLikelihood = NULL
, rroa.currentImpact = NULL

, rroa.predictedRisk = 0

where (rri.IsOrphan = 1 and rri.IsDeleted = 1) or rr.`status` = 'Inactive';
 