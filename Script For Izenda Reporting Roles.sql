INSERT IGNORE INTO `predict360`.`rolepermission`( `roleId`, `permissionId`, `createdBy`, `createdOn`, `modifiedBy`, `modifiedOn`)
select distinct x.roleId, x.permissionId, 1 as createdBy, CURRENT_TIMESTAMP() as createdOn,  1 as modifiedBy, CURRENT_TIMESTAMP() as modifiedOn   from 

(
	select r.id as roleId, p.id as permissionId, r.customerId
	from role r 
	left join 
	(
		select distinct roleid from facilityrole fr 
		inner join facility f on fr.facilityid = f.id 
		where f.isbu = 1 
	) BURoles
	on r.id = BURoles.roleid
	inner join customerreports cr on cr.customerId = r.customerId
	inner join (select id, replace(replace(code,'predict-izenda-reports-',''), 'predict-', '') as code  from permission where moduleId = 9 ) p
	on p.code = cr.reportName
	where r.id > 0 and (r.isSystemRole = 0 or r.isSystemRole is null) and (r.isTemplateRole = 0 or r.isTemplateRole is Null) and BURoles.RoleId is NULL 
 


	UNION ALL 
	 
	select r.id as roleId, p.id as permissionId,  r.customerId
	from role r 
	left join 
	(
		select distinct roleid from facilityrole fr 
		inner join facility f on fr.facilityid = f.id 
		where f.isbu = 1 
	) BURoles
	on r.id = BURoles.roleid
	cross join permission p on 1=1 and p.code in ('predict-reports', 'predict-adhoc-reports', 'predict-adhoc reports')
	where r.id > 0 and (r.isSystemRole = 0 or r.isSystemRole is null) and (r.isTemplateRole = 0 or r.isTemplateRole is Null) and BURoles.RoleId is NULL 
 
 
)x
order by  x.customerId, x.roleId, x.permissionId;