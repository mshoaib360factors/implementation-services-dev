drop temporary table if exists tempdb.t1;
create  temporary table tempdb.t1
select st.id  as Stid , ss.id as SSid, ssqb.id as SSQBid, sqb.id as SQBid , sq.id as SQid , sa.id as SAid FROM predict360.surveytemplates st 
inner join customer c on c.id = st.customerId 
left join surveysection ss on ss.surveyId = st.id
left join surveysectionsurveyquestionbank ssqb on ssqb.surveySectionId = ss.id
left join surveyquestionbank sqb on sqb.id = ssqb.surveyQuestionBankId
left join surveyquestion sq on sq.questionBankId = sqb.id
left join surveyanswer sa on sa.surveyQuestionId = sq.id
where  customerCode in ('RBANK', 'RBSB', 'LSB') and `type` in ('predict.survey.type.control.oea','predict.survey.type.control.design') ;
 

set SQL_SAFE_UPDATES = 0;

delete sa.* from surveyanswer  sa
inner join  tempdb.t1 t on t.SAid = sa.id;

delete sq.* from surveyquestion  sq
inner join  tempdb.t1 t on t.SQid = sq.id;

delete  ssqb  from surveysectionsurveyquestionbank  ssqb
inner join  (select  distinct SSQBid from tempdb.t1 ) t on t.SSQBid = ssqb.id;
 
delete qbsa  from questionbanksubjarea qbsa
inner join surveyquestionbank  sqb on sqb.id = qbsa.questionBankId
inner join  tempdb.t1 t on t.SQBid = sqb.id;

Delete qbcl   from surveyquestionbank  sqb 
inner join  tempdb.t1 t on t.SQBid = sqb.id
inner join questionbankcontentlibrary qbcl on qbcl.questionBankId = sqb.id ;
 
Delete sqb   from surveyquestionbank  sqb 
inner join  tempdb.t1 t on t.SQBid = sqb.id;

delete ss from surveysection ss
inner join  tempdb.t1 t on t.SSid = ss.id;

delete  stcl from surveytemplates st
inner join  tempdb.t1 t on t.Stid = st.id
inner join surveytemplatescontentlibrary stcl on stcl.SurveyTemplateId = st.id;

update  surveytemplates st
inner join  tempdb.t1 t on t.Stid = st.id
inner join riskregistercontrolitem rrci on rrci.operatingEffectivenessAssessmentId = st.id
set  rrci.operatingEffectivenessAssessmentId = NULL;


update surveytemplates st
inner join  tempdb.t1 t on t.Stid = st.id
inner join riskregistercontrolitem rrci on rrci.designAssessmentId = st.id
set  rrci.designAssessmentId = NULL;

delete st from surveytemplates st
inner join tempdb.t1 t on t.Stid = st.id;
 