/*******************************************
## Compliance AI => SourceId = 1
*******************************************/
set @source_customer = 'BYL',  @target_customer = 'BYLDEMO';
 
 
 
insert into predict360.feed_document (document_key, ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path, customer_key, dismiss, take_action, reason, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on)

SELECT 
replace(document_key,@source_customer, @target_customer ) as document_key,  ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path
, @target_customer  as customer_key, 0 as dismiss, 0 as take_action, NULL as reason
, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on
FROM predict360.feed_document where customer_key = @source_customer    order by imported_on ASC;


###########feed_agency
insert into predict360.feed_agency(ref_id, document_key, active, blacklisted, description, jurisdiction, name, parent_id, short_name, slug, times_cited,	type,	url)
select 
       fa.ref_id, replace(fa.document_key,@source_customer, @target_customer ) as document_key
     , fa.active, fa.blacklisted, fa.description, fa.jurisdiction, fa.`name`, fa.parent_id, fa.short_name, fa.slug, fa.times_cited,	fa.type, fa.url 

from predict360.feed_agency fa
inner join predict360.feed_document fd on fd.document_key = fa.document_key and fd.customer_key = @source_customer;

###########feed_cfr_part
insert into predict360.feed_cfr_part (document_key, cite)
select replace(fp.document_key,@source_customer, @target_customer ) as document_key, cite from predict360.feed_cfr_part fp
inner join predict360.feed_document fd on fd.document_key = fp.document_key and fd.customer_key = @source_customer;
 
 ##########feed_docket
 insert into predict360.feed_docket (document_key, docket_id)
 select replace(ft.document_key,@source_customer, @target_customer ) as document_key, ft.docket_id from predict360.feed_docket ft
 inner join predict360.feed_document fd on fd.document_key = ft.document_key and fd.customer_key = @source_customer;
 
  ##########feed_topic
	insert into  predict360.feed_topic (ref_id, document_key, judge_count, model_probability, `name`, positive_judgments)
	select 
			fto.ref_id, replace(fto.document_key,@source_customer, @target_customer ) as document_key
		, fto.judge_count, fto.model_probability, fto.`name`, fto.positive_judgments
	from predict360.feed_topic fto
	inner join predict360.feed_document fd on fd.document_key = fto.document_key and fd.customer_key = @source_customer;
	
 