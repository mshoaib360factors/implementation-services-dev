Drop VIEW `v_Category_Aggregated_Risk`;
CREATE VIEW `v_Category_Aggregated_Risk` AS
SELECT 
  `rri`.`BK_RiskRegisterItemId` AS `BK_RiskRegisterItemId`,
  `rri`.`PK_RiskRegisterItemId` AS `PK_RiskRegisterItemId`,
  `rri2`.`BK_RiskRegisterItemId` AS `BK_ParentRiskRegisterItemId`,
  `rri`.`Name` AS `Name`,
  `wr`.`StartDate` AS `EffectiveDate`,
  `wr`.`level` AS `level`,
  `wr`.`weightedInherentLikelihood` AS `weightedInherentLikelihood`,
  `wr`.`weightedInherentImpact` AS `weightedInherentImpact`,
  `wr`.`weightedInherentRiskRating` AS `weightedInherentRiskRating`,
  `wr`.`weightedResidualLikelihood` AS `weightedResidualLikelihood`,
  `wr`.`weightedResidualImpact` AS `weightedResidualImpact`,
  `wr`.`weightedResidualRiskRating` AS `weightedResidualRiskRating`,
  `wr`.`weightedCurrentRiskRating` AS `weightedCurrentRiskRating`,
  `wr`.`weightedPredictedRisk` AS `weightedPredictedRisk`,
  `wr`.`weightedInherentLikelihoodLabel` AS `weightedInherentLikelihoodLabel`,
  `wr`.`weightedInherentLikelihoodLabelColor` AS `weightedInherentLikelihoodLabelColor`,
  `wr`.`weightedInherentImpactLabel` AS `weightedInherentImpactLabel`,
  `wr`.`weightedInherentImpactLabelColor` AS `weightedInherentImpactLabelColor`,
  `wr`.`weightedInherentRiskRatingLabel` AS `weightedInherentRiskRatingLabel`,
  `wr`.`weightedInherentRiskRatingLabelColor` AS `weightedInherentRiskRatingLabelColor`,
  `wr`.`weightedResidualLikelihoodLabel` AS `weightedResidualLikelihoodLabel`,
  `wr`.`weightedResidualLikelihoodLabelColor` AS `weightedResidualLikelihoodLabelColor`,
  `wr`.`weightedResidualImpactLabel` AS `weightedResidualImpactLabel`,
  `wr`.`weightedResidualImpactLabelColor` AS `weightedResidualImpactLabelColor`,
  `wr`.`weightedResidualRiskRatingLabel` AS `weightedResidualRiskRatingLabel`,
  `wr`.`weightedResidualRiskRatingLabelColor` AS `weightedResidualRiskRatingLabelColor`,
  `wr`.`weightedCurrentRiskRatingLabel` AS `weightedCurrentRiskRatingLabel`,
  `wr`.`weightedCurrentRiskRatingLabelColor` AS `weightedCurrentRiskRatingLabelColor`,
  CONCAT('Q',
          CAST(QUARTER(`wr`.`StartDate`) AS CHAR CHARSET UTF8MB4)) AS `Quarter`,
  CAST(YEAR(`wr`.`StartDate`) AS CHAR CHARSET UTF8MB4) AS `Year`,
  CAST(MONTH(`wr`.`StartDate`) AS CHAR CHARSET UTF8MB4) AS `Month`,
  CAST(WEEK(`wr`.`StartDate`, 0) AS CHAR CHARSET UTF8MB4) AS `Week`,
  `t`.`BK_TenantCode` AS `customerCode`
FROM
  edw.`dim_weighted_usr_risk_register_cat_lvl` `wr`
  INNER JOIN edw.`dim_risk_register_item` `rri` ON `wr`.`FK_RiskRegisterItemId` = `rri`.`PK_RiskRegisterItemId`
  INNER JOIN edw.`dim_tenant` `t` ON `t`.`PK_TenantId` = `wr`.`FK_TenantId`
  INNER JOIN edw.`dim_risk_register_item_mapping` `rrim` ON `rri`.`PK_RiskRegisterItemId` = `rrim`.`FK_RiskRegisterItemId`
  LEFT JOIN edw.`dim_risk_register_item` `rri2` ON `rrim`.`FK_ParenRiskRegsiterItemtId` = `rri2`.`PK_RiskRegisterItemId`
where wr.roleId = -2500;
#ORDER BY `rri`.`BK_RiskRegisterItemId` , `wr`.`StartDate` DESC;
