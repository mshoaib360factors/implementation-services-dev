Drop VIEW if exists `v_Category_Aggregated_Risk_By_Quarter`;
Create VIEW `v_Category_Aggregated_Risk_By_Quarter` AS  

Select CP.* 
, ifnull(P1.WeightedCurrentRiskRatingLabel, 'No Data'  ) as p1WeightedCurrentRiskRatingLabel
, ifnull(P1.WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p1WeightedCurrentRiskRatingLabelColor

, ifnull(P2.WeightedCurrentRiskRatingLabel, 'No Data'  ) as p2WeightedCurrentRiskRatingLabel
, ifnull(P2.WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p2WeightedCurrentRiskRatingLabelColor

, ifnull(P3.WeightedCurrentRiskRatingLabel, 'No Data'  ) as p3WeightedCurrentRiskRatingLabel
, ifnull(P3.WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p3WeightedCurrentRiskRatingLabelColor

, ifnull(P4.WeightedCurrentRiskRatingLabel, 'No Data'  ) as p4WeightedCurrentRiskRatingLabel
, ifnull(P4.WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p4WeightedCurrentRiskRatingLabelColor

, case when P1.WeightedCurrentRiskRating is null then '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
       when CP.WeightedCurrentRiskRating > P1.WeightedCurrentRiskRating * 1.00 then '/predict360/brands/predict360/assets/img/kri-icons/red-up.png' 
       when CP.WeightedCurrentRiskRating < P1.WeightedCurrentRiskRating * 1.0 then '/predict360/brands/predict360/assets/img/kri-icons/green-down.png'
       else  '/predict360/brands/predict360/assets/img/kri-icons/green-flat.png' 
  end as CurrentTrendPath

, case when P1.WeightedCurrentRiskRating is null then '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
       when P1.WeightedCurrentRiskRating > P2.WeightedCurrentRiskRating * 1.1 then '/predict360/brands/predict360/assets/img/kri-icons/red-up.png' 
       when P1.WeightedCurrentRiskRating < P2.WeightedCurrentRiskRating * 1.1 then '/predict360/brands/predict360/assets/img/kri-icons/green-down.png'
       else   '/predict360/brands/predict360/assets/img/kri-icons/green-flat.png' 
   end as P1TrendPath
, null as weightedCurrentRiskMin
, null as weightedCurrentRiskMax


from v_Category_Aggregated_Risk_By_Quarter_All CP
left join  v_Category_Aggregated_Risk_By_Quarter_All P1 on P1.Rnk = 2 and P1.customerCode = CP.customerCode and P1.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P1.level = CP.level 
left join  v_Category_Aggregated_Risk_By_Quarter_All P2 on P2.Rnk = 3 and P2.customerCode = CP.customerCode and P2.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P2.level = CP.level 
left join  v_Category_Aggregated_Risk_By_Quarter_All P3 on P3.Rnk = 4 and P3.customerCode = CP.customerCode and P3.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P3.level = CP.level  
left join  v_Category_Aggregated_Risk_By_Quarter_All P4 on P4.Rnk = 5 and P4.customerCode = CP.customerCode and P4.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P4.level = CP.level   
where CP.Rnk = 1 #and CP.customerCode = 'HTB' and CP.name = 'operational'  ;
 
 