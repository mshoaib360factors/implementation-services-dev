Drop VIEW `v_Category_Aggregated_Risk_By_Quarter_All`;
CREATE VIEW `v_Category_Aggregated_Risk_By_Quarter_All` AS
select 
  RANK() Over (partition by a.BK_RiskRegisterItemId, a.level order by a.Year desc, a.Quarter desc) as Rnk
, a.customerCode, a.BK_RiskRegisterItemId, a.BK_ParentRiskRegisterItemId, a.EffectiveDate, a.Name, a.level, a.Year, a.Quarter
# WeightedInherentRiskRating
, a.weightedInherentRiskRating, a.weightedInherentRiskRatingLabel, concat( '/predict360/brands/predict360/assets/img/kri-icons/', a.weightedInherentImpactLabelColor, '.png' ) AS weightedInherentImpactLabelColor

# WeightedResidualRiskRating
, a.weightedResidualRiskRating, a.weightedResidualRiskRatingLabel, concat( '/predict360/brands/predict360/assets/img/kri-icons/', a.weightedResidualRiskRatingLabelColor, '.png' ) AS `weightedResidualRiskRatingLabelColor`

# WeightedCurrentRiskRating
, a.weightedCurrentRiskRating, a.weightedCurrentRiskRatingLabel, concat( '/predict360/brands/predict360/assets/img/kri-icons/', a.weightedCurrentRiskRatingLabelColor, '.png' ) AS `weightedCurrentRiskRatingLabelColor`

# WeightedPredictedRisk
, a.weightedPredictedRisk
, CASE
	WHEN round(a.`weightedPredictedRisk`, 0) = 3 THEN
	'+++'                                       
	WHEN round(a.`weightedPredictedRisk`, 0) = 2 THEN
	'++'                                        
	WHEN round(a.`weightedPredictedRisk`, 0) = 1 THEN
	'+'                                     
	WHEN round(a.`weightedPredictedRisk`, 0) = -  1 THEN
	'-'                                            
	WHEN round(a.`weightedPredictedRisk`, 0) = -  2 THEN
	'--'                                           
	WHEN round(a.`weightedPredictedRisk`, 0) = -  3 THEN
	'---' ELSE '...'
 END  as PredictedRiskText

, CASE
	WHEN round(a.`weightedPredictedRisk`, 0 ) < - 2 THEN
	'/predict360/brands/predict360/assets/img/kri-icons/redindicator.png' 
	WHEN round(a.`weightedPredictedRisk`, 0 ) < - 1 THEN
	'/predict360/brands/predict360/assets/img/kri-icons/orangeindicator.png' 
	WHEN round(a.`weightedPredictedRisk`, 0 ) < 0 THEN
	'/predict360/brands/predict360/assets/img/kri-icons/yellowindicator.png' 
	WHEN round(a.`weightedPredictedRisk`, 0 ) > 2 THEN
	'/predict360/brands/predict360/assets/img/kri-icons/down-arrow3-green.png' 
	WHEN round(a.`weightedPredictedRisk`, 0 ) > 1 THEN
	'/predict360/brands/predict360/assets/img/kri-icons/down-arrow2-green.png' 
	WHEN round(a.`weightedPredictedRisk`, 0 ) > 0 THEN
	'/predict360/brands/predict360/assets/img/kri-icons/down-arrow1-green.png' 
    ELSE '/predict360/brands/predict360/assets/img/kri-icons/green-flat.png' 
 END as `weightPredictedRiskLabelColor`
#, a.p1weightedCurrentRiskRatingLabelColor, p1weightedCurrentRiskRatingLabel, p2weightedCurrentRiskRatingLabelColor, p2weightedCurrentRiskRatingLabel, p3weightedCurrentRiskRatingLabelColor, p3weightedCurrentRiskRatingLabel, p4weightedCurrentRiskRatingLabelColor, p4weightedCurrentRiskRatingLabel, CurrentTrendPath, P1TrendPath, customerCode, weightedCurrentRiskMin, weightedCurrentRiskMax
from 
 v_Category_Aggregated_Risk a
inner join 
(
select 
  BK_RiskRegisterItemId, BK_ParentRiskRegisterItemId,level,Year, Quarter, max(EffectiveDate) As MaxEffectiveDate
from  v_Category_Aggregated_Risk
Group By BK_RiskRegisterItemId, BK_ParentRiskRegisterItemId,level,Year, Quarter
)b on  
b.BK_RiskRegisterItemId = a.BK_RiskRegisterItemId and ifnull(b.BK_ParentRiskRegisterItemId,-1) = ifnull(a.BK_ParentRiskRegisterItemId,-1) and b.level = a.level and b.Year = a.Year and b.Quarter = a.Quarter 
and b.MaxEffectiveDate = a.EffectiveDate 
;



