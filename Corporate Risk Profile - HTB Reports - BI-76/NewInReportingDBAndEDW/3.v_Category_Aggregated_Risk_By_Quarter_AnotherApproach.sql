Drop VIEW reporting.`v_Category_Aggregated_Risk_By_Quarter_1`;
CREATE VIEW reporting.`v_Category_Aggregated_Risk_By_Quarter_1` AS  
Select CP.* 
, ifnull(p1WeightedCurrentRiskRatingLabel, 'No Data'  ) as p1WeightedCurrentRiskRatingLabel
, ifnull(p1WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p1WeightedCurrentRiskRatingLabelColor

, ifnull(p2WeightedCurrentRiskRatingLabel, 'No Data'  ) as p2WeightedCurrentRiskRatingLabel
, ifnull(p2WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p2WeightedCurrentRiskRatingLabelColor

, ifnull(p3WeightedCurrentRiskRatingLabel, 'No Data'  ) as p3WeightedCurrentRiskRatingLabel
, ifnull(p3WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p3WeightedCurrentRiskRatingLabelColor

, ifnull(p4WeightedCurrentRiskRatingLabel, 'No Data'  ) as p4WeightedCurrentRiskRatingLabel
, ifnull(p4WeightedCurrentRiskRatingLabelColor, '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' ) as p4WeightedCurrentRiskRatingLabelColor

, case when P1WeightedCurrentRiskRating is null then '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
       when WeightedCurrentRiskRating > P1WeightedCurrentRiskRating * 1.00 then '/predict360/brands/predict360/assets/img/kri-icons/red-up.png' 
       when WeightedCurrentRiskRating < P1WeightedCurrentRiskRating * 1.0 then '/predict360/brands/predict360/assets/img/kri-icons/green-down.png'
       else  '/predict360/brands/predict360/assets/img/kri-icons/green-flat.png' 
  end as CurrentTrendPath

, case when P1WeightedCurrentRiskRating is null then '/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
       when P1WeightedCurrentRiskRating > P2WeightedCurrentRiskRating * 1.1 then '/predict360/brands/predict360/assets/img/kri-icons/red-up.png' 
       when P1WeightedCurrentRiskRating < P2WeightedCurrentRiskRating * 1.1 then '/predict360/brands/predict360/assets/img/kri-icons/green-down.png'
       else   '/predict360/brands/predict360/assets/img/kri-icons/green-flat.png' 
   end as P1TrendPath    
from
(
 Select 
  customerCode, BK_RiskRegisterItemId, BK_ParentRiskRegisterItemId, EffectiveDate, Name, level, Year, Quarter, 
  weightedInherentRiskRating, weightedInherentRiskRatingLabel, weightedInherentImpactLabelColor, weightedResidualRiskRating, weightedResidualRiskRatingLabel, weightedResidualRiskRatingLabelColor, WeightedCurrentRiskRating, WeightedCurrentRiskRatingLabel, WeightedCurrentRiskRatingLabelColor, weightedPredictedRisk, PredictedRiskText, weightPredictedRiskLabelColor
  
from v_Category_Aggregated_Risk_By_Quarter_All
where Rnk = 1
#and customerCode = 'HTB' and name = 'operational'  
)CP

left join 
(
 Select 
  customerCode, BK_RiskRegisterItemId, level, WeightedCurrentRiskRating as P1WeightedCurrentRiskRating, WeightedCurrentRiskRatingLabel as p1WeightedCurrentRiskRatingLabel , WeightedCurrentRiskRatingLabelColor as p1WeightedCurrentRiskRatingLabelColor
 from v_Category_Aggregated_Risk_By_Quarter_All
 where Rnk = 2
)P1
on  P1.customerCode = CP.customerCode and P1.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P1.level = CP.level 

left join 
(
 Select 
  customerCode, BK_RiskRegisterItemId, level, WeightedCurrentRiskRating as P2WeightedCurrentRiskRating, WeightedCurrentRiskRatingLabel as p2WeightedCurrentRiskRatingLabel , WeightedCurrentRiskRatingLabelColor as p2WeightedCurrentRiskRatingLabelColor
 from v_Category_Aggregated_Risk_By_Quarter_All
 where Rnk = 3
)P2
on  P2.customerCode = CP.customerCode and P2.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P2.level = CP.level 

left join 
(
 Select 
  customerCode, BK_RiskRegisterItemId, level, WeightedCurrentRiskRating as P3WeightedCurrentRiskRating, WeightedCurrentRiskRatingLabel as p3WeightedCurrentRiskRatingLabel , WeightedCurrentRiskRatingLabelColor as p3WeightedCurrentRiskRatingLabelColor
 from v_Category_Aggregated_Risk_By_Quarter_All
 where Rnk = 4
)P3
on  P3.customerCode = CP.customerCode and P3.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P3.level = CP.level 

left join 
(
 Select 
   customerCode, BK_RiskRegisterItemId, level, WeightedCurrentRiskRating as P4WeightedCurrentRiskRating, WeightedCurrentRiskRatingLabel as p4WeightedCurrentRiskRatingLabel , WeightedCurrentRiskRatingLabelColor as p4WeightedCurrentRiskRatingLabelColor
 from v_Category_Aggregated_Risk_By_Quarter_All
 where Rnk = 5
)P4
on  P4.customerCode = CP.customerCode and P4.BK_RiskRegisterItemId = CP.BK_RiskRegisterItemId and P4.level = CP.level 
;