/********************************
v_Category_Aggregated_Risk
*********************************/
SELECT
rrim.*,
`rri`.`BK_RiskRegisterItemId` AS `BK_RiskRegisterItemId`,
`rri`.`PK_RiskRegisterItemId` AS `PK_RiskRegisterItemId`,
#`rri2`.`BK_RiskRegisterItemId` AS `BK_ParentRiskRegisterItemId`,
`rri`.`Name` AS `Name`,
`wr`.`StartDate` AS `EffectiveDate`,
`wr`.`level` AS `level`,
`wr`.`weightedInherentLikelihood` AS `weightedInherentLikelihood`,
`wr`.`weightedInherentImpact` AS `weightedInherentImpact`,
`wr`.`weightedInherentRiskRating` AS `weightedInherentRiskRating`,
`wr`.`weightedResidualLikelihood` AS `weightedResidualLikelihood`,
`wr`.`weightedResidualImpact` AS `weightedResidualImpact`,
`wr`.`weightedResidualRiskRating` AS `weightedResidualRiskRating`,
`wr`.`weightedCurrentRiskRating` AS `weightedCurrentRiskRating`,
`wr`.`weightedPredictedRisk` AS `weightedPredictedRisk`,
`wr`.`weightedInherentLikelihoodLabel` AS `weightedInherentLikelihoodLabel`,
`wr`.`weightedInherentLikelihoodLabelColor` AS `weightedInherentLikelihoodLabelColor`,
`wr`.`weightedInherentImpactLabel` AS `weightedInherentImpactLabel`,
`wr`.`weightedInherentImpactLabelColor` AS `weightedInherentImpactLabelColor`,
`wr`.`weightedInherentRiskRatingLabel` AS `weightedInherentRiskRatingLabel`,
`wr`.`weightedInherentRiskRatingLabelColor` AS `weightedInherentRiskRatingLabelColor`,
`wr`.`weightedResidualLikelihoodLabel` AS `weightedResidualLikelihoodLabel`,
`wr`.`weightedResidualLikelihoodLabelColor` AS `weightedResidualLikelihoodLabelColor`,
`wr`.`weightedResidualImpactLabel` AS `weightedResidualImpactLabel`,
`wr`.`weightedResidualImpactLabelColor` AS `weightedResidualImpactLabelColor`,
`wr`.`weightedResidualRiskRatingLabel` AS `weightedResidualRiskRatingLabel`,
`wr`.`weightedResidualRiskRatingLabelColor` AS `weightedResidualRiskRatingLabelColor`,
`wr`.`weightedCurrentRiskRatingLabel` AS `weightedCurrentRiskRatingLabel`,
`wr`.`weightedCurrentRiskRatingLabelColor` AS `weightedCurrentRiskRatingLabelColor`,
concat( 'Q', cast( QUARTER ( `wr`.`StartDate` ) AS CHAR charset utf8mb4 ) ) AS `Quarter`,
cast( YEAR ( `wr`.`StartDate` ) AS CHAR charset utf8mb4 ) AS `Year`,
cast( MONTH ( `wr`.`StartDate` ) AS CHAR charset utf8mb4 ) AS `Month`,
cast( WEEK ( `wr`.`StartDate`, 0 ) AS CHAR charset utf8mb4 ) AS `Week`,
`t`.`BK_TenantCode` AS `customerCode` 
 
from dim_weighted_usr_risk_register_cat_lvl wr
inner join dim_risk_register_item rri on rri.PK_RiskRegisterItemId = wr.FK_RiskRegisterItemId
inner join dim_tenant t on t.PK_TenantId = wr.FK_TenantId
inner join `dim_risk_register_item_mapping` `rrim` ON  `rri`.`PK_RiskRegisterItemId` = `rrim`.`FK_RiskRegisterItemId` 
#inner join `dim_risk_register_item` `rri2` ON  ifnull(`rrim`.`FK_ParenRiskRegsiterItemtId`,-1) = `rri2`.`PK_RiskRegisterItemId` 
where wr.FK_TenantId = 581;
 

 
select * from dim_tenant where BK_TenantCode = 'HTB';
select * from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 and name = 'Operational';
select distinct BK_WeightedUsrRiskRegisterCatLevelId from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581;
 
 
 select * from v_Category_Aggregated_Risk where customerCode = 'HTB' and name = 'operational';
 select * from v_Category_Aggregated_Risk_By_Quarter_All where customerCode = 'HTB' and name = 'operational';
 
 
 
/****************************************
v_Category_Aggregated_Risk_By_Quarter_All
*****************************************/
 # select * from v_Category_Aggregated_Risk_By_Quarter_All;
 
 
 SELECT DISTINCT
`rx`.`BK_RiskRegisterItemId` AS `BK_RiskRegisterItemId`,
first_value ( `rx`.`BK_ParentRiskRegisterItemId` ) OVER `w` AS `BK_ParentRiskRegisterItemId`,
first_value ( `rx`.`EffectiveDate` ) OVER `w` AS `EffectiveDate`,
`rx`.`Name` AS `Name`,
`rx`.`level` AS `level`,
`rx`.`Year` AS `Year`,
`rx`.`Quarter` AS `Quarter`,
first_value ( `rx`.`weightedInherentRiskRating` ) OVER `w` AS `weightedInherentRiskRating`,
first_value ( `rx`.`weightedInherentRiskRatingLabel` ) OVER `w` AS `weightedInherentRiskRatingLabel`,
concat( '/predict360/brands/predict360/assets/img/kri-icons/', first_value ( `rx`.`weightedInherentImpactLabelColor` ) OVER `w`, '.png' ) AS `weightedInherentImpactLabelColor`,
first_value ( `rx`.`weightedResidualRiskRating` ) OVER `w` AS `weightedResidualRiskRating`,
first_value ( `rx`.`weightedResidualRiskRatingLabel` ) OVER `w` AS `weightedResidualRiskRatingLabel`,
concat( '/predict360/brands/predict360/assets/img/kri-icons/', first_value ( `rx`.`weightedResidualRiskRatingLabelColor` ) OVER `w`, '.png' ) AS `weightedResidualRiskRatingLabelColor`,
first_value ( `rx`.`weightedCurrentRiskRating` ) OVER `w` AS `weightedCurrentRiskRating`,
first_value ( `rx`.`weightedCurrentRiskRatingLabel` ) OVER `w` AS `weightedCurrentRiskRatingLabel`,
first_value ( `rx`.`weightedPredictedRisk` ) OVER `w` AS `weightedPredictedRisk`,
(
CASE
	
	WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) = 3 ) THEN
	'+++' 
	WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) = 2 ) THEN
	'++' 
	WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) = 1 ) THEN
	'+' 
	WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) = - ( 1 ) ) THEN
	'-' 
	WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) = - ( 2 ) ) THEN
	'--' 
	WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) = - ( 3 ) ) THEN
	'---' ELSE 'Expected' 
END 
	) AS `PredictedRiskText`,
	(
	CASE
			
			WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) < - ( 2 ) ) THEN
			'/predict360/brands/predict360/assets/img/kri-icons/redindicator.png' 
			WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) < - ( 1 ) ) THEN
			'/predict360/brands/predict360/assets/img/kri-icons/orangeindicator.png' 
			WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) < 0 ) THEN
			'/predict360/brands/predict360/assets/img/kri-icons/yellowindicator.png' 
			WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) > 2 ) THEN
			'/predict360/brands/predict360/assets/img/kri-icons/down-arrow3-green.png' 
			WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) > 1 ) THEN
			'/predict360/brands/predict360/assets/img/kri-icons/down-arrow2-green.png' 
			WHEN ( round( first_value ( `rx`.`weightedPredictedRisk` ) OVER `w`, 0 ) > 0 ) THEN
			'/predict360/brands/predict360/assets/img/kri-icons/down-arrow1-green.png' ELSE '/predict360/brands/predict360/assets/img/kri-icons/green-flat.png' 
		END 
		) AS `weightPredictedRiskLabelColor`,
		concat( '/predict360/brands/predict360/assets/img/kri-icons/', first_value ( `rx`.`weightedCurrentRiskRatingLabelColor` ) OVER `w`, '.png' ) AS `weightedCurrentRiskRatingLabelColor`,
		ifnull(
			concat(
				'/predict360/brands/predict360/assets/img/kri-icons/',
				(
				SELECT
					max( `r`.`weightedCurrentRiskRatingLabelColor` ) 
				FROM
					`v_Category_Aggregated_Risk` `r` 
				WHERE
					( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
				GROUP BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
				ORDER BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
					LIMIT 1,
					1 
				),
				'.png' 
			),
			'/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
		) AS `p1weightedCurrentRiskRatingLabelColor`,
		ifnull(
			(
			SELECT
				max( `r`.`weightedCurrentRiskRatingLabel` ) 
			FROM
				`v_Category_Aggregated_Risk` `r` 
			WHERE
				( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
			GROUP BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
			ORDER BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
				LIMIT 1,
				1 
			),
			'No Data' 
		) AS `p1weightedCurrentRiskRatingLabel`,
		ifnull(
			concat(
				'/predict360/brands/predict360/assets/img/kri-icons/',
				(
				SELECT
					max( `r`.`weightedCurrentRiskRatingLabelColor` ) 
				FROM
					`v_Category_Aggregated_Risk` `r` 
				WHERE
					( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
				GROUP BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
				ORDER BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
					LIMIT 2,
					1 
				),
				'.png' 
			),
			'/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
		) AS `p2weightedCurrentRiskRatingLabelColor`,
		ifnull(
			(
			SELECT
				max( `r`.`weightedCurrentRiskRatingLabel` ) 
			FROM
				`v_Category_Aggregated_Risk` `r` 
			WHERE
				( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
			GROUP BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
			ORDER BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
				LIMIT 2,
				1 
			),
			'No Data' 
		) AS `p2weightedCurrentRiskRatingLabel`,
		ifnull(
			concat(
				'/predict360/brands/predict360/assets/img/kri-icons/',
				(
				SELECT
					max( `r`.`weightedCurrentRiskRatingLabelColor` ) 
				FROM
					`v_Category_Aggregated_Risk` `r` 
				WHERE
					( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
				GROUP BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
				ORDER BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
					LIMIT 3,
					1 
				),
				'.png' 
			),
			'/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
		) AS `p3weightedCurrentRiskRatingLabelColor`,
		ifnull(
			(
			SELECT
				max( `r`.`weightedCurrentRiskRatingLabel` ) 
			FROM
				`v_Category_Aggregated_Risk` `r` 
			WHERE
				( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
			GROUP BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
			ORDER BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
				LIMIT 3,
				1 
			),
			'No Data' 
		) AS `p3weightedCurrentRiskRatingLabel`,
		ifnull(
			concat(
				'/predict360/brands/predict360/assets/img/kri-icons/',
				(
				SELECT
					max( `r`.`weightedCurrentRiskRatingLabelColor` ) 
				FROM
					`v_Category_Aggregated_Risk` `r` 
				WHERE
					( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
				GROUP BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
				ORDER BY
					`r`.`BK_RiskRegisterItemId`,
					`r`.`Year`,
					`r`.`Quarter` 
					LIMIT 4,
					1 
				),
				'.png' 
			),
			'/predict360/brands/predict360/assets/img/kri-icons/x-green.png' 
		) AS `p4weightedCurrentRiskRatingLabelColor`,
		ifnull(
			(
			SELECT
				max( `r`.`weightedCurrentRiskRatingLabel` ) 
			FROM
				`v_Category_Aggregated_Risk` `r` 
			WHERE
				( `r`.`BK_RiskRegisterItemId` = `rx`.`BK_RiskRegisterItemId` ) 
			GROUP BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
			ORDER BY
				`r`.`BK_RiskRegisterItemId`,
				`r`.`Year`,
				`r`.`Quarter` 
				LIMIT 4,
				1 
			),
			'No Data' 
		) AS `p4weightedCurrentRiskRatingLabel`
 
FROM `v_Category_Aggregated_Risk` `rx` 
where customerCode = 'HTB' and `name` = 'operational'
window `w` AS (PARTITION BY `rx`.`BK_RiskRegisterItemId`, `rx`.`Year`, `rx`.`Quarter` ORDER BY `rx`.`EffectiveDate` DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
;
 
 
select * from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 and FK_RiskRegisterItemId = 1191 and startDate < '2020-04-01' order by startDate desc;
select * from dim_risk_register_item where PK_RiskRegisterItemId = 1191;
 
 
 
 
 select count(distinct FK_RiskRegisterItemId) from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 ; #275, 7
select count(1) from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581  and roleId = -2500 and enddate = '9999-01-01';
select * from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 ;
select * from dim_tenant where PK_TenantId = 581 ;

select * from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 ;


select * from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 and FK_RiskRegisterItemId = 1191 and startDate < '2020-04-01' order by startDate desc;
select * from dim_risk_register_item where PK_RiskRegisterItemId = 1191;
 
 
 

/****************************************
v_Category_Aggregated_Risk_By_Quarter
*****************************************/
 
 
 
 select * from dim_tenant where BK_TenantCode = 'HTB';
 select * from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581 and name = 'Operational';
 select distinct BK_WeightedUsrRiskRegisterCatLevelId from dim_weighted_usr_risk_register_cat_lvl where FK_TenantId = 581;
 
 

select * from edw.v_Category_Aggregated_Risk where customerCode = 'HTB' and name = 'operational';
select * from v_Category_Aggregated_Risk_By_Quarter_All where customerCode = 'HTB' and name = 'operational';
select * from v_Category_Aggregated_Risk_By_Quarter where customerCode = 'HTB' and name = 'operational'; 
 
 
 
 
 
 
 
 
 
 
 

