select * from Customer where customerCode = 'CBTEST' => 2;

select * from Customer where customerCode = 'KBTEST' => 1; -> Compliance AI
/*******************************************
## Reg-Track => SourceId = 2
*******************************************/
set @source_customer = 'FFN',  @target_customer = 'CBTEST';


insert into predict360.feed_document (document_key, ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path, customer_key, dismiss, take_action, reason, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on)

SELECT 
replace(document_key,@source_customer, @target_customer ) as document_key,  ref_id, category, created_at, full_path, jurisdiction, official_id, provenance, publication_date, pdf_url, summary_text, title, updated_at, web_url, xml_path
, @target_customer  as customer_key, 0 as dismiss, 0 as take_action, NULL as reason
, clean_citations, named_regulation_ids, regulator_code, subj_area, topic, source_id, imported_on
FROM feed.feed_document where customer_key = @source_customer    order by imported_on ASC;




----- feed_doc_report

drop TEMPORARY table if exists tempdb.t2;
Create TEMPORARY table  tempdb.t2

select replace(doc_key, @source_customer, @target_customer) as doc_key, report_id from feed.feed_doc_report 
where doc_key in (
SELECT  document_key FROM predict360.feed_document where customer_key = @source_customer  
)  order by report_id  ;


drop TEMPORARY table if exists tempdb.t3;
Create TEMPORARY table  tempdb.t3
select * from tempdb.t2 where doc_key not in (select doc_key from predict360.feed_doc_report);


insert into predict360.feed_doc_report (doc_key, report_id)
select * from tempdb.t3;
 
----- feed_doc_region

drop TEMPORARY table if exists tempdb.t4;
Create TEMPORARY table  tempdb.t4
select replace(doc_key, @source_customer, @target_customer) as doc_key, region_id from  feed.feed_doc_region 
where doc_key in (
SELECT  document_key FROM feed.feed_document where customer_key = @source_customer  
)  order by id  ;



drop TEMPORARY table if exists tempdb.t5;
Create TEMPORARY table  tempdb.t5
select * from tempdb.t4 where doc_key not in (select doc_key from predict360.feed_doc_region);

insert into predict360.feed_doc_region (doc_key, region_id)
select * from tempdb.t5;


----- feed_doc_product
drop TEMPORARY table if exists tempdb.t6;
Create TEMPORARY table  tempdb.t6
select replace(doc_key, @source_customer, @target_customer) as doc_key, product_id from feed.feed_doc_product 
where doc_key in (
SELECT  document_key FROM feed.feed_document where customer_key = @source_customer  
)  order by id  ;

 
drop TEMPORARY table if exists tempdb.t7;
Create TEMPORARY table  tempdb.t7
select t.* from tempdb.t6 t
left join predict360.feed_doc_product p on p.doc_key = t.doc_key and p.product_id = t.product_id
where p.doc_key is null;


insert into predict360.feed_doc_product (doc_key, product_id)
select * from tempdb.t7;
 
 
 
----- feed_doc_country
drop TEMPORARY table if exists tempdb.t8;
Create TEMPORARY table  tempdb.t8
select replace(doc_key, @source_customer, @target_customer) as doc_key, country_id from feed.feed_doc_country 
where doc_key in (
SELECT  document_key FROM feed.feed_document where customer_key = @source_customer ) 
order by id  ;


drop TEMPORARY table if exists tempdb.t9;
Create TEMPORARY table  tempdb.t9
select t.* from tempdb.t8 t
left join feed.feed_doc_country p on p.doc_key = t.doc_key and p.country_id = t.country_id
where p.doc_key is null;


insert into predict360.feed_doc_country (doc_key, country_id)
select * from tempdb.t9;
 
 
----- feed_doc_function
drop TEMPORARY table if exists tempdb.t10;
Create TEMPORARY table  tempdb.t10
select replace(doc_key, @source_customer, @target_customer) as doc_key, function_id from feed.feed_doc_function 
where doc_key in (
SELECT  document_key FROM feed.feed_document where customer_key = @source_customer 
)  order by function_id  ;

 

drop TEMPORARY table if exists tempdb.t11;
Create TEMPORARY table  tempdb.t11
select t.* from tempdb.t10 t
left join feed.feed_doc_function f on f.doc_key = t.doc_key and f.function_id = t.function_id
where f.doc_key is null;



insert into predict360.feed_doc_function (doc_key, function_id)
select * from tempdb.t11;
 
 
----- feed_doc_regulator 

drop TEMPORARY table if exists tempdb.t12;
Create TEMPORARY table  tempdb.t12
select replace(doc_key, @source_customer, @target_customer) as doc_key, regulator_id from feed.feed_doc_regulator 
where doc_key in (
SELECT  document_key FROM feed.feed_document where customer_key = @source_customer
)  order by id  ;
 

drop TEMPORARY table if exists tempdb.t13;
Create TEMPORARY table  tempdb.t13
select t.* from tempdb.t12 t
left join feed.feed_doc_regulator r on r.doc_key = t.doc_key and r.regulator_id = t.regulator_id
where r.doc_key is null;


insert into predict360.feed_doc_regulator (doc_key, regulator_id)
select * from tempdb.t13;
 
 ----- feed_doc_entity_type 
drop TEMPORARY table if exists tempdb.t14;
Create TEMPORARY table  tempdb.t14
select replace(doc_key, @source_customer, @target_customer) as doc_key, entity_type_id from feed.feed_doc_entity_type 
where doc_key in (
SELECT  document_key FROM feed.feed_document where customer_key = @source_customer   
)  order by id  ;

drop TEMPORARY table if exists tempdb.t15;
Create TEMPORARY table  tempdb.t15 
select t.* from tempdb.t14 t
left join feed.feed_doc_entity_type e on e.doc_key = t.doc_key and e.entity_type_id = t.entity_type_id
where e.doc_key is null;


insert into predict360.feed_doc_entity_type (doc_key, entity_type_id)
select * from tempdb.t15;
