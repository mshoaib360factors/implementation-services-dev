Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.id, replace(replace(cfv.TEXTVALUE ,' ', ''),',',', ') as CorrectValue
FROM customfieldvalue cfv
inner JOIN jiraissue ji ON issue=ji.id 
WHERE project=24207 AND textvalue LIKE '%, %' and  CUSTOMFIELD in (20600,20601,20602);

 
update customfieldvalue cfv
inner join tempdb.t1 t on t.id = cfv.id
set TEXTVALUE = CorrectValue;



Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.id, replace(replace( replace(cfv.TEXTVALUE,'	','') ,' ', ''),',',', ') as CorrectValue
FROM customfieldvalue cfv
inner JOIN jiraissue ji ON issue=ji.id 
WHERE project=24207  AND textvalue LIKE '%, %' AND customfield IN (18606,20600,18614,20601,18618,18803,18804);

update customfieldvalue cfv
inner join tempdb.t1 t on t.id = cfv.id
set TEXTVALUE = CorrectValue;


Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.id, replace(cfv.TEXTVALUE ,', ',',') as CorrectValue
FROM customfieldvalue cfv
inner JOIN jiraissue ji ON issue=ji.id 
WHERE project=24207 AND textvalue LIKE '%, %' and  CUSTOMFIELD in (20602);

update customfieldvalue cfv
inner join tempdb.t1 t on t.id = cfv.id
set TEXTVALUE = CorrectValue;

----
Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.id, replace(cfv.TEXTVALUE ,', ',',') as CorrectValue
FROM customfieldvalue cfv
inner JOIN jiraissue ji ON issue=ji.id 
WHERE project=24207 AND textvalue LIKE '%, %' AND customfield IN (18606,20600,18614,20601,18618,18803,18804);

update customfieldvalue cfv
inner join tempdb.t1 t on t.id = cfv.id
set TEXTVALUE = CorrectValue;



-------------------Preparing EDW Queries -------------------

Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
select PK_CustomfieldValueId,replace(replace(cfv.TEXTVALUE ,' ', ''),',',', ') as CorrectValue  from dim_customfield_value cfv
inner join dim_customfield cf on cf.PK_CustomfieldId = cfv.FK_CustomfieldId
inner join dim_jira_issue ji on ji.PK_IssueId = cfv.FK_JiraIssueId
where cfv.textvalue LIKE '%, %' and BK_CustomfieldId in (20600,20601,20602) and Issuekey like 'FHB%'

update dim_customfield_value cfv
inner join tempdb.t1 t on t.PK_CustomfieldValueId = cfv.PK_CustomfieldValueId
set TEXTVALUE = CorrectValue;


Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.PK_CustomfieldValueId, replace(replace( replace(cfv.TEXTVALUE,'	','') ,' ', ''),',',', ') as CorrectValue
from dim_customfield_value cfv
inner join dim_customfield cf on cf.PK_CustomfieldId = cfv.FK_CustomfieldId
inner join dim_jira_issue ji on ji.PK_IssueId = cfv.FK_JiraIssueId
WHERE textvalue LIKE '%, %' AND BK_CustomfieldId IN (18606,20600,18614,20601,18618,18803,18804) and Issuekey like 'FHB%';

update dim_customfield_value cfv
inner join tempdb.t1 t on t.PK_CustomfieldValueId = cfv.PK_CustomfieldValueId
set TEXTVALUE = CorrectValue;


Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.PK_CustomfieldValueId, replace(cfv.TEXTVALUE ,', ',',') as CorrectValue
from dim_customfield_value cfv
inner join dim_customfield cf on cf.PK_CustomfieldId = cfv.FK_CustomfieldId
inner join dim_jira_issue ji on ji.PK_IssueId = cfv.FK_JiraIssueId
WHERE  Issuekey like 'FHB%' AND textvalue LIKE '%, %' and  BK_CustomfieldId in (20602);

update dim_customfield_value cfv
inner join tempdb.t1 t on t.PK_CustomfieldValueId = cfv.PK_CustomfieldValueId
set TEXTVALUE = CorrectValue;


Drop TEMPORARY table if exists tempdb.t1;
create TEMPORARY table tempdb.t1
SELECT cfv.PK_CustomfieldValueId, replace(cfv.TEXTVALUE ,', ',',') as CorrectValue
from dim_customfield_value cfv
inner join dim_customfield cf on cf.PK_CustomfieldId = cfv.FK_CustomfieldId
inner join dim_jira_issue ji on ji.PK_IssueId = cfv.FK_JiraIssueId
WHERE  Issuekey like 'FHB%' AND textvalue LIKE '%, %' AND BK_CustomfieldId IN (18606,20600,18614,20601,18618,18803,18804);


update dim_customfield_value cfv
inner join tempdb.t1 t on t.PK_CustomfieldValueId = cfv.PK_CustomfieldValueId
set TEXTVALUE = CorrectValue;

 