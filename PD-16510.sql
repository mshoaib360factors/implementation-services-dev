Drop TEMPORARY table if exists tempdb.t1;
Create TEMPORARY table tempdb.t1
select 	distinct 
					rri.id as RiskRegisterItemId, rr.id as RiskRegisterId
				,	rroa.id as RiskRegisterOverallAnalysisId
				, rrci.id as RiskRegisterContInstId, rrni.id as RiskRegisterNegImpId 

from riskregisteritem rri
inner join processcategoryriskitem pcr on pcr.riskRegisterItemId = rri.id
left join riskregisteritemmapping rrim on rrim.riskRegisterItemId = rri.id
inner join riskregister rr on rr.riskRegisterItemId = rri.id
left join riskregisteroverallanalysis rroa on rroa.riskId = rr.id
left join riskregistercontrolinstance rrci on rrci.riskRegisterId = rr.id
left join riskregisternegativeimpact rrni on rrni.riskRegisterId = rr.id
where rri.isLeaf = 1 and rrim.id is null;




select count(distinct RiskRegisterItemId), count(distinct RiskRegisterId), count(distinct RiskRegisterOverallAnalysisId)
			,count(distinct RiskRegisterContInstId),count(distinct RiskRegisterNegImpId)
from tempdb.t1


Delete from riskregisternegativeimpact where id in (
select distinct RiskRegisterNegImpId from tempdb.t1 where RiskRegisterNegImpId is not null );

delete from riskregistercontrolinstance where id in (
select distinct RiskRegisterContInstId from tempdb.t1 where RiskRegisterContInstId is not null);


delete from RiskRegisterOverallAnalysis where id in (
select distinct RiskRegisterOverallAnalysisId from tempdb.t1 where RiskRegisterOverallAnalysisId is not null );

#For EDW
select GROUP_CONCAT(RiskRegisterOverallAnalysisId) from
(select distinct RiskRegisterOverallAnalysisId from tempdb.t1 where RiskRegisterOverallAnalysisId is not null)x;

delete from riskregisterresponsibleperson where riskRegisterId in (select distinct RiskRegisterId from tempdb.t1);
 
delete from riskregistersite where riskRegisterId in (select distinct RiskRegisterId from tempdb.t1);

delete from riskregisterrequirement where riskRegisterId in (select distinct RiskRegisterId from tempdb.t1);

delete from riskregistersubjarea where riskRegisterId in (select distinct RiskRegisterId from tempdb.t1);

delete from riskregister where id in (select distinct RiskRegisterId from tempdb.t1 );

#For EDW
select GROUP_CONCAT(RiskRegisterId) from (select distinct RiskRegisterId from tempdb.t1)x;


delete from riskregisteritemapplicablity where riskRegisterItemId in (select distinct riskRegisterItemId from tempdb.t1);


drop TEMPORARY table if exists tempdb.t2;
Create TEMPORARY table tempdb.t2
select customerId, buFacilityId, riskRegisterItemId, applicablity, count(1) as Count_, min(id) as MinId  from riskregisteritemapplicablity 
group by customerId, buFacilityId, riskRegisterItemId , applicablity
HAVING count(1) > 1;




Delete  rria.* from riskregisteritemapplicablity rria
inner join tempdb.t2 t 
on t.customerId = rria.customerId and ifnull(t.buFacilityId, -999) = ifnull(rria.buFacilityId, -999) and t.riskRegisterItemId = rria.riskRegisterItemId
and t.applicablity = rria.applicablity and rria.id <> t.MinId ;

 



drop TEMPORARY table if exists tempdb.t3;
Create TEMPORARY table tempdb.t3
select customerId, buFacilityId, riskRegisterItemId,  count(1) as Count_  from riskregisteritemapplicablity 
group by customerId, buFacilityId, riskRegisterItemId 
HAVING count(1) > 1;

select distinct customerId,	buFacilityId, riskRegisterItemId from  tempdb.t3 t order by 1,3,2;

select rria.* from riskregisteritemapplicablity rria
inner join tempdb.t3 t 
on t.customerId = rria.customerId and ifnull(t.buFacilityId, -999) = ifnull(rria.buFacilityId, -999) and t.riskRegisterItemId = rria.riskRegisterItemId
where rria.applicablity <> 'Applicable'
order by customerId,riskRegisterItemId, buFacilityId ;


delete rria.* from riskregisteritemapplicablity rria
inner join tempdb.t3 t 
on t.customerId = rria.customerId and ifnull(t.buFacilityId, -999) = ifnull(rria.buFacilityId, -999) and t.riskRegisterItemId = rria.riskRegisterItemId
where rria.applicablity <> 'Applicable';


##########################################################################################
 ############################## On EDW ##############################
##########################################################################################


 select * from dim_risk_reg_control_ins_negative_imp limit 10; # Truncate Insert

delete from dim_risk_register_overall_analysis where BK_RiskRegisterOverallAnalysisId in
(6872,6873,6874,6875,6876,6877,6878,6858,6859,6860,6861,6862,6863,6864,6950,6951,6952,6953,6954,6955,6956,6957,6958,6959,6960,6961,6962,6963,6964,6965,6966,6967,6968,6969,6970,6971,6972,6973,6974,6975,6976,6977,6978,6979,6980,6981,6982,6983,6984,6985,6986,6987,6988,6989,6990,6991,6992,6993,6994,6995,6996,6997,6998,6999,7000,7001,7002,7003,7004,7005,7006,7007,7008,7009,7010,7011,7012,7013,7014,7015,7016,7017,6949,7038,7733,7783,7793) ;


select * from dim_risk_register_responsible_person limit 10; # Truncate Insert
dim_risk_register_site # Truncate Insert
select * from dim_risk_register_req limit 10; # Truncate Insert

SET FOREIGN_KEY_CHECKS=0;
delete from dim_risk_register where   BK_RiskRegisterId in 
(
11171,11176,10833,10834,10835,10836,10837,10838,10839,10819,10820,10821,10822,10823,10824,10825,11172,11177,11173,11178,10912,10913,10914,10915,10916,10917,10918,10919,10920,10921,10922,10923,10924,10925,10926,10927,10928,10929,10930,10931,10932,10933,10934,10935,10936,10937,10938,10939,10940,10941,10942,10943,10944,10945,10946,10947,10948,10949,10950,10951,10952,10953,10954,10955,10956,10957,10958,10959,10960,10961,10962,10963,10964,10965,10966,10967,10968,10969,10970,10971,10972,10973,10974,10975,10976,10977,10978,10979,10911,11041,12655,13484,13552,14252,14255,14327,14336);
SET FOREIGN_KEY_CHECKS=1;

select distinct BatchId from dim_risk_register_applicablity limit 10; # Truncate Insert
