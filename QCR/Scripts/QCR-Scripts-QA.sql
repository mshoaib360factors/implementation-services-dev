/**************************************************************************
			RiskRegisterItem  & RiskRegisterItemMapping
***************************************************************************/

select * from predict360.Customer where customerCode = 'QCRIMP';
select * from predict360.PredictUser where customerId= 2653;

SELECT distinct trim(Mega_Process) as Mega_Process FROM etl.`qcr.riskdataimport090820` where Mega_Process is not null;
SELECT distinct trim(Mega_Process) as Mega_Process FROM etl.`qcr.riskdataimport090820` where Mega_Process is not null and trim(Mega_Process) <> '';


SELECT distinct trim(Mega_Process) as Mega_Process, trim(Process) as Process FROM etl.`qcr.riskdataimport090820` 
where Process is not null or trim(Process) <>  '';
#Mega_Process is not null and trim(Mega_Process) <> ''

Drop temporary table if exists tempdb.DerivingRiskDescription2;
create temporary table tempdb.DerivingRiskDescription2
select 
Risk_Description
   , length(Risk_Description) as TotalLength
   ,case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as  NextLine
   ,case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
   ,case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
   ,case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation
from
(
	select distinct Risk_Description

	from 
	(
		SELECT distinct Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description FROM etl.`qcr.riskdataimport090820` 
	)x
	group by Risk_Description 
	having count(1) > 1

	UNION ALL 

	Select '"Individuals noted in appendix A of the M2 Policy have the authority to approve leases up to $250,000.  The M2 President and CEO can jointly approve leases/loans from $250,000-$500,000.  For leases/loans over this amount a credit write up is performed and emailed to the QCRH EVP Chief Credit Officer, EVP QCRH Chief Lending Officer EVP QCBT Chief Credit Officer, and CEO QCRH.  They review the write up and send an email back to the M2 CEO if they approve the lease/loan (appendix A lists the number of approvers required for various lease amounts).  The M2 CEO then adds the emails to his lease/loan documents.

	Leases/Loans may be signed by the customer prior to approval, but the leases/loans are not funded at that time.  Funding does not occur until it is approved by the appropriate individuals based on dollar amount.
	"'
	UNION ALL 

	Select 
	'"Individuals noted in appendix A of the M2 Policy have the authority to approve leases! up to $250,000 The M2 President  and CEO can jointly approve leases/loans from $250,000-$500,000 For leases/loans over this amount a credit write up is performed and emailed to the QCRH EVP Chief Credit Officer, EVP QCRH Chief Lending Officer EVP QCBT Chief Credit Officer, and CEO QCRH.  They review the write up and send an email back to the M2 CEO if they approve the lease/loan (appendix A lists the number of approvers required for various lease amounts).  The M2 CEO then adds the emails to his lease/loan documents.

	Leases/Loans may be signed by the customer prior to approval, but the leases/loans are not funded at that time.  Funding does not occur until it is approved by the appropriate individuals based on dollar amount.
	"
	'
    
	UNION ALL 

	Select 
	'"Individuals noted in appendix A? of the M2 Policy have the authority to approve leases! up to $250,000 The M2 President  and CEO can jointly approve leases/loans from $250,000-$500,000 For leases/loans over this amount a credit write up is performed and emailed to the QCRH EVP Chief Credit Officer, EVP QCRH Chief Lending Officer EVP QCBT Chief Credit Officer, and CEO QCRH.  They review the write up and send an email back to the M2 CEO if they approve the lease/loan (appendix A lists the number of approvers required for various lease amounts).  The M2 CEO then adds the emails to his lease/loan documents.

	Leases/Loans may be signed by the customer prior to approval, but the leases/loans are not funded at that time.  Funding does not occur until it is approved by the appropriate individuals based on dollar amount.
	"
	'
    
   UNION ALL 

	Select 
	'"Individuals noted in 
    appendix A? of the M2 Policy have the authority to approve leases! up to $250,000 The M2 President  and CEO can jointly approve leases/loans from $250,000-$500,000 For leases/loans over this amount a credit write up is performed and emailed to the QCRH EVP Chief Credit Officer, EVP QCRH Chief Lending Officer EVP QCBT Chief Credit Officer, and CEO QCRH.  They review the write up and send an email back to the M2 CEO if they approve the lease/loan (appendix A lists the number of approvers required for various lease amounts).  The M2 CEO then adds the emails to his lease/loan documents.

	Leases/Loans may be signed by the customer prior to approval, but the leases/loans are not funded at that time.  Funding does not occur until it is approved by the appropriate individuals based on dollar amount.
	"
	'
      UNION ALL  
    	Select 
	'"Individuals noted in performed and emailed to the QCRH EVP Chief Credit Officer, EVP QCRH Chief Lending Officer EVP QCBT Chief Credit Officer, and CEO QCRH They review the write up and send an email back to the M2 CEO if they approve the lease/loan (appendix A lists the number of approvers required for various lease amounts) The M2 CEO then adds the emails to his lease/loan documents. Leases/Loans may be signed by the customer prior to approval, but the leases/loans are not funded at that time Funding does not occur until it is approved by the appropriate individuals based on dollar amount.
	"
	'
)x
;


   
select a.* 
, case when a.TotalLength <= 255 then a.Risk_Description
     when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
     when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
     else  substring(a.Risk_Description,1, 255)
end as RiskName 
from tempdb.DerivingRiskDescription2 a
where Risk_Description LIKE '%\n%';


Drop temporary table if exists tempdb.DerivingRiskDescription3;
create temporary table tempdb.DerivingRiskDescription3
select
  if (substring(Risk_Description,1,1) = '\n',trim(substring(Risk_Description,2,length(Risk_Description))), Risk_Description) as Risk_Description
   , length(Risk_Description) as TotalLength
   ,case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as NextLine
   ,case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
   ,case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
   ,case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation
from 
	(
		SELECT distinct Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description FROM etl.`qcr.riskdataimport090820` 
	)x
    
   
 ;


select RiskName, count(1) from
(
select distinct Risk_Description
, case when a.TotalLength <= 255 then a.Risk_Description
     when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
     when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
     else  substring(a.Risk_Description,1, 255)
end as RiskName 
from tempdb.DerivingRiskDescription3 a 
)x
group by RiskName
having count(1) > 1
;

select * from 
(
select distinct Risk_Description
, case when a.TotalLength <= 255 then a.Risk_Description
     when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
     when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
     else  substring(a.Risk_Description,1, 255)
end as RiskName 
from tempdb.DerivingRiskDescription3 a 
)x 
where x.RiskName = 'Account balances could be intentionally or unintentionally misstated, with material errors or inappropriate activity going undetected.';





select Risk_Description , count(1) from 
(
SELECT distinct Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description 
FROM etl.`qcr.riskdataimport090820` where  trim(Risk_Description) <> '' and  Risk_Description is not null
)x
group by Risk_Description
having count(1) > 1
;
################################################################################################################################################################################
################################################################################################################################################################################
######################################################      FINAL     Script  Preparation   ####################################################################################
################################################################################################################################################################################
################################################################################################################################################################################
################################################################################################################################################################################

#### Inserting into RiskRegisterItem i.e. Risk Definitions
Drop temporary table if exists tempdb.RiskDefinitionHierarchyFromFile;
create temporary table tempdb.RiskDefinitionHierarchyFromFile
# Deriving RiskName
select distinct  Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description
, case when TotalLength <= 255 then Risk_Description
		when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
		when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
		else  substring(Risk_Description,1, 254)
end as RiskName 
From
(
	# Deriving the Columns for Applying Conditions to Derive RiskName
	select distinct
		a.Mega_Process, a.Process ,a.Sub_process
	, if (substring(a.Risk_Description,1,1) = '\n',trim(substring(a.Risk_Description,2,length(a.Risk_Description))), a.Risk_Description) as Risk_Description
    , length(Risk_Description) as TotalLength
	, case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as  NextLine
	, case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
	, case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
	, case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation

	from
	(
		# Distinct Values from Based Table
		SELECT distinct trim(Mega_Process) as Mega_Process, trim(Process) as Process, ifnull(Sub_process, Process) as Sub_process, trim(Risk_Description) as Risk_Description FROM etl.`qcr.riskdataimport090820` where Risk_Description is not null and trim(Risk_Description) <> '' 
        and Mega_Process is not null and Process is not null 
	)a
)b ;


# Multiple RiskDefinition for 1 RiskName
Drop temporary table if exists tempdb.UniqueRiskDefinitions;
create temporary table tempdb.UniqueRiskDefinitions
select RiskName, max(Risk_Description) as Risk_Description from  tempdb.RiskDefinitionHierarchyFromFile
group by RiskName;

Drop temporary table if exists tempdb.FinalRiskDefinitionHierarchyFromFile;
create temporary table tempdb.FinalRiskDefinitionHierarchyFromFile
select distinct Mega_Process, Process, Sub_process, b.Risk_Description as Risk_Description , a.RiskName
from  tempdb.RiskDefinitionHierarchyFromFile a 
inner join  tempdb.UniqueRiskDefinitions b 
on a.RiskName = b.RiskName;

#### Flattening the Taxnomies 


Drop temporary table if exists tempdb.RiskTaxonimies;
create temporary table tempdb.RiskTaxonimies
select Level1Id, level1, Level2Id, Level2, ifnull(l3.id, Level2Id) as Level3Id, ifnull(l3.Name, Level2) as Level3,  ifnull(l3.id, Level2Id) LeafParentId from
(
	select Level1Id, level1, l2.id as Level2Id, l2.Name as Level2, m2.*
	 from 
	(
		select l1.id as Level1Id, l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
		inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
		where l1.CustomerId = @CustomerId
	)level1
	inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
	inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId
)Level2
left join predict360.RiskRegisterItemMapping m3 on m3.parentId = Level2.riskRegisterItemId
left join predict360.RiskRegisterItem l3 on l3.id = m3.riskRegisterItemId


UNION 

select Level1Id, level1, Level2Id, Level2, Level2Id as Level3Id, Level2 as Level3,  Level2Id as LeafParentId  from
(
	select Level1Id, level1, l2.Id as Level2Id,  l2.Name as Level2, m2.*
	 from 
	(
		select l1.id as Level1Id,l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
		inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
		where l1.CustomerId = @CustomerId
	)level1
	inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
	inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId
)Level2;


Drop temporary table if exists tempdb.FinalRiskDefData;
create temporary table tempdb.FinalRiskDefData
select distinct * from tempdb.RiskTaxonimies rt
inner join tempdb.FinalRiskDefinitionHierarchyFromFile d 
on d.Mega_Process = rt.Level1 and d.Process = rt.Level2 and d.Sub_process = rt.Level3 ;




/**************************************************************************
			predict360.RiskRegisterItemApplicablity
***************************************************************************/



Drop temporary table if exists tempdb.RiskDefHierarchyWithBUs;
create temporary table tempdb.RiskDefHierarchyWithBUs
# Deriving RiskName
select distinct  Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description, Business_Unit
, case when TotalLength <= 255 then Risk_Description
		when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
		when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
		else  substring(Risk_Description,1, 254)
end as RiskName 
From
(
	# Deriving the Columns for Applying Conditions to Derive RiskName
	select distinct
		a.Mega_Process, a.Process ,a.Sub_process
	, if (substring(a.Risk_Description,1,1) = '\n',trim(substring(a.Risk_Description,2,length(a.Risk_Description))), a.Risk_Description) as Risk_Description
    , a.Business_Unit
    , length(Risk_Description) as TotalLength
	, case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as  NextLine
	, case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
	, case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
	, case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation

	from
	(
		SELECT distinct trim(Business_Unit) as Business_Unit, trim(Mega_Process) as Mega_Process, trim(Process) as Process, ifnull(Sub_process, Process) as Sub_process, trim(Risk_Description) as Risk_Description 
		FROM etl.`qcr.riskdataimport090820` 
		where Risk_Description is not null and trim(Risk_Description) <> '' and Mega_Process is not null and Process is not null 
	)a
)b;

### Creating Risk Taxonomies with Definition
Drop temporary table if exists tempdb.RiskTaxL1L2;
create temporary table tempdb.RiskTaxL1L2
Select L1.*, L2.riskRegisterItemId as Level2Id, L2.Level2  from 
(
	select m1.RiskRegisterItemId as Level1Id, l1.name as Level1 from  predict360.RiskRegisterItem l1 
	inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
	where l1.CustomerId = @CustomerId 
	#and l1.Name = 'Deposit Operations'
)L1
inner join 

(
	select rri.Name as Level2, rrm.* from predict360.RiskRegisterItem rri 
	inner join predict360.RiskRegisterItemMapping rrm on rrm.RiskRegisterItemId = rri.id and CustomerId = @CustomerId
	where rri.IsLeaf = 0 and rrm.ParentId is not null 

) L2
on L2.ParentId = L1.Level1Id;

Drop temporary table if exists tempdb.RiskTaxL1L2ForL3;
create temporary table tempdb.RiskTaxL1L2ForL3
Select a.*, a.Level2Id as Level3Id , a.Level2 as Level3 from  tempdb.RiskTaxL1L2 a ;
 

Drop temporary table if exists tempdb.RiskDefHierarchy;
create temporary table tempdb.RiskDefHierarchy

select distinct L1L2L3.*, RDef.riskRegisterItemId as RDefId, RDef.RiskDefinition from 
(
	Select L1L2.*, L3.riskRegisterItemId as Level3Id, L3.Level3 as Level3  from  tempdb.RiskTaxL1L2 L1L2
	inner join 
	(
		select rri.Name as Level3, rrm.* from predict360.RiskRegisterItem rri 
		inner join predict360.RiskRegisterItemMapping rrm on rrm.RiskRegisterItemId = rri.id
		where rri.IsLeaf = 0 and rrm.ParentId is not null
	) L3
	on L3.ParentId = L1L2.Level2Id
    UNION  
    Select * from  tempdb.RiskTaxL1L2ForL3 a
    
)L1L2L3
inner join 

(
	select rri.Name as RiskDefinition, rrm.* from predict360.RiskRegisterItem rri 
	inner join predict360.RiskRegisterItemMapping rrm on rrm.RiskRegisterItemId = rri.id
	where rri.IsLeaf = 1 and rrm.ParentId is not null
)RDef
on RDef.ParentId = L1L2L3.Level3Id;
 
 
select distinct @CustomerId,  f.id as buFacilityId, 'Applicable' as applicablity, r.RDefId as riskRegisterItemId , r.Level3ID as immediateParentId
from tempdb.RiskDefHierarchyWithBUs d 
inner join tempdb.RiskDefHierarchy r on r.level1 = d.Mega_Process and r.Level2 = d.Process and r.Level3 = d.Sub_process and r.RiskDefinition = d.RiskName
inner join predict360.Facility f on f.`name` = d.Business_Unit and CustomerId = @CustomerId
;













 