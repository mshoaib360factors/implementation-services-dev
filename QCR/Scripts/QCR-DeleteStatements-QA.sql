set SQL_SAFE_UPDATES = 0;

######################################################################################

Delete from  predict360.RiskRegisterOverallAnalysis where CustomerId = @CustomerId;
######################################################################################


Delete from predict360.RiskRegister  where customerId = @CustomerId 
and riskRegisterItemId <> 2229 -- Temporary Condition
;

######################################################################################

delete from predict360.RiskRegisterItemApplicablity where customerid = @CustomerId ;

######################################################################################

#Deleting Mappings of Risk Defintions 
delete rrm.* from  predict360.RiskRegisterItemMapping rrm
inner join  predict360.RiskRegisterItem rri on rri.id = rrm.riskRegisterItemId and customerid = @CustomerId and createdBy = @UserId and isLeaf = 1
and rrm.riskRegisterItemId not in (2229,2230) ;-- Temporary Condition


#Deleting Risk Defintions 
delete from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf = 1 and id not in (2229,2230);

######################################################################################

#Deleting Level 2 Risk Taxonomies
delete from predict360.RiskRegisterItemMapping where riskRegisterItemId in (
select distinct rri2.id as Level2Id
 from 
(select distinct  trim(RISK_AREA_TYPE) as RISK_AREA_TYPE, trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_ from  etl.`qcr.riskdataimport090820` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '' ) l2
# Levels id identifications from RiskRegisterItem
inner join 
(select id, name from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf = 0) rri2 on rri2.name = l2.Risk_Area__Sub_category_
inner join 
(select id, name from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf = 0) rri1 on rri1.name = l2.RISK_AREA_TYPE
);


drop temporary table if exists tempdb.Level2RiskTaxonomies;
create temporary table tempdb.Level2RiskTaxonomies
select distinct rri.id 
from  etl.`qcr.riskdataimport090820` df
inner join predict360.RiskRegisterItem rri on rri.`name` = trim(df.Risk_Area__Sub_category_) and customerid = @CustomerId and createdBy = @UserId and isLeaf = 0
where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '';

delete from RiskRegisterItem where id in (select id from tempdb.Level2RiskTaxonomies) ;

######################################################################################
 
#Deleting Level 1 Risk Taxonomies
delete from predict360.RiskRegisterItemMapping where riskRegisterItemId in (
select distinct rri1.id 
from 
(select distinct trim(Mega_Process) as Mega_Process from  etl.`qcr.riskdataimport090820` where Mega_Process is not null and trim(Mega_Process) <> '') l1 
inner join 
(select id, name from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf =0) rri1 on rri1.name = l1.Mega_Process
);


delete from predict360.RiskRegisterItem where CustomerId = @CustomerId and createdBy = @UserId and isleaf = 0 
and id <> 2175;

set SQL_SAFE_UPDATES = 1;



############ Delteting All Taxonomies Levels ######################################################
set SQL_SAFE_UPDATES = 0;


Delete from predict360.RiskRegisterItemMapping where RiskRegisterItemId in (
select id from predict360.RiskRegisterItem where CustomerId = @CustomerId);

Delete from predict360.RiskRegisterItem where CustomerId = @CustomerId ;


set SQL_SAFE_UPDATES = 1;