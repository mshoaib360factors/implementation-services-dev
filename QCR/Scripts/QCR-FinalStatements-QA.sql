set @CustomerId = 2653;
set @UserId = 21086;

/**************************************************************************
			RiskRegisterItem  & RiskRegisterItemMapping
***************************************************************************/
########### Level 1

# Risk Taxonomy  (Category) --- Level 1
insert into predict360.RiskRegisterItem (`name`, isLeaf, createdBy, createdDate, customerId, description )
Select distinct trim(Mega_Process),0, @UserId, current_timestamp(), @CustomerId, ''
FROM etl.`qcr.riskdataimport090820` where Mega_Process is not null and trim(Mega_Process) <> '';

 

# RiskRegisterItemMapping Inserting Level 1 Information
insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri1.id as Level1Id, null 
from 
(Select distinct trim(Mega_Process) as Mega_Process FROM etl.`qcr.riskdataimport090820` where Mega_Process is not null and trim(Mega_Process) <> '') l1 
inner join 
(select id, name from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf =0) rri1 on rri1.name = l1.Mega_Process;


######################################################################################

########### Level2

# Risk Taxonomy  (Category) --- Level 2
insert into predict360.RiskRegisterItem (`name`,parentId, isLeaf, createdBy, createdDate,  customerId,  description)
select distinct  trim(Process),rri.id, 0, @UserId, current_timestamp(),  @CustomerId, ''
FROM etl.`qcr.riskdataimport090820` d
inner join predict360.RiskRegisterItem rri on rri.name =  trim(d.Mega_Process) and CustomerId = @CustomerId
where (Process is not null or trim(Process) <>  '') and trim(Mega_Process) <> trim(Process) and (Mega_Process is not null or trim(Mega_Process) <> '');

#Mapping the Links between Level 2 and Level 1 Taxonomies
insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select id, parentId from predict360.RiskRegisterItem where CustomerId = @CustomerId and parentId is not null;

Update predict360.RiskRegisterItem
set parentId = NULL 
where CustomerId = @CustomerId and parentId is not null;



######################################################################################

########### Level 3

insert into predict360.RiskRegisterItem (`name`, ParentId, isLeaf,  createdBy, createdDate,  customerId,  description)
SELECT distinct trim(Sub_process) as Sub_process, Level2Id as Parent, 0, @UserId, current_timestamp(),  @CustomerId, ''
FROM etl.`qcr.riskdataimport090820` d
inner join 
(
	select level1, l2.Name as Level2, l2.id as Level2Id
	 from 
	(
		select l1.id as Level1Id, l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
		inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
		where l1.CustomerId = @CustomerId
	)level1
	inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
	inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId
)r 
on r.level1 = d.Mega_Process and r.Level2 = d.process
where trim(Sub_process) <> trim(Process) And (Sub_process is not null or trim(Sub_process) <>  '') and Process is not null;



insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select id, parentId from predict360.RiskRegisterItem where CustomerId = @CustomerId and parentId is not null;

Update predict360.RiskRegisterItem
set parentId = NULL 
where CustomerId = @CustomerId and parentId is not null;


################################################################################################



#### #### #### #### Inserting into RiskRegisterItem i.e. Risk Definitions #### #### #### #### 
Drop temporary table if exists tempdb.RiskDefinitionHierarchyFromFile;
create temporary table tempdb.RiskDefinitionHierarchyFromFile
# Deriving RiskName
select distinct  Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description
, case when TotalLength <= 255 then Risk_Description
		when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
		when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
		else  substring(Risk_Description,1, 254)
end as RiskName 
From
(
	# Deriving the Columns for Applying Conditions to Derive RiskName
	select distinct
		a.Mega_Process, a.Process ,a.Sub_process
	, if (substring(a.Risk_Description,1,1) = '\n',trim(substring(a.Risk_Description,2,length(a.Risk_Description))), a.Risk_Description) as Risk_Description
    , length(Risk_Description) as TotalLength
	, case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as  NextLine
	, case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
	, case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
	, case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation

	from
	(
		# Distinct Values from Based Table
		SELECT distinct Mega_Process, Process, ifnull(Sub_process, Process) as Sub_process, trim(Risk_Description) as Risk_Description FROM etl.`qcr.riskdataimport090820` where Risk_Description is not null and trim(Risk_Description) <> '' 
        and Mega_Process is not null and Process is not null 
	)a
)b ;


# Multiple RiskDefinition for 1 RiskName
Drop temporary table if exists tempdb.UniqueRiskDefinitions;
create temporary table tempdb.UniqueRiskDefinitions
select RiskName, max(Risk_Description) as Risk_Description from  tempdb.RiskDefinitionHierarchyFromFile
group by RiskName;

Drop temporary table if exists tempdb.FinalRiskDefinitionHierarchyFromFile;
create temporary table tempdb.FinalRiskDefinitionHierarchyFromFile
select distinct Mega_Process, Process, Sub_process, b.Risk_Description as Risk_Description , a.RiskName
from  tempdb.RiskDefinitionHierarchyFromFile a 
inner join  tempdb.UniqueRiskDefinitions b 
on a.RiskName = b.RiskName;

#### Flattening the Taxnomies 


Drop temporary table if exists tempdb.RiskTaxonimies;
create temporary table tempdb.RiskTaxonimies
select Level1Id, level1, Level2Id, Level2, ifnull(l3.id, Level2Id) as Level3Id, ifnull(l3.Name, Level2) as Level3,  ifnull(l3.id, Level2Id) LeafParentId from
(
	select Level1Id, level1, l2.id as Level2Id, l2.Name as Level2, m2.*
	 from 
	(
		select l1.id as Level1Id, l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
		inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
		where l1.CustomerId = @CustomerId
	)level1
	inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
	inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId
)Level2
left join predict360.RiskRegisterItemMapping m3 on m3.parentId = Level2.riskRegisterItemId
left join predict360.RiskRegisterItem l3 on l3.id = m3.riskRegisterItemId


UNION 

select Level1Id, level1, Level2Id, Level2, Level2Id as Level3Id, Level2 as Level3,  Level2Id as LeafParentId  from
(
	select Level1Id, level1, l2.Id as Level2Id,  l2.Name as Level2, m2.*
	 from 
	(
		select l1.id as Level1Id,l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
		inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
		where l1.CustomerId = @CustomerId
	)level1
	inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
	inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId
)Level2;


Drop temporary table if exists tempdb.FinalRiskDefData;
create temporary table tempdb.FinalRiskDefData
select distinct * from tempdb.RiskTaxonimies rt
inner join tempdb.FinalRiskDefinitionHierarchyFromFile d 
on d.Mega_Process = rt.Level1 and d.Process = rt.Level2 and d.Sub_process = rt.Level3 ;


insert into predict360.RiskRegisterItem (`name`, isLeaf, createdBy, createdDate, customerId, description)
select distinct RiskName, 1, @UserId,  current_timestamp(), @CustomerId, Risk_Description  from tempdb.FinalRiskDefData;
 

#### Mapping the links between Risk and Categories
insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri.id, LeafParentId from  predict360.RiskRegisterItem rri
inner join tempdb.FinalRiskDefData f on rri.name = f.RiskName
where CustomerId = @CustomerId and isLeaf = 1;


/**************************************************************************
			predict360.RiskRegisterItemApplicablity
***************************************************************************/



Drop temporary table if exists tempdb.RiskDefHierarchyWithBUs;
create temporary table tempdb.RiskDefHierarchyWithBUs
# Deriving RiskName
select distinct  Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description, Business_Unit
, case when TotalLength <= 255 then Risk_Description
		when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
		when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
		else  substring(Risk_Description,1, 254)
end as RiskName 
From
(
	# Deriving the Columns for Applying Conditions to Derive RiskName
	select distinct
		a.Mega_Process, a.Process ,a.Sub_process
	, if (substring(a.Risk_Description,1,1) = '\n',trim(substring(a.Risk_Description,2,length(a.Risk_Description))), a.Risk_Description) as Risk_Description
    , a.Business_Unit
    , length(Risk_Description) as TotalLength
	, case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as  NextLine
	, case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
	, case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
	, case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation

	from
	(
		SELECT distinct trim(Business_Unit) as Business_Unit, trim(Mega_Process) as Mega_Process, trim(Process) as Process, ifnull(Sub_process, Process) as Sub_process, trim(Risk_Description) as Risk_Description 
		FROM etl.`qcr.riskdataimport090820` 
		where Risk_Description is not null and trim(Risk_Description) <> '' and Mega_Process is not null and Process is not null 
	)a
)b;

### Creating Risk Taxonomies with Definition
Drop temporary table if exists tempdb.RiskTaxL1L2;
create temporary table tempdb.RiskTaxL1L2
Select L1.*, L2.riskRegisterItemId as Level2Id, L2.Level2  from 
(
	select m1.RiskRegisterItemId as Level1Id, l1.name as Level1 from  predict360.RiskRegisterItem l1 
	inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
	where l1.CustomerId = @CustomerId 
	#and l1.Name = 'Deposit Operations'
)L1
inner join 

(
	select rri.Name as Level2, rrm.* from predict360.RiskRegisterItem rri 
	inner join predict360.RiskRegisterItemMapping rrm on rrm.RiskRegisterItemId = rri.id and CustomerId = @CustomerId
	where rri.IsLeaf = 0 and rrm.ParentId is not null 

) L2
on L2.ParentId = L1.Level1Id;

Drop temporary table if exists tempdb.RiskTaxL1L2ForL3;
create temporary table tempdb.RiskTaxL1L2ForL3
Select a.*, a.Level2Id as Level3Id , a.Level2 as Level3 from  tempdb.RiskTaxL1L2 a ;
 

Drop temporary table if exists tempdb.RiskDefHierarchy;
create temporary table tempdb.RiskDefHierarchy

select distinct L1L2L3.*, RDef.riskRegisterItemId as RDefId, RDef.RiskDefinition from 
(
	Select L1L2.*, L3.riskRegisterItemId as Level3Id, L3.Level3 as Level3  from  tempdb.RiskTaxL1L2 L1L2
	inner join 
	(
		select rri.Name as Level3, rrm.* from predict360.RiskRegisterItem rri 
		inner join predict360.RiskRegisterItemMapping rrm on rrm.RiskRegisterItemId = rri.id
		where rri.IsLeaf = 0 and rrm.ParentId is not null
	) L3
	on L3.ParentId = L1L2.Level2Id
    UNION  
    Select * from  tempdb.RiskTaxL1L2ForL3 a
    
)L1L2L3
inner join 

(
	select rri.Name as RiskDefinition, rrm.* from predict360.RiskRegisterItem rri 
	inner join predict360.RiskRegisterItemMapping rrm on rrm.RiskRegisterItemId = rri.id
	where rri.IsLeaf = 1 and rrm.ParentId is not null
)RDef
on RDef.ParentId = L1L2L3.Level3Id;
 

insert into predict360.RiskRegisterItemApplicablity ( customerId, buFacilityId, applicablity, riskRegisterItemId, immediateParentId)
select distinct @CustomerId,  f.id as buFacilityId, 'Applicable' as applicablity, r.RDefId as riskRegisterItemId , r.Level3Id as immediateParentId
from tempdb.RiskDefHierarchyWithBUs d 
inner join tempdb.RiskDefHierarchy r on r.level1 = d.Mega_Process and r.Level2 = d.Process and r.Level3 = d.Sub_process and r.RiskDefinition = d.RiskName
inner join predict360.Facility f on f.`name` = d.Business_Unit and CustomerId = @CustomerId
;





## Start from here.
/**************************************************************************
			predict360.RiskRegister
***************************************************************************/

insert into predict360.RiskRegister (riskName, riskDescription, customerId, riskRegisterItemId, buFacilityId, frequencyCount, frequencyOccurence, status, createdBy, createdDate, weight)
select distinct 
  rri.`name` as riskName, rri.description, a.customerId, riskRegisterItemId, buFacilityId, 1 as frequencyCount, 'Year' as frequencyOccurence, 'Active' as `status`
, @UserId as createdBy, current_timestamp() as createdDate, 1 as weight
from predict360.RiskRegisterItemApplicablity a
inner join predict360.RiskRegisterItem rri on rri.id = a.riskRegisterItemId and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.isLeaf = 1
where a.CustomerId = @CustomerId and applicablity = 'Applicable'; 



/**************************************************************************
			predict360.RiskRegisterOverallAnalysis
***************************************************************************/
 
### No information is available here to store the data


/*****************************************************************************************
******************************************************************************************
******************************************************************************************

									Controls

******************************************************************************************
******************************************************************************************
*****************************************************************************************/



/**************************************************************************
			predict360.ControlType
***************************************************************************/
insert into predict360.ControlType (name, description, customerId, createdBy, createdOn)
select distinct trim(Control_Type__preventive__detective__corrective_) as `name`, trim(Control_Type__preventive__detective__corrective_) as `description` 
, @CustomerId, @UserId,  current_timestamp()
from  etl.`gmg.riskdataimport05102020__`  
where trim(Control_Type__preventive__detective__corrective_) not in (select trim(name)  from predict360.ControlType where customerId = @customerId) and
(Control_Type__preventive__detective__corrective_ is not null or trim(Control_Type__preventive__detective__corrective_) <> '');


## Start from here.
/**************************************************************************
			predict360.RiskRegisterControlItem -- Level 1
***************************************************************************/

### Inserting Level 1: Temporary Statements
ALTER TABLE `etl`.`gmg.riskdataimport05102020__` 
ADD COLUMN `ControlCategory_TmpCol` VARCHAR(100) NULL DEFAULT 'Temp Control Category' AFTER `Inherent_Risk_Rating`;

insert into predict360.RiskRegisterControlItem(name, createdBy, createdDate, updatedBy, updatedDate, isLeaf, customerId, isDeleted)
select distinct ControlCategory_TmpCol as Level1, @UserId, current_timestamp(), @UserId , current_timestamp(), 0 as isLeaf, @CustomerId, 0 as isDeleted 
from  etl.`gmg.riskdataimport05102020__` ;

/**************************************************************************
			predict360.RiskRegisterControlItem
***************************************************************************/

###### fDo consult with Aamir or GK. About the transition of column Efficacy from predict360.RiskRegisterControlItem to ControlInstance or RiskRegisterControlInstance

insert into predict360.RiskRegisterControlItem ( name, parentId, createdBy, createdDate, updatedBy, updatedDate, isLeaf, customerId, isDeleted, controlTypeId, controlDefinitionId)
select 

 d.Control_Name as Control_Name, p.id as parentId, @UserId, current_timestamp(), @UserId , current_timestamp(), 1 as isLeaf
, @CustomerId, 0 as isDeleted, ct.id as ControlTypeId, Control_Index as controlDefinitionId
from 
(
	select distinct 
	  Trim(ControlCategory_TmpCol) as ControlCategory_TmpCol
	, trim(Control_Index) as Control_Index
	, trim(Control_Name) as Control_Name
	, trim(Control_Type__preventive__detective__corrective_) as Control_Type__preventive__detective__corrective_
	, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_
	from  etl.`gmg.riskdataimport05102020__` 
	where Control_Index is not null 
	and Control_Index not in ('**', 'see ''Finance'' process',  'IS Control', 'IS Controls', 'TBD', 
	'RB-C46' # Removing Temporarily
	)
	and Control_Type__preventive__detective__corrective_  is  not null and Control_Execution__automated__manual_ is not null
)d
inner join predict360.RiskRegisterControlItem p 
on Trim(d.ControlCategory_TmpCol) =  p.name and isLeaf = 0 and p.customerid = @CustomerId and isDeleted = 0
inner join predict360.ControlType ct on ct.name = d.Control_Type__preventive__detective__corrective_ and ct.CustomerId =  @CustomerId ;


/**************************************************************************
	 predict360.ControlInstance & predict360.RiskRegisterControlInstance
***************************************************************************/

 
Drop temporary table if exists tempdb.ContItemToRR;
create temporary table tempdb.ContItemToRR
select distinct rr.id as RiskIds, rr.riskRegisterItemId, rri.riskDefinitionId, d.Control_Index, ci.id as ControlItemId from predict360.RiskRegister rr 
inner join predict360.RiskRegisterItem rri on rr.riskRegisterItemId = rri.id and rr.customerId = rri.customerId 
inner join etl.`gmg.riskdataimport05102020__` d on rri.riskDefinitionId = d.Risk_Index 
inner join  predict360.RiskRegisterControlItem ci on ci.controlDefinitionId = ifnull(d.Control_Index,-1) and ci.customerId = rr.customerId
where rr.customerId = 2641;



# predict360.ControlInstance
insert into predict360.ControlInstance (name, riskRegisterControlItemId, efficacy, implemented, createdBy, createdDate, updatedBy, updatedDate, isDeleted)
select distinct substring(d.Control_Description,1,255) as `name`,  t.ControlItemId as riskRegisterControlItemId, 100.00 as efficacy, 100.00 as implemented
, @UserId as createdBy, current_timestamp() as createdDate, @UserId as updatedBy, current_timestamp() as updatedDate, 0 isDeleted
from tempdb.ContItemToRR t
inner join  etl.`gmg.riskdataimport05102020__` d on d.Control_Index = t.Control_Index;


# predict360.RiskRegisterControlInstance

Drop temporary table if exists tempdb.TotalControls;
create temporary table tempdb.TotalControls
select RiskIds, count(1) as TotalControls from
(
 select distinct RiskIds,ci.id  from tempdb.ContItemToRR t
 inner join predict360.ControlInstance ci on t.ControlItemId  = ci.riskRegisterControlItemId
)a
group by RiskIds;

insert into predict360.RiskRegisterControlInstance (riskRegisterId, controlInstanceId, weight)
select distinct t.RiskIds as riskRegisterId,ci.id as controlInstanceId, 100/ TotalControls as weight from tempdb.ContItemToRR t
inner join predict360.ControlInstance ci on t.ControlItemId  = ci.riskRegisterControlItemId
inner join tempdb.TotalControls tc on tc.RiskIds = t.RiskIds;
 










