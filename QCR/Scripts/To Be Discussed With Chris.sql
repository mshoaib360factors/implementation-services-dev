Drop temporary table if exists temp.DerivingRiskDescription3;
create temporary table temp.DerivingRiskDescription3
select
  if (substring(Risk_Description,1,1) = '\n',trim(substring(Risk_Description,2,length(Risk_Description))), Risk_Description) as Risk_Description
   , length(Risk_Description) as TotalLength
   ,case when LOCATE('\n', Risk_Description) = 0 then 99999999 else LOCATE('\n', Risk_Description) end as NextLine
   ,case when LOCATE('.', Risk_Description)  = 0 then 99999999 else LOCATE('.', Risk_Description)  end as Deci
   ,case when LOCATE('?', Risk_Description)  = 0 then 99999999 else LOCATE('?', Risk_Description)  end as QuesMark
   ,case when LOCATE('!', Risk_Description)  = 0 then 99999999 else LOCATE('!', Risk_Description)  end as Exclaimation
from 
	(
		SELECT distinct Mega_Process, Process, Sub_process, trim(Risk_Description) as Risk_Description FROM etl.`qcr.riskdataimport090820` 
	)x
    
   
 ;


select RiskName, count(1) from
(
select distinct Risk_Description
, case when a.TotalLength <= 255 then a.Risk_Description
     when NextLine <= 255 then substring(Risk_Description,1,LOCATE('\n', Risk_Description))
     when least(NextLine, Deci, QuesMark, Exclaimation) <= 255 then substring(Risk_Description,1, least(NextLine, Deci, QuesMark, Exclaimation))
     else  substring(a.Risk_Description,1, 255)
end as RiskName 
from temp.DerivingRiskDescription3 a 
)x
group by RiskName
having count(1) > 1


############ Start from here 