use predict360;
DROP PROCEDURE IF EXISTS `usp_risk_screen`;
DELIMITER $$
CREATE PROCEDURE `usp_risk_screen`(
    in_customerId BIGINT(20),
    in_PC BOOLEAN,
    in_BU VARCHAR(1000)
)
BEGIN

SET sql_safe_updates = 0;


DROP TEMPORARY TABLE IF EXISTS tempdb.riskcustomfield;
CREATE TEMPORARY TABLE tempdb.`riskcustomfield` (
  `Cfvid` BIGINT(20) NOT NULL DEFAULT '0',
  `entityName` ENUM('Risk Item', 'Risk Definition') CHARACTER SET utf8 DEFAULT NULL,
  `CustomFieldName` VARCHAR(255) CHARACTER SET utf8 NOT NULL,
  `fieldvalue` LONGTEXT CHARACTER SET utf8,
  `Entityid` BIGINT(20) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1;

INSERT INTO  tempdb.riskcustomfield
SELECT a.id AS Cfvid, e.entityName, c.`name`  AS CustomFieldName 
, 	CASE
		WHEN (a.`Stringvalue` IS NOT NULL) THEN IFNULL(d.optionLabel, a.Stringvalue) 
		WHEN (a.`textValue` IS NOT NULL) THEN a.`textValue`
		WHEN (a.`numericValue` IS NOT NULL) THEN a.`numericValue`
		WHEN (a.`decimalValue` IS NOT NULL) THEN a.`decimalValue`
		WHEN (a.`dateValue` IS NOT NULL) THEN a.`dateValue`
	ELSE
	'' END AS `fieldvalue`
, b.applicableEntityInstanceId AS Entityid

FROM CustomFieldValue a
INNER JOIN CustomFieldApplicableEntityInstance b ON a.CustomFieldApplicableEntityInstanceId = b.id
INNER JOIN CustomField c ON b.customFieldId = c.id 
INNER JOIN CustomFieldApplicableEntity e ON e.id = c.applicableEntityId  AND  e.entityName IN ( 'Risk Item', 'Risk Definition')
LEFT JOIN CustomFieldOption d ON c.id = d.customFieldId AND a.STRINGVALUE = d.id 
WHERE a.customerId = in_customerId;

DROP TEMPORARY TABLE IF EXISTS tempdb.MultiToSingleRiskCustomField;
CREATE TEMPORARY TABLE tempdb.MultiToSingleRiskCustomField
SELECT entityName,	Entityid, CustomFieldName, GROUP_CONCAT(fieldvalue) AS FieldValue FROM tempdb.riskcustomfield
GROUP BY entityName, Entityid, CustomFieldName;
ALTER TABLE tempdb.MultiToSingleRiskCustomField 
ADD INDEX idx_1(Entityid),
ADD INDEX idx_2(entityName);

DROP TEMPORARY TABLE IF EXISTS tempdb.riskcustomfieldflattenedRiskReg;
CREATE TEMPORARY TABLE tempdb.riskcustomfieldflattenedRiskReg
SELECT Entityid AS RiskRegister, JSON_OBJECTAGG(CustomFieldName, FieldValue) AS RiskRegCustomField FROM tempdb.MultiToSingleRiskCustomField
WHERE entityName = 'Risk Item'
GROUP BY Entityid;
ALTER TABLE tempdb.riskcustomfieldflattenedRiskReg ADD INDEX idx_1(RiskRegister);


DROP TEMPORARY TABLE IF EXISTS tempdb.riskcustomfieldflattenedRiskItem;
CREATE TEMPORARY TABLE tempdb.riskcustomfieldflattenedRiskItem
SELECT Entityid AS RiskItem, JSON_OBJECTAGG(CustomFieldName, FieldValue) AS RiskItemCustomField FROM tempdb.MultiToSingleRiskCustomField
WHERE entityName = 'Risk Definition'
GROUP BY Entityid;
ALTER TABLE tempdb.riskcustomfieldflattenedRiskItem ADD INDEX idx_1(RiskItem);



DROP TEMPORARY TABLE IF EXISTS tempdb.riskregisterresponsibleperson;
CREATE TEMPORARY TABLE tempdb.riskregisterresponsibleperson
SELECT a.riskRegisterId, json_arrayagg( json_object('id',a.personId,'username',b.username,'firstName',b.firstName,'lastName',b.lastName)) AS riskRegRespPerson
FROM riskregisterresponsibleperson a
JOIN predictuser b ON a.personId = b.id
GROUP BY 1;
ALTER TABLE tempdb.riskregisterresponsibleperson ADD INDEX idx_1(riskRegisterId);


DROP TEMPORARY TABLE IF EXISTS tempdb.riskregistersite;
CREATE TEMPORARY TABLE tempdb.riskregistersite
SELECT a.riskRegisterId, json_arrayagg( json_object('id',a.siteid,'name',b.name,'label',b.name)) AS riskRegSite
FROM riskregistersite a
JOIN site b ON a.siteid = b.id
GROUP BY 1;
ALTER TABLE tempdb.riskregistersite ADD INDEX idx_1(riskRegisterId);

DROP TEMPORARY TABLE IF EXISTS  cte;
    CREATE TEMPORARY TABLE cte
	WITH RECURSIVE cte AS (
	SELECT     rri.id, rri.isleaf,  rrim.parentId, 1 AS lvl, 
			
			JSON_ARRAY( JSON_OBJECT('id',rri.id, 'name',rri.`name`, 'description', rri.`description`, 'managementComments', rri.managementComments, 'isLeaf', CAST(rri.isLeaf AS UNSIGNED) )) AS hierarchy
	FROM       riskregisteritemmapping rrim
	INNER JOIN riskregisteritem rri ON rri.id = rrim.riskRegisterItemId AND rrim.isdeleted = 0
	WHERE	
	rrim.parentId IS NULL    
	AND	rri.customerId = in_customerid 		 
	UNION ALL
	  
	SELECT     rri.id,  rri.isleaf, rrim.parentId, lvl + 1 AS lvl,
	  JSON_MERGE(hierarchy,  
							JSON_ARRAY(JSON_OBJECT(
							'id',rri.id, 'name',rri.`name`, 'description', rri.`description`, 'managementComments', rri.managementComments, 'isLeaf', CAST(rri.isLeaf AS UNSIGNED)))                     
				)AS hierarchy
	FROM       riskregisteritemmapping rrim
	INNER JOIN cte  ON rrim.parentId = cte.id AND rrim.isdeleted = 0
	INNER JOIN riskregisteritem rri ON rri.id = rrim.riskRegisterItemId 
	)
    SELECT * FROM cte ;
    
    ALTER TABLE cte ADD INDEX idx_1(id,isLeaf);

DROP TEMPORARY TABLE IF EXISTS tempdb.riskregister;
SET @qry = CONCAT("
create temporary table tempdb.riskregister
select 
*
from riskregister rr where rr.customerid = ",in_customerid," and rr.status = 'Active' ",
IF(
	in_BU IS NOT NULL,
    CONCAT(" and rr.buFacilityId in (",in_BU,")"),
    ""
    ),
"; ");
PREPARE stmp FROM @qry;
EXECUTE stmp ;
DEALLOCATE PREPARE stmp;
ALTER TABLE tempdb.riskregister ADD INDEX idx_1(riskRegisterItemId);



IF in_PC IS NULL OR in_PC = FALSE THEN
	SET @qry = CONCAT("
	SELECT 
		cte.hierarchy
	, 	rria.customerId, rria.id AS riskRegItemApplicability_id, rria.buFacilityId, f.name AS FacilityName, f.rocType AS facility_rocType, applicablity, rria.riskRegisterItemId, rria.isProcessCategory, rria.isDeleted AS riskRegItemApplicability_isDeleted
	, 	rria.createdBy AS riskRegItemApplicability_createdBy, rria.createdOn AS riskRegItemApplicability_createdOn,	rria.modifiedBy AS riskRegItemApplicability_modifiedBy, rria.modifiedOn AS riskRegItemApplicability_modifiedOn



	,	rri.name AS riskRegItem_name, rri.createdBy AS riskRegItem_createdBy, rri.createdDate AS riskRegItem_createdDate, rri.updatedBy AS riskRegItem_updatedBy, rri.updatedDate AS riskRegItem_updatedDate,  rri.isDeleted AS riskRegItem_isDeleted
	, 	rri.description AS riskRegItem_description,	rri.isSystemRiskRegisterItem AS  riskRegItem_isSystemRiskRegisterItem, rri.isOrphan AS riskRegItem_isOrphan, rri.riskDefinitionId AS riskRegItem_riskDefinitionId
	, 	rri.libraryRiskId AS riskRegItem_libraryRiskId, rri.riskUpdateStatus AS riskRegItem_riskUpdateStatus, 	rri.riskTaxonomyId AS riskRegItem_riskTaxonomyId, rri.processTaxonomyId AS riskRegItem_processTaxonomyId
	, 	rri.contentLibraryId AS riskRegItem_contentLibraryId, rri.guid AS riskRegItem_guid, rri.isOrphanForProcess AS riskRegItem_isOrphanForProcess, 	rri.managementComments AS riskRegItem_managementComments
	, 	rri.referencedRiskRegisterItemId AS  riskRegItem_referencedRiskRegisterItemId, 	rri.riaSurveyId AS riskRegItem_riaSurveyId, rri.rpaSurveyId AS riskRegItem_rpaSurveyId


	, 	rr.id AS riskRegister_id, rr.riskName AS riskRegister_riskName, rr.frequencyCount  AS riskRegister_frequencyCount, rr.frequencyOccurence AS riskRegister_frequencyOccurence
	, 	rr.frequencyExplanation AS riskRegister_frequencyExplanation, 	rr.outcome AS riskRegister_outcome, rr.outcomeDescription AS riskRegister_outcomeDescription
	, 	rr.estimatedOutcome AS riskRegister_estimatedOutcome, rr.status AS riskRegister_status , rr.managementComments AS riskRegister_managementComments
	, 	rr.context AS riskRegister_context, rr.createdBy AS riskRegister_createdBy,	pu.firstName as riskRegister_createdByfirstName, pu.lastName as riskRegister_createdBylastName,
    rr.createdDate AS riskRegister_createdDate, rr.modifiedBy AS riskRegister_modifiedBy, rr.modifiedDate AS riskRegister_modifiedDate
	, 	rr.identifiedBy AS riskRegister_identifiedBy, rr.monetaryImpact AS riskRegister_monetaryImpact, rr.riskEventID AS riskRegister_riskEventID,
    re.riskEventName as riskRegister_riskEventName, rr.approach AS riskRegister_approach
	, 	rr.description AS riskRegister_description,	rr.surveyInstanceId AS riskRegister_surveyInstanceIdas, rr.surveySectionId AS riskRegister_surveySectionId
	, 	rr.surveyQuestionId AS riskRegister_surveyQuestionId, rr.controls AS riskRegister_controls, rr.lastReviewDate AS riskRegister_lastReviewDate
	, 	rr.isReviewed AS riskRegister_isReviewed, rr.changeExplanation AS riskRegister_changeExplanation, rr.weight AS riskRegister_weight
	, 	rr.relativeMagnitudeId AS riskRegister_relativeMagnitudeId, rr.topRisk AS riskRegister_topRisk, rr.riskInstanceId AS riskRegister_riskInstanceId

	, 	rroa.id AS riskRegOverallAnalysis_id, rroa.riskId AS riskRegOverallAnalysis_riskId, rroa.inherentLikelihood AS riskRegOverallAnalysis_inherentLikelihood, rroa.inherentImpact AS riskRegOverallAnalysis_inherentImpact
	, 	rroa.inherentRiskRating AS riskRegOverallAnalysis_inherentRiskRating, rroa.residualLikelihood AS riskRegOverallAnalysis_residualLikelihood
	, 	rroa.residualImpact AS riskRegOverallAnalysis_residualImpact, rroa.residualRiskRating AS riskRegOverallAnalysis_residualRiskRating, rroa.treatedRating AS riskRegOverallAnalysis_treatedRating, rroa.customerId AS riskRegOverallAnalysis_customerId
	, 	rroa.createdBy AS riskRegOverallAnalysis_createdBy, rroa.createdDate AS riskRegOverallAnalysis_createdDate, rroa.modifiedBy AS riskRegOverallAnalysis_modifiedBy, rroa.modifiedDate AS riskRegOverallAnalysis_modifiedDate, rroa.currentRiskRating AS riskRegOverallAnalysis_currentRiskRating
	, 	rroa.inherentMonetaryImpact AS riskRegOverallAnalysis_inherentMonetaryImpact, rroa.residualMonetaryImpact AS riskRegOverallAnalysis_residualMonetaryImpact, rroa.currentMonetaryImpact AS riskRegOverallAnalysis_currentMonetaryImpact
	, 	rroa.currentLikelihood AS riskRegOverallAnalysis_currentLikelihood, rroa.currentImpact AS riskRegOverallAnalysis_currentImpact, rroa.predictedRisk AS riskRegOverallAnalysis_predictedRisk

	, 	rrp.riskRegRespPerson 
	, 	rrs.riskRegSite 
	, 	rcri.RiskItemCustomField
	, 	rcrr.RiskRegCustomField
    , rics.id as riskinstancecontrolstrengthId
    , rics.controlStrength
    , rics.riskAnalysisDimensionId
    , rad.value as riskAnalysisDimensionValue
	, p.id
    , p.name                 
	, badef.name as businessAreasDef
    , p.type as businessAreasDefType
    , bacat1.name as businessAreasCat1
    , bacat2.name as businessAreasCat2
	FROM RiskRegisterItemApplicablity rria 
    LEFT JOIN product p ON rria.productID = p.id
    LEFT JOIN businessareas badef on p.businessAreasDef = badef.id
    LEFT JOIN businessareas bacat1 on p.businessAreasCat1 = bacat1.id
    LEFT JOIN businessareas bacat2 on p.businessAreasCat2 = bacat2.id
    LEFT JOIN facility f ON rria.buFacilityId = f.id
	INNER JOIN riskregisteritem rri ON rri.id = rria.riskRegisterItemId
	INNER JOIN tempdb.riskregister rr ON rr.riskRegisterItemId = rria.riskRegisterItemId AND rr.buFacilityId <=> rria.buFacilityId AND rr.productId <=> rria.productId
    AND rr.status = 'Active'
    JOIN predictuser pu on rr.createdBy = pu.id 
    left join riskevent re on rr.riskEventID = re.id
	LEFT JOIN riskregisteroverallanalysis rroa ON rroa.riskId = rr.id
    LEFT JOIN riskinstancecontrolstrength rics ON rr.id = rics.riskRegisterId
    LEFT JOIN riskanalysisdimension rad ON rics.riskAnalysisDimensionId = rad.id
	INNER JOIN cte ON cte.id = rria.riskRegisterItemId AND cte.isLeaf  = 1
	LEFT JOIN tempdb.riskregisterresponsibleperson rrp ON rrp.riskRegisterId = rr.id
	LEFT JOIN tempdb.riskregistersite rrs ON rrs.riskRegisterId = rr.id
	LEFT JOIN tempdb.riskcustomfieldflattenedRiskItem rcri ON rcri.RiskItem = rri.id
	LEFT JOIN tempdb.riskcustomfieldflattenedRiskReg rcrr ON rcrr.RiskRegister = rr.id
	WHERE rria.customerid = ",in_customerId," 
    AND rria.applicablity = 'Applicable' and ",
    IF(
	in_BU IS NOT NULL,
    CONCAT("rria.buFacilityId in (",in_BU,") and "),
    ""
    )," rria.isProcessCategory = 0 AND rria.isDeleted = 0 ;");
	
    PREPARE stmp FROM @qry;
	EXECUTE stmp ;
	DEALLOCATE PREPARE stmp;

ELSE 

	SET @qry = CONCAT("
	SELECT 
		cte.hierarchy
		, 	rria.customerId, rria.id AS riskRegItemApplicability_id, rria.buFacilityId, applicablity, rria.riskRegisterItemId, rria.isProcessCategory, rria.isDeleted AS riskRegItemApplicability_isDeleted
		, 	rria.createdBy AS riskRegItemApplicability_createdBy, rria.createdOn AS riskRegItemApplicability_createdOn,	rria.modifiedBy AS riskRegItemApplicability_modifiedBy, rria.modifiedOn AS riskRegItemApplicability_modifiedOn
	
	
	
		,	rri.name AS riskRegItem_name, rri.createdBy AS riskRegItem_createdBy, rri.createdDate AS riskRegItem_createdDate, rri.updatedBy AS riskRegItem_updatedBy, rri.updatedDate AS riskRegItem_updatedDate,  rri.isDeleted AS riskRegItem_isDeleted
		, 	rri.description AS riskRegItem_description,	rri.isSystemRiskRegisterItem AS  riskRegItem_isSystemRiskRegisterItem, rri.isOrphan AS riskRegItem_isOrphan, rri.riskDefinitionId AS riskRegItem_riskDefinitionId
		, 	rri.libraryRiskId AS riskRegItem_libraryRiskId, rri.riskUpdateStatus AS riskRegItem_riskUpdateStatus, 	rri.riskTaxonomyId AS riskRegItem_riskTaxonomyId, rri.processTaxonomyId AS riskRegItem_processTaxonomyId
		, 	rri.contentLibraryId AS riskRegItem_contentLibraryId, rri.guid AS riskRegItem_guid, rri.isOrphanForProcess AS riskRegItem_isOrphanForProcess, 	rri.managementComments AS riskRegItem_managementComments
		, 	rri.referencedRiskRegisterItemId AS  riskRegItem_referencedRiskRegisterItemId, 	rri.riaSurveyId AS riskRegItem_riaSurveyId, rri.rpaSurveyId AS riskRegItem_rpaSurveyId
	
	
		, 	rr.id AS riskRegister_id, rr.riskName AS riskRegister_riskName, rr.frequencyCount  AS riskRegister_frequencyCount, rr.frequencyOccurence AS riskRegister_frequencyOccurence
		, 	rr.frequencyExplanation AS riskRegister_frequencyExplanation, 	rr.outcome AS riskRegister_outcome, rr.outcomeDescription AS riskRegister_outcomeDescription
		, 	rr.estimatedOutcome AS riskRegister_estimatedOutcome, rr.status AS riskRegister_status , rr.managementComments AS riskRegister_managementComments
		, 	rr.context AS riskRegister_context, rr.createdBy AS riskRegister_createdBy,	pu.firstName as riskRegister_createdByfirstName, pu.lastName as riskRegister_createdBylastName,
        rr.createdDate AS riskRegister_createdDate, rr.modifiedBy AS riskRegister_modifiedBy, rr.modifiedDate AS riskRegister_modifiedDate
		, 	rr.identifiedBy as riskRegister_identifiedBy, rr.monetaryImpact as riskRegister_monetaryImpact, rr.riskEventID as riskRegister_riskEventID, 
        re.riskEventName as riskRegister_riskEventName,rr.approach as riskRegister_approach
		, 	rr.description as riskRegister_description,	rr.surveyInstanceId as riskRegister_surveyInstanceIdas, rr.surveySectionId as riskRegister_surveySectionId
		, 	rr.surveyQuestionId as riskRegister_surveyQuestionId, rr.controls as riskRegister_controls, rr.lastReviewDate as riskRegister_lastReviewDate
		, 	rr.isReviewed as riskRegister_isReviewed, rr.changeExplanation as riskRegister_changeExplanation, rr.weight as riskRegister_weight
		, 	rr.relativeMagnitudeId as riskRegister_relativeMagnitudeId, rr.topRisk as riskRegister_topRisk, rr.riskInstanceId as riskRegister_riskInstanceId
	
		, 	rroa.id as riskRegOverallAnalysis_id, rroa.riskId as riskRegOverallAnalysis_riskId, rroa.inherentLikelihood as riskRegOverallAnalysis_inherentLikelihood, rroa.inherentImpact as riskRegOverallAnalysis_inherentImpact
		, 	rroa.inherentRiskRating as riskRegOverallAnalysis_inherentRiskRating, rroa.residualLikelihood as riskRegOverallAnalysis_residualLikelihood
		, 	rroa.residualImpact as riskRegOverallAnalysis_residualImpact, rroa.residualRiskRating as riskRegOverallAnalysis_residualRiskRating, rroa.treatedRating as riskRegOverallAnalysis_treatedRating, rroa.customerId as riskRegOverallAnalysis_customerId
		, 	rroa.createdBy as riskRegOverallAnalysis_createdBy, rroa.createdDate as riskRegOverallAnalysis_createdDate, rroa.modifiedBy as riskRegOverallAnalysis_modifiedBy, rroa.modifiedDate as riskRegOverallAnalysis_modifiedDate, rroa.currentRiskRating as riskRegOverallAnalysis_currentRiskRating
		, 	rroa.inherentMonetaryImpact as riskRegOverallAnalysis_inherentMonetaryImpact, rroa.residualMonetaryImpact as riskRegOverallAnalysis_residualMonetaryImpact, rroa.currentMonetaryImpact as riskRegOverallAnalysis_currentMonetaryImpact
		, 	rroa.currentLikelihood as riskRegOverallAnalysis_currentLikelihood, rroa.currentImpact as riskRegOverallAnalysis_currentImpact, rroa.predictedRisk as riskRegOverallAnalysis_predictedRisk
	
		, 	rrp.riskRegRespPerson
		, 	rrs.riskRegSite
		, 	rcri.RiskItemCustomField
		, 	rcrr.RiskRegCustomField
		, rics.id as riskinstancecontrolstrengthId
		, rics.controlStrength
		, rics.riskAnalysisDimensionId
		, rad.value as riskAnalysisDimensionValue
		, p.id
        , p.name
    from cte   
	inner join processcategoryriskitem pcri on pcri.processCategoryId = cte.id
	inner join RiskRegisterItemApplicablity rria on rria.riskRegisterItemId = pcri.riskRegisterItemId
	LEFT JOIN product p ON rria.productID = p.id 
	inner join riskregisteritem rri on rri.id = rria.riskRegisterItemId
	inner join tempdb.riskregister rr on rr.riskRegisterItemId = rri.id
    JOIN predictuser pu on rr.createdBy = pu.id 
    left join riskevent re on rr.riskEventID = re.id
	left join riskregisteroverallanalysis rroa on rroa.riskId = rr.id
    LEFT JOIN riskinstancecontrolstrength rics ON rr.id = rics.riskRegisterId
    LEFT JOIN riskanalysisdimension rad ON rics.riskAnalysisDimensionId = rad.id
	left join tempdb.riskregisterresponsibleperson rrp on rrp.riskRegisterId = rr.id
	left join tempdb.riskregistersite rrs on rrs.riskRegisterId = rr.id
	left join tempdb.riskcustomfieldflattenedRiskItem rcri on rcri.RiskItem = rri.id
	left join tempdb.riskcustomfieldflattenedRiskReg rcrr on rcrr.RiskRegister = rr.id
	where rria.customerid = ",in_customerId," and rria.applicablity = 'Applicable' and ", 
    IF(
	in_BU IS NOT NULL,
    CONCAT("rria.buFacilityId in (",in_BU,") and "),
    ""
    ),
    " rria.isProcessCategory = 1 and rria.isDeleted = 0 ;");

	PREPARE stmp FROM @qry;
	EXECUTE stmp ;
	DEALLOCATE PREPARE stmp;

END IF;
 
 
END$$
DELIMITER ;
