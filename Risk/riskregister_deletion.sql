-- Finding customer id 
select * from customer c
where c.customerCode = 'CBI';
-- 747
-- creating backups 

drop table if exists riskregister_deleted_20221006;
create table riskregister_deleted_20221006 like riskregister;
insert into riskregister_deleted_20221006
select a.* from riskregister a
join riskregisteritem b on a.riskRegisterItemId = b.id
where b.customerId = 747;

drop table if exists riskregisteritemapplicablity_deleted_20221006;
create table riskregisteritemapplicablity_deleted_20221006 like riskregisteritemapplicablity;
insert into riskregisteritemapplicablity_deleted_20221006
select a.* from riskregisteritemapplicablity a
join riskregisteritem b on a.riskRegisterItemId = b.id
where b.customerId = 747;

drop table if exists riskregisteroverallanalysis_deleted_20221006;
create table riskregisteroverallanalysis_deleted_20221006 like riskregisteroverallanalysis;
insert into riskregisteroverallanalysis_deleted_20221006
select a.* from riskregisteroverallanalysis a
join riskregister b on a.riskId = b.id
where b.customerId = 747;

-- deletion

set sql_safe_updates = 0;

delete a.* from riskregisteroverallanalysis a
join riskregister b on a.riskId = b.id
where b.customerId = 747;

delete a.* from riskregister a
join riskregisteritem b on a.riskRegisterItemId = b.id
where b.customerId = 747;

delete a.* from riskregisteritemapplicablity a
join riskregisteritem b on a.riskRegisterItemId = b.id
where b.customerId = 747;

set sql_safe_updates = 1;

-- creating backup on EDW
-- FInding PK


select * from dim_tenant t
where t.BK_TenantCode = 'CBI'; -- 918

drop table if exists dim_risk_register_overall_analysis_deleted_20221006;
create table dim_risk_register_overall_analysis_deleted_20221006 like dim_risk_register_overall_analysis;
insert into dim_risk_register_overall_analysis_deleted_20221006
select a.* from dim_risk_register_overall_analysis a
join dim_risk_register b on a.FK_RiskRegisterId = b.PK_RiskRegisterId
where b.FK_TenantId = 918;

drop table if exists dim_risk_register_deleted_20221006;
create table dim_risk_register_deleted_20221006 like dim_risk_register;
insert into dim_risk_register_deleted_20221006
select a.* from dim_risk_register a 
join dim_risk_register_item b 
on a.FK_RiskRegisterItemId = b.PK_RiskRegisterItemId 
where b.FK_TenantId = 918;

drop table if exists dim_risk_register_applicablity_deleted_20221006;
create table dim_risk_register_applicablity_deleted_20221006 like dim_risk_register_applicablity;
insert into dim_risk_register_applicablity_deleted_20221006
select a.* from dim_risk_register_applicablity a
join dim_risk_register_item b on a.FK_RiskRegisterItemId = b.PK_RiskRegisterItemId
where b.FK_TenantId = 918;
-- deletion from EDW
set sql_safe_updates = 0;

delete a.* from dim_risk_register_overall_analysis a
join dim_risk_register b on a.FK_RiskRegisterId = b.PK_RiskRegisterId
where b.FK_TenantId = 918;

delete a.* from dim_risk_register a 
join dim_risk_register_item b 
on a.FK_RiskRegisterItemId = b.PK_RiskRegisterItemId 
where b.FK_TenantId = 918;

delete a.* from dim_risk_register_applicablity a
join dim_risk_register_item b on a.FK_RiskRegisterItemId = b.PK_RiskRegisterItemId
where b.FK_TenantId = 918;

set sql_safe_updates = 1;



