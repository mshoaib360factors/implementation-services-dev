drop procedure usp_RiskRollup;

DELIMITER $$
CREATE  PROCEDURE `usp_RiskRollup`(
    in_customerId bigint(20),
    in_userid bigint(20),
    in_roleid bigint(20),
    in_BU varchar(1000),
    in_PC BOOLEAN 
)
BEGIN

set sql_safe_updates = 0;

 

Drop temporary table If  exists `TempRiskItemsBU` ;
CREATE temporary TABLE `TempRiskItemsBU` (
`buFacilityId` bigint(20) NOT NULL DEFAULT '0',
`riskRegisterItemId` bigint(20) NOT NULL,
`parentId` bigint(20) DEFAULT NULL,
`customerId` bigint(20) DEFAULT NULL,
`riskId` bigint(20) NOT NULL,
`inherentLikelihood` double NOT NULL DEFAULT '0',
`inherentImpact` double NOT NULL DEFAULT '0',
`inherentRiskRating` double NOT NULL DEFAULT '0',
`residualLikelihood` double NOT NULL DEFAULT '0',
`residualImpact` double NOT NULL DEFAULT '0',
`residualRiskRating` double NOT NULL DEFAULT '0',
`currentRiskRating` double NOT NULL DEFAULT '0',
`predictedRisk` double DEFAULT null,
`weight` double NOT NULL DEFAULT '0',
`EffectiveDate` varchar(19) CHARACTER SET utf8 NOT NULL DEFAULT '',
KEY `Ix_buFacilityId` (`buFacilityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

if in_customerId = -2 then 
	if in_BU = -1 then 
		

		DROP TEMPORARY TABLE IF EXISTS tempdb.`TempWeightedUsrRiskRegisterCat`;
		CREATE TEMPORARY TABLE tempdb.`TempWeightedUsrRiskRegisterCat` (

		   `customerId` bigint(20) DEFAULT NULL,
		   `CatId` bigint(20) DEFAULT NULL,
		   `BU` bigint(20) DEFAULT NULL,
		   
		   `inherentLikelihood` double DEFAULT NULL,
		   `inherentImpact` double DEFAULT NULL,
		   `inherentRiskRating` double DEFAULT NULL,
		   
		   `residualLikelihood` double DEFAULT NULL,
		   `residualImpact` double DEFAULT NULL,
		   `residualRiskRating` double DEFAULT NULL,

		   `currentRiskRating` double DEFAULT NULL,
		   
		   `predictedRisk` double DEFAULT NULL,
		   
		   `weight` double DEFAULT NULL,
			inherentLikelihoodWeight double NOT NULL DEFAULT '0',
			inherentImpactweight double NOT NULL DEFAULT '0',
			inherentRiskRatingweight double NOT NULL DEFAULT '0',
			residualLikelihoodweight double NOT NULL DEFAULT '0',
			residualImpactweight double NOT NULL DEFAULT '0',
			residualRiskRatingweight double NOT NULL DEFAULT '0',
			currentRiskRatingweight double NOT NULL DEFAULT '0',
		   `kri_weight` double DEFAULT NULL,
		   `effectiveDate` datetime DEFAULT NULL,
			 KEY `IndexOnKeys` (`customerId`,`CatId`,`BU`)
		) ENGINE=InnoDB;
		
		
		
		set @qry = concat('
		Insert into TempRiskItemsBU 
		select distinct
		ifnull(rria.buFacilityId,-2500) as buFacilityId
		, rria.riskRegisterItemId',
		if (in_PC = TRUE , ', pcri.processCategoryId' , ', rrim.parentId'), ' as parentId 
		, rroa.customerId
		, rroa.riskId

		, ifnull(rroa.inherentLikelihood , 0) as inherentLikelihood
		, ifnull(rroa.inherentImpact , 0)     as inherentImpact
		, ifnull(rroa.inherentRiskRating , 0) as inherentRiskRating

		, ifnull(rroa.residualLikelihood , 0) as residualLikelihood
		, ifnull(rroa.residualImpact , 0)     as residualImpact
		, ifnull(rroa.residualRiskRating , 0) as residualRiskRating

		, ifnull(rroa.currentRiskRating  , 0) as currentRiskRating

		,rroa.predictedRisk

		, ifnull(rr.weight  , 1) as weight

		, greatest(ifnull(rri.updatedDate,rri.createdDate),ifnull(rr.modifiedDate,rr.createdDate),ifnull(rroa.modifiedDate,rroa.createdDate)) as EffectiveDate
		from RiskRegisterItemApplicablity rria #
		inner join RiskRegisterItem rri on rri.id = rria.riskRegisterItemId and rri.customerId = rria.customerId and rri.isDeleted = 0',

		if (in_PC = TRUE , '\n inner join processcategoryriskitem pcri on rri.id = pcri.riskRegisterItemId' , ' \n inner join RiskRegisterItemMapping rrim on rri.id = rrim.riskRegisterItemId and rrim.isDeleted = 0') ,


		'
		inner join RiskRegister rr on rr.riskRegisterItemId = rria.riskRegisterItemId and rr.buFacilityId <=> rria.buFacilityId and rr.isDeleted = 0
		and rr.customerId = rria.customerId and rr.customerId = rri.customerId
		inner join RiskRegisterOverallAnalysis rroa on rroa.customerId = rria.customerId and rroa.riskId = rr.id
		where rria.isDeleted = 0 and rria.applicablity=\'Applicable\' and rr.`status` = \'Active\' ;');
			
		prepare stmp from @qry;
		execute stmp ;
		deallocate prepare stmp;



	    	
		Drop temporary table if exists tempdb.RiskItemsGrm;
		Create temporary table tempdb.RiskItemsGrm
		(index Ix_Grp56 (customerId, CatId))
		select 
		distinct 
		  tri.riskRegisterItemId
	        , tri.riskId
		, round((inherentLikelihood * tri.weight),2) as inherentLikelihoodMultWeight
		, if(inherentLikelihood=0,0,tri.weight) as inherentLikelihoodWeight
			, round((inherentImpact * tri.weight),2) as inherentImpactMultWeight
		, if(inherentImpact=0,0,tri.weight) as inherentImpactWeight
			, round((inherentRiskRating *tri.weight),2) as inherentRiskRatingMultWeight
			, if(inherentRiskRating=0,0,tri.weight) as inherentRiskRatingWeight
			, round((residualLikelihood * tri.weight),2) as residualLikelihoodMultWeight
		, if(residualLikelihood=0,0,tri.weight) as residualLikelihoodWeight
			, round((residualImpact * tri.weight),2) as residualImpactMultWeight
		, if(residualImpact=0,0,tri.weight) as residualImpactWeight
			, round((residualRiskRating * tri.weight),2) as residualRiskRatingMultWeight
		, if(residualRiskRating=0,0,tri.weight) as residualRiskRatingWeight
			, round((currentRiskRating  * tri.weight),2) as currentRiskRatingMultWeight
			, if(currentRiskRating=0,0,tri.weight) as currentRiskRatingWeight
			, round(if(Kri.riskregisterItemId is not null and krid.status = 1,tri.predictedRisk * tri.weight, null),2) as predictedRisk		
		, if(Kri.riskregisterItemId is not null and krid.status = 1 and tri.predictedRisk <> 0, tri.weight, null) as  kri_weight
		, tri.weight, parentId as CatId, tri.customerId, EffectiveDate
		, buFacilityId as BU
		from TempRiskItemsBU tri
		left join Kri on tri.riskRegisterItemId = Kri.riskRegisterItemId
		left join KriDefination krid on Kri.kriDefinationId = krid.id;

		
		Drop temporary table if exists tempdb.GroupCat2;
		Create temporary table tempdb.GroupCat2
		(index Ix_Grp78 (CatId))
		SELECT 
		  customerId, CatId, BU
		, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk
		, sum(weight) as weight
		, sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
		, sum(inherentImpactweight) as inherentImpactweight
		, sum(inherentRiskRatingweight) as inherentRiskRatingweight
		, sum(residualLikelihoodweight) as residualLikelihoodweight
		, sum(residualImpactweight) as residualImpactweight
		, sum(residualRiskRatingweight) as residualRiskRatingweight
		, sum(currentRiskRatingweight) as currentRiskRatingweight
		, sum(kri_weight) as kri_weight
		, max(EffectiveDate) as EffectiveDate  
		FROM tempdb.RiskItemsGrm 
		group by customerId, CatId, BU ;
		 


		

		set @WhileHeirarchy =1;

		while @WhileHeirarchy > 0 Do 

		insert into tempdb.`TempWeightedUsrRiskRegisterCat`  
		(customerId, CatId,BU, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
		inherentLikelihoodWeight,
		inherentImpactweight,
		inherentRiskRatingweight,
		residualLikelihoodweight,
		residualImpactweight,
		residualRiskRatingweight,
		currentRiskRatingweight,
		kri_weight, EffectiveDate)
		select 
		customerId, CatId,BU, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
		inherentLikelihoodWeight,
		inherentImpactweight,
		inherentRiskRatingweight,
		residualLikelihoodweight,
		residualImpactweight,
		residualRiskRatingweight,
		currentRiskRatingweight,
		kri_weight, EffectiveDate
		From tempdb.GroupCat2 where CatId is not null;

		Drop temporary table if exists tempdb.GroupCat;
		Create temporary table tempdb.GroupCat
		select * from tempdb.GroupCat2 where CatId is not null;

		
		Drop temporary table if exists tempdb.GroupCatMultWeight;
		set @qry = concat('
		create temporary table tempdb.GroupCatMultWeight
		(index Ix_Grp91 (customerId, CatId))
		select a.customerId, c.parentId  as CatId, a.BU

		, round((a.inherentLikelihood * a.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
		, round((a.inherentImpact * a.inherentImpactweight),2) as inherentImpactMultWeight
		, round((a.inherentRiskRating * a.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

		, round((a.residualLikelihood * a.residualLikelihoodweight),2) as residualLikelihoodMultWeight
		, round((a.residualImpact * a.residualImpactweight),2) as residualImpactMultWeight
		, round((a.residualRiskRating * a.residualRiskRatingweight),2) as residualRiskRatingMultWeight

		, round((a.currentRiskRating  * a.currentRiskRatingweight),2) as currentRiskRatingMultWeight

		, round(a.predictedRisk * a.kri_weight,2) as predictedRisk

		, a.weight
		,a.inherentLikelihoodWeight,
		a.inherentImpactweight,
		a.inherentRiskRatingweight,
		a.residualLikelihoodweight,
		a.residualImpactweight,
		a.residualRiskRatingweight,
		a.currentRiskRatingweight,
		a.kri_weight
		, EffectiveDate
		from  tempdb.GroupCat a ',
		if(in_PC = TRUE, 'inner join processcategory c on c.id =a.catId and c.isDeleted = 0', 
		'\n inner join RiskRegisterItem b on b.id =a.catId and b.isDeleted = 0
		inner join RiskRegisterItemMapping c on b.id = c.riskRegisterItemId and c.isDeleted = 0'), ';'
		);

		
		prepare stmp from @qry;
		execute stmp ;
		deallocate prepare stmp;

		
		
		Drop temporary table if exists tempdb.CatIdAlreadyExists;
		create temporary table tempdb.CatIdAlreadyExists
		(index Ix_Grp1 (customerId, CatId))
		select distinct 
		e.customerId, e.CatId, e.BU
		, round((e.inherentLikelihood * e.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
		, round((e.inherentImpact * e.inherentImpactweight),2) as inherentImpactMultWeight
		, round((e.inherentRiskRating * e.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

		, round((e.residualLikelihood * e.residualLikelihoodweight),2) as residualLikelihoodMultWeight
		, round((e.residualImpact * e.residualImpactweight),2) as residualImpactMultWeight
		, round((e.residualRiskRating * e.residualRiskRatingweight),2) as residualRiskRatingMultWeight

		, round((e.currentRiskRating  * e.currentRiskRatingweight),2) as currentRiskRatingMultWeight

		, round(e.predictedRisk * e.kri_weight,2)  as predictedRisk
		, e.weight,
		e.inherentLikelihoodWeight,
		e.inherentImpactweight,
		e.inherentRiskRatingweight,
		e.residualLikelihoodweight,
		e.residualImpactweight,
		e.residualRiskRatingweight,
		e.currentRiskRatingweight,
		 e.kri_weight
		, e.EffectiveDate
		 from  tempdb.GroupCatMultWeight n
		inner join tempdb.`TempWeightedUsrRiskRegisterCat` e on  e.customerId = n.customerId 
		and e.catId = n.catId and e.BU = n.BU;

		
		insert into  tempdb.GroupCatMultWeight
		select *  from  tempdb.CatIdAlreadyExists;

		
		Delete twc from  tempdb.TempWeightedUsrRiskRegisterCat twc
		inner join  tempdb.CatIdAlreadyExists e on  e.customerId = twc.customerId 
		and e.catId = twc.catId and e.BU = twc.BU; 


		
		Drop temporary table if exists tempdb.GroupCat2;
		create temporary table tempdb.GroupCat2
		(index Ix_Grp2 (CatId))
		select a.customerId,a.CatId , a.BU
		, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk

		, sum(weight) as weight
		, sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
		, sum(inherentImpactweight) as inherentImpactweight
		, sum(inherentRiskRatingweight) as inherentRiskRatingweight
		, sum(residualLikelihoodweight) as residualLikelihoodweight
		, sum(residualImpactweight) as residualImpactweight
		, sum(residualRiskRatingweight) as residualRiskRatingweight
		, sum(currentRiskRatingweight) as currentRiskRatingweight
		, sum(kri_weight) as kri_weight
		, max(EffectiveDate) as EffectiveDate 
		 from  tempdb.GroupCatMultWeight a
		group by a.customerId, a.CatId, a.BU ;


		set @WhileHeirarchy = (select count(1) from tempdb.GroupCat2);

		end while;
		
		Select 
		customerId
		, CatId as riskRegisterItemCatLevelId
		, BU
		, inherentLikelihood as weightedInherentLikelihood
		, inherentImpact as weightedInherentImpact
		, inherentRiskRating as weightedInherentRiskRating

		, residualLikelihood as weightedResidualLikelihood
		, residualImpact as weightedResidualImpact
		, residualRiskRating as weightedResidualRiskRating

		, currentRiskRating as weightedCurrentRiskRating

		, predictedRisk as weightedPredictedRisk

		, EffectiveDate as effectiveDate
		from tempdb.`TempWeightedUsrRiskRegisterCat`;

	else
		
		
	        
	        
	        
    
		DROP TEMPORARY TABLE IF EXISTS tempdb.`TempWeightedUsrRiskRegisterCat`;
		CREATE TEMPORARY TABLE tempdb.`TempWeightedUsrRiskRegisterCat` (

		   `customerId` bigint(20) DEFAULT NULL,
		   `userId` bigint(20) NOT NULL,

		   `CatId` bigint(20) DEFAULT NULL,
		   
		   `inherentLikelihood` double DEFAULT NULL,
		   `inherentImpact` double DEFAULT NULL,
		   `inherentRiskRating` double DEFAULT NULL,
		   
		   `residualLikelihood` double DEFAULT NULL,
		   `residualImpact` double DEFAULT NULL,
		   `residualRiskRating` double DEFAULT NULL,

		   `currentRiskRating` double DEFAULT NULL,
		   
		   `predictedRisk` double DEFAULT NULL,
		   
		   `weight` double DEFAULT NULL,
	   		inherentLikelihoodWeight double NOT NULL DEFAULT '0',
			inherentImpactweight double NOT NULL DEFAULT '0',
			inherentRiskRatingweight double NOT NULL DEFAULT '0',
			residualLikelihoodweight double NOT NULL DEFAULT '0',
			residualImpactweight double NOT NULL DEFAULT '0',
			residualRiskRatingweight double NOT NULL DEFAULT '0',
			currentRiskRatingweight double NOT NULL DEFAULT '0',
		   `kri_weight` double DEFAULT NULL,
		   `effectiveDate` datetime DEFAULT NULL,
			 KEY `IndexOnKeys` (`customerId`,`userId`,`CatId`)
		) ENGINE=InnoDB;
        

		
		set @qry = concat(
	        'Insert into TempRiskItemsBU
	        select distinct
	    	ifnull(rria.buFacilityId,-2500) as buFacilityId
		, rria.riskRegisterItemId' ,
        	if (in_PC = TRUE , ', pcri.processCategoryId' , ', rrim.parentId'), ' as parentId 
		, rroa.customerId
		, rroa.riskId

		, ifnull(rroa.inherentLikelihood , 0) as inherentLikelihood
		, ifnull(rroa.inherentImpact , 0)     as inherentImpact
		, ifnull(rroa.inherentRiskRating , 0) as inherentRiskRating

		, ifnull(rroa.residualLikelihood , 0) as residualLikelihood
		, ifnull(rroa.residualImpact , 0)     as residualImpact
		, ifnull(rroa.residualRiskRating , 0) as residualRiskRating

		, ifnull(rroa.currentRiskRating  , 0) as currentRiskRating

		,  predictedRisk

		, ifnull(rr.weight  , 1) as weight

		, greatest(ifnull(rri.updatedDate,rri.createdDate),ifnull(rr.modifiedDate,rr.createdDate),ifnull(rroa.modifiedDate,rroa.createdDate)) as EffectiveDate
		
        	from RiskRegisterItemApplicablity rria #
		inner join RiskRegisterItem rri on rri.id = rria.riskRegisterItemId and rri.customerId = rria.customerId and rri.isDeleted = 0',
        
        	if (in_PC = TRUE , '\n inner join processcategoryriskitem pcri on rri.id = pcri.riskRegisterItemId' , ' \n inner join RiskRegisterItemMapping rrim on rri.id = rrim.riskRegisterItemId and rrim.isDeleted = 0') ,
		
		'
		inner join RiskRegister rr on rr.riskRegisterItemId = rria.riskRegisterItemId and rr.buFacilityId <=> rria.buFacilityId	and rr.customerId = rria.customerId and rr.customerId = rri.customerId 
		inner join RiskRegisterOverallAnalysis rroa on rroa.customerId = rria.customerId and rroa.riskId = rr.id
		left join Facility f on f.id = ifnull(rria.buFacilityId,-2500)  
		left join FacilityRole fr on fr.facilityId = f.id  
		left join UserRole ur on fr.roleId = ur.roleId
	    	where rria.isDeleted = 0 and rria.applicablity=\'Applicable\'	and rri.isDeleted = 0 and rr.`status` = \'Active\';');
		
		prepare stmp from @qry;
		execute stmp ;
		deallocate prepare stmp;
		
	    	Drop temporary table if exists tempdb.UserRiskItems;
		
        	create temporary table tempdb.UserRiskItems
		(index Ix_Grp3 (customerId, userId, parentId))
		select 
		 ur.userId
		, a.riskRegisterItemId
		, round((inherentLikelihood *a.weight),2) as inherentLikelihoodMultWeight
        	, if(inherentLikelihood=0,0,a.weight) as inherentLikelihoodWeight
		, round((inherentImpact * a.weight),2) as inherentImpactMultWeight
        	, if(inherentImpact=0,0,a.weight) as inherentImpactWeight
		, round((inherentRiskRating *a.weight),2) as inherentRiskRatingMultWeight
		, if(inherentRiskRating=0,0,a.weight) as inherentRiskRatingWeight
		, round((residualLikelihood * a.weight),2) as residualLikelihoodMultWeight
        	, if(residualLikelihood=0,0,a.weight) as residualLikelihoodWeight
		, round((residualImpact * a.weight),2) as residualImpactMultWeight
        	, if(residualImpact=0,0,a.weight) as residualImpactWeight
		, round((residualRiskRating * a.weight),2) as residualRiskRatingMultWeight
        	, if(residualRiskRating=0,0,a.weight) as residualRiskRatingWeight
		, round((currentRiskRating  * a.weight),2) as currentRiskRatingMultWeight
		, if(currentRiskRating=0,0,a.weight) as currentRiskRatingWeight
		, round(if(Kri.riskregisterItemId is not null and krid.status = 1,a.predictedRisk * a.weight, null),2) as predictedRisk
		, if(Kri.riskregisterItemId is not null and krid.status = 1 and a.predictedRisk <> 0, a.weight, null) as  kri_weight
		, a.weight
		, a.parentId
		, a.customerId
		, a.EffectiveDate
		, krid.id
		From TempRiskItemsBU a  
		left join Kri on a.riskRegisterItemId = Kri.riskRegisterItemId
		left join KriDefination krid on Kri.kriDefinationId = krid.id
		inner join Facility f on f.id = a.buFacilityId  
		inner join FacilityRole fr on fr.facilityId = f.id  
		inner join UserRole ur on fr.roleId = ur.roleId
		where a.buFacilityId <> -2500;


		
		Drop temporary table if exists tempdb.GroupCat2;
		Create temporary table tempdb.GroupCat2
		(index Ix_CatId (CatId))
		SELECT 
		  customerId, userId, parentId as CatId
		, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk
		, sum(weight) as weight
		, sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
        	, sum(inherentImpactweight) as inherentImpactweight
        	, sum(inherentRiskRatingweight) as inherentRiskRatingweight
        	, sum(residualLikelihoodweight) as residualLikelihoodweight
        	, sum(residualImpactweight) as residualImpactweight
        	, sum(residualRiskRatingweight) as residualRiskRatingweight
        	, sum(currentRiskRatingweight) as currentRiskRatingweight
		, sum(kri_weight) as kri_weight


		, max(EffectiveDate) as EffectiveDate  
		FROM tempdb.UserRiskItems
		group by customerId, userId, parentId ;
		

		

		set @WhileHeirarchy =1;

		while @WhileHeirarchy > 0 Do 

			insert into tempdb.`TempWeightedUsrRiskRegisterCat`  
			(customerId, userId, CatId, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
	        	inherentLikelihoodWeight,
			inherentImpactweight,
			inherentRiskRatingweight,
			residualLikelihoodweight,
			residualImpactweight,
			residualRiskRatingweight,
			currentRiskRatingweight,
	        	kri_weight, EffectiveDate)
			select 
			customerId, userId,  CatId, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
	        	inherentLikelihoodWeight,
			inherentImpactweight,
			inherentRiskRatingweight,
			residualLikelihoodweight,
			residualImpactweight,
			residualRiskRatingweight,
			currentRiskRatingweight,
	        	kri_weight, EffectiveDate
			From tempdb.GroupCat2 where CatId is not null;

			Drop temporary table if exists tempdb.GroupCat;
			Create temporary table tempdb.GroupCat
			(index Ix_GrpCat (customerId, CatId))
			select * from tempdb.GroupCat2 where CatId is not null;

			
			Drop temporary table if exists tempdb.GroupCatMultWeight;
			set @qry = concat(
			'create temporary table tempdb.GroupCatMultWeight
			(index Ix_Grp12 (customerId,userId, CatId))
			select a.customerId, a.userId,  c.parentId as catId 

			, round((a.inherentLikelihood * a.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
			, round((a.inherentImpact * a.inherentImpactweight),2) as inherentImpactMultWeight
			, round((a.inherentRiskRating * a.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

			, round((a.residualLikelihood * a.residualLikelihoodweight),2) as residualLikelihoodMultWeight
			, round((a.residualImpact * a.residualImpactweight),2) as residualImpactMultWeight
			, round((a.residualRiskRating * a.residualRiskRatingweight),2) as residualRiskRatingMultWeight

			, round((a.currentRiskRating  * a.currentRiskRatingweight),2) as currentRiskRatingMultWeight

			, round(a.predictedRisk * a.kri_weight,2) as predictedRisk

			, a.weight
	        	,a.inherentLikelihoodWeight,
			a.inherentImpactweight,
			a.inherentRiskRatingweight,
			a.residualLikelihoodweight,
			a.residualImpactweight,
			a.residualRiskRatingweight,
			a.currentRiskRatingweight
			, a.kri_weight
			, EffectiveDate

			 from  tempdb.GroupCat a ', 
			if(in_PC = TRUE, 'inner join processcategory c on c.id =a.catId  and c.isDeleted = 0', 
			' inner join RiskRegisterItem b on b.id =a.catId and b.isDeleted = 0
			 inner join RiskRegisterItemMapping c on b.id = c.riskRegisterItemId  and c.isDeleted = 0'), ';');
			 
			prepare stmp from @qry;
			execute stmp ;
			deallocate prepare stmp;

			


			
			Drop temporary table if exists tempdb.CatIdAlreadyExists;
			create temporary table tempdb.CatIdAlreadyExists
			(index Ix_Grp34 (customerId,userId, CatId))
			select  distinct
			e.customerId, e.userId, e.CatId
			, round((e.inherentLikelihood * e.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
			, round((e.inherentImpact * e.inherentImpactweight),2) as inherentImpactMultWeight
			, round((e.inherentRiskRating * e.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

			, round((e.residualLikelihood * e.residualLikelihoodweight),2) as residualLikelihoodMultWeight
			, round((e.residualImpact * e.residualImpactweight),2) as residualImpactMultWeight
			, round((e.residualRiskRating * e.residualRiskRatingweight),2) as residualRiskRatingMultWeight

			, round((e.currentRiskRating  * e.currentRiskRatingweight),2) as currentRiskRatingMultWeight

			, round(e.predictedRisk * e.kri_weight,2)  as predictedRisk
			, e.weight,
	        	e.inherentLikelihoodWeight,
			e.inherentImpactweight,
			e.inherentRiskRatingweight,
			e.residualLikelihoodweight,
			e.residualImpactweight,
			e.residualRiskRatingweight,
			e.currentRiskRatingweight
			, e.kri_weight
			, e.EffectiveDate
			 from  tempdb.GroupCatMultWeight n
			inner join tempdb.`TempWeightedUsrRiskRegisterCat` e on  e.customerId = n.customerId and e.userId = n.userId and e.catId = n.catId;

			
			insert into  tempdb.GroupCatMultWeight
			select *  from  tempdb.CatIdAlreadyExists;

			
			Delete twc from  tempdb.TempWeightedUsrRiskRegisterCat twc
			inner join  tempdb.CatIdAlreadyExists e on  e.customerId = twc.customerId and e.userId = twc.userId and e.catId = twc.catId; 

			
			Drop temporary table if exists tempdb.GroupCat2;
			create temporary table tempdb.GroupCat2
			(index Ix_CatId (CatId))
			select a.customerId, a.userId, a.catId
			, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk

			, sum(weight) as weight
		        , sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
		        , sum(inherentImpactweight) as inherentImpactweight
		        , sum(inherentRiskRatingweight) as inherentRiskRatingweight
		        , sum(residualLikelihoodweight) as residualLikelihoodweight
		        , sum(residualImpactweight) as residualImpactweight
		        , sum(residualRiskRatingweight) as residualRiskRatingweight
		        , sum(currentRiskRatingweight) as currentRiskRatingweight
			, sum(kri_weight) as kri_weight
			, max(EffectiveDate) as EffectiveDate 
			 from  tempdb.GroupCatMultWeight a
			group by a.customerId, a.userId, a.catId ;


			set @WhileHeirarchy = (select count(1) from tempdb.GroupCat2);


		end while;


	        
	        
	        

		Drop temporary table if exists tempdb.RiskItemsGrm;
		Create temporary table tempdb.RiskItemsGrm
		(index Ix_Grp56 (customerId,userId, CatId))
		select 
		distinct 
		  -2500 as UserId
		, tri.riskRegisterItemId
                , riskId
		, round((inherentLikelihood *tri.weight),2) as inherentLikelihoodMultWeight
        	, if(inherentLikelihood=0,0,tri.weight) as inherentLikelihoodWeight
		, round((inherentImpact * tri.weight),2) as inherentImpactMultWeight
        	, if(inherentImpact=0,0,tri.weight) as inherentImpactWeight
		, round((inherentRiskRating *tri.weight),2) as inherentRiskRatingMultWeight
		, if(inherentRiskRating=0,0,tri.weight) as inherentRiskRatingWeight
		, round((residualLikelihood * tri.weight),2) as residualLikelihoodMultWeight
        	, if(residualLikelihood=0,0,tri.weight) as residualLikelihoodWeight
		, round((residualImpact * tri.weight),2) as residualImpactMultWeight
        	, if(residualImpact=0,0,tri.weight) as residualImpactWeight
		, round((residualRiskRating * tri.weight),2) as residualRiskRatingMultWeight
        	, if(residualRiskRating=0,0,tri.weight) as residualRiskRatingWeight
		, round((currentRiskRating  * tri.weight),2) as currentRiskRatingMultWeight
		, if(currentRiskRating=0,0,tri.weight) as currentRiskRatingWeight
		, round(if(Kri.riskregisterItemId is not null and krid.status = 1,tri.predictedRisk * tri.weight, null),2) as predictedRisk		
        	, if(Kri.riskregisterItemId is not null and krid.status = 1 and tri.predictedRisk <> 0, tri.weight, null) as  kri_weight
		, tri.weight,
		parentId as CatId, tri.customerId, EffectiveDate
		from TempRiskItemsBU tri
		left join Kri on tri.riskRegisterItemId = Kri.riskRegisterItemId
		left join KriDefination krid on Kri.kriDefinationId = krid.id;
		

		
		Drop temporary table if exists tempdb.GroupCat2;
		Create temporary table tempdb.GroupCat2
		(index Ix_Grp78 (CatId))
		SELECT 
		  customerId, userId, CatId
		, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk
		, sum(weight) as weight
		, sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
	        , sum(inherentImpactweight) as inherentImpactweight
	        , sum(inherentRiskRatingweight) as inherentRiskRatingweight
	        , sum(residualLikelihoodweight) as residualLikelihoodweight
	        , sum(residualImpactweight) as residualImpactweight
	        , sum(residualRiskRatingweight) as residualRiskRatingweight
	        , sum(currentRiskRatingweight) as currentRiskRatingweight
		, sum(kri_weight) as kri_weight
		, max(EffectiveDate) as EffectiveDate  
		FROM tempdb.RiskItemsGrm 
		group by customerId, userId, CatId ;
		 

	
		

		set @WhileHeirarchy =1;

		while @WhileHeirarchy > 0 Do 

			insert into tempdb.`TempWeightedUsrRiskRegisterCat`  
			(customerId, userId, CatId, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
	        	inherentLikelihoodWeight,
			inherentImpactweight,
			inherentRiskRatingweight,
			residualLikelihoodweight,
			residualImpactweight,
			residualRiskRatingweight,
			currentRiskRatingweight,
	        	kri_weight,  EffectiveDate)
			select 
			customerId, userId,  CatId, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
	        	inherentLikelihoodWeight,
			inherentImpactweight,
			inherentRiskRatingweight,
			residualLikelihoodweight,
			residualImpactweight,
			residualRiskRatingweight,
			currentRiskRatingweight,kri_weight,  EffectiveDate
			From tempdb.GroupCat2 where CatId is not null;

			Drop temporary table if exists tempdb.GroupCat;
			Create temporary table tempdb.GroupCat
			select * from tempdb.GroupCat2 where CatId is not null;

			 
			Drop temporary table if exists tempdb.GroupCatMultWeight;
			set @qry = concat(
			'create temporary table tempdb.GroupCatMultWeight
			(index Ix_Grp91 (customerId,userId, CatId))
			select a.customerId, a.userId, c.parentId  as CatId

			, round((a.inherentLikelihood * a.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
			, round((a.inherentImpact * a.inherentImpactweight),2) as inherentImpactMultWeight
			, round((a.inherentRiskRating * a.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

			, round((a.residualLikelihood * a.residualLikelihoodweight),2) as residualLikelihoodMultWeight
			, round((a.residualImpact * a.residualImpactweight),2) as residualImpactMultWeight
			, round((a.residualRiskRating * a.residualRiskRatingweight),2) as residualRiskRatingMultWeight

			, round((a.currentRiskRating  * a.currentRiskRatingweight),2) as currentRiskRatingMultWeight

			, round(a.predictedRisk * a.kri_weight,2) as predictedRisk

			, a.weight
	        	,a.inherentLikelihoodWeight,
			a.inherentImpactweight,
			a.inherentRiskRatingweight,
			a.residualLikelihoodweight,
			a.residualImpactweight,
			a.residualRiskRatingweight,
			a.currentRiskRatingweight,
			a.kri_weight
			, EffectiveDate
			 from  tempdb.GroupCat a', 
			if(in_PC = TRUE, ' inner join processcategory c on c.id =a.catId and c.isDeleted = 0 ', 
			' inner join RiskRegisterItem b on b.id =a.catId and b.isDeleted = 0
			 inner join RiskRegisterItemMapping c on b.id = c.riskRegisterItemId and c.isDeleted = 0'), ';');
			
			prepare stmp from @qry;
			execute stmp ;
			deallocate prepare stmp;

			
			
			Drop temporary table if exists tempdb.CatIdAlreadyExists;
			create temporary table tempdb.CatIdAlreadyExists
			(index Ix_Grp1 (customerId,userId, CatId))
			select  distinct
			e.customerId, e.userId, e.CatId
			, round((e.inherentLikelihood * e.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
			, round((e.inherentImpact * e.inherentImpactweight),2) as inherentImpactMultWeight
			, round((e.inherentRiskRating * e.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

			, round((e.residualLikelihood * e.residualLikelihoodweight),2) as residualLikelihoodMultWeight
			, round((e.residualImpact * e.residualImpactweight),2) as residualImpactMultWeight
			, round((e.residualRiskRating * e.residualRiskRatingweight),2) as residualRiskRatingMultWeight

			, round((e.currentRiskRating  * e.currentRiskRatingweight),2) as currentRiskRatingMultWeight

			, round(e.predictedRisk * e.kri_weight,2)  as predictedRisk
			, e.weight,
	        	e.inherentLikelihoodWeight,
			e.inherentImpactweight,
			e.inherentRiskRatingweight,
			e.residualLikelihoodweight,
			e.residualImpactweight,
			e.residualRiskRatingweight,
			e.currentRiskRatingweight,
			 e.kri_weight
			, e.EffectiveDate
			 from  tempdb.GroupCatMultWeight n
			inner join tempdb.`TempWeightedUsrRiskRegisterCat` e on  e.customerId = n.customerId and e.userId = n.userId and e.catId = n.catId;

			
			insert into  tempdb.GroupCatMultWeight
			select *  from  tempdb.CatIdAlreadyExists;
			

			
			Delete twc from  tempdb.TempWeightedUsrRiskRegisterCat twc
			inner join  tempdb.CatIdAlreadyExists e on  e.customerId = twc.customerId and e.userId = twc.userId and e.catId = twc.catId; 



			
			Drop temporary table if exists tempdb.GroupCat2;
			create temporary table tempdb.GroupCat2
			(index Ix_Grp2 (CatId))
			select a.customerId, a.userId, a.CatId 
			, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk

			, sum(weight) as weight
		        , sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
		        , sum(inherentImpactweight) as inherentImpactweight
		        , sum(inherentRiskRatingweight) as inherentRiskRatingweight
		        , sum(residualLikelihoodweight) as residualLikelihoodweight
		        , sum(residualImpactweight) as residualImpactweight
		        , sum(residualRiskRatingweight) as residualRiskRatingweight
		        , sum(currentRiskRatingweight) as currentRiskRatingweight
			, sum(kri_weight) as kri_weight
			, max(EffectiveDate) as EffectiveDate 
			 from  tempdb.GroupCatMultWeight a
			group by a.customerId, a.userId, a.CatId ;


			set @WhileHeirarchy = (select count(1) from tempdb.GroupCat2);


		end while;

		Select 
		customerId
		, case when userId = -2500 then null else userId end as userId
		, case when userId = -2500 then userId else null end as roleId
		, CatId as riskRegisterItemCatLevelId

		, inherentLikelihood as weightedInherentLikelihood
		, inherentImpact as weightedInherentImpact
		, inherentRiskRating as weightedInherentRiskRating

		, residualLikelihood as weightedResidualLikelihood
		, residualImpact as weightedResidualImpact
		, residualRiskRating as weightedResidualRiskRating

		, currentRiskRating as weightedCurrentRiskRating

		, predictedRisk as weightedPredictedRisk

		, EffectiveDate as effectiveDate
		from tempdb.`TempWeightedUsrRiskRegisterCat`;
    	end if;
else


	
	
	

	
	if in_BU is null or in_BU = '' then 
		set @qry = concat(
		'Insert into TempRiskItemsBU
		select distinct
		ifnull(rria.buFacilityId,-2500) as buFacilityId
		, rria.riskRegisterItemId ' ,
	        if (in_PC = TRUE , ', pcri.processCategoryId' , ', rrim.parentId'), ' as parentId 
	        
		, rroa.customerId
		, rroa.riskId

		, ifnull(rroa.inherentLikelihood , 0) as inherentLikelihood
		, ifnull(rroa.inherentImpact , 0)     as inherentImpact
		, ifnull(rroa.inherentRiskRating , 0) as inherentRiskRating

		, ifnull(rroa.residualLikelihood , 0) as residualLikelihood
		, ifnull(rroa.residualImpact , 0)     as residualImpact
		, ifnull(rroa.residualRiskRating , 0) as residualRiskRating

		, ifnull(rroa.currentRiskRating  , 0) as currentRiskRating

		, predictedRisk

		, ifnull(rr.weight  , 1) as weight

		, greatest(ifnull(rri.updatedDate,rri.createdDate),ifnull(rr.modifiedDate,rr.createdDate),ifnull(rroa.modifiedDate,rroa.createdDate)) as EffectiveDate
		from RiskRegisterItemApplicablity rria 
		inner join RiskRegisterItem rri on rri.id = rria.riskRegisterItemId and rri.customerId = rria.customerId and rri.isDeleted = 0',

	        if (in_PC = TRUE , '\n inner join processcategoryriskitem pcri on rri.id = pcri.riskRegisterItemId' , ' \n inner join RiskRegisterItemMapping rrim on rri.id = rrim.riskRegisterItemId and rrim.isDeleted = 0') ,
			
			'
	        inner join RiskRegister rr on rr.riskRegisterItemId = rria.riskRegisterItemId and rr.buFacilityId <=> rria.buFacilityId	and rr.customerId = rria.customerId and rr.customerId = rri.customerId
		inner join RiskRegisterOverallAnalysis rroa on rroa.customerId = rria.customerId and rroa.riskId = rr.id
		left join Facility f on f.id = ifnull(rria.buFacilityId,-2500)  
		left join FacilityRole fr on fr.facilityId = f.id  
		left join UserRole ur on fr.roleId = ur.roleId
		where rria.customerId = ',in_customerId,' and  ',
	        
		if(in_roleid=-2500,'',concat('ur.userid = ',in_userid,' and (rria.buFacilityId <> -2500 or rria.buFacilityId is not null) and')),
		'rria.isDeleted = 0 and rria.applicablity=\'Applicable\'
		and rr.`status` = \'Active\';');

		
		
		prepare stmp from @qry;
		execute stmp ;
		deallocate prepare stmp;
	else
		
		
		
		set @qry = concat(
		'Insert into TempRiskItemsBU
		select distinct
		ifnull(rria.buFacilityId,-2500) as buFacilityId
		, rria.riskRegisterItemId ' ,
        	if (in_PC = TRUE , ', pcri.processCategoryId' , ', rrim.parentId'), ' as parentId 
		
		, rroa.customerId
		, rroa.riskId

		, ifnull(rroa.inherentLikelihood , 0) as inherentLikelihood
		, ifnull(rroa.inherentImpact , 0)     as inherentImpact
		, ifnull(rroa.inherentRiskRating , 0) as inherentRiskRating

		, ifnull(rroa.residualLikelihood , 0) as residualLikelihood
		, ifnull(rroa.residualImpact , 0)     as residualImpact
		, ifnull(rroa.residualRiskRating , 0) as residualRiskRating

		, ifnull(rroa.currentRiskRating  , 0) as currentRiskRating

		, predictedRisk

		, ifnull(rr.weight  , 1) as weight

		, greatest(ifnull(rri.updatedDate,rri.createdDate),ifnull(rr.modifiedDate,rr.createdDate),ifnull(rroa.modifiedDate,rroa.createdDate)) as EffectiveDate
		from RiskRegisterItemApplicablity rria #
		inner join RiskRegisterItem rri on rri.id = rria.riskRegisterItemId and rri.customerId = rria.customerId and rri.isDeleted = 0',
        
        	if (in_PC = TRUE , '\n inner join processcategoryriskitem pcri on rri.id = pcri.riskRegisterItemId' , ' \n inner join RiskRegisterItemMapping rrim on rri.id = rrim.riskRegisterItemId and rrim.isDeleted = 0') ,
		
		'
		
		inner join RiskRegister rr on rr.riskRegisterItemId = rria.riskRegisterItemId and rr.buFacilityId = rria.buFacilityId and rr.customerId = rria.customerId and rr.customerId = rri.customerId
		inner join RiskRegisterOverallAnalysis rroa on rroa.customerId = rria.customerId and rroa.riskId = rr.id
		left join Facility f on f.id = ifnull(rria.buFacilityId,-2500)  
		left join FacilityRole fr on fr.facilityId = f.id  
		left join UserRole ur on fr.roleId = ur.roleId
		where rria.customerId = ',in_customerId,' and ',
		
		if(in_roleid=-2500,'',concat('ur.userid = ',in_userid,' and (rria.buFacilityId <> -2500 or rria.buFacilityId is not null) and')),
		'rria.isDeleted = 0 and rria.applicablity=\'Applicable\' and rria.buFacilityId in (',in_BU,')
		and rr.`status` = \'Active\';');
		
		prepare stmp from @qry;
		execute stmp ;
		deallocate prepare stmp;
	end if;

	
	DROP TEMPORARY TABLE IF EXISTS tempdb.`TempWeightedUsrRiskRegisterCat`;
	CREATE TEMPORARY TABLE tempdb.`TempWeightedUsrRiskRegisterCat` (
	   `CatId` bigint(20) DEFAULT NULL,
	   `inherentLikelihood` double DEFAULT NULL,
	   `inherentImpact` double DEFAULT NULL,
	   `inherentRiskRating` double DEFAULT NULL,
	   
	   `residualLikelihood` double DEFAULT NULL,
	   `residualImpact` double DEFAULT NULL,
	   `residualRiskRating` double DEFAULT NULL,

	   `currentRiskRating` double DEFAULT NULL,
	   
	   `predictedRisk` double DEFAULT NULL,
	   
	   `weight` double DEFAULT NULL,
	   inherentLikelihoodWeight double NOT NULL DEFAULT '0',
	inherentImpactweight double NOT NULL DEFAULT '0',
	inherentRiskRatingweight double NOT NULL DEFAULT '0',
	residualLikelihoodweight double NOT NULL DEFAULT '0',
	residualImpactweight double NOT NULL DEFAULT '0',
	residualRiskRatingweight double NOT NULL DEFAULT '0',
	currentRiskRatingweight double NOT NULL DEFAULT '0',
	   `kri_weight` double DEFAULT NULL,
	   `effectiveDate` datetime DEFAULT NULL,
		 KEY `IndexOnKeys` (`CatId`)
	 ) ENGINE=InnoDB;

	Drop temporary table if exists tempdb.RiskItemsGrm;
	Create temporary table tempdb.RiskItemsGrm
	(index Ix_Grp56 (CatId))
	select 
	distinct 
	 a.riskRegisterItemId,  a.riskId
	, round((inherentLikelihood *a.weight),2) as inherentLikelihoodMultWeight
        , if(inherentLikelihood=0,0,a.weight) as inherentLikelihoodWeight
		, round((inherentImpact * a.weight),2) as inherentImpactMultWeight
        , if(inherentImpact=0,0,a.weight) as inherentImpactWeight
		, round((inherentRiskRating *a.weight),2) as inherentRiskRatingMultWeight
		, if(inherentRiskRating=0,0,a.weight) as inherentRiskRatingWeight
		, round((residualLikelihood * a.weight),2) as residualLikelihoodMultWeight
        , if(residualLikelihood=0,0,a.weight) as residualLikelihoodWeight
		, round((residualImpact * a.weight),2) as residualImpactMultWeight
        , if(residualImpact=0,0,a.weight) as residualImpactWeight
		, round((residualRiskRating * a.weight),2) as residualRiskRatingMultWeight
        , if(residualRiskRating=0,0,a.weight) as residualRiskRatingWeight
		, round((currentRiskRating  * a.weight),2) as currentRiskRatingMultWeight
		, if(currentRiskRating=0,0,a.weight) as currentRiskRatingWeight
		, round(if(Kri.riskregisterItemId is not null and krid.status = 1,a.predictedRisk * a.weight, null),2) as predictedRisk		
        , if(Kri.riskregisterItemId is not null and krid.status = 1 and a.predictedRisk <> 0, a.weight, null) as  kri_weight
		, a.weight
	
	, a.parentId as CatId, a.EffectiveDate
	, a.buFacilityId as BU
	from TempRiskItemsBU a
	left join Kri on a.riskRegisterItemId = Kri.riskRegisterItemId
	left join KriDefination krid on Kri.kriDefinationId = krid.id;

	
	Drop temporary table if exists tempdb.GroupCat2;
	Create temporary table tempdb.GroupCat2
	(index Ix_Grp78 (CatId))
	SELECT 
	  CatId
	, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk
		, sum(weight) as weight
		, sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
        , sum(inherentImpactweight) as inherentImpactweight
        , sum(inherentRiskRatingweight) as inherentRiskRatingweight
        , sum(residualLikelihoodweight) as residualLikelihoodweight
        , sum(residualImpactweight) as residualImpactweight
        , sum(residualRiskRatingweight) as residualRiskRatingweight
        , sum(currentRiskRatingweight) as currentRiskRatingweight
		, sum(kri_weight) as kri_weight
	, max(EffectiveDate) as EffectiveDate  
	FROM tempdb.RiskItemsGrm 
	group by CatId ;

	

	set @WhileHeirarchy =1;

	while @WhileHeirarchy > 0 Do 

	insert into tempdb.`TempWeightedUsrRiskRegisterCat`  
	(CatId, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
    	inherentLikelihoodWeight,
	inherentImpactweight,
	inherentRiskRatingweight,
	residualLikelihoodweight,
	residualImpactweight,
	residualRiskRatingweight,
	currentRiskRatingweight,
    	kri_weight,  EffectiveDate)
	select 
	CatId, inherentLikelihood, inherentImpact, inherentRiskRating, residualLikelihood, residualImpact, residualRiskRating, currentRiskRating, predictedRisk, weight,
    	inherentLikelihoodWeight,
	inherentImpactweight,
	inherentRiskRatingweight,
	residualLikelihoodweight,
	residualImpactweight,
	residualRiskRatingweight,
	currentRiskRatingweight,
    	kri_weight,  EffectiveDate
	From tempdb.GroupCat2 where CatId is not null;

	Drop temporary table if exists tempdb.GroupCat;
	Create temporary table tempdb.GroupCat
	(index idx_cat (CatId))
	select * from tempdb.GroupCat2 where CatId is not null;


	
	Drop temporary table if exists tempdb.GroupCatMultWeight;
	set @qry = concat('
	create temporary table tempdb.GroupCatMultWeight
	(index Ix_Grp91 (CatId))
	select 
	c.parentId  as CatId

	, round((a.inherentLikelihood * a.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
	, round((a.inherentImpact * a.inherentImpactweight),2) as inherentImpactMultWeight
	, round((a.inherentRiskRating * a.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

	, round((a.residualLikelihood * a.residualLikelihoodweight),2) as residualLikelihoodMultWeight
	, round((a.residualImpact * a.residualImpactweight),2) as residualImpactMultWeight
	, round((a.residualRiskRating * a.residualRiskRatingweight),2) as residualRiskRatingMultWeight

	, round((a.currentRiskRating  * a.currentRiskRatingweight),2) as currentRiskRatingMultWeight

	, round(a.predictedRisk * a.kri_weight,2) as predictedRisk

	, a.weight
        ,a.inherentLikelihoodWeight,
	a.inherentImpactweight,
	a.inherentRiskRatingweight,
	a.residualLikelihoodweight,
	a.residualImpactweight,
	a.residualRiskRatingweight,
	a.currentRiskRatingweight,
	a.kri_weight
	, EffectiveDate
	 from  tempdb.GroupCat a ', 
	if(in_PC = TRUE, 'inner join processcategory c on c.id =a.catId and c.isDeleted = 0 ', 
	   ' inner join RiskRegisterItem b on b.id =a.catId and b.isDeleted = 0
		 inner join RiskRegisterItemMapping c on b.id = c.riskRegisterItemId and c.isDeleted = 0'),
	';'
	);
    
	prepare stmp from @qry;
	execute stmp ;
	deallocate prepare stmp;
    

	
	
	Drop temporary table if exists tempdb.CatIdAlreadyExists;
	create temporary table tempdb.CatIdAlreadyExists
	(index Ix_Grp1 (CatId))
	select  distinct
	 e.CatId
	, round((e.inherentLikelihood * e.inherentLikelihoodweight),2) as inherentLikelihoodMultWeight
	, round((e.inherentImpact * e.inherentImpactweight),2) as inherentImpactMultWeight
	, round((e.inherentRiskRating * e.inherentRiskRatingweight),2) as inherentRiskRatingMultWeight

	, round((e.residualLikelihood * e.residualLikelihoodweight),2) as residualLikelihoodMultWeight
	, round((e.residualImpact * e.residualImpactweight),2) as residualImpactMultWeight
	, round((e.residualRiskRating * e.residualRiskRatingweight),2) as residualRiskRatingMultWeight

	, round((e.currentRiskRating  * e.currentRiskRatingweight),2) as currentRiskRatingMultWeight

	, round(e.predictedRisk * e.kri_weight,2)  as predictedRisk
	, e.weight,
        e.inherentLikelihoodWeight,
	e.inherentImpactweight,
	e.inherentRiskRatingweight,
	e.residualLikelihoodweight,
	e.residualImpactweight,
	e.residualRiskRatingweight,
	e.currentRiskRatingweight,
	e.kri_weight
	, e.EffectiveDate
	 from  tempdb.GroupCatMultWeight n
	inner join tempdb.`TempWeightedUsrRiskRegisterCat` e 
	on  e.catId = n.catId;

	
	insert into  tempdb.GroupCatMultWeight
	select *  from  tempdb.CatIdAlreadyExists;

	
	Delete twc from  tempdb.TempWeightedUsrRiskRegisterCat twc
	inner join  tempdb.CatIdAlreadyExists e on  
	e.catId = twc.catId ; 


	
	Drop temporary table if exists tempdb.GroupCat2;
	create temporary table tempdb.GroupCat2
	(index Ix_Grp2 (CatId))
	select a.CatId 
	, round((sum(inherentLikelihoodMultWeight)/ if(sum(inherentLikelihoodWeight)=0,1,sum(inherentLikelihoodWeight)) ),2) as inherentLikelihood
		, round((sum(inherentImpactMultWeight)/if(sum(inherentImpactweight)=0,1,sum(inherentImpactweight))),2) as inherentImpact
		, round((sum(inherentRiskRatingMultWeight)/if(sum(inherentRiskRatingweight)=0,1,sum(inherentRiskRatingweight))),2) as inherentRiskRating

		, round((sum(residualLikelihoodMultWeight)/if(sum(residualLikelihoodweight)=0,1,sum(residualLikelihoodweight))),2) as residualLikelihood
		, round((sum(residualImpactMultWeight)/if(sum(residualImpactweight)=0,1,sum(residualImpactweight))),2) as residualImpact
		, round((sum(residualRiskRatingMultWeight)/if(sum(residualRiskRatingweight)=0,1,sum(residualRiskRatingweight))),2) as residualRiskRating

		, round((sum(currentRiskRatingMultWeight)/if(sum(currentRiskRatingweight)=0,1,sum(currentRiskRatingweight))),2) as currentRiskRating

		, if(sum(kri_weight)=0,0,round(sum(predictedRisk)/ if(sum(kri_weight)=0,1,sum(kri_weight)) ,2)) as predictedRisk

	, sum(weight) as weight
        , sum(inherentLikelihoodWeight) as inherentLikelihoodWeight
        , sum(inherentImpactweight) as inherentImpactweight
        , sum(inherentRiskRatingweight) as inherentRiskRatingweight
        , sum(residualLikelihoodweight) as residualLikelihoodweight
        , sum(residualImpactweight) as residualImpactweight
        , sum(residualRiskRatingweight) as residualRiskRatingweight
        , sum(currentRiskRatingweight) as currentRiskRatingweight
		, sum(kri_weight) as kri_weight
	, max(EffectiveDate) as EffectiveDate 
	from  tempdb.GroupCatMultWeight a
	group by a.CatId;


	set @WhileHeirarchy = (select count(1) from tempdb.GroupCat2);


	end while;

	drop temporary table if exists  tempdb.rollup_bu;
	create temporary table tempdb.rollup_bu
	Select 
	 CatId as riskRegisterItemCatLevelId
	, inherentLikelihood as weightedInherentLikelihood
	, inherentImpact as weightedInherentImpact
	, inherentRiskRating as weightedInherentRiskRating

	, residualLikelihood as weightedResidualLikelihood
	, residualImpact as weightedResidualImpact
	, residualRiskRating as weightedResidualRiskRating

	, currentRiskRating as weightedCurrentRiskRating

	, predictedRisk as weightedPredictedRisk

	, EffectiveDate as effectiveDate
	from tempdb.`TempWeightedUsrRiskRegisterCat`;

	select * from tempdb.rollup_bu;
end if;

END$$
DELIMITER ;
