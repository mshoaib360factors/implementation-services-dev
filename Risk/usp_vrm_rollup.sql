DROP PROCEDURE IF EXISTS `usp_vrm_rollup`;
DELIMITER $$
CREATE PROCEDURE `usp_vrm_rollup`(
    in_customerId BIGINT(20)
)
BEGIN


SET @qry = CONCAT("
with RECURSIVE cte as (
	select c.id as ParentCategory, avg(ifnull(a.riskRating,0)) as riskRating from vendorriskregister a
	join vendortype b on a.vendorTypeId = b.id
	join vendorcategory c on b.parentCategoryId = c.id
    ",if(in_customerId is not null or in_customerId <> "",concat(" where a.customerId = ",in_customerId),""),"
    group by 1
	union all
	select b.parentid as ParentCategory,cte.riskRating   from cte
	join vendorcategory b on cte.ParentCategory = b.id
	where b.parentid is not null
)
select ParentCategory as vrmcategoryid, avg(riskRating) as rolledupRiskRating from cte
group by 1;");
PREPARE stmp FROM @qry;
EXECUTE stmp ;
DEALLOCATE PREPARE stmp;

 
 
END$$
DELIMITER ;
