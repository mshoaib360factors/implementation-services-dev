DROP PROCEDURE IF EXISTS `usp_risk_screen`;
DELIMITER $$
CREATE PROCEDURE `usp_risk_screen`(
    in_customerId bigint(20),
    in_PC BOOLEAN 
)
BEGIN

set sql_safe_updates = 0;

### Custom Fields integrations
drop temporary table if exists tempdb.riskcustomfield;
create temporary table tempdb.riskcustomfield
select a.id as Cfvid, e.entityName, c.`name`  as CustomFieldName 
, 	CASE
		WHEN (a.`Stringvalue` IS NOT NULL) THEN ifnull(d.optionLabel, a.Stringvalue) 
		WHEN (a.`textValue` IS NOT NULL) THEN a.`textValue`
		WHEN (a.`numericValue` IS NOT NULL) THEN a.`numericValue`
		WHEN (a.`decimalValue` IS NOT NULL) THEN a.`decimalValue`
		WHEN (a.`dateValue` IS NOT NULL) THEN a.`dateValue`
	else
	'' end AS `fieldvalue`
, b.applicableEntityInstanceId as Entityid

from CustomFieldValue a
inner join CustomFieldApplicableEntityInstance b on a.CustomFieldApplicableEntityInstanceId = b.id
inner join CustomField c on b.customFieldId = c.id 
inner join CustomFieldApplicableEntity e on e.id = c.applicableEntityId  and  e.entityName in ( 'Risk Item', 'Risk Definition')
left join CustomFieldOption d on c.id = d.customFieldId and a.STRINGVALUE = d.id 
where a.customerId = in_customerId;

drop temporary table if exists tempdb.MultiToSingleRiskCustomField;
create temporary table tempdb.MultiToSingleRiskCustomField
select entityName,	Entityid, CustomFieldName, group_concat(fieldvalue) as FieldValue from tempdb.riskcustomfield
group by entityName, Entityid, CustomFieldName;

drop temporary table if exists tempdb.riskcustomfieldflattenedRiskItem;
create temporary table tempdb.riskcustomfieldflattenedRiskItem
select Entityid as RiskItem, JSON_OBJECTAGG(CustomFieldName, FieldValue) as RiskItemCustomField from tempdb.MultiToSingleRiskCustomField
where entityName = 'Risk Item'
group by Entityid;

drop temporary table if exists tempdb.riskcustomfieldflattenedRiskReg;
create temporary table tempdb.riskcustomfieldflattenedRiskReg
select Entityid as RiskRegister, JSON_OBJECTAGG(CustomFieldName, FieldValue) as RiskRegCustomField from tempdb.MultiToSingleRiskCustomField
where entityName = 'Risk Definition'
group by Entityid;

### Integrations of Rest of the tables.
drop temporary table if exists tempdb.riskregisterresponsibleperson;
create temporary table tempdb.riskregisterresponsibleperson
SELECT riskRegisterId, group_concat(personId)  as personId FROM riskregisterresponsibleperson
group by riskRegisterId;

drop temporary table if exists tempdb.riskregistersite;
create temporary table tempdb.riskregistersite
SELECT riskRegisterId, group_concat(siteId) as siteId FROM riskregistersite
group by riskRegisterId;

## Creating the result set of Risk Register Screen

if in_PC is null or in_PC = false then

	with recursive cte as (
	select     rri.id, rri.isleaf,  rrim.parentId, 1 as lvl, 
			
			JSON_ARRAY( JSON_OBJECT('id',rri.id, 'name',rri.`name`, 'description', rri.`description`, 'managementComments', rri.managementComments )) as hierarchy
	from       riskregisteritemmapping rrim
	inner join riskregisteritem rri on rri.id = rrim.riskRegisterItemId
	where	
	rrim.parentId is null    
	-- and rrim.riskRegisterItemId = 185888 
	and	rri.customerId = in_customerId		 
	union all
	  
	select     rri.id,  rri.isleaf, rrim.parentId, lvl + 1 as lvl,
	  JSON_MERGE(hierarchy,  
							JSON_ARRAY(JSON_OBJECT(
							'id',rri.id, 'name',rri.`name`, 'description', rri.`description`, 'managementComments', rri.managementComments))                     
				)as hierarchy
	from       riskregisteritemmapping rrim
	inner join cte  on rrim.parentId = cte.id
	inner join riskregisteritem rri on rri.id = rrim.riskRegisterItemId 
	)
	select 
		cte.hierarchy
	, 	rria.customerId, rria.id as riskRegItemApplicability_id, rria.buFacilityId, applicablity, rria.riskRegisterItemId, rria.isProcessCategory, rria.isDeleted as riskRegItemApplicability_isDeleted
	, 	rria.createdBy as riskRegItemApplicability_createdBy, createdOn as riskRegItemApplicability_createdOn,	rria.modifiedBy as riskRegItemApplicability_modifiedBy, modifiedOn as riskRegItemApplicability_modifiedOn



	,	rri.name as riskRegItem_name, rri.createdBy as riskRegItem_createdBy, rri.createdDate as riskRegItem_createdDate, rri.updatedBy as riskRegItem_updatedBy, rri.updatedDate as riskRegItem_updatedDate,  rri.isDeleted as riskRegItem_isDeleted
	, 	rri.description as riskRegItem_description,	rri.isSystemRiskRegisterItem as  riskRegItem_isSystemRiskRegisterItem, rri.isOrphan as riskRegItem_isOrphan, rri.riskDefinitionId as riskRegItem_riskDefinitionId
	, 	rri.libraryRiskId as riskRegItem_libraryRiskId, rri.riskUpdateStatus as riskRegItem_riskUpdateStatus, 	rri.riskTaxonomyId as riskRegItem_riskTaxonomyId, rri.processTaxonomyId as riskRegItem_processTaxonomyId
	, 	rri.contentLibraryId as riskRegItem_contentLibraryId, rri.guid as riskRegItem_guid, rri.isOrphanForProcess as riskRegItem_isOrphanForProcess, 	rri.managementComments as riskRegItem_managementComments
	, 	rri.referencedRiskRegisterItemId as  riskRegItem_referencedRiskRegisterItemId, 	rri.riaSurveyId as riskRegItem_riaSurveyId, rri.rpaSurveyId as riskRegItem_rpaSurveyId


	, 	rr.id as riskRegister_id, rr.riskName as riskRegister_riskName, rr.frequencyCount  as riskRegister_frequencyCount, rr.frequencyOccurence as riskRegister_frequencyOccurence
	, 	rr.frequencyExplanation as riskRegister_frequencyExplanation, 	rr.outcome as riskRegister_outcome, rr.outcomeDescription as riskRegister_outcomeDescription
	, 	rr.estimatedOutcome as riskRegister_estimatedOutcome, rr.status as riskRegister_status , rr.managementComments as riskRegister_managementComments
	, 	rr.context as riskRegister_context, rr.createdBy as riskRegister_createdBy,	rr.createdDate as riskRegister_createdDate, rr.modifiedBy as riskRegister_modifiedBy, rr.modifiedDate as riskRegister_modifiedDate
	, 	rr.identifiedBy as riskRegister_identifiedBy, rr.monetaryImpact as riskRegister_monetaryImpact, rr.riskEventID as riskRegister_riskEventID, rr.approach as riskRegister_approach
	, 	rr.description as riskRegister_description,	rr.surveyInstanceId as riskRegister_surveyInstanceIdas, rr.surveySectionId as riskRegister_surveySectionId
	, 	rr.surveyQuestionId as riskRegister_surveyQuestionId, rr.controls as riskRegister_controls, rr.lastReviewDate as riskRegister_lastReviewDate
	, 	rr.isReviewed as riskRegister_isReviewed, rr.changeExplanation as riskRegister_changeExplanation, rr.weight as riskRegister_weight
	, 	rr.relativeMagnitudeId as riskRegister_relativeMagnitudeId, rr.topRisk as riskRegister_topRisk, rr.riskInstanceId as riskRegister_riskInstanceId

	, 	rroa.id as riskRegOverallAnalysis_id, rroa.riskId as riskRegOverallAnalysis_riskId, rroa.inherentLikelihood as riskRegOverallAnalysis_inherentLikelihood, rroa.inherentImpact as riskRegOverallAnalysis_inherentImpact
	, 	rroa.inherentRiskRating as riskRegOverallAnalysis_inherentRiskRating, rroa.residualLikelihood as riskRegOverallAnalysis_residualLikelihood
	, 	rroa.residualImpact as riskRegOverallAnalysis_residualImpact, rroa.residualRiskRating as riskRegOverallAnalysis_residualRiskRating, rroa.treatedRating as riskRegOverallAnalysis_treatedRating, rroa.customerId as riskRegOverallAnalysis_customerId
	, 	rroa.createdBy as riskRegOverallAnalysis_createdBy, rroa.createdDate as riskRegOverallAnalysis_createdDate, rroa.modifiedBy as riskRegOverallAnalysis_modifiedBy, rroa.modifiedDate as riskRegOverallAnalysis_modifiedDate, rroa.currentRiskRating as riskRegOverallAnalysis_currentRiskRating
	, 	rroa.inherentMonetaryImpact as riskRegOverallAnalysis_inherentMonetaryImpact, rroa.residualMonetaryImpact as riskRegOverallAnalysis_residualMonetaryImpact, rroa.currentMonetaryImpact as riskRegOverallAnalysis_currentMonetaryImpact
	, 	rroa.currentLikelihood as riskRegOverallAnalysis_currentLikelihood, rroa.currentImpact as riskRegOverallAnalysis_currentImpact, rroa.predictedRisk as riskRegOverallAnalysis_predictedRisk

	, 	rrp.personId as riskRegRespPerson_personId
	, 	rrs.siteId as riskRegSite_SiteId
	, 	rcri.RiskItemCustomField
	, 	rcrr.RiskRegCustomField

	from RiskRegisterItemApplicablity rria 
	inner join riskregisteritem rri on rri.id = rria.riskRegisterItemId
	inner join riskregister rr on rr.riskRegisterItemId = rri.id and rr.buFacilityId <=> rria.buFacilityId
	left join riskregisteroverallanalysis rroa on rroa.riskId = rr.id
	inner join cte on cte.id = rria.riskRegisterItemId and cte.isLeaf  = 1
	left join tempdb.riskregisterresponsibleperson rrp on rrp.riskRegisterId = rr.id
	left join tempdb.riskregistersite rrs on rrs.riskRegisterId = rr.id
	left join tempdb.riskcustomfieldflattenedRiskItem rcri on rcri.RiskItem = rri.id
	left join tempdb.riskcustomfieldflattenedRiskReg rcrr on rcrr.RiskRegister = rr.id
	where rria.customerid = in_customerId and rria.applicablity = 'Applicable' and rria.isProcessCategory = 0 and rria.isDeleted = 0 ;


else 

	
	with recursive cte as (
	Select pc.id, pc.parentId, 1 as lvl,
	JSON_ARRAY( JSON_OBJECT('id',pc.id, 'name',pc.`name`, 'description', pc.`description`)) as hierarchy
	From processcategory pc where pc.customerId = in_customerId and pc.parentId is null 
	
	union all 
	
	select     pc.id,  pc.parentId, lvl + 1 as lvl,
		JSON_MERGE(hierarchy,  
							JSON_ARRAY(JSON_OBJECT(
							'id',pc.id, 'name',pc.`name`, 'description', pc.`description`))                     
				)as hierarchy
	from cte
	inner join processcategory pc on pc.parentId = cte.id
	)
	select 
		cte.hierarchy
		, 	rria.customerId, rria.id as riskRegItemApplicability_id, rria.buFacilityId, applicablity, rria.riskRegisterItemId, rria.isProcessCategory, rria.isDeleted as riskRegItemApplicability_isDeleted
		, 	rria.createdBy as riskRegItemApplicability_createdBy, createdOn as riskRegItemApplicability_createdOn,	rria.modifiedBy as riskRegItemApplicability_modifiedBy, modifiedOn as riskRegItemApplicability_modifiedOn
	
	
	
		,	rri.name as riskRegItem_name, rri.createdBy as riskRegItem_createdBy, rri.createdDate as riskRegItem_createdDate, rri.updatedBy as riskRegItem_updatedBy, rri.updatedDate as riskRegItem_updatedDate,  rri.isDeleted as riskRegItem_isDeleted
		, 	rri.description as riskRegItem_description,	rri.isSystemRiskRegisterItem as  riskRegItem_isSystemRiskRegisterItem, rri.isOrphan as riskRegItem_isOrphan, rri.riskDefinitionId as riskRegItem_riskDefinitionId
		, 	rri.libraryRiskId as riskRegItem_libraryRiskId, rri.riskUpdateStatus as riskRegItem_riskUpdateStatus, 	rri.riskTaxonomyId as riskRegItem_riskTaxonomyId, rri.processTaxonomyId as riskRegItem_processTaxonomyId
		, 	rri.contentLibraryId as riskRegItem_contentLibraryId, rri.guid as riskRegItem_guid, rri.isOrphanForProcess as riskRegItem_isOrphanForProcess, 	rri.managementComments as riskRegItem_managementComments
		, 	rri.referencedRiskRegisterItemId as  riskRegItem_referencedRiskRegisterItemId, 	rri.riaSurveyId as riskRegItem_riaSurveyId, rri.rpaSurveyId as riskRegItem_rpaSurveyId
	
	
		, 	rr.id as riskRegister_id, rr.riskName as riskRegister_riskName, rr.frequencyCount  as riskRegister_frequencyCount, rr.frequencyOccurence as riskRegister_frequencyOccurence
		, 	rr.frequencyExplanation as riskRegister_frequencyExplanation, 	rr.outcome as riskRegister_outcome, rr.outcomeDescription as riskRegister_outcomeDescription
		, 	rr.estimatedOutcome as riskRegister_estimatedOutcome, rr.status as riskRegister_status , rr.managementComments as riskRegister_managementComments
		, 	rr.context as riskRegister_context, rr.createdBy as riskRegister_createdBy,	rr.createdDate as riskRegister_createdDate, rr.modifiedBy as riskRegister_modifiedBy, rr.modifiedDate as riskRegister_modifiedDate
		, 	rr.identifiedBy as riskRegister_identifiedBy, rr.monetaryImpact as riskRegister_monetaryImpact, rr.riskEventID as riskRegister_riskEventID, rr.approach as riskRegister_approach
		, 	rr.description as riskRegister_description,	rr.surveyInstanceId as riskRegister_surveyInstanceIdas, rr.surveySectionId as riskRegister_surveySectionId
		, 	rr.surveyQuestionId as riskRegister_surveyQuestionId, rr.controls as riskRegister_controls, rr.lastReviewDate as riskRegister_lastReviewDate
		, 	rr.isReviewed as riskRegister_isReviewed, rr.changeExplanation as riskRegister_changeExplanation, rr.weight as riskRegister_weight
		, 	rr.relativeMagnitudeId as riskRegister_relativeMagnitudeId, rr.topRisk as riskRegister_topRisk, rr.riskInstanceId as riskRegister_riskInstanceId
	
		, 	rroa.id as riskRegOverallAnalysis_id, rroa.riskId as riskRegOverallAnalysis_riskId, rroa.inherentLikelihood as riskRegOverallAnalysis_inherentLikelihood, rroa.inherentImpact as riskRegOverallAnalysis_inherentImpact
		, 	rroa.inherentRiskRating as riskRegOverallAnalysis_inherentRiskRating, rroa.residualLikelihood as riskRegOverallAnalysis_residualLikelihood
		, 	rroa.residualImpact as riskRegOverallAnalysis_residualImpact, rroa.residualRiskRating as riskRegOverallAnalysis_residualRiskRating, rroa.treatedRating as riskRegOverallAnalysis_treatedRating, rroa.customerId as riskRegOverallAnalysis_customerId
		, 	rroa.createdBy as riskRegOverallAnalysis_createdBy, rroa.createdDate as riskRegOverallAnalysis_createdDate, rroa.modifiedBy as riskRegOverallAnalysis_modifiedBy, rroa.modifiedDate as riskRegOverallAnalysis_modifiedDate, rroa.currentRiskRating as riskRegOverallAnalysis_currentRiskRating
		, 	rroa.inherentMonetaryImpact as riskRegOverallAnalysis_inherentMonetaryImpact, rroa.residualMonetaryImpact as riskRegOverallAnalysis_residualMonetaryImpact, rroa.currentMonetaryImpact as riskRegOverallAnalysis_currentMonetaryImpact
		, 	rroa.currentLikelihood as riskRegOverallAnalysis_currentLikelihood, rroa.currentImpact as riskRegOverallAnalysis_currentImpact, rroa.predictedRisk as riskRegOverallAnalysis_predictedRisk
	
		, 	rrp.personId as riskRegRespPerson_personId
		, 	rrs.siteId as riskRegSite_SiteId
		, 	rcri.RiskItemCustomField
		, 	rcrr.RiskRegCustomField
	
	from cte  
	inner join processcategoryriskitem pcri on pcri.processCategoryId = cte.id
	inner join RiskRegisterItemApplicablity rria on rria.riskRegisterItemId = pcri.riskRegisterItemId
	inner join riskregisteritem rri on rri.id = rria.riskRegisterItemId
	inner join riskregister rr on rr.riskRegisterItemId = rri.id and rr.buFacilityId <=> rria.buFacilityId
	left join riskregisteroverallanalysis rroa on rroa.riskId = rr.id
	left join tempdb.riskregisterresponsibleperson rrp on rrp.riskRegisterId = rr.id
	left join tempdb.riskregistersite rrs on rrs.riskRegisterId = rr.id
	left join tempdb.riskcustomfieldflattenedRiskItem rcri on rcri.RiskItem = rri.id
	left join tempdb.riskcustomfieldflattenedRiskReg rcrr on rcrr.RiskRegister = rr.id
	where rria.customerid = in_customerId and rria.applicablity = 'Applicable' and rria.isProcessCategory = 1 and rria.isDeleted = 0 ;


end if;
 
 
END$$
DELIMITER ;
