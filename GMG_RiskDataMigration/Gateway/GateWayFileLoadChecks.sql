Drop temporary table if exists tempdb.fileNameExactMatch;
Create temporary table tempdb.fileNameExactMatch
select File_Name, Document_Title, Template_Title, Replace_Headers_with_Template__Optional_, Replace_Footers_with_Template__Optional_, Parent_File_Name__Optional_, Status_workflow_status_, Document_Owner_ID, Document_Owner_Name__Optional_, Assigned_Proxy_ID__Optional_, Assigned_Proxy_Name__Optional_, Document_Creator_ID__Optional_, Document_Creator_Name__Optional_, Review_Interval__Optional_, Warn_Period__Optional_, Intended_Publication_Date__Optional_, Last_Review_Approval_Date__Optional_, Reader_Task_End_Date__Optional_, Archive_Date__Optional_, Disable_Assignees__Optional_, Document_Keywords__Optional_, Original_Creation_Date__Optional_, Read_Interval__Optional_, Sites__Optional_, Categories__Optional_, Departments__Optional_, Language__Optional_, Change_Summary__Optional_, Reference_Number__Optional_, Doc_Owner_Instructions__Optional_, Create_Task_for_Owner__Optional_, Version_Number__Optional_, Notification_Settings_for_Doc_Owners___Proxies__Optional_, Security__Optional_, Has_Inserted_Categories, Reference_Doc_IDs, Reference_Doc_IDs_Status, Linked_Doc_IDs, Linked_Doc_IDs_Status, Old_DocumentID, New_Document_ID, Attachments,  'Loaded Successfully' Import_Result
from document d
inner join  gateway g
on trim(d.DocumentName) = trim(g.File_Name);

Drop temporary table if exists tempdb.DocNameWithOutExt;
Create temporary table tempdb.DocNameWithOutExt
select DocumentName, replace(replace(replace(replace(replace(replace(replace(replace(DocumentName, '.docx',''),'.xlsx',''), '.doc',''),'.xls',''), '.pptx',''),'.db',''),'.csv',''),'.pdf','') as DocumentNameWithOutExt, FilePath
from document where DocumentName not in (select File_Name from tempdb.fileNameExactMatch);

Drop temporary table if exists tempdb.FileNameWithOutExt;
Create temporary table tempdb.FileNameWithOutExt
select distinct replace(replace(replace(replace(replace(replace(replace(replace(File_Name, '.docx',''),'.xlsx',''), '.doc',''),'.xls',''), '.pptx',''),'.db',''),'.csv',''),'.pdf','') as FileNameWithOutExt, g.*
from gateway g where File_Name not in (select File_Name from tempdb.fileNameExactMatch);

Drop temporary table if exists tempdb.FileWithAnotherFormat;
Create temporary table tempdb.FileWithAnotherFormat
select distinct D.DocumentName, File_Name, Document_Title, Template_Title, Replace_Headers_with_Template__Optional_, Replace_Footers_with_Template__Optional_, Parent_File_Name__Optional_, Status_workflow_status_, Document_Owner_ID, Document_Owner_Name__Optional_, Assigned_Proxy_ID__Optional_, Assigned_Proxy_Name__Optional_, Document_Creator_ID__Optional_, Document_Creator_Name__Optional_, Review_Interval__Optional_, Warn_Period__Optional_, Intended_Publication_Date__Optional_, Last_Review_Approval_Date__Optional_, Reader_Task_End_Date__Optional_, Archive_Date__Optional_, Disable_Assignees__Optional_, Document_Keywords__Optional_, Original_Creation_Date__Optional_, Read_Interval__Optional_, Sites__Optional_, Categories__Optional_, Departments__Optional_, Language__Optional_, Change_Summary__Optional_, Reference_Number__Optional_, Doc_Owner_Instructions__Optional_, Create_Task_for_Owner__Optional_, Version_Number__Optional_, Notification_Settings_for_Doc_Owners___Proxies__Optional_, Security__Optional_, Has_Inserted_Categories, Reference_Doc_IDs, Reference_Doc_IDs_Status, Linked_Doc_IDs, Linked_Doc_IDs_Status, Old_DocumentID, New_Document_ID, Attachments, 'Available With Another Format' Import_Result 
from tempdb.DocNameWithOutExt D 
inner join tempdb.FileNameWithOutExt F on 
D.DocumentNameWithOutExt = F.FileNameWithOutExt;

Drop temporary table if exists tempdb.final;
Create temporary table tempdb.final
select distinct
G.File_Name, G.Document_Title, G.Template_Title, G.Replace_Headers_with_Template__Optional_, G.Replace_Footers_with_Template__Optional_, G.Parent_File_Name__Optional_, G.Status_workflow_status_, G.Document_Owner_ID, G.Document_Owner_Name__Optional_, G.Assigned_Proxy_ID__Optional_, G.Assigned_Proxy_Name__Optional_, G.Document_Creator_ID__Optional_, G.Document_Creator_Name__Optional_, G.Review_Interval__Optional_, G.Warn_Period__Optional_, G.Intended_Publication_Date__Optional_, G.Last_Review_Approval_Date__Optional_, G.Reader_Task_End_Date__Optional_, G.Archive_Date__Optional_, G.Disable_Assignees__Optional_, G.Document_Keywords__Optional_, G.Original_Creation_Date__Optional_, G.Read_Interval__Optional_, G.Sites__Optional_, G.Categories__Optional_, G.Departments__Optional_, G.Language__Optional_, G.Change_Summary__Optional_, G.Reference_Number__Optional_, G.Doc_Owner_Instructions__Optional_, G.Create_Task_for_Owner__Optional_, G.Version_Number__Optional_, G.Notification_Settings_for_Doc_Owners___Proxies__Optional_, G.Security__Optional_, G.Has_Inserted_Categories, G.Reference_Doc_IDs, G.Reference_Doc_IDs_Status, G.Linked_Doc_IDs, G.Linked_Doc_IDs_Status, G.Old_DocumentID, G.New_Document_ID, G.Attachments,
ifnull(FEM.Import_Result,ifnull(FAF.Import_Result, 'File Not Found')) Import_Result
 From  gateway G
left join  tempdb.fileNameExactMatch FEM
on G.File_Name = FEM.File_Name
left join tempdb.FileWithAnotherFormat FAF 
on G.File_Name = FAF.File_Name;


select Import_Result, count(1) from tempdb.final
group by Import_Result;

create temporary table  tempdb.final2
select  * from 
(select * from document where DocumentName not in (select File_Name from tempdb.fileNameExactMatch)) x where DocumentName not in(select DocumentName from tempdb.FileWithAnotherFormat);


select * from tempdb.final2 where DocumentName like '%2016.06.055.ITPR.Incident Response Procedure (5192)%';
select * from tempdb.fileNameExactMatch where File_Name like '%2016.06.055.ITPR.Incident Response Procedure (5192)%';
select * from tempdb.FileWithAnotherFormat where File_Name like '%2016.06.055.ITPR.Incident Response Procedure (5192)%';


select * from tempdb.final2 where DocumentName like '%2015.10.054.CPF TRID Closing Disclosure Process Flow (233)%';
select * from tempdb.fileNameExactMatch where File_Name like '%2015.10.054.CPF TRID Closing Disclosure Process Flow (233)%';
select * from tempdb.FileWithAnotherFormat where File_Name like '%2015.10.054.CPF TRID Closing Disclosure Process Flow (233)%';