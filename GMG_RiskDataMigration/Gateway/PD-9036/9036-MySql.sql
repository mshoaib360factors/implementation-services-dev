#Analysis
/*
SELECT file_name, count(distinct Parent_File_Name__Optional_) FROM etl.gateway_new group by 1 having count(distinct parent_File_Name__Optional_) > 1;
SELECT  * FROM etl.gateway_new where  Version_Number__Optional_ is null;

SELECT 	Document_Keywords__Optional_, LENGTH(Document_Keywords__Optional_) as Lngth, 
		LENGTH(REPLACE(Document_Keywords__Optional_,',','')) ,
        ((LENGTH(Document_Keywords__Optional_) - LENGTH(REPLACE(Document_Keywords__Optional_,',',''))) + 1) AS `n`
FROM Test;
*/

#File Name with Max Version having Tax
Drop temporary table if exists FileNameWithTags;
create temporary table FileNameWithTags
select file_name, concat(Document_Keywords__Optional_, ',') as Document_Keywords__Optional_ from etl.gateway_new s1
inner join
(
SELECT  Parent_File_Name__Optional_, max(Version_Number__Optional_) as Version_Number__Optional_ FROM etl.gateway_new 
where Document_Keywords__Optional_ is not null
group by Parent_File_Name__Optional_
)s2
on s1.Parent_File_Name__Optional_ = s2.Parent_File_Name__Optional_ and s1.Version_Number__Optional_=s2.Version_Number__Optional_;

 

#Caluculating No. of Commas in a Text
Drop temporary table if exists NumberOfCommas;
create temporary table NumberOfCommas
SELECT DISTINCT
 ((LENGTH(Document_Keywords__Optional_) - LENGTH(REPLACE(Document_Keywords__Optional_, ',',''))) + 1) AS `n`
FROM   FileNameWithTags order by 1 ;

insert into NumberOfCommas values (1); 
insert into NumberOfCommas values (12); 
insert into NumberOfCommas values (13); 
insert into NumberOfCommas values (14);  


#Pivot and Unpivoting Tags 
Drop temporary table if exists FileWithTags;
create temporary table FileWithTags
SELECT distinct 
	file_name,
	trim(SUBSTRING_INDEX(SUBSTRING_INDEX(Document_Keywords__Optional_, ',', `dn`.`n`),',',-(1))) AS `Tags`
FROM
FileNameWithTags e
JOIN NumberOfCommas `dn` 
WHERE
((CHAR_LENGTH(Document_Keywords__Optional_) -1) - CHAR_LENGTH(REPLACE(Document_Keywords__Optional_, ',', ''))) >= (`dn`.`n`-1 )
order   BY file_name;
 

select concat('insert into MDFileWithTags values (''',replace(file_name,'''',''''''),''', ''',replace(tags,'''',''''''), ''');') as InsertFileWithTags from FileWithTags ;




/*
Drop temporary table if exists TagsOnly;
create temporary table TagsOnly
select distinct max(Tags) from FileWithTags ;

select concat('insert into none.tag (label) values (''',tags,''');') as InsertStatement from TagsOnly;
*/




 