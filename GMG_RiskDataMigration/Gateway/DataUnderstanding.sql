/*
SELECT *FROM etl.gateway order by Version_Number__Optional_ desc;


SELECT s.Version_Number__Optional_,s.Document_Keywords__Optional_, s.*FROM etl.gateway s  where Document_Keywords__Optional_ is not null AND Version_Number__Optional_ <> '1' ORDER BY 1 DESC;


SELECT count(distinct s.Document_Keywords__Optional_), Parent_File_Name__Optional_ FROM etl.gateway s 
group by Parent_File_Name__Optional_ 
having count(distinct s.Document_Keywords__Optional_)  > 1; 

select s.Document_Keywords__Optional_, s.* from etl.gateway s  where Parent_File_Name__Optional_ = '2015.12.069.SEP.Escrow Waiver (326).docx';
 GIPPPolicytechReleaseNotes9.2SystemRelease

SELECT count(distinct Parent_File_Name__Optional_) FROM etl.gateway ;


SELECT distinct  Document_Title FROM etl.gateway s  where Parent_File_Name__Optional_ = '2015.07.023.CP.Vendor Management Policy (60).docx';


SELECT distinct Document_Keywords__Optional_ FROM etl.gateway s  
WHERE `Document_Keywords__Optional_`   REGEXP '[^a-zA-Z0-9]';
*/

/*** Finding Special Characters in Tags*/
Drop temporary table if exists temp.gatewayTags;
Create temporary table if not exists temp.gatewayTags
SELECT distinct 
replace(
replace(
replace(
replace(
replace(
replace(
replace(
replace(
replace(
replace(
replace(Document_Keywords__Optional_
,' ',''),
',',''), 
';',''), 
':',''), 
'.',''),
'-',''),
'/',''),
'%',''),
'+',''),
'''',''),
'&','')
as Document_Keywords__Optional_ FROM etl.gateway  where Document_Keywords__Optional_ is not null;

select * from temp.gatewayTags where  Document_Keywords__Optional_   REGEXP '[^a-zA-Z0-9]';
/*


*/
select * From etl.gateway s  ;
select Document_Keywords__Optional_ From etl.gateway_new s;
 

select * from etl.gateway_new limit 10;

SELECT File_Name,trim(replace(trim(File_Name), concat('.',SUBSTRING_INDEX(trim(File_Name), '.' ,-1)),'')) as File_Name_New, 
trim(concat('.',SUBSTRING_INDEX(trim(File_Name), '.' ,-1))) as extension	 from etl.gateway_new limit 10;

SELECT max(length(File_Name))  from etl.gateway_new limit 10;

SELECT Document_Title, count(1) from etl.gateway_new group by  Document_Title
having count(1) > 1 
limit 10;

SELECT *  from etl.gateway_new where Document_Title = '2015.04.006b.NRB.GIPP Reference Guide';
SELECT distinct Document_Creator_Name__Optional_  from etl.gateway_new ;


SELECT distinct Change_Summary__Optional_ from etl.gateway_new where length(Change_Summary__Optional_) < 
 (SELECT max(length(Change_Summary__Optional_)) from etl.gateway_new);
 
