SELECT * FROM tbicustone.folder where id=31653 limit 10;
 
WITH RECURSIVE nodes_cte(id, label, parent_id, depth, path) AS (
 SELECT tn.id, tn.displayname, tn.parentfolderid, 1::INT AS depth, tn.displayname::TEXT AS path
 FROM gmg.folder  AS tn
 WHERE tn.parentfolderid = 25637 and ismydocfolder = false and issystemfolder=false
UNION ALL
 SELECT c.id, c.displayname, c.parentfolderid, p.depth + 1 AS depth,
        (p.path || '->' || c.displayname::TEXT)
 FROM nodes_cte AS p, gmg.folder  AS c
 WHERE c.parentfolderid = p.id  and ismydocfolder = false and issystemfolder=false
)
SELECT * FROM nodes_cte AS n ;