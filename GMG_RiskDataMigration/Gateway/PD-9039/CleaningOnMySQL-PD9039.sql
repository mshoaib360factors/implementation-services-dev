/*
#Function for removing simple HTML
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `fnStripTags`( Dirty varchar(4000) ) RETURNS varchar(4000) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
  DECLARE iStart, iEnd, iLength int;
    WHILE Locate( '<', Dirty ) > 0 And Locate( '>', Dirty, Locate( '<', Dirty )) > 0 DO
      BEGIN
        SET iStart = Locate( '<', Dirty ), iEnd = Locate( '>', Dirty, Locate('<', Dirty ));
        SET iLength = ( iEnd - iStart) + 1;
        IF iLength > 0 THEN
          BEGIN
            SET Dirty = Insert( Dirty, iStart, iLength, '');
          END;
        END IF;
      END;
    END WHILE;
    RETURN Dirty;
END$$
DELIMITER ;

*/


# Replaceing Spance for <br />
Drop temporary table if exists AddingSpaceForBr;
create temporary table AddingSpaceForBr
SELECT distinct File_Name, trim(replace(Change_Summary__Optional_,'<br />','<br /> ')) as  Change_Summary__Optional_
FROM etl.gateway where  Change_Summary__Optional_ is not null; 
#1573


# Removing Simple Tags of HTML like </x> </x> 
Drop temporary table if exists Simple_Html_Removed;
create temporary table Simple_Html_Removed
SELECT distinct File_Name, fnStripTags(Change_Summary__Optional_) as Des FROM AddingSpaceForBr where  Change_Summary__Optional_ is not null; 
#1573




# Removing Other tags 
Drop temporary table if exists All_Html_Removed;
create temporary table All_Html_Removed
select distinct File_Name, 
Replace(Replace(Replace(Replace(Replace(Des, '&nbsp;', ' '), '&amp;',' '), '&quot;' ,' '), '&#39;' , ' '), '&gt;',' ') as Des  
from  Simple_Html_Removed;

# Final Cleansing
Drop temporary table if exists Final;
create temporary table Final
select x.File_Name, 
case when length(x.Des) > 255 then 
concat(substring(Des,1,252),'...') else Des end as Des
from 
(select distinct trim(File_Name) as File_Name, trim(Replace(Des, '  ', ' ')) as Des from All_Html_Removed )x;

# Insert Statement Preparation for DMS
select concat('insert into MDFileDesc values (''', replace(File_Name, '''',''''''),''' , ''',
 replace(Des, '''',''''''), ''');') as InsertStatement from final ;
  
select * from Simple_Html_Removed where des like '%<%';
select * from All_Html_Removed where des like '%<%';
select * from Final where des like '%<%';

/*
## Analysis
SELECT count(1) FROM etl.gateway where Change_Summary__Optional_ is not null; #1573
SELECT Change_Summary__Optional_ FROM etl.gateway where Change_Summary__Optional_ is not null; #1573
SELECT distinct  Change_Summary__Optional_ FROM etl.gateway ; #887
SELECT distinct  Change_Summary__Optional_ FROM etl.gateway where Change_Summary__Optional_ is not null and Change_Summary__Optional_ not like '%<%'  ; ; #131
*/