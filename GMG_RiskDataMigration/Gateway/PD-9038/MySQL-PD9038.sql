SELECT File_Name,  Original_Creation_Date__Optional_ FROM etl.gateway;
SELECT File_Name,  Original_Creation_Date__Optional_ FROM etl.gateway;

drop temporary table if exists GateFileCreationDate;
create temporary table GateFileCreationDate
SELECT File_Name, 
 str_to_date(
trim(substring(Original_Creation_Date__Optional_,1,length(Original_Creation_Date__Optional_)-3)),
'%m/%d/%Y %h:%i:%s') as Original_Creation_Date__Optional_New
FROM etl.gateway where Original_Creation_Date__Optional_ is not null ;

select concat('insert into MDFile values (''', Replace(File_Name,'''',''''''),''' , ''',Original_Creation_Date__Optional_New
, ''');') as FileName From GateFileCreationDate ;

 
 