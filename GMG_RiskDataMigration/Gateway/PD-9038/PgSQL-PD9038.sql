-- Final Implementation
Drop  table MDFile ;
create temporary table MDFile 
(
FileName character varying(255) NULL,
OrginalDate timestamp without time zone NOT NULL
);

insert into MDFile values ('2015.04.003.CB.TRID Comparison Current vs. New (9).docx' , '2015-04-07 05:00:00');
insert into MDFile values ('2015.06.012.CB.TRID FAQs (11).docx' , '2015-05-29 05:00:00');
insert into MDFile values ('2015.04.006a.NRB.Gateway Information & Policy Portal (GIPP) Announcement (12).docx' , '2015-04-13 05:00:00');
insert into MDFile values ('2015.06.010.CB.Home Ownership Counseling Disclosure (17).docx' , '2015-04-15 05:00:00');
insert into MDFile values ('2015.07.020.CB.GFE & TIL Re-Disclosure Requirements (19).docx' , '2015-06-29 05:00:00');
insert into MDFile values ('2015.07.019.CB.Cancelling a Loan Due to a Mistake (20).docx' , '2015-06-30 05:00:00');
insert into MDFile values ('2015.05.007.CP.Fair and Responsible Lending Policy (25).docx' , '2015-04-01 05:00:00');
insert into MDFile values ('2015.05.007.CP.Fair and Responsible Lending Policy (467).docx' , '2015-04-01 05:00:00');
insert into MDFile values ('2015.05.009.CB.Federal Holiday Business Day Reminder for Memorial Day (28).docx' , '2015-05-15 05:00:00');
insert into MDFile values ('2015.06.014.NRB.GLADS Overview (34).pdf' , '2015-05-28 05:00:00');
insert into MDFile values ('2015.06.011.CB.TRID Closing Disclosure (35).docx' , '2015-05-29 05:00:00');
insert into MDFile values ('2015.06.015.UB.Full_CPM Condo Review Master Insurance Policy Requirements (43).docx' , '2015-06-12 05:00:00');
insert into MDFile values ('2015.09.040.CP.TILA RESPA Integrated Disclosure (TRID) Policy (44).docx' , '2015-06-13 05:00:00');
insert into MDFile values ('2015.09.040.CP.TILA RESPA Integrated Disclosure (TRID) Policy (2576).docx' , '2015-06-13 05:00:00');
insert into MDFile values ('2015.06.016.CB.Requesting AllRegs Online Access (45).docx' , '2015-06-12 05:00:00');
insert into MDFile values ('2015.07.021.CB.Deleting Documents in BYTE (47).docx' , '2015-06-18 05:00:00');
insert into MDFile values ('2015.06.017.CB.Federal Holiday Business Day Reminder for Independence Day (49).docx' , '2015-06-18 05:00:00');
insert into MDFile values ('2015.08.033.CP.Gateway''s Definition of Application (51).docx' , '2015-06-22 05:00:00');
insert into MDFile values ('2017.11.009.CP.Home Mortgage Disclosure Act (HMDA) Policy (54).docx' , '2015-06-15 05:00:00');
insert into MDFile values ('2016.01.009.CP.Home Mortgage Disclosure Act (HMDA) (2600).docx' , '2015-06-15 05:00:00');
insert into MDFile values ('2015.06.018.CB.APR Clarification (55).docx' , '2015-06-26 05:00:00');
insert into MDFile values ('2015.07.023.CP.Vendor Management Policy (60).docx' , '2015-07-02 05:00:00');
insert into MDFile values ('2016.08.101.CP.Vendor Management Policy (2565).docx' , '2015-07-02 05:00:00');
insert into MDFile values ('2015.07.022a.CB.TRID Upcoming Procedural Changes (61).docx' , '2015-07-04 05:00:00');
insert into MDFile values ('2015.09.039.VMPR.Vendor Management Procedure (62).docx' , '2015-07-06 05:00:00');
insert into MDFile values ('2015.07.022b.CB.TRID Authorization to Furnish Closing Disclosure (63).docx' , '2015-07-07 05:00:00');
insert into MDFile values ('2015.08.029.CP.Consumer Complaint Policy (66).docx' , '2014-12-16 06:00:00');
insert into MDFile values ('2015.08.029.CP.Consumer Complaint Policy (3629).docx' , '2014-12-16 06:00:00');
insert into MDFile values ('Consumer Complaint Policy (5405).docx' , '2014-12-16 06:00:00');
insert into MDFile values ('Appraisal Fee Announcement (125).docx' , '2015-07-29 05:00:00');
insert into MDFile values ('2015.07.024.CB.Appraisal Fee Announcement UPDATE (129).docx' , '2015-07-29 05:00:00');
insert into MDFile values ('Compliance Bulletin CB-2015.07-005 Gateway Learning & Development (GLADS) Announcement (127).docx' , '2015-07-31 05:00:00');
insert into MDFile values ('2015.08.025.CB.Gateway Learning & Development (GLADS) Announcement (172).docx' , '2015-07-31 05:00:00');
insert into MDFile values ('2017.07.07.CB.Compliance Course Bulletin. (4000).docx' , '2015-07-31 05:00:00');
insert into MDFile values ('2015.08.028b.PB.Retail Non-Conforming- SG Product Description 071715 (136).docx' , '2015-08-05 05:00:00');
insert into MDFile values ('2015.08.028b.PB.Retail Non-Conforming- SG Product Description 071715 (255).docx' , '2015-08-05 05:00:00');
insert into MDFile values ('2015.08.028b.PB.Retail Non-Conforming- SG Product Description 071715 (1503).docx' , '2015-08-05 05:00:00');
insert into MDFile values ('2015.08.026.CB.Credit Report Fee Reduction (137).docx' , '2015-08-05 05:00:00');
insert into MDFile values ('2015.08.032.PD.FANNIE MAE CONFORMING ARM – Product Description (143).pdf' , '2015-08-07 05:00:00');
insert into MDFile values ('2015.09.038.RP.Appraisal Escalation Policy (179).docx' , '2015-08-27 05:00:00');
insert into MDFile values ('2015.09.038.RP.Appraisal Escalation Policy (5254).docx' , '2015-08-27 05:00:00');
insert into MDFile values ('2016.01.003a.CB.Withdraw Confirmation Letter Announcement (182).docx' , '2015-10-26 05:00:00');
insert into MDFile values ('2015.09.037.UB.USDA RD Guarantee Fee Increase (196).docx' , '2015-09-10 05:00:00');
insert into MDFile values ('2015.09.041.LP.Email And Business Communications Policy (206).docx' , '2015-09-17 05:00:00');
insert into MDFile values ('2015.09.041.LP.Email And Business Communications Policy (377).docx' , '2015-09-17 05:00:00');
insert into MDFile values ('2015.09.042.CB.Compliance Hotline (207).docx' , '2015-09-17 05:00:00');
insert into MDFile values ('2015.09.042.CB.Compliance Hotline (2571).docx' , '2015-09-17 05:00:00');
insert into MDFile values ('2015.09.044.CB.Ethics and Fraud Hotline Announcement (209).docx' , '2015-09-21 05:00:00');
insert into MDFile values ('2016.04.041.CP.Anti-Money Laundering, Bank Secrecy Act, And OFAC Policy (210).docx' , '2015-09-23 05:00:00');
insert into MDFile values ('2016.04.041.CP.Anti-Money Laundering, Bank Secrecy Act, And OFAC Policy (4007).docx' , '2015-09-23 05:00:00');
insert into MDFile values ('2016.04.041.CP.Anti-Money Laundering, Bank Secrecy Act, And OFAC Policy (5418).docx' , '2015-09-23 05:00:00');
insert into MDFile values ('2015.10.045.CP.Employee Loan Policy (211).docx' , '2015-09-29 05:00:00');
insert into MDFile values ('2015.10.045.CP.Employee Loan Policy (4733).docx' , '2015-09-29 05:00:00');
insert into MDFile values ('2015.12.068.ITP.Remote Access Policy (213).docx' , '2015-09-29 05:00:00');
insert into MDFile values ('2015.10.055.RPF Standard Loan File Submission Process Flow (215).docx' , '2015-10-02 05:00:00');
insert into MDFile values ('2015.11.057.UPD.Portfolio Early Access (218).docx' , '2015-10-07 05:00:00');
insert into MDFile values ('2015.11.057.UPD.Portfolio Early Access (333).docx' , '2015-10-07 05:00:00');
insert into MDFile values ('2015.11.057.PRDPD.Portfolio Early Access (421).docx' , '2015-10-07 05:00:00');
insert into MDFile values ('2015.10.046.OB.Lender''s and Owner''s Title Policy (219).docx' , '2015-10-07 05:00:00');
insert into MDFile values ('2015.10.048.CB.Updated Compliance Calculator Tool (221).docx' , '2015-10-13 05:00:00');
insert into MDFile values ('2015.10.049.CB.TRID Owner''s Lender''s Title Policy Announcement (222).docx' , '2015-10-10 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred 95% LTV (228).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred 95% LTV (3739).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4045).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4091).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4107).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4493).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4565).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4574).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4671).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4758).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4807).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4831).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4833).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4896).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4904).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (4945).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (5052).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (5099).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (5117).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma HFA Preferred (5232).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.050.UPD.Oklahoma Housing Finance Agency - Dream_Gold Programs (HFA Preferred) (5389).docx' , '2015-10-16 05:00:00');
insert into MDFile values ('2015.10.052.UPD.Florida Housing Finance Corporation (Conventional) (230).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (3910).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (3958).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4015).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4070).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4098).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4101).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4532).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4589).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4635).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4642).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4790).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4867).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4912).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (4933).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (5045).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (5207).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (5286).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Conventional) (5300).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation - FLHFC - Conventional (5350).docx' , '2015-10-19 05:00:00');
insert into MDFile values ('2015.10.051.UPD.Florida Housing Finance Corporation (Government) (231).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (3911).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4009).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4093).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4533).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4627).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4643).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4791).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (4934).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (5285).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (5301).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation - FLHFC -Government (5351).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2015.10.054.CPF TRID Closing Disclosure Process Flow (233).docx' , '2015-10-21 05:00:00');
insert into MDFile values ('2015.12.062a.UB.DU Vs. LP Comparison Chart Announcement (252).docx' , '2015-10-29 05:00:00');
insert into MDFile values ('2015.10.056.ITB.Closing Disclosure With Non-Purchasing Spouse (254).docx' , '2015-10-30 05:00:00');
insert into MDFile values ('2015.12.063.CB.Pre-Qualifications, Pre-Approvals, Approvals, And Disclosures (256).docx' , '2015-11-02 06:00:00');
insert into MDFile values ('2015.11.060a.UB.Revised Retail Underwritng Matrix (263).docx' , '2015-11-10 06:00:00');
insert into MDFile values ('GIPP Department And Document Type Abbreviation Guide (271).docx' , '2015-11-12 06:00:00');
insert into MDFile values ('GIPP Department And Document Type Abbreviation Guide (465).docx' , '2015-11-12 06:00:00');
insert into MDFile values ('2016.04.040.CP.Suspicious Activity Report (SAR) Policy (272).docx' , '2015-11-13 06:00:00');
insert into MDFile values ('2015.12.066.UB.New U.S. Bank Bond Requirements (277).docx' , '2015-11-17 06:00:00');
insert into MDFile values ('2015.11.061.AccP.Travel and Expense Policy (292).docx' , '2015-11-20 06:00:00');
insert into MDFile values ('2015.11.066.SAPD.City of San Antonio HIP (302).docx' , '2015-11-30 06:00:00');
insert into MDFile values ('2015.12.062b.UB.DU Vs. LP Comparison Chart (303).docx' , '2015-12-01 06:00:00');
insert into MDFile values ('2015.12.064.CB.Disclosure Printing And Storage Updates In Byte Announcement (304).docx' , '2015-12-02 06:00:00');
insert into MDFile values ('Setting Up Your Documents For Publication (308).docx' , '2015-12-07 06:00:00');
insert into MDFile values ('2015.12.067.ITP.Avoiding Phishing Attacks Announcement (309).docx' , '2015-12-08 06:00:00');
insert into MDFile values ('Setting Up Documents For Publication In GIPP (318).docx' , '2015-12-09 06:00:00');
insert into MDFile values ('GIPP Document Publication Process Flow (319).docx' , '2015-12-09 06:00:00');
insert into MDFile values ('2015.12.###.CLP.Declined Loan Procedure (321).docx' , '2015-12-10 06:00:00');
insert into MDFile values ('2015.12.069.SEP.Escrow Waiver (326).docx' , '2015-12-16 06:00:00');
insert into MDFile values ('2016.12.069.SEP.Escrow Waiver Policy (3722).docx' , '2015-12-16 06:00:00');
insert into MDFile values ('2016.12.069.SEP.Escrow Waiver Policy (4563).docx' , '2015-12-16 06:00:00');
insert into MDFile values ('2016.12.069.SEP.Escrow Waiver Policy (4980).docx' , '2015-12-16 06:00:00');
insert into MDFile values ('2016.06.058.RP.Appraiser Independence Policy (329).docx' , '2015-12-22 06:00:00');
insert into MDFile values ('2016.04.034.CP.Red Flags -- Identity Theft Policy (334).docx' , '2015-12-23 06:00:00');
insert into MDFile values ('2016.01.002a.CB.Notice of Incompleteness Letter Announcement (335).docx' , '2015-12-29 06:00:00');
insert into MDFile values ('2015.12.070.COPR.Compliance Corner 1_ Seven-Day Waiting Period For Revised Disclosures (336).docx' , '2015-12-28 06:00:00');
insert into MDFile values ('2016.01.001.COPR.Compliance Corner Q&A 2_ Suspicious Activity Reporting (SAR) (337).docx' , '2015-12-28 06:00:00');
insert into MDFile values ('2016.01.004.CB.Compliance Interactive Calculation Tool Update Announcement (338).docx' , '2015-12-28 06:00:00');
insert into MDFile values ('Soliciting FHA Bankruptcy loans for Loss Mitigation (339).docx' , '2015-12-29 06:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4691).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.01.002b.CB.Sample Notice of Incompleteness Letter (342).docx' , '2016-01-05 06:00:00');
insert into MDFile values ('2016.01.003b.CB.Sample Withdraw Confirmation Letter (344).docx' , '2016-01-05 06:00:00');
insert into MDFile values ('2016.02.020.CP.Equal Credit Opportunity Act (ECOA) Policy (345).docx' , '2016-01-06 06:00:00');
insert into MDFile values ('2016.02.012.CB.Reverse Occupancy Scheme Notice Bulletin (348).docx' , '2016-01-14 06:00:00');
insert into MDFile values ('2016.05.044.SEP.Fair and Responsible Collections Policy (363).docx' , '2016-01-26 06:00:00');
insert into MDFile values ('2016.09.106.SEP.Fair and Responsible Collections Policy (3616).docx' , '2016-01-26 06:00:00');
insert into MDFile values ('2016.09.106.SEP.Fair and Responsible Collections Policy (4404).docx' , '2016-01-26 06:00:00');
insert into MDFile values ('2016.09.106.SEP.Fair and Responsible Collections Policy (4537).docx' , '2016-01-26 06:00:00');
insert into MDFile values ('2016.09.106.SEP.Fair and Responsible Collections Policy (4717).docx' , '2016-01-26 06:00:00');
insert into MDFile values ('2016.01.###.Property Preservation Procedures (368).docx' , '2016-01-27 06:00:00');
insert into MDFile values ('2016.03.024.CB.Initial Disclosures and the 3-Day Rule (383).docx' , '2016-02-08 06:00:00');
insert into MDFile values ('2016.02.014.CB.Flood Certifications for Second Liens Announcement (385).docx' , '2016-02-08 06:00:00');
insert into MDFile values ('2016.02.013.CB.Providing Loan Estimates After Closing Disclosures (386).docx' , '2016-02-08 06:00:00');
insert into MDFile values ('2016.02.015a.CB.Removal of Flood Disaster Form From IDS Announcement (387).docx' , '2016-02-09 06:00:00');
insert into MDFile values ('2016.02.015b.CB.Sample IDS Flood Disaster Form Letter (388).docx' , '2016-02-09 06:00:00');
insert into MDFile values ('2016.02.019.CP.SAFE Act Policy (389).docx' , '2016-02-10 06:00:00');
insert into MDFile values ('2016.02.019.CP.SAFE Act Policy (3657).docx' , '2016-02-10 06:00:00');
insert into MDFile values ('2016.04.038.SEP.Short Payoff Policy (390).docx' , '2016-02-15 06:00:00');
insert into MDFile values ('2016.02.016a.CB.Patriot Act Disclosure and Patriot Act ID Form Announcement (391).docx' , '2016-02-15 06:00:00');
insert into MDFile values ('2016.02.016b.CB.Patriot Act Disclosure and Patriot Act ID Form Sample Letters (392).docx' , '2016-02-15 06:00:00');
insert into MDFile values ('2016.03.###.SEP.Mortgage Insurance Management Policy (393).docx' , '2016-02-16 06:00:00');
insert into MDFile values ('2016.10.120.CP.Anti-Predatory Lending Policy (395).docx' , '2016-02-19 06:00:00');
insert into MDFile values ('2016.04.033.SEP.Payment Return Policy (396).docx' , '2016-02-19 06:00:00');
insert into MDFile values ('2016.09.033.SEP.Payment Reversal and Return Policy (3654).docx' , '2016-02-19 06:00:00');
insert into MDFile values ('2016.02.018.CB.FInal Inspection Fee DIsclosure Announcement (397).docx' , '2016-02-22 06:00:00');
insert into MDFile values ('2016.02.021.CB.Update To Co-Borrower Credit Bureau Field In Byte (398).docx' , '2016-02-24 06:00:00');
insert into MDFile values ('2016.03.022.OOPR.Byte Pro User''s Reference (406).docx' , '2016-03-02 06:00:00');
insert into MDFile values ('2016.03.023.CB.Loan Originator Business Cards Reminder (407).docx' , '2016-03-02 06:00:00');
insert into MDFile values ('2016.03.025.CB.Gateway Lock-In Agreement Requirement  Announcement (408).docx' , '2016-03-08 06:00:00');
insert into MDFile values ('2016.06.060.SEP.Loss Draft Policy (410).docx' , '2016-03-08 06:00:00');
insert into MDFile values ('2016.06.060.SEP Loss Draft Policy (2563).docx' , '2016-03-08 06:00:00');
insert into MDFile values ('2016.06.063.SEP Homeowners and Wind_Hail Insurance Policy (411).docx' , '2016-03-10 06:00:00');
insert into MDFile values ('2016.06.063.SEP Homeowners and Wind_Hail Insurance Policy (5261).docx' , '2016-03-10 06:00:00');
insert into MDFile values ('2016.06.057.ITP.Data Classification Policy (412).docx' , '2016-03-11 06:00:00');
insert into MDFile values ('2017.Data Classification Policy (3964).docx' , '2016-03-11 06:00:00');
insert into MDFile values ('ITP.Data Classification Policy (4915).docx' , '2016-03-11 06:00:00');
insert into MDFile values ('2016.04.039.OPD.TCF Bank HELOC - Home Equity Line of Credit (414).docx' , '2016-03-14 05:00:00');
insert into MDFile values ('2016.04.039.OPD.TCF Bank HELOC - Home Equity Line of Credit (477).docx' , '2016-03-14 05:00:00');
insert into MDFile values ('TCF Bank HELOC - Home Equity Line of Credit (1498).docx' , '2016-03-14 05:00:00');
insert into MDFile values ('2016.03.026.OB.Preventing Zero-Dollar Closing Fees Announcement (416).docx' , '2016-03-15 05:00:00');
insert into MDFile values ('2016.03.026.OB.GLADS Catalog List Announcement (418).docx' , '2016-03-15 05:00:00');
insert into MDFile values ('2016.06.054a.CP.Records Management Policy (424).docx' , '2016-03-21 05:00:00');
insert into MDFile values ('2016.06.056.ITP.Acceptable Encryption Policy (427).docx' , '2016-03-22 05:00:00');
insert into MDFile values ('2017.ITP.Acceptable Encryption Policy (3963).docx' , '2016-03-22 05:00:00');
insert into MDFile values ('ITP.Acceptable Encryption Policy (4677).docx' , '2016-03-22 05:00:00');
insert into MDFile values ('2016.03.###.PRDPD.Texas Veterans Land Board (429).docx' , '2016-03-22 05:00:00');
insert into MDFile values ('2016.06.055.ITPR.Incident Response Procedure (431).docx' , '2016-03-23 05:00:00');
insert into MDFile values ('2016.06.055.ITPR.Incident Response Procedure (3856).docx' , '2016-03-23 05:00:00');
insert into MDFile values ('2016.06.055.ITPR.Incident Response Procedure (5192).docx' , '2016-03-23 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (433).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4021).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4066).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4166).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4288).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4545).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4561).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4650).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4652).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4745).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4767).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4830).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4877).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (4963).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.032.OPD.TXVET-Texas Veterans Land Board (5082).docx' , '2016-03-24 05:00:00');
insert into MDFile values ('2016.04.029.UPD.Home Ready (439).pdf' , '2016-03-29 05:00:00');
insert into MDFile values ('2016.04.030.OOPR.FNMA HomeReady Byte Submission Tips (443).docx' , '2016-03-30 05:00:00');
insert into MDFile values ('2016.04.043.SEP.Recast Policy (449).docx' , '2016-04-05 05:00:00');
insert into MDFile values ('2017.02.043.SEP.Recast Policy (3829).docx' , '2016-04-05 05:00:00');
insert into MDFile values ('2017.02.043.SEP.Recast Policy (4071).docx' , '2016-04-05 05:00:00');
insert into MDFile values ('2017.02.043.SEP.Recast Policy (5021).docx' , '2016-04-05 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4774).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.04.033.ITLMS.Customer Welcome Letter (454).docx' , '2016-04-07 05:00:00');
insert into MDFile values ('2016.04.036.CApp.FinCEN Mortgage Loan Red Flags Checklist (458).docx' , '2016-04-11 05:00:00');
insert into MDFile values ('Pre-Funding QC Review (460).docx' , '2016-04-19 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Gift100 Conventional (462).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Gift100 Conventional (3622).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (3874).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4090).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4123).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4144).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4556).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4575).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4653).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4606).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4804).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (4892).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (5188).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (5330).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.05.046.PRDPD.REI Conventional Gift100 (5390).docx' , '2016-04-20 05:00:00');
insert into MDFile values ('2016.06.064.OB.New Construction Loan Field In Byte (464).docx' , '2016-04-25 05:00:00');
insert into MDFile values ('The Gateway Mortgage Group Writer''s Style Guide (466).docx' , '2016-04-25 05:00:00');
insert into MDFile values ('2016.06.###.CP.Real Estate Settlement Procedures Act (RESPA) Policy (473).docx' , '2016-05-03 05:00:00');
insert into MDFile values ('2016.05.052.ITPR.Employee Name Change Procedure (475).docx' , '2016-05-06 05:00:00');
insert into MDFile values ('2016.05.052.ITPR.Employee Name Change Procedure (2555).docx' , '2016-05-06 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4713).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.06.062.SEP.Check Retention Policy (487).docx' , '2016-05-23 05:00:00');
insert into MDFile values ('2016.06.062.SEP.Check Retention Policy (4072).docx' , '2016-05-23 05:00:00');
insert into MDFile values ('2016.05.051.UP.Retail Underwriting OFAC Policy (1487).docx' , '2016-05-24 05:00:00');
insert into MDFile values ('2016.06.054b.CApp.Record Management Schedule (1500).docx' , '2016-06-01 05:00:00');
insert into MDFile values ('2016.06.061.PB.Ordering HVE for Freddie Mac Relief Refinanced (1504).docx' , '2016-06-08 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (1519).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (3627).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (3793).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (3891).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4141).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4197).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4399).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4534).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4708).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4729).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4874).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Home Advantage Government (4964).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.07.088.PRDPD.North Carolina Housing Finance Agency - Home Advantage (Government) (5374).docx' , '2016-06-14 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (1526).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (1544).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (1545).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (3789).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (3818).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4077).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4189).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4486).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4529).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4633).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4638).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4824).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (4921).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (5200).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (5214).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (5227).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (5240).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (5281).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.06.066.PRDPD. Homeownership Across Texas (5420).docx' , '2016-06-20 05:00:00');
insert into MDFile values ('2016.07.080.AccP.Electronic Funds Transfer Policy (1535).docx' , '2016-06-24 05:00:00');
insert into MDFile values ('2016.07.075.UP.Conflict Of Interest Policy (1537).docx' , '2016-06-27 05:00:00');
insert into MDFile values ('2016.07.078.CB.GIPP System Update Bulletin (2546).docx' , '2016-07-05 05:00:00');
insert into MDFile values ('2016.07.077.CO.GIPP 9.2 Release Notes (2550).docx' , '2016-07-08 05:00:00');
insert into MDFile values ('2016.12.###.SEPR.Processing Servicing Releases Procedure (2560).docx' , '2016-07-14 05:00:00');
insert into MDFile values ('2016.07.083.OB.Flood Certification Amendment Announcement (2561).docx' , '2016-07-14 05:00:00');
insert into MDFile values ('2016.07.###.UB.Crediting And Documenting Earnest Money Bulletin (2564).docx' , '2016-07-18 05:00:00');
insert into MDFile values ('2016.07.###.PCPR.Insuring FHA Loan Procedure (2584).docx' , '2016-07-26 05:00:00');
insert into MDFile values ('2016.07.###.PCPR.Insuring FHA Loan Procedure (5156).docx' , '2016-07-26 05:00:00');
insert into MDFile values ('2016.07.###.SEPR.Verification Of Mortgage.Procedure (2587).docx' , '2016-07-28 05:00:00');
insert into MDFile values ('2017.03.PCPR.Insuring Correspondent RSDA (RD) (2590).docx' , '2016-07-26 05:00:00');
insert into MDFile values ('2016.10.122.PCPR.Auditing Correspondent Notes Procedure (2591).docx' , '2016-07-26 05:00:00');
insert into MDFile values ('2016.10.114.PCPR.Correcting Correspondent Notes Procedure (2592).docx' , '2016-07-27 05:00:00');
insert into MDFile values ('2016.09.104.PCPR.Post Pools Procedure (2593).docx' , '2016-07-27 05:00:00');
insert into MDFile values ('2016.09.104.PCPR.Post Pools Procedure (3949).docx' , '2016-07-27 05:00:00');
insert into MDFile values ('2016.09.110.PRDPD.City of Killeen HAP Product Description (2596).docx' , '2015-02-01 06:00:00');
insert into MDFile values ('2016.07.###.SEPR.Responding To An Automated Consumer Dispute Verification (ACDV) (2597).docx' , '2016-07-28 05:00:00');
insert into MDFile values ('2016.07.###.SEPR.Processing NTC Exceptions for Lien Releases (2599).docx' , '2016-07-28 05:00:00');
insert into MDFile values ('2016.10.117.PCPR.Auditing Fannie Mae Collateral Documents Procedure (3618).docx' , '2016-08-19 05:00:00');
insert into MDFile values ('2016.09.111.PCPR.PA-Purchased Loans Report Import Procedure (3632).docx' , '2016-08-25 05:00:00');
insert into MDFile values ('2016.09.108.PCPR.Shipping Request Import Procedure (3633).docx' , '2016-08-25 05:00:00');
insert into MDFile values ('2016.09.109.PCPR Warehouse Collateral Received Not Sent Import Procedure (3635).docx' , '2016-08-25 05:00:00');
insert into MDFile values ('2016.08.###.PCPR Post Closing Shipping Receiving Procedure (3636).docx' , '2016-08-25 05:00:00');
insert into MDFile values ('2016.08.###.PCPR Post Closing Shipping Receiving Procedure (3948).docx' , '2016-08-25 05:00:00');
insert into MDFile values ('2016.MM.###.PCPR.Matching for 184 Loans Procedure (3648).docx' , '2016-08-30 05:00:00');
insert into MDFile values ('2016.10.121.CP.Higher-Priced Mortgage Loan Policy (3650).docx' , '2016-09-01 05:00:00');
insert into MDFile values ('Update Loan Notes in PA Screen (3651).docx' , '2016-09-06 05:00:00');
insert into MDFile values ('Accessing Warehouse Bank Wiring Instructions for Investors (3658).docx' , '2016-09-13 05:00:00');
insert into MDFile values ('Byte - adding to stored documents & status updates (3659).docx' , '2016-09-13 05:00:00');
insert into MDFile values ('2016.10.126.PCPR.Endorsing Correspondent Notes Procedure (3670).docx' , '2016-09-28 05:00:00');
insert into MDFile values ('2017.03.28 PCPR (3674).docx' , '2016-10-06 05:00:00');
insert into MDFile values ('2017.03.28 PCPR.Verifying HUD 184 Loans Procedure (3916).docx' , '2016-10-06 05:00:00');
insert into MDFile values ('Verifying Guarantees on Rural Development Loans Procedure (3675).docx' , '2016-10-06 05:00:00');
insert into MDFile values ('2017.03.PCPR.Correspondent Note Return (3678).docx' , '2016-10-13 05:00:00');
insert into MDFile values ('2016.10.###.PCPR. Correspondent Note Collateral (3680).docx' , '2016-10-13 05:00:00');
insert into MDFile values ('2017.03.PRPC.Correspondent Prior to Funding_Cancelled Trailing Docs Received (3681).docx' , '2016-10-13 05:00:00');
insert into MDFile values ('2016.10.127.CB.GIPP User Interface Update Bulletin (3708).docx' , '2016-10-20 05:00:00');
insert into MDFile values ('2017.03.PCPR.Family First Notes (3718).docx' , '2016-10-24 05:00:00');
insert into MDFile values ('2017.03.PCPR.In House Notes (3961).docx' , '2016-10-24 05:00:00');
insert into MDFile values ('2017.03.PCPR.Correspondent Scan, Upload and Filing of Notes (3719).docx' , '2016-10-24 05:00:00');
insert into MDFile values ('2017.03.PCPR.Correspondent Title Policy Corrections (3721).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('Requesting a Check or an ACH Transfer (3724).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('Tennessee Housing Development Agency (3725).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('2nd Lien Original Notes for Bond Loans (3726).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('Fiserv Pay History (3727).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('Scanning Documents (3728).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('2017.03.PCPR.Correspondent Trailing Docs Received (3729).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('2016.10.###.PCPR.Correspondent Missing Trailng Docs (3730).docx' , '2016-10-25 05:00:00');
insert into MDFile values ('TVLB Citi Files (3731).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('Requesting Original Notes to be delivered to the Investors (3732).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('Purchase Advices (3733).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('OHFA - Oklahoma Housing Finance Agency (3734).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('US Bank loan files (3735).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('2016.10.###.PCPR.Correcting Correspondent Mortgages (3737).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('BYTE Fannie Mae Document Report Procedure (3741).docx' , '2016-10-07 05:00:00');
insert into MDFile values ('Oklahoma REI (3744).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('MCC Stand Alone Files (3745).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('Stand Alone - Galton 2nd Lien Files (3746).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('Jumbo Files (3747).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('MHDC - Missouri Housing Devolpment Commission (3748).docx' , '2016-10-26 05:00:00');
insert into MDFile values ('2016.10.###.PCPR.Insuring Correspondent VA Loans (3751).docx' , '2016-10-27 05:00:00');
insert into MDFile values ('2016.11.###.SEPR.Running Monthly OFAC Search (3753).docx' , '2016-10-28 05:00:00');
insert into MDFile values ('South Carolina Housing (3754).docx' , '2016-11-02 05:00:00');
insert into MDFile values ('Louisiana Housing Corporation - Standard Mortgage (3755).docx' , '2016-11-02 05:00:00');
insert into MDFile values ('CHFAPREF – CHFA Preferred Plus (CONV) (3781).docx' , '2016-11-28 06:00:00');
insert into MDFile values ('CHFAPREF – CHFA Preferred Plus (CONV) (3913).docx' , '2016-11-28 06:00:00');
insert into MDFile values ('2016.11.30.ITPR.Information Security Plan (3792).docx' , '2016-11-30 06:00:00');
insert into MDFile values ('2017.MM.###.SEPR.Performing QRPC Procedure (3798).docx' , '2016-12-09 06:00:00');
insert into MDFile values ('2017.02.###.SEPR.Reconciling FNMA and FHLMC Cash Sales (3857).docx' , '2017-02-22 06:00:00');
insert into MDFile values ('2017.03.059.PRDPD.Florida Housing Finance Corporation (Government) (5044).docx' , '2015-10-20 05:00:00');
insert into MDFile values ('2017.03.21.PC.GNMA Matching Exception Report (3900).docx' , '2017-03-21 05:00:00');
insert into MDFile values ('2017.06.###.C.SCRA Policy (4006).docx' , '2017-06-01 05:00:00');
insert into MDFile values ('Hold Transfer Upload (4097).docx' , '2017-08-10 05:00:00');
insert into MDFile values ('Servicing Delinquency Management Strategy (4662).docx' , '2018-01-24 06:00:00');

select * from MDFile;

select *  from tbicustone.document d 
inner join MDFile m on upper(trim(d.displayname ||'.' || d.extension)) =  upper(m.FileName);

/*
-- Testing 
select *  from tbicustone.document where displayname like '%Testing%';
select id, upper(displayname), version, createdon, modifiedon  from tbicustone.document where id= 15538;
select id, displayname, version, createdon, modifiedon  from tbicustone.documentversion where documentid= 15538;

update tbicustone.document
set  createdon= '2018-01-01 05:00:00' ,modifiedon= '2018-01-01 05:00:00'
where id= 15538;

update tbicustone.documentversion
set createdon= '2018-01-01 05:00:00',modifiedon= '2018-01-01 05:00:00'
where documentid= 15538;

Drop  table MDFile ;
create temporary table MDFile 
(
FileName character varying(255) NULL,
OrginalDate timestamp without time zone NOT NULL
);

insert into MDFile values ( 'testingUpdates.txt', '2018-01-01 05:00:00');
insert into MDFile values ( 'test6.txt', '2018-02-01 05:00:00');

select * from MDFile;
 
select *  from tbicustone.document d 
inner join MDFile m on upper(trim(d.displayname ||'.' || d.extension)) =  upper(m.FileName);

 
select *  from tbicustone.document d 
, MDFile m where upper(trim(d.displayname ||'.' || d.extension)) =  upper(m.FileName);


select id, displayname,extension, createdon,modifiedon from tbicustone.document where id  =15017;
select id, documentid,displayname, extension, displayname, createdon,modifiedon from tbicustone.documentversion where documentid  =15017;


update tbicustone.document d 
set 
createdon= '2017-01-01 05:00:00'
,modifiedon= '2017-01-01 05:00:00'
from MDFile m
where upper(trim(d.displayname ||'.' || d.extension)) =  upper(m.FileName);


update tbicustone.documentversion d 
set 
createdon= '2017-01-01 05:00:00'
,modifiedon= '2017-01-01 05:00:00'
from MDFile m
where upper(trim(d.displayname ||'.' || d.extension)) =  upper(m.FileName);
*/

