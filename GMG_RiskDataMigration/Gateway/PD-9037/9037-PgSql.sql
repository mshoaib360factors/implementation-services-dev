Drop  table if exists MDFileUser ;
create temporary table MDFileUser 
(
FileName character varying(255) NULL,
LastName character varying(255) NULL,
FirstName character varying(255) NULL
);

select concat('insert into MDFileUser values (''',File_Name,''', ''', LastName,''', ''',FirstName,''');') as insertStatement From 
(
select  Replace(File_Name,'''','''''') as File_Name, Replace(LastName,'''','''''') as LastName,  Replace(FirstName,'''','''''') as FirstName from
(
select DISTINCT File_Name, trim(substr(LnFn,1,instr(`LnFn`,',')-1)) as LastName, trim(substr(LnFn,instr(`LnFn`,',')+1, length(`LnFn`))) FirstName   from 
(
SELECT File_Name, trim(`Document_Owner_Name__Optional_`) as LnFn FROM etl.gateway where Document_Owner_Name__Optional_ like '%Israel%'
)x
)y
)z; 







 SELECT 
    u.id, d.createdby, d.modifiedby
FROM
    MDFileUser m
        INNER JOIN
    gmg.dmsuser u ON UPPER(TRIM(m.lastname)) = UPPER(TRIM(u.lastname))
        AND UPPER(TRIM(m.firstname)) = UPPER(TRIM(u.firstname))
        INNER JOIN
    gmg.documentversion d ON UPPER(m.filename) = CONCAT(UPPER(displayname),
            '.',
            UPPER(extension))
WHERE
    d.createdby = 12569
        AND d.modifiedby = 12569
        AND m.filename = '2015.10.050.UPD.Oklahoma Housing Finance Agency - Dream_Gold Programs (HFA Preferred) (5389).docx'
LIMIT 10;
