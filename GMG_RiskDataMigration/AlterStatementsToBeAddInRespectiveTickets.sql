#PD-14251
ALTER TABLE `predict360`.`RiskRegister` 
ADD COLUMN `riskDescription` TEXT NULL DEFAULT NULL AFTER `riskName`;

#PD-14235
ALTER TABLE `predict360`.`ControlInstance` 
ADD COLUMN `ControlInstDesc` TEXT NULL DEFAULT NULL AFTER `name`;
 