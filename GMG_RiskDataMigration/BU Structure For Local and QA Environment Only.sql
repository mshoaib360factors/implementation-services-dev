Drop table if exists etl.`gmg.RiskDataImport15092020_tempBUs`;
create table if not exists etl.`gmg.RiskDataImport15092020_tempBUs`
(
`Level1` varchar(255) DEFAULT NULL,
`Level2` varchar(255) DEFAULT NULL,
`Level3` varchar(255) DEFAULT NULL
);

insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', '', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Bank Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Deposit Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Loan Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Commercial Lending', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Consumer Lending', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Private Banking', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Retail Banking', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Bank', 'Treasury Management', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', '', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Marketing', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Quantitative Analytics', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Executive Management', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Legal', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Legal', 'Internal Audit');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Information Technology', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Human Resources', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Accounting & Finance', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Facilities', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Corporate Communications', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Risk', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Risk', 'Credit Administration');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Risk', 'Enterprise Risk Management (ERM)');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Risk', 'Operational Risk');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Change Management (Project Management)', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Quality Control', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Compliance', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Corporate', 'Vendor Management', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', '', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Mortgage Operations Support', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Closing', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Underwriting', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Consumer Direct', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Specialty Lending', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Appraisal Management', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Mortgage Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Correspondent Origination', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Correspondent Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Post -Closing', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Secondary', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Strategic Products', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Retail Production', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Origination', 'Retail Production', 'Retail');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Servicing', '', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Servicing', 'Servicing Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Servicing', 'Servicing Default Operations', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Servicing', 'Servicing Customer Care', '');
insert into etl.`gmg.RiskDataImport15092020_tempBUs` values ('Mortgage Servicing', 'Servicing Performing Loan Operations', '');

ALTER TABLE `etl`.`gmg.RiskDataImport15092020_tempBUs` ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);

set sql_safe_updates = 0;

update `etl`.`gmg.RiskDataImport15092020_tempBUs` 
set Level1 = NULL 
where Level1 = '';

update `etl`.`gmg.RiskDataImport15092020_tempBUs` 
set Level2 = NULL 
where Level2 = '';

update `etl`.`gmg.RiskDataImport15092020_tempBUs` 
set Level3 = NULL 
where Level3 = '';




set sql_safe_updates = 1;


select distinct Level1, Level2, Level3 from etl.`gmg.RiskDataImport15092020_tempBUs`;

select distinct Level3 from etl.`gmg.RiskDataImport15092020_tempBUs` where Level3 is not null;

select * from predict360.Facility ;

#Inserting LEvel1
insert into predict360.Facility ( name, customerId, createdBy, createdOn, modifiedBy, modifiedOn, isBU, status)
select distinct trim(Level1), @CustomerId, @UserId ,current_timestamp(), @UserId , current_timestamp() , 1, 1 
from etl.`gmg.RiskDataImport15092020_tempBUs` where Level1 is not null;

#Inserting LEvel2
insert into predict360.Facility ( name, parentId, customerId, createdBy, createdOn, modifiedBy, modifiedOn, isBU, status)
select distinct trim(Level2), f.id, @CustomerId, @UserId ,current_timestamp(), @UserId , current_timestamp() , 1, 1 
from etl.`gmg.RiskDataImport15092020_tempBUs` d
inner join predict360.Facility f on f.name = d.Level1
where  Level2 is not null and  customerid = @CustomerId and createdBy = @UserId and modifiedBy = @UserId ;
  
#Inserting LEvel3
insert into predict360.Facility ( name, parentId, customerId, createdBy, createdOn, modifiedBy, modifiedOn, isBU, status) 
select distinct trim(Level3), f.id, @CustomerId, @UserId ,current_timestamp(), @UserId , current_timestamp() , 1, 1 
from etl.`gmg.RiskDataImport15092020_tempBUs` d
inner join predict360.Facility f on f.name = d.Level2
where  Level3 is not null and  customerid = @CustomerId and createdBy = @UserId and modifiedBy = @UserId ;

# Adding the remaining which are not in list.
insert into predict360.Facility ( name, customerId, createdBy, createdOn, modifiedBy, modifiedOn, isBU, status)
select distinct trim(Business_Unit_Name), @CustomerId, @UserId ,current_timestamp(), @UserId , current_timestamp() , 1, 1  from etl.`gmg.riskdataimport05102020` 
where trim(Business_Unit_Name) not in (select distinct name from predict360.Facility where customerid = @CustomerId and createdBy = @UserId and modifiedBy = @UserId );

