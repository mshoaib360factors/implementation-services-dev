set @CustomerId = 2641;
set @UserId = 20951;

/**************************************************************************
			RiskRegisterItem  & RiskRegisterItemMapping
***************************************************************************/

# Risk Taxonomy  (Category) --- Level 1
insert into predict360.RiskRegisterItem (`name`, isLeaf, createdBy, createdDate, customerId, description )
Select distinct trim(RISK_AREA_TYPE),0, @UserId, current_timestamp(), @CustomerId, ''
from etl.`gmg.riskdataimport21102020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '';

# RiskRegisterItemMapping Inserting Level 1 Information
insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri1.id as Level1Id, null 
from 
(select distinct Risk_Index, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.riskdataimport21102020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '') l1 
inner join 
(select id, name from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf =0) rri1 on rri1.name = l1.RISK_AREA_TYPE;

######################################################################################

# Risk Taxonomy  (Category) --- Level 2
insert into predict360.RiskRegisterItem (`name`, parentId, isLeaf, createdBy, createdDate,  customerId,  description)
select distinct trim(Risk_Area__Sub_category_),  rri.id as parentId, 0, @UserId, current_timestamp(),  @CustomerId, ''
from  etl.`gmg.riskdataimport21102020` d
inner join  predict360.RiskRegisterItem rri on rri.name = d.RISK_AREA_TYPE and CustomerId = @CustomerId
where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '';


#Mapping the Links between Level 2 and Level 1 Taxonomies
insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select id, parentId from predict360.RiskRegisterItem where CustomerId = @CustomerId and parentId is not null and isLeaf = 0;

Update predict360.RiskRegisterItem
set parentId = NULL 
where CustomerId = @CustomerId and parentId is not null;

######################################################################################

#### Inserting into RiskRegisterItem i.e. Risk Definitions

Drop temporary table if exists tempdb.RiskTaxonimies;
create temporary table tempdb.RiskTaxonimies
select Level1Id, level1, l2.id as Level2Id, l2.Name as Level2, l2.id as LeafParentId
	from 
(
	select l1.id as Level1Id, l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
	inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
	where l1.CustomerId = @CustomerId
)level1
inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId and isLeaf = 0

UNION ALL 
 
select l1.id as Level1Id,l1.name as Level1, -1 as Level2Id, 'NaN' as Level2, l1.id as LeafParentId  from  predict360.RiskRegisterItem l1 
inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
where l1.CustomerId = @CustomerId;



Drop temporary table if exists tempdb.FinalRiskDef;
create temporary table tempdb.FinalRiskDef
select distinct  d.Risk_Name, rt.LeafParentId, 1, @UserId,  current_timestamp(), @CustomerId, Risk_Description, Risk_Index 
 from
(
	select ri.*, ifnull(p.Risk_Area__Sub_category_, 'NaN') as Risk_Area__Sub_category_  , p.RISK_AREA_TYPE from 
	(
		select distinct Risk_Index, trim(Risk_Name) as Risk_Name, trim(Risk_Description) as Risk_Description from  etl.`gmg.riskdataimport21102020` where Risk_Name is not null and Risk_Index is not null
	) ri
	inner join  
	(
		select distinct Risk_Index,  Trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_ ,Trim(RISK_AREA_TYPE) as RISK_AREA_TYPE 
		from  etl.`gmg.riskdataimport21102020`
		where  Risk_Name is not null and Risk_Index is not null and (RISK_AREA_TYPE is not null or trim(RISK_AREA_TYPE) <> '') 
	)p
    on p.Risk_Index = ri.Risk_Index
)d

inner join  tempdb.RiskTaxonimies rt
on  rt.level2 = d.Risk_Area__Sub_category_ and rt.level1 = d.RISK_AREA_TYPE ;

insert into predict360.RiskRegisterItem (`name`, isLeaf, createdBy, createdDate, customerId, description, riskDefinitionId)
select distinct Risk_Name, 1, @UserId, current_timestamp(), @CustomerId, Risk_Description, Risk_Index from tempdb.FinalRiskDef; 


#### Mapping the links between Risk and Categories
insert into predict360.RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri.id, fr.LeafParentId from tempdb.FinalRiskDef fr
inner join predict360.RiskRegisterItem rri on rri.riskDefinitionId = fr.Risk_Index and customerId =@CustomerId ;


#Start from here

/**************************************************************************
			predict360.RiskRegisterItemApplicablity
***************************************************************************/

# RiskRegisterItem with Business Units


Drop temporary table if exists tempdb.InitialDataFromFile;
create temporary table tempdb.InitialDataFromFile
select Risk_Index, ifnull(Risk_Area__Sub_category_, 'NaN') as Risk_Area__Sub_category_, RISK_AREA_TYPE, Parent, Business_Unit_Name from 
(
	select * from 
	(
		select distinct 
					Risk_Index, trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE
				, 	ifnull(trim(Risk_Area__Sub_category_), trim(RISK_AREA_TYPE)) as Parent
				, 	trim(Business_Unit_Name) as Business_Unit_Name  
		from  etl.`gmg.riskdataimport21102020` 
		where  Risk_Index is not null and (trim(Business_Unit_Name) <> '' or Business_Unit_Name is not null) 
	)x 
	where Parent is not null And Parent <> ''

	UNION ALL 

	select * from 
	(
	  select	distinct 
					Risk_Index, trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE
				,	ifnull(trim(Risk_Area__Sub_category_), trim(RISK_AREA_TYPE)) as Parent
				,	trim(Business_Unit_Name) as Business_Unit_Name from  etl.`gmg.riskdataimport21102020` 
	  where  Risk_Index is not null and (trim(Business_Unit_Name) = '' or Business_Unit_Name is null)
	 )x
	where Parent is not null And Parent <> ''
) y;



Drop temporary table if exists tempdb.RiskTaxonimiesL1L2L3;
create temporary table tempdb.RiskTaxonimiesL1L2L3
select  distinct Level2.*, l3.RiskDefinitionId as RiskDefId, l3.Id as RiskRegisterItemId
 from
(
	select Level1Id, level1, l2.id as Level2Id, l2.Name as Level2
		from 
	(
		select l1.id as Level1Id, l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
		inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
		where l1.CustomerId = @CustomerId
	)level1
	inner join predict360.RiskRegisterItemMapping m2 on m2.parentId = level1.riskRegisterItemId
	inner join predict360.RiskRegisterItem l2 on l2.id = m2.riskRegisterItemId and isLeaf = 0
)Level2
inner join predict360.RiskRegisterItemMapping m3 on m3.parentId = Level2.Level2Id
inner join predict360.RiskRegisterItem l3 on l3.id = m3.riskRegisterItemId and isLeaf = 1

UNION 

select Level1Id, level1, -1 as Level2Id, 'NaN' as Level2, l3.RiskDefinitionId as RiskDefId , l3.Id as RiskRegisterItemId
	from 
(
	select l1.id as Level1Id, l1.name as Level1, m1.* from  predict360.RiskRegisterItem l1 
	inner join predict360.RiskRegisterItemMapping m1 on m1.RiskRegisterItemId = l1.id and m1.ParentId is null
	where l1.CustomerId = @CustomerId
)level1
inner join predict360.RiskRegisterItemMapping m3 on m3.parentId = level1.riskRegisterItemId
inner join predict360.RiskRegisterItem l3 on l3.id = m3.riskRegisterItemId and isLeaf = 1;


Drop temporary table if exists tempdb.FinalSetOfData;
create temporary table tempdb.FinalSetOfData
select distinct rt.RiskRegisterItemId, if(rt.Level2Id = -1, rt.Level1Id, rt.Level2Id)  as immediateParentId,Business_Unit_Name
#, d.*, rt.* 
from  tempdb.InitialDataFromFile d
inner join  tempdb.RiskTaxonimiesL1L2L3 rt on rt.RiskDefId =  d.Risk_Index and rt.Level2 =  d.Risk_Area__Sub_category_ and rt.level1 =  d.RISK_AREA_TYPE  ;


insert into predict360.RiskRegisterItemApplicablity ( customerId, buFacilityId, applicablity, riskRegisterItemId, immediateParentId)
select distinct @CustomerId, Business_Unit_Name as buFacilityId, 'Applicable' as applicablity, RiskRegisterItemId, immediateParentId from   tempdb.FinalSetOfData d where Business_Unit_Name is null;

set SQL_SAFE_UPDATES = 0;

delete from   tempdb.FinalSetOfData  where Business_Unit_Name is null;

set SQL_SAFE_UPDATES = 1;

insert into predict360.RiskRegisterItemApplicablity ( customerId, buFacilityId, applicablity, riskRegisterItemId, immediateParentId)
select distinct @CustomerId, og.id as buFacilityId, 'Applicable' as applicablity, RiskRegisterItemId, immediateParentId  
from  tempdb.FinalSetOfData d
inner join predict360.Facility og 
on og.name = trim(d.Business_Unit_Name) and og.customerid = @CustomerId ;




/**************************************************************************
			predict360.RiskRegister
***************************************************************************/
# Start from here it should be 342 i think which is equivalent to no. of rows in ItemApplicability.

insert into predict360.RiskRegister (riskName, riskDescription, customerId, riskRegisterItemId, buFacilityId, frequencyCount, frequencyOccurence, status, createdBy, createdDate, weight, riskInstanceId)
select distinct 
  rri.`name` as riskName, trim(d.Risk_Description) as Risk_Description, a.customerId, riskRegisterItemId, buFacilityId, 1 as frequencyCount, 'Year' as frequencyOccurence, 'Active' as `status`
, @UserId as createdBy, current_timestamp() as createdDate, 1 as weight, rri.riskDefinitionId
from predict360.RiskRegisterItemApplicablity a
inner join predict360.RiskRegisterItem rri on rri.id = a.riskRegisterItemId and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.isLeaf = 1
inner join etl.`gmg.riskdataimport21102020` d on d.Risk_Index = rri.riskDefinitionId  and d.Risk_Description is not null
where a.CustomerId = @CustomerId and applicablity = 'Applicable' ;


/**************************************************************************
			predict360.RiskRegisterOverallAnalysis
***************************************************************************/
 
insert into predict360.RiskRegisterOverallAnalysis (riskId, inherentLikelihood, inherentImpact, inherentRiskRating, residualRiskRating, customerId, createdBy, createdDate)
select 
 r.id as riskId, d.Likelihood_1_5 as inherentLikelihood, Impact_1_5 as  inherentImpact, Inherent_Risk_Rating as inherentRiskRating,  Residual_Risk_Rating as residualRiskRating
, @CustomerId, @UserId as createdBy,  current_timestamp() as createdDate

from 
(
 select distinct rr.id, rri.riskDefinitionId, rr.riskRegisterItemId, rr.buFacilityId from predict360.RiskRegister rr
 inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId
 where rr.customerId = @CustomerId and rr.customerid = @CustomerId and rr.createdBy = @UserId
) r
left join 
(
select distinct Risk_Index,  Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, Business_Unit_Name, og.id as buFacilityId  from
 (
  select distinct Risk_Index, ifnull(Risk_Area__Sub_category_,RISK_AREA_TYPE) Parent, trim(Business_Unit_Name) as Business_Unit_Name, Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, max(Residual_Risk_Rating) as Residual_Risk_Rating
  from  etl.`gmg.riskdataimport21102020` 
  where  Risk_Index is not null 
  and (Impact_1_5 is not null or Likelihood_1_5 is not null or Inherent_Risk_Rating is not null or Residual_Risk_Rating is not null)
  group by Risk_Index, ifnull(Risk_Area__Sub_category_,RISK_AREA_TYPE), trim(Business_Unit_Name), Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating
 )i
 left join predict360.Facility og on og.name = i.Business_Unit_Name and og.customerid = @CustomerId
 where i.Parent is not null
)d
on d.Risk_Index = r.riskDefinitionId and ifnull(d.buFacilityId,-1) = ifnull(r.buFacilityId, -1); 



/*****************************************************************************************
******************************************************************************************
******************************************************************************************

									Controls

******************************************************************************************
******************************************************************************************
*****************************************************************************************/



/**************************************************************************
			predict360.ControlType
***************************************************************************/
insert into predict360.ControlType (name, description, customerId, createdBy, createdOn)
select distinct trim(Control_Type__preventive__detective__corrective_) as `name`, trim(Control_Type__preventive__detective__corrective_) as `description` 
, @CustomerId, @UserId,  current_timestamp()
from  etl.`gmg.riskdataimport21102020`  
where trim(Control_Type__preventive__detective__corrective_) not in (select trim(name)  from predict360.ControlType where customerId = @customerId) and
Control_Type__preventive__detective__corrective_ is not null and trim(Control_Type__preventive__detective__corrective_) <> '';



/**************************************************************************
			predict360.RiskRegisterControlItem -- Level 1
***************************************************************************/
/*
### Inserting Level 1: Temporary Statements
ALTER TABLE `etl`.`gmg.riskdataimport21102020` 
ADD COLUMN `ControlCategory_TmpCol` VARCHAR(100) NULL DEFAULT 'Temp Control Category' AFTER `Inherent_Risk_Rating`;

insert into predict360.RiskRegisterControlItem(name, createdBy, createdDate, updatedBy, updatedDate, isLeaf, customerId, isDeleted)
select distinct ControlCategory_TmpCol as Level1, @UserId, current_timestamp(), @UserId , current_timestamp(), 0 as isLeaf, @CustomerId, 0 as isDeleted 
from  etl.`gmg.riskdataimport21102020` ;

/**************************************************************************
			predict360.RiskRegisterControlItem
***************************************************************************/
/*
###### fDo consult with Aamir or GK. About the transition of column Efficacy from predict360.RiskRegisterControlItem to ControlInstance or RiskRegisterControlInstance

insert into predict360.RiskRegisterControlItem ( name, parentId, createdBy, createdDate, updatedBy, updatedDate, isLeaf, customerId, isDeleted, controlTypeId, controlDefinitionId)
select 

 d.Control_Name as Control_Name, p.id as parentId, @UserId, current_timestamp(), @UserId , current_timestamp(), 1 as isLeaf
, @CustomerId, 0 as isDeleted, ct.id as ControlTypeId, Control_Index as controlDefinitionId
from 
(
	select distinct 
	  Trim(ControlCategory_TmpCol) as ControlCategory_TmpCol
	, trim(Control_Index) as Control_Index
	, trim(Control_Name) as Control_Name
	, trim(Control_Type__preventive__detective__corrective_) as Control_Type__preventive__detective__corrective_
	, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_
	from  etl.`gmg.riskdataimport21102020` 
	where Control_Index is not null 
	and Control_Index not in ('**', 'see ''Finance'' process',  'IS Control', 'IS Controls', 'TBD', 
	'RB-C46' # Removing Temporarily
	)
	and Control_Type__preventive__detective__corrective_  is  not null and Control_Execution__automated__manual_ is not null
)d
inner join predict360.RiskRegisterControlItem p 
on Trim(d.ControlCategory_TmpCol) =  p.name and isLeaf = 0 and p.customerid = @CustomerId and isDeleted = 0
inner join predict360.ControlType ct on ct.name = d.Control_Type__preventive__detective__corrective_ and ct.CustomerId =  @CustomerId ;


/**************************************************************************
	           predict360.ControlInstance
***************************************************************************/

 # predict360.ControlInstance
 
insert into predict360.ControlInstance (name, ControlInstDesc, riskRegisterControlItemId, efficacy, implemented, createdBy, createdDate, isDeleted, controlInstanceId, controlStrength)
select Control_Name, Control_Description, riskRegisterControlItemId, efficacy, implemented, UserId, CreatedOn, IsDeleted, Control_Index, max(controlStrength)
from 
(
	select distinct substring(Control_Name,1,255) as Control_Name, Control_Description, riskRegisterControlItemId, 100.00 as efficacy, 100.00 as implemented, @UserId as UserId,  current_timestamp() as CreatedOn,0 as IsDeleted, controlStrength, Control_Index  from
	(
		select  Risk_Index, Control_Index, riskRegisterControlItemId, max(Control_Name) as Control_Name, max(Control_Description) as Control_Description, max(controlStrength) as controlStrength
		from  
		(
			select distinct d.Risk_Index, d.Control_Index, ci.riskRegisterControlItemId , d.Control_Name , d.Control_Description, ad.value as controlStrength  from 
			(

				select distinct 
				  Risk_Index
				, Control_Index
				, trim(Entity_Level_or_General) as Entity_Level_or_General
				, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
				, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 
				, trim(Control_Name) as Control_Name
				, trim(Control_Description) as Control_Description
				, trim(Control_Effectiveness) as Control_Effectiveness
				from etl.`gmg.riskdataimport21102020` 

			) d
			 
			inner join
			(

				select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
				inner join predict360.ControlType ct on ct.id = l.controlTypeId
				inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
				where l.CustomerId = @CustomerId

			)ci
			on  ci.controlExecution = Control_Execution__automated__manual_ 
			and ci.ControlType = Control_Type__preventive__detective__corrective_
			and ci.ParentName = Entity_Level_or_General
			
			left join predict360.RiskAnalysisDimension ad on ad.CustomerId = @CustomerId and ad.type = 'controlStrength' and ad.label = Control_Effectiveness
			

			where
				 Control_Index is not null and Risk_Index is not null
			and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
			and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') 
			
		)x
		group by Risk_Index, Control_Index, riskRegisterControlItemId
	)y
)z
group by Control_Name, Control_Description, riskRegisterControlItemId, efficacy, implemented, UserId, CreatedOn, IsDeleted,Control_Index;



/**************************************************************************
				predict360.RiskRegisterControlInstance
***************************************************************************/

Drop temporary table if exists tempdb.Consolidated;
create temporary table tempdb.Consolidated
select distinct RiskIds, ControlInsId, Weight from 
(
	select a.RiskIds, a.riskDefinitionId, a.BusinessUnit, a.Control_Index, cin.id as ControlInsId, ifnull(min(d.Control_Weight),100) as Weight 
	from
	(
	select rr.id as RiskIds , rri.riskDefinitionId, f.name as BusinessUnit, d.Control_Index, max(trim(d.Control_Name)) as Control_Name, max(trim(d.Control_Description)) as Control_Description
	from predict360.RiskRegister rr 
	left join predict360.Facility f on f.id = rr.buFacilityId
	inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
	left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and ifnull(trim(d.Business_Unit_Name), -1)  =  ifnull(f.name, -1)
	where rr.CustomerId = @CustomerId
	#and rri.riskDefinitionId = 'RM-R1'
	#and rri.riskDefinitionId = 'RM-R10' and d.Control_Index = 'BC-C5'
	# and rri.riskDefinitionId = 'MO-R1' and d.Control_Index = 'MO-C2'
	group by rr.id, rri.riskDefinitionId, f.name, d.Control_Index
	) a
	inner join
	(
		select c.id, c.controlInstanceId, c.name as ControlInsName, c.ControlInstDesc, ct.* from predict360.ControlInstance c
		inner join 

		(
			select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
			inner join predict360.ControlType ct on ct.id = l.controlTypeId
			inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
			where l.CustomerId = @CustomerId
		)ct
		on ct.riskRegisterControlItemId = c.riskRegisterControlItemId

	)cin  on cin.controlInstanceId =  a.Control_Index and cin.ControlInsName = substring(a.Control_Name,1,255)  and cin.ControlInstDesc =a.Control_Description 

	inner join etl.`gmg.riskdataimport21102020` d 
	on d.Control_Index is not null and substring(trim(d.Control_Name),1,255) = cin.ControlInsName and trim(d.Control_Description) = cin.ControlInstDesc 
	and ifnull(trim(d.Business_Unit_Name),-1) = ifnull(a.BusinessUnit,-1) and a.riskDefinitionId = d.Risk_Index 
	and cin.controlExecution = trim(d.Control_Execution__automated__manual_) and cin.ControlType = trim(d.Control_Type__preventive__detective__corrective_) and cin.ParentName = trim(d.Entity_Level_or_General)
	group by a.RiskIds, a.riskDefinitionId, a.BusinessUnit, a.Control_Index, cin.id 
)final ;



Drop temporary table if exists tempdb.OddTotalWeights;
create temporary table tempdb.OddTotalWeights
select distinct RiskIds from 
(
select RiskIds, sum(Weight) as TotalWeight from tempdb.Consolidated 
group by RiskIds
)x
where TotalWeight <> 100
;

Drop temporary table if exists tempdb.OddTotalWeightsControlsCount;
create temporary table tempdb.OddTotalWeightsControlsCount
select c.RiskIds, count(distinct c.ControlInsId) as TotalControlInst from  tempdb.Consolidated c
inner join  tempdb.OddTotalWeights o on o.RiskIds = c.RiskIds
group by c.RiskIds;



insert into predict360.RiskRegisterControlInstance (riskRegisterId, controlInstanceId, weight)
select c.RiskIds, c.ControlInsId, 100/TotalControlInst as Weight 
from tempdb.OddTotalWeightsControlsCount o
inner join tempdb.Consolidated c on c.RiskIds = o.RiskIds;
 

set sql_safe_updates= 0;
delete from tempdb.Consolidated where RiskIds in (select distinct RiskIds from tempdb.OddTotalWeights);
set sql_safe_updates= 1;

insert into predict360.RiskRegisterControlInstance (riskRegisterId, controlInstanceId, weight)
select * from tempdb.Consolidated ;

