/**************************************************************************
			RiskRegisterItem  & RiskRegisterItemMapping
***************************************************************************/

select * from Customer where CustomerCode = 'GMGIMP'; # 2641
select * from PredictUser where CustomerId = 2641; # 20951

select count(1) from
(select distinct Risk_Name from `gmg.RiskDataImport15092020`)x ; #313

select count(1) from
(select distinct Risk_Index from `gmg.RiskDataImport15092020`)x ; #310

select Risk_Name, count(1) from `gmg.RiskDataImport15092020` group by Risk_Name having count(1) > 1;
select Risk_Index, count(1) from `gmg.RiskDataImport15092020` group by Risk_Index having count(1) > 1;

select Risk_Index, count(1) from `gmg.RiskDataImport15092020` group by Risk_Index having count(1) > 1;

# Taxonomy Level 1
#Select RISK_AREA_TYPE, Risk_Area__Sub_category_ from etl.`gmg.RiskDataImport15092020` ;

/*
set SQL_SAFE_UpdateS = 0;
Update  etl.`gmg.RiskDataImport15092020` 
set Risk_Area__Sub_category_ = 'OpSubCat 8'
where RISK_AREA_TYPE = 'Operational'  and Risk_Index like '%8';




Update  etl.`gmg.RiskDataImport15092020` 
set Risk_Area__Sub_category_ = 'OpSubCat 7'
where RISK_AREA_TYPE = 'Operational'  and Risk_Index like '%7';


Update  etl.`gmg.RiskDataImport15092020` 
set Risk_Area__Sub_category_ = 'OpSubCat 9'
where RISK_AREA_TYPE = 'Operational'  and Risk_Index like '%9';

set SQL_SAFE_UpdateS = 1; 
*/

select 
distinct 
Risk_Area__Sub_category_  
from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '';


/** Levels Relation Identifications from File ***/

#Level 1


select  distinct rri1.id as Level1Id, null from 
(select distinct Risk_Index, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '') l1 
inner join 
(select id, name from RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and UpdatedBy = @UserId and isLeaf is null) rri1 on rri1.name = l1.RISK_AREA_TYPE;




select distinct RISK_AREA_TYPE as Level1, rri1.id as Level1Id, Risk_Area__Sub_category_ as Level2, rri2.id as Level2Id from 
(select distinct Risk_Index, Trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_ from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '' ) l2
inner join 
(select distinct Risk_Index, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '') l1 on l1.Risk_Index = l2.Risk_Index
# Levels id identifications from RiskRegisterItem
inner join 
(select id, name from RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and UpdatedBy = @UserId and isLeaf is null) rri2 on rri2.name = l2.Risk_Area__Sub_category_
inner join 
(select id, name from RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and UpdatedBy = @UserId and isLeaf is null) rri1 on rri1.name = l1.RISK_AREA_TYPE;


###>>> Start From Here 

#Risk Name
select distinct Risk_Index, Risk_Name, Risk_Description, RISK_AREA_TYPE, Risk_Area__Sub_category_  from  etl.`gmg.RiskDataImport15092020` ;

select * from 
(
select distinct Risk_Index  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
) ri
inner join  etl.`gmg.RiskDataImport15092020` TaxLvl1 on TaxLvl1.Risk_Index = ri.Risk_Index;


## Start from here

Drop temporary table if exists tempdb.t1;
create temporary table tempdb.t1
select distinct Risk_Index, Risk_Name, Risk_Description, rri.id as RiskId,final.parent, rrip.id as ParentId from 
(
	select distinct ri.*, rn.Risk_Name, Risk_Description, ifnull( TaxLevel2.Risk_Area__Sub_category_,  TaxLevel1.RISK_AREA_TYPE) as Parent from 
	(
	select distinct Risk_Index  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
	and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
	) ri
	left join (select distinct Risk_Index,  Trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '')TaxLevel1
	on TaxLevel1.Risk_Index = ri.Risk_Index
	left join (select distinct Risk_Index,  Trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_  from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) is not null) TaxLevel2
	on TaxLevel2.Risk_Index = ri.Risk_Index
	inner join (select Risk_Index, trim(Risk_Name) as Risk_Name, trim(Risk_Description) as Risk_Description from  etl.`gmg.RiskDataImport15092020` where Risk_Name is not null) RN
	on RN.Risk_Index = ri.Risk_Index
)Final
inner join RiskRegisterItem rrip on rrip.name = final.Parent and rrip.customerid = @CustomerId and rrip.createdBy = @UserId and rrip.UpdatedBy = @UserId and rrip.isLeaf is null

inner join RiskRegisterItem rri on 
# rri.name = final.Risk_Name and  rri.description = final.Risk_Description 
rri.riskDefinitionId = final.Risk_Index
and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.UpdatedBy = @UserId and rri.isLeaf = 1
# >> Change the above condition to RiskDefinition ID to avoid duplications.
where Final.Parent is not null ;


select Risk_Name, count(distinct Risk_Index ) from tempdb.t1 group by Risk_Name having count(distinct Risk_Index ) > 1;

select * from tempdb.t1  where Risk_Name = 'Excessive Lender Paid Fees';

/**************************************************************************
			RiskRegister
***************************************************************************/

select * from predict360.Facility;

select max(createdDate) from RiskRegister where frequencyCount is null or frequencyOccurence is null;

select distinct Process_Name, Business_Unit_Name from etl.`gmg.RiskDataImport15092020`;

show create table RiskRegister;

select * from predict360.RiskRegister where customerId = @CustomerId;
select * from predict360.RiskRegisterItemApplicablity where customerId = @CustomerId;
select * from predict360.RiskRegisterItem;
 

select * from RiskRegisterOverallAnalysis ;

select * from controlinstance;
 
 


select * from  etl.`gmg.RiskDataImport15092020` ;

select distinct ri.Risk_Index, count(1)
from 
(
select distinct Risk_Index, Business_Unit_Name  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
) ri
group by Risk_Index 
having count(1) > 1;


insert into predict360.RiskRegister (riskName, customerId, riskRegisterItemId, buFacilityId, frequencyCount, frequencyOccurence, status, createdBy, createdDate, modifiedBy, modifiedDate, weight)

select distinct 
  rri.`name` as riskName, a.customerId, riskRegisterItemId, buFacilityId, 1 as frequencyCount, 'Year' as frequencyOccurence, 'Active' as `status`
, @UserId as createdBy, current_timestamp() as createdDate, @UserId as modifiedBy, current_timestamp() as modifiedDate, 1 as weight
from predict360.RiskRegisterItemApplicablity a
inner join predict360.RiskRegisterItem rri on rri.id = a.riskRegisterItemId and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.UpdatedBy = @UserId and rri.isLeaf = 1
where a.CustomerId = @CustomerId and applicablity = 'Applicable' ;
 
 # Starting from here 
select * from predict360.RiskRegisterOverallAnalysis limit 10;
select * from etl.`gmg.RiskDataImport15092020` limit 10;
  

drop temporary table if exists tempdb.t1;
create temporary table tempdb.t1
select distinct Risk_Index,  Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, Business_Unit_Name  from
(
select distinct Risk_Index, ifnull(Risk_Area__Sub_category_,RISK_AREA_TYPE) Parent, Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, Business_Unit_Name
from  etl.`gmg.RiskDataImport15092020` 
where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
and (Impact_1_5 is not null or Likelihood_1_5 is not null or Inherent_Risk_Rating is not null or Residual_Risk_Rating)
)d
where Parent is not null;

select * from tempdb.t1 where Risk_Index in ( 'RM-R1', 'RM-R2');

select distinct Risk_Index,  Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, Business_Unit_Name, og.id as buFacilityId  from
 (
  select distinct Risk_Index, ifnull(Risk_Area__Sub_category_,RISK_AREA_TYPE) Parent, Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, trim(Business_Unit_Name) as Business_Unit_Name
  from  etl.`gmg.RiskDataImport15092020` 
  where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
  and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
  and (Impact_1_5 is not null or Likelihood_1_5 is not null or Inherent_Risk_Rating is not null or Residual_Risk_Rating)
 )i
 left join predict360.Facility og on og.name = i.Business_Unit_Name and og.customerid = @CustomerId
 where i.Parent is not null and  Risk_Index in ( 'MA-R1', 'MA-R2');
 
select distinct rri.riskDefinitionId, rr.riskRegisterItemId, rr.buFacilityId from predict360.RiskRegister rr
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId
where rr.customerId = @CustomerId and rr.customerid = @CustomerId and rr.createdBy = @UserId and rr.modifiedBy = @UserId and rri.riskDefinitionId in ( 'MA-R1', 'MA-R2');

select * from predict360.Facility og  where id in (4018,3956);
 
select * from  predict360.RiskRegisterOverallAnalysis;


/*****************************************************************************************
******************************************************************************************
******************************************************************************************

									Controls

******************************************************************************************
******************************************************************************************
*****************************************************************************************/




Select * from predict360.ControlType;
Select * from predict360.RiskRegisterControlItem;

select * from Customer where CustomerCode = 'ERM';
select * from RiskRegisterItem where CustomerId = 2255 and name = 'Borrower Credit Risk - Gateway portfolio' ;
select * from RiskRegisterItemApplicablity where CustomerId = 2255 and riskregisterItemId = 2150;
select * from RiskRegister where CustomerId = 2255 and riskregisterItemId = 2150;
select * from RiskRegisterOverallAnalysis where CustomerId = 2255 and riskId = 2018;

select * from RiskRegisterControlItem where CustomerId = 2255 and id = 667;
select * from ControlInstance where riskRegisterControlItemId = 667;
select * from RiskRegisterControlInstance where ControlInstanceId = 847;

describe RiskRegisterControlInstance;


Select * from predict360.RiskRegisterControlItem;
Select * from predict360.RiskRegisterControlItem;

Select * from predict360.ControlInstance;
Select * from predict360.RiskRegisterControlInstance;

/**************************************************************************
	 predict360.ControlInstance & predict360.RiskRegisterControlInstance
***************************************************************************/
/*

# Cases to verify 
# a)

select * from predict360.RiskRegister 
where customerId = 2641 and riskRegisterItemId = 3157;

# b)
select * from predict360.RiskRegister 
where customerId = 2641 and riskRegisterItemId in (3170, 3269);

*/
# Start from Here



select max(length(Control_Description)) from etl.`gmg.RiskDataImport15092020` ;

select distinct ControlItemId from tempdb.ContItemToRR;

select * from predict360.RiskRegisterControlItem ci where customerId = 2641 and id not in (select ControlItemId from  tempdb.t1 ) ;

select * from
(SELECT distinct Risk_Index, Control_Index FROM etl.`gmg.riskdataimport15092020` where Risk_Index is not null ) r 
inner join 
 ;

select m.riskRegisterItemId, count(1) from  predict360.RiskRegisterItemMapping m
inner join predict360.RiskRegisterItem rri on rri.id = m.riskRegisterItemId and rri.customerId = 2641
group by riskRegisterItemId having count(1) > 1;

 
 select * from predict360.RiskRegisterItem where customerId = 2641;
 
 select * from predict360.RiskRegisterControlInstance order by id desc limit 174;
 
 


















