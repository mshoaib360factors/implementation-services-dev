set SQL_SAFE_UPDATES = 0;



######################################################################################
Delete from predict360.RiskRegisterControlInstance  
where riskRegisterId in (select id from predict360.RiskRegister where CustomerId = @CustomerId);

#797

######################################################################################
Delete  from predict360.ControlInstance 
where riskRegisterControlItemId in (select id from predict360.RiskRegisterControlItem where CustomerId = @CustomerId);
#539
######################################################################################

Delete from  predict360.RiskRegisterOverallAnalysis where CustomerId = @CustomerId;

######################################################################################


Delete from predict360.RiskRegister  where customerId = @CustomerId 
and riskRegisterItemId <> 2229 -- Temporary Condition
;

######################################################################################

delete from predict360.RiskRegisterItemApplicablity where customerid = @CustomerId ;

######################################################################################

#Deleting Mappings of Risk Defintions 
delete rrm.* from  predict360.RiskRegisterItemMapping rrm
inner join  predict360.RiskRegisterItem rri on rri.id = rrm.riskRegisterItemId and customerid = @CustomerId and createdBy = @UserId and isLeaf = 1
and rrm.riskRegisterItemId not in (2229,2230) ;-- Temporary Condition


#Deleting Risk Defintions 
delete from predict360.RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and isLeaf = 1 and id not in (2229,2230);

######################################################################################

#Deleting Level 2 & Level 1 Risk Taxonomies
delete from predict360.RiskRegisterItemMapping 
where riskRegisterItemId in (select id from predict360.RiskRegisterItem where customerid = @CustomerId  );

 

delete from predict360.RiskRegisterItem where CustomerId = @CustomerId  ;

set SQL_SAFE_UPDATES = 1;