drop temporary table if exists tempdb.t1;
create temporary table tempdb.t1
select distinct rr.id as RiskIds , rri.riskDefinitionId, d.Control_Index, cin.id as ControlInsId , cin.name, cin.ControlInstDesc , d.Business_Unit_Name , d.Control_Weight
from predict360.RiskRegister rr 
left join predict360.Facility f on f.id = rr.buFacilityId
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and ifnull(trim(d.Business_Unit_Name), -1) = ifnull(f.name, -1)
inner join  predict360.ControlInstance cin  on cin.controlInstanceId =  d.Control_Index and cin.name = substring(trim(d.Control_Name),1,255)  and cin.ControlInstDesc =trim(d.Control_Description)
where rr.CustomerId = @CustomerId;


select RiskIds,  ControlInsId, count(1) from  tempdb.t1 group by RiskIds, ControlInsId having count(1) > 1;

select RiskIds, TotalWeight from
(
select RiskIds, Business_Unit_Name, sum(Control_Weight) as TotalWeight from tempdb.t1
where RiskIds not in (3631,3755)
group by  RiskIds, Business_Unit_Name
)x
where TotalWeight > 100;


select * from  tempdb.t1 where RiskIds in (3631,3755);







select distinct rr.id as RiskIds , rri.riskDefinitionId,d.Business_Unit_Name, cin.*
from predict360.RiskRegister rr
left join predict360.Facility f on f.id = rr.buFacilityId
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and ifnull(trim(d.Business_Unit_Name), -1) = ifnull(f.name, -1)
left join  predict360.ControlInstance cin  on cin.controlInstanceId =  d.Control_Index and cin.name = substring(trim(d.Control_Name),1,255)  and cin.ControlInstDesc =trim(d.Control_Description)
 where rr.CustomerId = @CustomerId and cin.id is null;

Drop temporary table if exists tempdb.t;
create temporary table tempdb.t
select distinct rr.id as RiskIds , rri.riskDefinitionId, f.name as BusinessUnit, d.Control_Index, cin.id as ControlInsId/* , cin.name, cin.ControlInstDesc , d.Business_Unit_Name , d.Control_Weight*/
from predict360.RiskRegister rr 
left join predict360.Facility f on f.id = rr.buFacilityId
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and ifnull(trim(d.Business_Unit_Name), -1) = ifnull(f.name, -1)
inner join  predict360.ControlInstance cin  on cin.controlInstanceId =  d.Control_Index and cin.name = substring(trim(d.Control_Name),1,255)  and cin.ControlInstDesc =trim(d.Control_Description)
where rr.CustomerId = @CustomerId;


select RiskIds, riskDefinitionId, BusinessUnit, Control_Index, count(1) from tempdb.t group by RiskIds, riskDefinitionId, BusinessUnit, Control_Index having count(1) > 1;

select RiskIds, Control_Index, count(1) from tempdb.t group by RiskIds, Control_Index having count(1) > 1;

select RiskIds, ControlInsId, count(1) from tempdb.t group by RiskIds, ControlInsId having count(1) > 1;


select * from tempdb.t  where RiskIds = 3755  ;

select * from predict360.ControlInstance where id in (1451,1477); # This needs to be Correct with the addition of Join Clause ControlTax

select * from predict360.ControlInstance where id in (1615,1616); # This needs to be Correct with the addition of Join Clause ControlTax


Drop temporary table if exists tempdb.t;
create temporary table tempdb.t
select distinct a.RiskIds, a.riskDefinitionId, a.BusinessUnit, a.Control_Index, cin.id as ControlInsId, d.Control_Weight from
(
select 
distinct rr.id as RiskIds , rri.riskDefinitionId, f.name as BusinessUnit, d.Control_Index, max(trim(d.Control_Name)) as Control_Name, max(trim(d.Control_Description)) as Control_Description
from predict360.RiskRegister rr 
left join predict360.Facility f on f.id = rr.buFacilityId
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and trim(d.Business_Unit_Name)  =  f.name
where rr.CustomerId = @CustomerId
# and rri.riskDefinitionId = 'RM-R10' and d.Control_Index = 'RM-C1'
 and rri.riskDefinitionId = 'RM-R10' and d.Control_Index = 'BC-C5'
group by rr.id, rri.riskDefinitionId, f.name, d.Control_Index
) a
inner join  predict360.ControlInstance cin  on cin.controlInstanceId =  a.Control_Index and cin.name = substring(a.Control_Name,1,255)  and cin.ControlInstDesc =a.Control_Description
left join etl.`gmg.riskdataimport21102020` d 
on d.Control_Index is not null and substring(trim(d.Control_Name),1,255) = cin.name and trim(d.Control_Description) = cin.ControlInstDesc 
and trim(d.Business_Unit_Name) = a.BusinessUnit and a.riskDefinitionId = d.Risk_Index ;




Drop temporary table if exists tempdb.t;
create temporary table tempdb.t
select a.RiskIds, a.riskDefinitionId, a.BusinessUnit, a.Control_Index, cin.id as ControlInsId, min(d.Control_Weight) as Control_Weight from
(
select 
distinct rr.id as RiskIds , rri.riskDefinitionId, f.name as BusinessUnit, d.Control_Index, max(trim(d.Control_Name)) as Control_Name, max(trim(d.Control_Description)) as Control_Description
from predict360.RiskRegister rr 
left join predict360.Facility f on f.id = rr.buFacilityId
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and trim(d.Business_Unit_Name)  =  f.name
where rr.CustomerId = @CustomerId
# and rri.riskDefinitionId = 'RM-R10' and d.Control_Index = 'RM-C1'
# and rri.riskDefinitionId = 'RM-R10' and d.Control_Index = 'BC-C5'
 and rri.riskDefinitionId = 'RM-R1' and d.Control_Index = 'MO-C2'
group by rr.id, rri.riskDefinitionId, f.name, d.Control_Index
) a
inner join  predict360.ControlInstance cin  on cin.controlInstanceId =  a.Control_Index and cin.name = substring(a.Control_Name,1,255)  and cin.ControlInstDesc =a.Control_Description
left join etl.`gmg.riskdataimport21102020` d 
on d.Control_Index is not null and substring(trim(d.Control_Name),1,255) = cin.name and trim(d.Control_Description) = cin.ControlInstDesc 
and trim(d.Business_Unit_Name) = a.BusinessUnit and a.riskDefinitionId = d.Risk_Index 
group by a.RiskIds, a.riskDefinitionId, a.BusinessUnit, a.Control_Index, cin.id  ;




#### Here 



select * From  tempdb.Consolidated where RiskIds = 3535;
select * From  predict360.ControlInstance where id in (select ControlInsId From  tempdb.Consolidated where RiskIds = 3535);

select * From  predict360.RiskRegister where id = 3535;
select * From  predict360.RiskRegisterItem where id = 4503;




select distinct rr.id as RiskIds , rri.riskDefinitionId, f.name as BusinessUnit, d.Control_Index, cin.id as ControlInsId/* , cin.name, cin.ControlInstDesc , d.Business_Unit_Name , d.Control_Weight*/
from predict360.RiskRegister rr 
left join predict360.Facility f on f.id = rr.buFacilityId
inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId 
left join etl.`gmg.riskdataimport21102020` d on rri.riskDefinitionId = d.Risk_Index and Control_Index is not null and ifnull(trim(d.Business_Unit_Name), -1) = ifnull(f.name, -1)
inner join  predict360.ControlInstance cin  on cin.controlInstanceId =  d.Control_Index and cin.name = substring(trim(d.Control_Name),1,255)  and cin.ControlInstDesc =trim(d.Control_Description)
where rr.CustomerId = @CustomerId and rri.riskDefinitionId = 'RM-R10' and d.Control_Index = 'RM-C1';


select controlInstanceId, riskRegisterControlItemId, count(1) 
from predict360.ControlInstance cin
inner join predict360.RiskRegisterControlItem ci on ci.id = cin.riskRegisterControlItemId
where customerID = @CustomerId
group by controlInstanceId, riskRegisterControlItemId
having count(1) > 1;


select *
from predict360.ControlInstance cin
inner join predict360.RiskRegisterControlItem ci on ci.id = cin.riskRegisterControlItemId
where controlInstanceId in ('FM-C20', 'IS-C12', 'M3-C2', 'RM-C1');






select distinct substring(Control_Name,1,255) as Control_Name, Control_Description, riskRegisterControlItemId, 100.00 as efficacy, 100.00 as implemented, @UserId,  current_timestamp(),0, Control_Index  from
(
	select  Risk_Index, Control_Index, riskRegisterControlItemId, max(Control_Name) as Control_Name, max(Control_Description) as Control_Description
    from  
    (
		select distinct d.Risk_Index, d.Control_Index, ci.riskRegisterControlItemId , d.Control_Name , d.Control_Description  from 
		(

			select distinct 
			  Risk_Index
			, Control_Index
			, trim(Entity_Level_or_General) as Entity_Level_or_General
			, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
			, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 
			, trim(Control_Name) as Control_Name
			, trim(Control_Description) as Control_Description
            , trim(Business_Unit_Name) as Business_Unit_Name
			from etl.`gmg.riskdataimport21102020` 

		) d
		 
		left join
		(

			select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
			inner join predict360.ControlType ct on ct.id = l.controlTypeId
			inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
			where l.CustomerId = @CustomerId

		)ci
		on  ci.controlExecution = Control_Execution__automated__manual_ 
		and ci.ControlType = Control_Type__preventive__detective__corrective_
		and ci.ParentName = Entity_Level_or_General

		where
			 Control_Index is not null and Risk_Index is not null
		and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
		and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') 
		
    )x
	group by Risk_Index, Control_Index, riskRegisterControlItemId
)y
;















select distinct Control_Index, d.* from etl.`gmg.riskdataimport21102020` d;

(
select distinct 
  Risk_Index
, Control_Index
, trim(Entity_Level_or_General) as Entity_Level_or_General
, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 
, trim(Control_Name) as Control_Name
, trim(Control_Description) as Control_Description
#, trim(Business_Unit_Name) as Business_Unit_Name

from etl.`gmg.riskdataimport21102020` 
where
		Control_Index is not null and Risk_Index is not null
and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') 
)d;


select count(1) from predict360.RiskRegisterItem where CustomerId = @CustomerId and isleaf = 1; # 372
 

select count(distinct RiskRegisterItemId) from predict360.RiskRegister where CustomerId = @CustomerId ;

select * from predict360.RiskRegisterItem where CustomerId = @CustomerId and id not in 
(select distinct RiskRegisterItemId from predict360.RiskRegister where CustomerId = @CustomerId);

















############################################################
############################################################
############################################################


select distinct Risk_Index, Control_Index, Control_Name, Entity_Level_or_General, Control_Description
, Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_, Control_Effectiveness, Control_Weight, Control_Type_Value, Control_Effectiveness_Value1, Business_Unit_Name
 from  etl.`gmg.riskdataimport21102020` where Control_Index is not null ;
 

select  Control_Index, Entity_Level_or_General, Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_ , max(Control_Name) as Control_Name, max(Control_Description) as Control_Description
from  etl.`gmg.riskdataimport21102020` 
where Control_Index is not null 
and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') 
and Control_Index not in ('HR-C22', 'HR-C27', 'MO-C2', 'MO-C7', 'RM-C7')  -- Temporary Condition
group by Control_Index, Entity_Level_or_General, Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_ ;


############################################################
############################################################
############################################################

Drop temporary table if exists tempdb.t1;
create temporary table tempdb.t1
select distinct 
  Control_Index
, trim(Entity_Level_or_General) as Entity_Level_or_General
, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 


from etl.`gmg.riskdataimport21102020` 
where
	 Control_Index is not null 
and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') ; 




Drop temporary table if exists tempdb.t2;
create temporary table tempdb.t2
select distinct d.Control_Index, ci.riskRegisterControlItemId  
 from 
(
select distinct 
  Control_Index
, trim(Entity_Level_or_General) as Entity_Level_or_General
, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 

from etl.`gmg.riskdataimport21102020` 
) d
 
left join
(
select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
inner join predict360.ControlType ct on ct.id = l.controlTypeId
inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
where l.CustomerId = @CustomerId
)ci
on  ci.controlExecution = Control_Execution__automated__manual_ 
and ci.ControlType = Control_Type__preventive__detective__corrective_
and ci.ParentName = Entity_Level_or_General

where
	 Control_Index is not null 
and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') ; 




Drop temporary table if exists tempdb.t3;
create temporary table tempdb.t3
select distinct d.Control_Index, ci.riskRegisterControlItemId , d.Control_Name , d.Control_Description  from 
(

select distinct 
  Control_Index
, trim(Entity_Level_or_General) as Entity_Level_or_General
, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 
, trim(Control_Name) as Control_Name
, trim(Control_Description) as Control_Description
from etl.`gmg.riskdataimport21102020` 

) d
 
left join
(

select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
inner join predict360.ControlType ct on ct.id = l.controlTypeId
inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
where l.CustomerId = @CustomerId

)ci
on  ci.controlExecution = Control_Execution__automated__manual_ 
and ci.ControlType = Control_Type__preventive__detective__corrective_
and ci.ParentName = Entity_Level_or_General

where
	 Control_Index is not null 
and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') ; 





Drop temporary table if exists tempdb.t4;
create temporary table tempdb.t4
select distinct d.Risk_Index, d.Control_Index, ci.riskRegisterControlItemId , d.Control_Name , d.Control_Description  from 
(

	select distinct 
	  Risk_Index
	, Control_Index
	, trim(Entity_Level_or_General) as Entity_Level_or_General
	, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
	, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 
	, trim(Control_Name) as Control_Name
	, trim(Control_Description) as Control_Description
	from etl.`gmg.riskdataimport21102020` 

) d
 
left join
(

	select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
	inner join predict360.ControlType ct on ct.id = l.controlTypeId
	inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
	where l.CustomerId = @CustomerId

)ci
on  ci.controlExecution = Control_Execution__automated__manual_ 
and ci.ControlType = Control_Type__preventive__detective__corrective_
and ci.ParentName = Entity_Level_or_General

where
	 Control_Index is not null and Risk_Index is not null
and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') ; 







select Control_Index, count(1)  from tempdb.t3 group by Control_Index having count(1) > 1 order by 1 ;
select Control_Index, count(1)  from tempdb.t2 group by Control_Index having count(1) > 1 order by 1;


select Risk_Index,Control_Index, count(1)  from tempdb.t4
group by Risk_Index, Control_Index 
having count(1) > 1
order by 2;



select distinct  Control_Index, Control_Name, Entity_Level_or_General, Control_Description
, Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_ #, Control_Weight, Business_Unit_Name
 from  etl.`gmg.riskdataimport21102020` where Control_Index  in  ('HR-C22', 'HR-C27', 'MO-C2', 'MO-C7', 'RM-C7') order by 1;
 
 
select distinct  * from  tempdb.t2 where Control_Index in ('HR-C22', 'HR-C27', 'MO-C2', 'MO-C7', 'RM-C7') order by 1;
select distinct  * from  tempdb.t3 where Control_Index in ('HR-C22', 'HR-C27', 'MO-C2', 'MO-C7', 'RM-C7') order by 1;



select distinct Control_Index, riskRegisterControlItemId, Control_Name, Control_Description from
(
	select  Risk_Index, Control_Index, riskRegisterControlItemId, max(Control_Name) as Control_Name, max(Control_Description) as Control_Description
    from  
    (
		select distinct d.Risk_Index, d.Control_Index, ci.riskRegisterControlItemId , d.Control_Name , d.Control_Description  from 
		(

			select distinct 
			  Risk_Index
			, Control_Index
			, trim(Entity_Level_or_General) as Entity_Level_or_General
			, trim(Control_Type__preventive__detective__corrective_)  as Control_Type__preventive__detective__corrective_
			, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_ 
			, trim(Control_Name) as Control_Name
			, trim(Control_Description) as Control_Description
			from etl.`gmg.riskdataimport21102020` 

		) d
		 
		left join
		(

			select l.controlExecution, ct.name as ControlType, p.name as ParentName, l.id as riskRegisterControlItemId from predict360.RiskRegisterControlItem l
			inner join predict360.ControlType ct on ct.id = l.controlTypeId
			inner join predict360.RiskRegisterControlItem p on p.id = l.parentId
			where l.CustomerId = @CustomerId

		)ci
		on  ci.controlExecution = Control_Execution__automated__manual_ 
		and ci.ControlType = Control_Type__preventive__detective__corrective_
		and ci.ParentName = Entity_Level_or_General

		where
			 Control_Index is not null and Risk_Index is not null
		and (Control_Type__preventive__detective__corrective_ is not null or  Control_Type__preventive__detective__corrective_ <> '')
		and (Control_Execution__automated__manual_ is not null or Control_Execution__automated__manual_ <> '') 
		
    )x
	group by Risk_Index, Control_Index, riskRegisterControlItemId
)y
;














select * from predict360.ControlType where CustomerId = @CustomerId;


 

select * from predict360.RiskRegisterControlItem limit 100;
select * from predict360.ControlInstance limit 100;
select * from predict360.RiskRegisterControlInstance limit 100;


select * from  predict360.RiskRegisterItem where riskDefinitionId = 'FR-R1';
select * from  predict360.RiskRegister where RiskRegisterItemId = 2749;
select * from  predict360.RiskRegister where RiskRegisterItemId = 2749;


select distinct * from  etl.`gmg.riskdataimport21102020` where Control_Index is not null and Control_Name is not null;
select distinct
  Control_Index, Control_Name, Entity_Level_or_General, Control_Description, Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_
#, Control_Weight, Business_Unit_Name
from  etl.`gmg.riskdataimport21102020` where Control_Index is not null and Control_Name is not null
and Control_Index = 'EC-C1';

select Control_Index, count(1) from 
(
select distinct
  Control_Index #, Control_Name
  , Entity_Level_or_General #, Control_Description
  , Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_
#, Control_Weight, Business_Unit_Name
from  etl.`gmg.riskdataimport21102020` where Control_Index is not null and Control_Name is not null
)x
group by Control_Index
having count(1) > 1;

select distinct
  Control_Index #, Control_Name
  , Entity_Level_or_General #, Control_Description
  , Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_
#, Control_Weight, Business_Unit_Name
from  etl.`gmg.riskdataimport21102020` where Control_Index is not null and Control_Name is not null
and Control_Index in ( 

select Control_Index from 
(
select distinct
  Control_Index #, Control_Name
  , Entity_Level_or_General #, Control_Description
  , Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_
#, Control_Weight, Business_Unit_Name
from  etl.`gmg.riskdataimport21102020` where Control_Index is not null and Control_Name is not null
)x
group by Control_Index
having count(1) > 1

)
order by 1 ;


select Control_Index, count(1) from
(
	select distinct
	  Control_Index  -- 473
	 # ,  Risk_index
	  , Control_Name
	  /*, Entity_Level_or_General #, Control_Description
	  , Control_Type__preventive__detective__corrective_, Control_Execution__automated__manual_
	#, Control_Weight, Business_Unit_Name*/
	from  etl.`gmg.riskdataimport21102020` where Control_Index is not null and Control_Name is not null
)x
group by Control_Index
having count(1) > 1;


select * from  etl.`gmg.riskdataimport21102020` where Control_Index = 'RB-C21';



