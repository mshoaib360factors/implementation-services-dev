set @CustomerId = 2641;
set @UserId = 20951;

/**************************************************************************
			RiskRegisterItem  & RiskRegisterItemMapping
***************************************************************************/

# Risk Taxonomy  (Category) --- Level 1
insert into RiskRegisterItem (`name`               , createdBy, createdDate         , updatedBy, updatedDate       , customerId, description)
Select distinct                trim(RISK_AREA_TYPE), @UserId    , current_timestamp() , @UserId    , current_timestamp(), @CustomerId     , null
from etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '';



# Risk Taxonomy  (Category) --- Level 2

insert into RiskRegisterItem (`name`                         , createdBy, createdDate         , updatedBy, updatedDate       ,  customerId, description)
select distinct                trim(Risk_Area__Sub_category_), @UserId    , current_timestamp() , @UserId    , current_timestamp(), @CustomerId     , null
from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '';


# RiskRegisterItemMapping 

#Inserting Level 1 Information
insert into RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri1.id as Level1Id, null 
from 
(select distinct Risk_Index, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '') l1 
inner join 
(select id, name from RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and UpdatedBy = @UserId and isLeaf is null) rri1 on rri1.name = l1.RISK_AREA_TYPE;


#Mapping the Links between Level 2 and Level 1 Taxonomies

insert into RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri2.id as Level2Id, rri1.id as Level1Id  from 
(select distinct Risk_Index, Trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_ from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) <> '' ) l2
inner join 
(select distinct Risk_Index, trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '') l1 on l1.Risk_Index = l2.Risk_Index
# Levels id identifications from RiskRegisterItem
inner join 
(select id, name from RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and UpdatedBy = @UserId and isLeaf is null) rri2 on rri2.name = l2.Risk_Area__Sub_category_
inner join 
(select id, name from RiskRegisterItem where customerid = @CustomerId and createdBy = @UserId and UpdatedBy = @UserId and isLeaf is null) rri1 on rri1.name = l1.RISK_AREA_TYPE;



 #### Inserting into RiskRegisterItem

insert into RiskRegisterItem (`name`, isLeaf, createdBy, createdDate, updatedBy, updatedDate, customerId, description, riskDefinitionId)
select distinct Risk_Name, 1, @UserId,  current_timestamp(), @UserId,  current_timestamp(),@CustomerId, Risk_Description, Risk_Index from 
(
select distinct ri.*, rn.Risk_Name, rn.Risk_Description, ifnull( TaxLevel2.Risk_Area__Sub_category_,  TaxLevel1.RISK_AREA_TYPE) as Parent from 
(
select distinct Risk_Index  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
) ri
left join (select distinct Risk_Index,  Trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '')TaxLevel1
on TaxLevel1.Risk_Index = ri.Risk_Index
left join (select distinct Risk_Index,  Trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_  from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) is not null) TaxLevel2
on TaxLevel2.Risk_Index = ri.Risk_Index
inner join (select Risk_Index, trim(Risk_Name) as Risk_Name, trim(Risk_Description) as Risk_Description from  etl.`gmg.RiskDataImport15092020` where Risk_Name is not null) RN
on RN.Risk_Index = ri.Risk_Index
)Final
where Final.Parent is not null;


 #### Mapping the links between Risk and Categories
insert into RiskRegisterItemMapping (riskRegisterItemId, parentId)
select distinct rri.id as RiskId, rrip.id as ParentId from 
(
	select distinct ri.*, rn.Risk_Name, Risk_Description, ifnull( TaxLevel2.Risk_Area__Sub_category_,  TaxLevel1.RISK_AREA_TYPE) as Parent from 
	(
	select distinct Risk_Index  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
	and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
	) ri
	left join (select distinct Risk_Index,  Trim(RISK_AREA_TYPE) as RISK_AREA_TYPE from  etl.`gmg.RiskDataImport15092020` where RISK_AREA_TYPE is not null and trim(RISK_AREA_TYPE) <> '')TaxLevel1
	on TaxLevel1.Risk_Index = ri.Risk_Index
	left join (select distinct Risk_Index,  Trim(Risk_Area__Sub_category_) as Risk_Area__Sub_category_  from  etl.`gmg.RiskDataImport15092020` where Risk_Area__Sub_category_ is not null and trim(Risk_Area__Sub_category_) is not null) TaxLevel2
	on TaxLevel2.Risk_Index = ri.Risk_Index
	inner join (select Risk_Index, trim(Risk_Name) as Risk_Name, trim(Risk_Description) as Risk_Description from  etl.`gmg.RiskDataImport15092020` where Risk_Name is not null) RN
	on RN.Risk_Index = ri.Risk_Index
)Final
inner join RiskRegisterItem rrip on rrip.name = final.Parent and rrip.customerid = @CustomerId and rrip.createdBy = @UserId and rrip.UpdatedBy = @UserId and rrip.isLeaf is null

inner join RiskRegisterItem rri on 
# rri.name = final.Risk_Name and  rri.description = final.Risk_Description 
rri.riskDefinitionId = final.Risk_Index
and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.UpdatedBy = @UserId and rri.isLeaf = 1
# >> Change the above condition to RiskDefinition ID to avoid duplications.
where Final.Parent is not null ;



/**************************************************************************
			predict360.RiskRegisterItemApplicablity
***************************************************************************/

# RiskRegisterItem with Business Units

insert into predict360.RiskRegisterItemApplicablity ( customerId, buFacilityId, applicablity, riskRegisterItemId, immediateParentId)
select @CustomerId, og.id as buFacilityId, 'Applicable' as applicablity, rri.id as riskRegisterItemId, rri.parentId as immediateParentId from 
(
select rri.id, rri.name, rri.riskDefinitionId, map.parentId
from predict360.RiskRegisterItem rri 
inner join predict360.RiskRegisterItemMapping map 
on map.riskRegisterItemId = rri.id and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.UpdatedBy = @UserId and rri.isleaf = 1
) rri
inner join 
(
select distinct Risk_Index, trim(Business_Unit_Name) as Business_Unit_Name  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
and (trim(Business_Unit_Name) <> '' or Business_Unit_Name is not null)
) d
on rri.riskDefinitionId = d.Risk_Index
inner join predict360.Facility og 
on og.name = trim(d.Business_Unit_Name) and og.customerid = @CustomerId and og.createdBy = @UserId and og.modifiedBy = @UserId


UNION ALL

# RiskRegisterItem without Business Units
select @CustomerId, null as buFacilityId, 'Applicable' as applicablity, rri.id as riskRegisterItemId, rri.parentId as immediateParentId 
from 
(
select rri.id, rri.name, rri.riskDefinitionId, map.parentId
from predict360.RiskRegisterItem rri 
inner join predict360.RiskRegisterItemMapping map 
on map.riskRegisterItemId = rri.id and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.UpdatedBy = @UserId and rri.isleaf = 1
) rri
inner join 
(
select distinct Risk_Index, trim(Business_Unit_Name) as Business_Unit_Name  from  etl.`gmg.RiskDataImport15092020` where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
and (trim(Business_Unit_Name) = '' or Business_Unit_Name is null)
) d
on rri.riskDefinitionId = d.Risk_Index;


/**************************************************************************
			predict360.RiskRegister
***************************************************************************/


insert into predict360.RiskRegister (riskName, customerId, riskRegisterItemId, buFacilityId, frequencyCount, frequencyOccurence, status, createdBy, createdDate, modifiedBy, modifiedDate, weight)
select distinct 
  rri.`name` as riskName, a.customerId, riskRegisterItemId, buFacilityId, 1 as frequencyCount, 'Year' as frequencyOccurence, 'Active' as `status`
, @UserId as createdBy, current_timestamp() as createdDate, @UserId as modifiedBy, current_timestamp() as modifiedDate, 1 as weight
from predict360.RiskRegisterItemApplicablity a
inner join predict360.RiskRegisterItem rri on rri.id = a.riskRegisterItemId and rri.customerid = @CustomerId and rri.createdBy = @UserId and rri.UpdatedBy = @UserId and rri.isLeaf = 1
where a.CustomerId = @CustomerId and applicablity = 'Applicable' ;



/**************************************************************************
			predict360.RiskRegisterOverallAnalysis
***************************************************************************/
 
insert into predict360.RiskRegisterOverallAnalysis (riskId, inherentLikelihood, inherentImpact, inherentRiskRating, residualRiskRating, customerId, createdBy, createdDate, modifiedBy, modifiedDate)
select 
 r.id as riskId, d.Likelihood_1_5 as inherentLikelihood, Impact_1_5 as  inherentImpact, Inherent_Risk_Rating as inherentRiskRating,  Residual_Risk_Rating as residualRiskRating
, @CustomerId, @UserId as createdBy,  current_timestamp() as createdDate, @UserId as modifiedBy,  current_timestamp() as modifiedDate

from 
(
 select distinct rr.id, rri.riskDefinitionId, rr.riskRegisterItemId, rr.buFacilityId from predict360.RiskRegister rr
 inner join predict360.RiskRegisterItem rri on rri.id = rr.riskRegisterItemId
 where rr.customerId = @CustomerId and rr.customerid = @CustomerId and rr.createdBy = @UserId and rr.modifiedBy = @UserId 
) r
left join 
(
select distinct Risk_Index,  Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, Business_Unit_Name, og.id as buFacilityId  from
 (
  select distinct Risk_Index, ifnull(Risk_Area__Sub_category_,RISK_AREA_TYPE) Parent, Impact_1_5, Likelihood_1_5, Inherent_Risk_Rating, Residual_Risk_Rating, trim(Business_Unit_Name) as Business_Unit_Name
  from  etl.`gmg.RiskDataImport15092020` 
  where  Risk_Index is not null and Risk_Index  not in ( 'Prevention of Fraud', 'Detection of Fraud', 'Response to Fraud Incidents' , 'Monitoring Fraud Activity', '(New)')
  and risk_index not in ('TM-R7','HR-R9') ############# Ignoring Temporarily
  and (Impact_1_5 is not null or Likelihood_1_5 is not null or Inherent_Risk_Rating is not null or Residual_Risk_Rating)
 )i
 left join predict360.Facility og on og.name = i.Business_Unit_Name and og.customerid = @CustomerId
 where i.Parent is not null
)d
on d.Risk_Index = r.riskDefinitionId and ifnull(d.buFacilityId,-1) = ifnull(r.buFacilityId, 1);




/*****************************************************************************************
******************************************************************************************
******************************************************************************************

									Controls

******************************************************************************************
******************************************************************************************
*****************************************************************************************/



/**************************************************************************
			predict360.ControlType
***************************************************************************/
insert into predict360.ControlType (name, description, customerId, createdBy, createdOn, modifiedBy, modifiedOn)
select distinct trim(Control_Type__preventive__detective__corrective_) as `name`, trim(Control_Type__preventive__detective__corrective_) as `description` 
, @CustomerId, @UserId,  current_timestamp(), @UserId,  current_timestamp()
from  etl.`gmg.RiskDataImport15092020`  
where trim(Control_Type__preventive__detective__corrective_) not in (select trim(name)  from predict360.ControlType where customerId = @customerId) and
(Control_Type__preventive__detective__corrective_ is not null or trim(Control_Type__preventive__detective__corrective_) <> '');


/**************************************************************************
			predict360.ControlType
***************************************************************************/

### Inserting Level 1: Temporary Statements
ALTER TABLE `etl`.`gmg.riskdataimport15092020` 
ADD COLUMN `ControlCategory_TmpCol` VARCHAR(100) NULL DEFAULT 'Temp Control Category' AFTER `Inherent_Risk_Rating`;

insert into predict360.RiskRegisterControlItem(name, createdBy, createdDate, updatedBy, updatedDate, isLeaf, customerId, isDeleted)
select distinct ControlCategory_TmpCol as Level1, @UserId, current_timestamp(), @UserId , current_timestamp(), 0 as isLeaf, @CustomerId, 0 as isDeleted 
from  etl.`gmg.RiskDataImport15092020` ;

/**************************************************************************
			predict360.RiskRegisterControlItem
***************************************************************************/

###### fDo consult with Aamir or GK. About the transition of column Efficacy from predict360.RiskRegisterControlItem to ControlInstance or RiskRegisterControlInstance

insert into predict360.RiskRegisterControlItem ( name, parentId, createdBy, createdDate, updatedBy, updatedDate, isLeaf, customerId, isDeleted, controlTypeId, controlDefinitionId)
select 

 d.Control_Name as Control_Name, p.id as parentId, @UserId, current_timestamp(), @UserId , current_timestamp(), 1 as isLeaf
, @CustomerId, 0 as isDeleted, ct.id as ControlTypeId, Control_Index as controlDefinitionId
from 
(
	select distinct 
	  Trim(ControlCategory_TmpCol) as ControlCategory_TmpCol
	, trim(Control_Index) as Control_Index
	, trim(Control_Name) as Control_Name
	, trim(Control_Type__preventive__detective__corrective_) as Control_Type__preventive__detective__corrective_
	, trim(Control_Execution__automated__manual_) as Control_Execution__automated__manual_
	from  etl.`gmg.RiskDataImport15092020` 
	where Control_Index is not null 
	and Control_Index not in ('**', 'see ''Finance'' process',  'IS Control', 'IS Controls', 'TBD', 
	'RB-C46' # Removing Temporarily
	)
	and Control_Type__preventive__detective__corrective_  is  not null and Control_Execution__automated__manual_ is not null
)d
inner join predict360.RiskRegisterControlItem p 
on Trim(d.ControlCategory_TmpCol) =  p.name and isLeaf = 0 and p.customerid = @CustomerId and isDeleted = 0
inner join predict360.ControlType ct on ct.name = d.Control_Type__preventive__detective__corrective_ and ct.CustomerId =  @CustomerId ;


/**************************************************************************
	 predict360.ControlInstance & predict360.RiskRegisterControlInstance
***************************************************************************/

 
Drop temporary table if exists tempdb.ContItemToRR;
create temporary table tempdb.ContItemToRR
select distinct rr.id as RiskIds, rr.riskRegisterItemId, rri.riskDefinitionId, d.Control_Index, ci.id as ControlItemId from predict360.RiskRegister rr 
inner join predict360.RiskRegisterItem rri on rr.riskRegisterItemId = rri.id and rr.customerId = rri.customerId 
inner join etl.`gmg.riskdataimport15092020` d on rri.riskDefinitionId = d.Risk_Index 
inner join  predict360.RiskRegisterControlItem ci on ci.controlDefinitionId = ifnull(d.Control_Index,-1) and ci.customerId = rr.customerId
where rr.customerId = 2641;



# predict360.ControlInstance
insert into predict360.ControlInstance (name, riskRegisterControlItemId, efficacy, implemented, createdBy, createdDate, updatedBy, updatedDate, isDeleted)
select distinct substring(d.Control_Description,1,255) as `name`,  t.ControlItemId as riskRegisterControlItemId, 100.00 as efficacy, 100.00 as implemented
, @UserId as createdBy, current_timestamp() as createdDate, @UserId as updatedBy, current_timestamp() as updatedDate, 0 isDeleted
from tempdb.ContItemToRR t
inner join  etl.`gmg.riskdataimport15092020` d on d.Control_Index = t.Control_Index;


# predict360.RiskRegisterControlInstance

Drop temporary table if exists tempdb.TotalControls;
create temporary table tempdb.TotalControls
select RiskIds, count(1) as TotalControls from
(
 select distinct RiskIds,ci.id  from tempdb.ContItemToRR t
 inner join predict360.ControlInstance ci on t.ControlItemId  = ci.riskRegisterControlItemId
)a
group by RiskIds;

insert into predict360.RiskRegisterControlInstance (riskRegisterId, controlInstanceId, weight)
select distinct t.RiskIds as riskRegisterId,ci.id as controlInstanceId, 100/ TotalControls as weight from tempdb.ContItemToRR t
inner join predict360.ControlInstance ci on t.ControlItemId  = ci.riskRegisterControlItemId
inner join tempdb.TotalControls tc on tc.RiskIds = t.RiskIds;
 










