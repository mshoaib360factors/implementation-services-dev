set @MinSeqRisk = (select  min(sequence) from Module where parentModuleId = 5 );
set @RiskStartingPoint = (select  min(id) from Module where parentModuleId = 5 );


update  Module 
set sequence = sequence+100
where id >= @RiskStartingPoint;

update  Module 
set sequence = sequence-99
where id >= @RiskStartingPoint;


UPDATE Module SET displayName='Risk Management' WHERE id=5;

INSERT INTO Module (name, displayName, description, sequence, showInFilter, isFeature, parentModuleId ) 
VALUES ('riskcontrolregister', 'Risk and Control Register', 'Risk and Control Register',  @MinSeqRisk , 1, 1, 5);

set @IdOfNewFeature = (select id from Module where sequence = @MinSeqRisk);


insert into CustomerModule (customerId, moduleId, createdBy, createdOn, modifiedBy, modifiedOn)
select  cm.customerId, @IdOfNewFeature, cm.createdBy, cm.createdOn, cm.modifiedBy, cm.modifiedOn from CustomerModule cm 
inner join Module m on cm.moduleId = m.id and  m.id = 5;




insert into ResellerModule (resellerId, moduleId, createdBy, createdOn, modifiedBy, modifiedOn)
select rm.resellerId,  @IdOfNewFeature, rm.createdBy, rm.createdOn, rm.modifiedBy, rm.modifiedOn from ResellerModule rm
inner join Module m on rm.moduleId = m.id and  m.id = 5;