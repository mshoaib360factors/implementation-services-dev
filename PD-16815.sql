# For None Space
update riskregisteritem 
set 
		updatedBy = 20004
	,	updatedDate = CURRENT_TIMESTAMP()
	,	riskDefinitionId= NULL
	,	libraryRiskId= NULL
	, 	riskTaxonomyId = NULL		

where customerid =-1 and isLeaf = 1 and IsDeleted = 1 and libraryRiskId in 
(
'GV.SF-1', 'GV.SF-2', 'GV.SF-3', 'GV.SF-4', 'GV.RM-1', 'GV.RM-2', 'GV.RM-3', 'GV.PL-1', 'GV.PL-2', 'GV.PL-3', 'GV.RR-1', 'GV.RR-2', 'GV.SP-1', 'GV.SP-2', 'GV.IR-1', 'GV.IR-2', 'GV.IR-3', 'GV.AU-1', 'GV.AU-2', 'GV.AU-3', 'GV.TE-1', 'GV.TE-2', 'ID.AM-1', 'ID.AM-2', 'ID.AM-3', 'ID.AM-4', 'ID.AM-5', 'ID.AM-6', 'ID.RA-1', 'ID.RA-2', 'ID.RA-3', 'ID.RA-4', 'ID.RA-5', 'ID.RA-6', 'PR.AC-1', 'PR.AC-2', 'PR.AC-3', 'PR.AC-4', 'PR.AC-5', 'PR.AC-6', 'PR.AC-7', 'PR.AT-1', 'PR.AT-2', 'PR.AT-3', 'PR.AT-4', 'PR.AT-5', 'PR.DS-1', 'PR.DS-2', 'PR.DS-3', 'PR.DS-4', 'PR.DS-5', 'PR.DS-6', 'PR.DS-7', 'PR.DS-8', 'PR.IP-1', 'PR.IP-2', 'PR.IP-3', 'PR.IP-4', 'PR.IP-5', 'PR.IP-6', 'PR.IP-7', 'PR.IP-8', 'PR.IP-9', 'PR.IP-10', 'PR.IP-11', 'PR.IP-12', 'PR.MA-1', 'PR.MA-2', 'PR.PT-1', 'PR.PT-2', 'PR.PT-3', 'PR.PT-4', 'PR.PT-5', 'DE.AE-1', 'DE.AE-2', 'DE.AE-3', 'DE.AE-4', 'DE.AE-5', 'DE.CM-1', 'DE.CM-2', 'DE.CM-3', 'DE.CM-4', 'DE.CM-5', 'DE.CM-6', 'DE.CM-7', 'DE.CM-8', 'DE.DP-1', 'DE.DP-2', 'DE.DP-3', 'DE.DP-4', 'DE.DP-5', 'RS.RP-1', 'RS.CO-1', 'RS.CO-2', 'RS.CO-3', 'RS.CO-4', 'RS.CO-5', 'RS.AN-1', 'RS.AN-2', 'RS.AN-3', 'RS.AN-4', 'RS.AN-5', 'RS.MI-1', 'RS.MI-2', 'RS.MI-3', 'RS.IM-1', 'RS.IM-2', 'RC.RP-1', 'RC.IM-1', 'RC.IM-2', 'RC.CO-1', 'RC.CO-2', 'RC.CO-3', 'DM.ID-1', 'DM.ID-2', 'DM.ED-1', 'DM.ED-2', 'DM.ED-3', 'DM.ED-4', 'DM.ED-5', 'DM.ED-6', 'DM.ED-7', 'DM.RS-1', 'DM.RS-2', 'DM.BE-1', 'DM.BE-2', 'DM.BE-3'
) ;

#### For the customer "Third National Bank of Alpine"

# Deleting the applicability link so that the active records belongs to given libraryRiskId are no more visible in Risk Register grid.
delete from riskregisteritemapplicablity where riskRegisterItemId in (


	select id  
	-- concat(riskDefinitionId,' || PD-16815'), concat(libraryRiskId,' || PD-16815') 
	from riskregisteritem  
	where customerid =596 and isLeaf = 1 and IsDeleted = 0 and isOrphan = 0 and libraryRiskId in 
	(
	'GV.SF-1', 'GV.SF-2', 'GV.SF-3', 'GV.SF-4', 'GV.RM-1', 'GV.RM-2', 'GV.RM-3', 'GV.PL-1', 'GV.PL-2', 'GV.PL-3', 'GV.RR-1', 'GV.RR-2', 'GV.SP-1', 'GV.SP-2', 'GV.IR-1', 'GV.IR-2', 'GV.IR-3', 'GV.AU-1', 'GV.AU-2', 'GV.AU-3', 'GV.TE-1', 'GV.TE-2', 'ID.AM-1', 'ID.AM-2', 'ID.AM-3', 'ID.AM-4', 'ID.AM-5', 'ID.AM-6', 'ID.RA-1', 'ID.RA-2', 'ID.RA-3', 'ID.RA-4', 'ID.RA-5', 'ID.RA-6', 'PR.AC-1', 'PR.AC-2', 'PR.AC-3', 'PR.AC-4', 'PR.AC-5', 'PR.AC-6', 'PR.AC-7', 'PR.AT-1', 'PR.AT-2', 'PR.AT-3', 'PR.AT-4', 'PR.AT-5', 'PR.DS-1', 'PR.DS-2', 'PR.DS-3', 'PR.DS-4', 'PR.DS-5', 'PR.DS-6', 'PR.DS-7', 'PR.DS-8', 'PR.IP-1', 'PR.IP-2', 'PR.IP-3', 'PR.IP-4', 'PR.IP-5', 'PR.IP-6', 'PR.IP-7', 'PR.IP-8', 'PR.IP-9', 'PR.IP-10', 'PR.IP-11', 'PR.IP-12', 'PR.MA-1', 'PR.MA-2', 'PR.PT-1', 'PR.PT-2', 'PR.PT-3', 'PR.PT-4', 'PR.PT-5', 'DE.AE-1', 'DE.AE-2', 'DE.AE-3', 'DE.AE-4', 'DE.AE-5', 'DE.CM-1', 'DE.CM-2', 'DE.CM-3', 'DE.CM-4', 'DE.CM-5', 'DE.CM-6', 'DE.CM-7', 'DE.CM-8', 'DE.DP-1', 'DE.DP-2', 'DE.DP-3', 'DE.DP-4', 'DE.DP-5', 'RS.RP-1', 'RS.CO-1', 'RS.CO-2', 'RS.CO-3', 'RS.CO-4', 'RS.CO-5', 'RS.AN-1', 'RS.AN-2', 'RS.AN-3', 'RS.AN-4', 'RS.AN-5', 'RS.MI-1', 'RS.MI-2', 'RS.MI-3', 'RS.IM-1', 'RS.IM-2', 'RC.RP-1', 'RC.IM-1', 'RC.IM-2', 'RC.CO-1', 'RC.CO-2', 'RC.CO-3', 'DM.ID-1', 'DM.ID-2', 'DM.ED-1', 'DM.ED-2', 'DM.ED-3', 'DM.ED-4', 'DM.ED-5', 'DM.ED-6', 'DM.ED-7', 'DM.RS-1', 'DM.RS-2', 'DM.BE-1', 'DM.BE-2', 'DM.BE-3'
	) 
);

# Appending the libraryRiskId, riskDefinitionId with  ' || PD-16815' and riskTaxonomyId as  NULL to remove the links between None Risk definitions with Customer Risk's Definitions.
update riskregisteritem
set 
	updatedBy = 20004
,	updatedDate = CURRENT_TIMESTAMP()
,	riskDefinitionId = concat(riskDefinitionId,' || PD-16815')
, libraryRiskId = concat(libraryRiskId,' || PD-16815') 
, riskTaxonomyId = NULL		
	   
	where customerid = 596 and isLeaf = 1 and libraryRiskId in 
	(
	'GV.SF-1', 'GV.SF-2', 'GV.SF-3', 'GV.SF-4', 'GV.RM-1', 'GV.RM-2', 'GV.RM-3', 'GV.PL-1', 'GV.PL-2', 'GV.PL-3', 'GV.RR-1', 'GV.RR-2', 'GV.SP-1', 'GV.SP-2', 'GV.IR-1', 'GV.IR-2', 'GV.IR-3', 'GV.AU-1', 'GV.AU-2', 'GV.AU-3', 'GV.TE-1', 'GV.TE-2', 'ID.AM-1', 'ID.AM-2', 'ID.AM-3', 'ID.AM-4', 'ID.AM-5', 'ID.AM-6', 'ID.RA-1', 'ID.RA-2', 'ID.RA-3', 'ID.RA-4', 'ID.RA-5', 'ID.RA-6', 'PR.AC-1', 'PR.AC-2', 'PR.AC-3', 'PR.AC-4', 'PR.AC-5', 'PR.AC-6', 'PR.AC-7', 'PR.AT-1', 'PR.AT-2', 'PR.AT-3', 'PR.AT-4', 'PR.AT-5', 'PR.DS-1', 'PR.DS-2', 'PR.DS-3', 'PR.DS-4', 'PR.DS-5', 'PR.DS-6', 'PR.DS-7', 'PR.DS-8', 'PR.IP-1', 'PR.IP-2', 'PR.IP-3', 'PR.IP-4', 'PR.IP-5', 'PR.IP-6', 'PR.IP-7', 'PR.IP-8', 'PR.IP-9', 'PR.IP-10', 'PR.IP-11', 'PR.IP-12', 'PR.MA-1', 'PR.MA-2', 'PR.PT-1', 'PR.PT-2', 'PR.PT-3', 'PR.PT-4', 'PR.PT-5', 'DE.AE-1', 'DE.AE-2', 'DE.AE-3', 'DE.AE-4', 'DE.AE-5', 'DE.CM-1', 'DE.CM-2', 'DE.CM-3', 'DE.CM-4', 'DE.CM-5', 'DE.CM-6', 'DE.CM-7', 'DE.CM-8', 'DE.DP-1', 'DE.DP-2', 'DE.DP-3', 'DE.DP-4', 'DE.DP-5', 'RS.RP-1', 'RS.CO-1', 'RS.CO-2', 'RS.CO-3', 'RS.CO-4', 'RS.CO-5', 'RS.AN-1', 'RS.AN-2', 'RS.AN-3', 'RS.AN-4', 'RS.AN-5', 'RS.MI-1', 'RS.MI-2', 'RS.MI-3', 'RS.IM-1', 'RS.IM-2', 'RC.RP-1', 'RC.IM-1', 'RC.IM-2', 'RC.CO-1', 'RC.CO-2', 'RC.CO-3', 'DM.ID-1', 'DM.ID-2', 'DM.ED-1', 'DM.ED-2', 'DM.ED-3', 'DM.ED-4', 'DM.ED-5', 'DM.ED-6', 'DM.ED-7', 'DM.RS-1', 'DM.RS-2', 'DM.BE-1', 'DM.BE-2', 'DM.BE-3'
	) ;
