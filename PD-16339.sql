select * from Customer where customerCode = 'gwydemo';

select count(1) from RiskRegisterItem r
inner join riskregisteritemmapping m on m.riskregisteritemid = r.id
where CustomerId = 626 and IsLeaf = 1 ;

create temporary table tempdb.t1
select a.id as riskRegisterControlItemId , b.id  as controlInstanceId from RiskRegisterControlItem a
inner join controlinstance b on b.riskRegisterControlItemId = a.id  
where a.customerId = 626 and a.isleaf = 1;

delete from controlinstanceowner where controlInstanceId in (select  distinct controlInstanceId from tempdb.t1) ;
Delete from controlinstance where id in (select  distinct controlInstanceId from tempdb.t1) ;
Delete from RiskRegisterControlItem where id in (select  distinct riskRegisterControlItemId from tempdb.t1) ;

delete from RiskRegisterControlItem   
where customerId = 626 and isleaf = 1;

delete from RiskRegisterControlItem   
where customerId = 626 and parentid is not null;


delete from RiskRegisterControlItem   
where customerId = 626 and parentid is null;



########### Deleting Prod BI ############### 
select * from dim_tenant where BK_TenantCode = 'GWYDEMO'; 541

create TEMPORARY table tempdb.t1 
select rr.PK_RiskRegisterId, rri.PK_RiskRegisterItemId from dim_tenant t 
inner join dim_risk_register_item rri on rri.FK_TenantId = t.PK_TenantId 
inner join dim_risk_register rr on rr.FK_RiskRegisterItemId = rri.PK_RiskRegisterItemId
where BK_TenantCode = 'GWYDEMO' ;

delete from dim_risk_register_overall_analysis where FK_RiskRegisterId in 
(
select distinct PK_RiskRegisterId from tempdb.t1
);

 
delete from dim_risk_register where PK_RiskRegisterId in (select distinct PK_RiskRegisterId from tempdb.t1);



delete from dim_risk_register_item_mapping where FK_RiskRegisterItemId in (select PK_RiskRegisterItemId from tempdb.t1 );



create TEMPORARY table tempdb.t2
select distinct rci.PK_RiskRegisterControlItemId, c.PK_ControlInsNegativeImpId from dim_tenant t 
inner join dim_risk_register_control_item rci on rci.FK_TenantId = t.PK_TenantId 
inner join dim_control_ins_negative_imp c on  c.FK_RiskRegisterControlItemId = rci.PK_RiskRegisterControlItemId
where BK_TenantCode = 'GWYDEMO' ;
 
 

delete From dim_control_instance_doc where FK_ControlInstanceId in (select PK_ControlInsNegativeImpId from  tempdb.t2) ;

delete  From dim_control_ins_negative_imp where FK_RiskRegisterControlItemId in  (select PK_RiskRegisterControlItemId from tempdb.t2);

delete from dim_risk_register_control_item where PK_RiskRegisterControlItemId in (select PK_RiskRegisterControlItemId from tempdb.t2); 



create TEMPORARY table tempdb.t3
select PK_RiskRegisterItemId, rri.BK_RiskRegisterItemId, rri.IsDeleted, rri.isOrphan, rri.EndDate from dim_tenant t 
inner join dim_risk_register_item rri on rri.FK_TenantId = t.PK_TenantId  
where BK_TenantCode = 'GWYDEMO' and IsLeaf = 1 and rri.EndDate <> '9999-01-01 00:00:00'
order by  BK_RiskRegisterItemId asc;


delete from dim_risk_register_item where PK_RiskRegisterItemId in (select PK_RiskRegisterItemId from tempdb.t3 );





delete from dim_riskregistertask where FK_RiskRegisterItemId in (select PK_RiskRegisterItemId from dim_risk_register_item where FK_TenantId = 541);

Delete from dim_risk_register_item where FK_TenantId = 541 and IsLeaf = 0;


select * from dim_risk_register_item where FK_TenantId = 541 ;