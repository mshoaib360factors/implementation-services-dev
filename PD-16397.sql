
SELECT *  FROM SurveyQuestionBank sqb
inner join surveyquestionbanktype sqbt on sqbt.id = sqb.questionBankTypeId
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE st.customerId=-1 
AND st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
AND sqb.questionBankTypeId  IN (4,5,6);



Drop TEMPORARY table if exists tempdb.t2;
create TEMPORARY table tempdb.t2 
(Index Idx_Id (`id`))
SELECT distinct 
	sqb.id,sqb.status, sqb.modifiedOn, sqb.questionBankTypeId
, case when st.type = 'predict.survey.type.control.design' then 4  
			 when st.type = 'predict.survey.type.control.oea' then 5  
			 when st.type = 'predict.survey.type.control.pap' then 6  
	 end as questionBankTypeId_New
,	st.type  

FROM SurveyQuestionBank sqb
inner join surveyquestionbanktype sqbt on sqbt.id = sqb.questionBankTypeId
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE st.customerId=-1 
AND st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
AND sqb.questionBankTypeId NOT IN (4,5,6);


select count(1) from tempdb.t2 ;
select count(distinct id)  from tempdb.t2; #5511
select * from SurveyQuestionBank limit 10;

update SurveyQuestionBank a
inner join  tempdb.t2  b on a.id = b.id
set a.modifiedOn = CURRENT_TIMESTAMP(),  a.questionBankTypeId = b.questionBankTypeId_New;








Drop TEMPORARY table if exists tempdb.t3;
create TEMPORARY table tempdb.t3 
(index ix_said (`AnswersId`))
SELECT distinct Sa.id as AnswersId, sa.risklevel,sa.score, sqb.id as SQBID FROM SurveyAnswer sa
INNER JOIN SurveyQuestion sq ON (sq.id=sa.surveyQuestionId)
INNER JOIN SurveyQuestionBank sqb ON (sqb.id=sq.questionBankId)
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE st.customerId=-1 
AND st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
and sa.score is null ;

update  SurveyAnswer sa
inner join tempdb.t3 t on t.AnswersId = sa.id
set sa.score = t.risklevel , sa.risklevel = 0, modifiedOn = CURRENT_TIMESTAMP();



select count(distinct SQBID)  from tempdb.t3; #5512
select count(1)  from tempdb.t3; #47470
select count(distinct AnswersId)  from tempdb.t3; #47470 



 
Drop TEMPORARY table if exists tempdb.t7;
create TEMPORARY table tempdb.t7
(index ix_Quesid (`id`))
SELECT  
sq.id, sq.modifiedDate, sq.createdDate, sq.createdBy, sq.modifiedBy, sq.HighRiskValue, lowRiskValue, highRiskSign, lowRiskSign 

from SurveyQuestion sq 
INNER JOIN SurveyQuestionBank sqb ON (sqb.id=sq.questionBankId)
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE st.customerId=-1 AND st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
and ifnull(sq.HighRiskValue, 9999) <> 9 and ifnull(lowRiskValue, 9999)  <>  1 

update SurveyQuestion  sq
inner join  tempdb.t7 t on t.id = sq.id 
set  sq.HighRiskValue = 9, sq.lowRiskValue= 1, sq.highRiskSign= '>=', sq.lowRiskSign= '<=',  sq.modifiedDate = CURRENT_TIMESTAMP();



 
/*############################################################################
####################### Updating in Customer's Space #######################
############################################################################ */

Drop TEMPORARY table if exists tempdb.t4;
create TEMPORARY table tempdb.t4 
(Index Idx_Id (`id`))
SELECT distinct 
	sqb.id,sqb.status, sqb.modifiedOn, sqb.questionBankTypeId
, case when st.type = 'predict.survey.type.control.design' then 4  
			 when st.type = 'predict.survey.type.control.oea' then 5  
			 when st.type = 'predict.survey.type.control.pap' then 6  
	 end as questionBankTypeId_New
,	st.type  
, st.CustomerId

FROM SurveyQuestionBank sqb
inner join surveyquestionbanktype sqbt on sqbt.id = sqb.questionBankTypeId
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE #st.customerId <> -1  AND
st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
AND sqb.questionBankTypeId NOT IN (4,5,6);

select count(1) from tempdb.t4 ;
select count(distinct id)  from tempdb.t4; #5443

update SurveyQuestionBank a
inner join  tempdb.t4  b on a.id = b.id
set a.modifiedOn = CURRENT_TIMESTAMP(),  a.questionBankTypeId = b.questionBankTypeId_New;







Drop TEMPORARY table if exists tempdb.t5;
create TEMPORARY table tempdb.t5
(index ix_said (`AnswersId`))
SELECT distinct Sa.id as AnswersId, sa.risklevel,sa.score, sqb.id as SQBID FROM SurveyAnswer sa
INNER JOIN SurveyQuestion sq ON (sq.id=sa.surveyQuestionId)
INNER JOIN SurveyQuestionBank sqb ON (sqb.id=sq.questionBankId)
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
and sa.score is null ;


select count(distinct SQBID)  from tempdb.t5; #5444
select count(1)  from tempdb.t5; #46700
select count(distinct AnswersId)  from tempdb.t5; #46700 

select * from tempdb.t5 limit 1000;


update  SurveyAnswer sa
inner join tempdb.t5 t on t.AnswersId = sa.id
set sa.score = t.risklevel , sa.risklevel = 0, modifiedOn = CURRENT_TIMESTAMP();




Drop TEMPORARY table if exists tempdb.t6;
create TEMPORARY table tempdb.t6
(index ix_Quesid (`id`))
SELECT 
sq.id, sq.modifiedDate, sq.createdDate, sq.createdBy, sq.modifiedBy, sq.HighRiskValue, lowRiskValue, highRiskSign, lowRiskSign 
from SurveyQuestion sq 
INNER JOIN SurveyQuestionBank sqb ON (sqb.id=sq.questionBankId)
INNER JOIN `surveysectionsurveyquestionbank` sssqb ON (sqb.id=sssqb.`surveyQuestionBankId`)
INNER JOIN `surveysection` ss ON (ss.id=sssqb.`surveySectionId`)
INNER JOIN SurveyTemplates st ON (st.id=ss.`surveyId`)
WHERE st.customerId not in (-1, 751, 620) AND st.`type` IN ('predict.survey.type.control.design','predict.survey.type.control.oea','predict.survey.type.control.pap')
and ifnull(sq.HighRiskValue, 9999) <> 9 and ifnull(lowRiskValue, 9999)  <>  1 ;


update SurveyQuestion  sq
inner join  tempdb.t6 t on t.id = sq.id 
set  sq.HighRiskValue = 9, sq.lowRiskValue= 1, sq.highRiskSign= '>=', sq.lowRiskSign= '<=',  sq.modifiedDate = CURRENT_TIMESTAMP();


 












