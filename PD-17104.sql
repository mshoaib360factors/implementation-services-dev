Drop PROCEDURE if exists reporting.`udf_list_of_doc`;
DELIMITER $$
CREATE PROCEDURE reporting.`udf_list_of_doc`(
    in_customer_code varchar(255) -- 'Customer Code agains which the data needs to be filtered'
)
BEGIN
set session group_concat_max_len = 100000;

/*
Created By: 	Muhammad Shoaib
Created On: 	15-02-2022
Ticket Id: 		PD-17104
Description:	This Stored will look for all the document ids in (reporting.rptca_complaintcase & reporting.rptca_task) tables based on the customer Code passed in the parameter
				And will return the Issue Type, File Name, Issue Keys and file path associated with each document.
*/

Drop TEMPORARY table if exists tempdb.document_cc;
create TEMPORARY table tempdb.document_cc
SELECT distinct rcc.Issuekey, rcc.document FROM reporting.rptca_complaintcase rcc
inner join edw.dim_tenant t on t.PK_TenantId = rcc.FK_TenantId
where t.BK_TenantCode = in_customer_code and rcc.document is not null ;

Drop TEMPORARY table if exists tempdb.document_task;
create TEMPORARY table tempdb.document_task
SELECT rt.Issuekey, rt.document FROM reporting.rptca_task rt
inner join edw.dim_tenant t on t.PK_TenantId = rt.FK_TenantId
where t.BK_TenantCode = in_customer_code and rt.document is not null ;


Drop TEMPORARY table if exists tempdb.docs_to_find;
create TEMPORARY table tempdb.docs_to_find
(Index Ix_Doc (doc_id))

select distinct 'Compalaint Case' as IssueType, convert(trim(doc_id) , unsigned integer) as doc_id, group_concat(Issuekey separator ', ') as Issuekeys   FROM  tempdb.document_cc 
CROSS JOIN JSON_TABLE(replace(CONCAT('["', REPLACE(document, ',', '","'), '"]'), ',""',''),
                      "$[*]" COLUMNS (doc_id VARCHAR(50) PATH "$")) JS
group by convert(trim(doc_id) , unsigned integer)

UNION

SELECT  'Task' as IssueType, convert(trim(doc_id) , unsigned integer) as doc_id, group_concat(Issuekey separator ', ') as Issuekeys   FROM
 tempdb.document_task
CROSS JOIN JSON_TABLE(replace(CONCAT('["', REPLACE(document, ',', '","'), '"]'), ',""',''),
                      "$[*]" COLUMNS (doc_id VARCHAR(50) PATH "$")) JS
 group by convert(trim(doc_id) , unsigned integer) ;
 
 

select 
	f.IssueType as `Issue Type`
, 	concat(d.FileName, '.' , d.DocType) as `File Name`
, 	f.Issuekeys as `Issue keys`
, 	replace(d.FilePath,'/','\\') as `File Path`
from edw.dim_document d
inner join tempdb.docs_to_find f on f.doc_id = d.DmsDocId
where  isDeleted = 0 and d.EndDate >= "9999-01-01" ;
 
 
 
END$$
DELIMITER ;
 
 