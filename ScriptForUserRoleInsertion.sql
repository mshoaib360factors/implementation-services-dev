insert into userrole (userId, roleId, createdBy, createdOn, modifiedBy, modifiedOn)
Select pu.id as userId #, pu.customerId
      , case when m.name = 'riskmanagement' then -2500
			       when m.name = 'bcm' then -2502
			  end as roleId 
			, 1 createby 
			, CURRENT_TIMESTAMP() as createdOn
			, 1 modifiedBy
			, CURRENT_TIMESTAMP() as modifiedOn
			
		
		from PredictUser pu
inner join CustomerModule cm on cm.customerId = pu.customerId
inner join Module m on m.id = cm.moduleId and m.name in ('riskmanagement','bcm')
where pu.username like 'reseller@360factors'
and isConsultantUser is false
and consultantUserId= 13337
and pu.customerId = 460
order by pu.customerId, cm.moduleId;