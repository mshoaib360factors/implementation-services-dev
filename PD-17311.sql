
DROP TEMPORARY TABLE IF EXISTS tempdb.RiskInsContStren;
CREATE TEMPORARY TABLE tempdb.RiskInsContStren
SELECT riskregisterId, ROUND(SUM(sum_of_Product)) AS controlStrength FROM
(
	SELECT 
			rrci.riskregisterId
		, rrci.controlInstanceId
		-- , rrci.weight , ifnull(rad.`value`, 0) as sum_of_Product
		, rrci.weight * IFNULL(rad.`value`, 0) / 100 AS sum_of_Product
	 
	FROM module m 
	INNER JOIN customermodule cm ON cm.moduleId = m.id
	INNER JOIN riskregister rr ON rr.customerId = cm.customerId
	LEFT JOIN riskinstancecontrolstrength rrcs ON rrcs.riskRegisterId = rr.id
	INNER JOIN riskregistercontrolinstance rrci ON rrci.riskRegisterId = rr.id
	INNER JOIN controlinstance ci ON ci.id = rrci.controlInstanceId
	LEFT JOIN riskanalysisdimension rad ON rad.id = ci.controlStrengthId
	WHERE m.name = 'controlstrength' AND rrcs.riskRegisterId IS NULL 
)X
GROUP BY riskregisterId;


INSERT INTO riskinstancecontrolstrength (riskRegisterId,	controlStrength,	createdBy, CreatedDate) 
SELECT riskRegisterId,	controlStrength,	1 AS createdBy, CURRENT_TIMESTAMP()	AS CreatedDate
FROM  tempdb.RiskInsContStren;




DROP TEMPORARY TABLE IF EXISTS tempdb.ResidualRiskRatingRiskInsContStren;
CREATE TEMPORARY TABLE tempdb.ResidualRiskRatingRiskInsContStren
 
SELECT x.id AS RROA_Id,  IF(aggregatedValue > 0 , ROUND(aggregatedValue,2), 0.01) AS ResidualRiskrating

FROM 
(
	SELECT rroa.id,
	(1 - (controlStrength/100)) * inherentRiskRating AS aggregatedValue
	FROM tempdb.RiskInsContStren rics 
	INNER JOIN riskregisteroverallanalysis rroa ON rroa.riskId = rics.riskRegisterId
	WHERE  rroa.inherentRiskRating > 0
)X;


 
UPDATE riskregisteroverallanalysis rroa 
INNER JOIN tempdb.ResidualRiskRatingRiskInsContStren t
ON rroa.id = t.RROA_Id
 
SET 
   rroa.residualRiskRating = t.ResidualRiskrating
 , rroa.modifiedDate = CURRENT_TIMESTAMP()
;
